FROM nichemarket-backend_cache as CACHE

FROM maven:3.6.1-jdk-11-slim AS MAVEN_TOOL_CHAIN
COPY pom.xml /tmp/pom.xml
COPY src /tmp/src/
COPY --from=CACHE /root/.m2 /root/.m2
WORKDIR /tmp/
RUN mvn clean package

FROM openjdk:11-jdk-slim AS jdk
COPY --from=MAVEN_TOOL_CHAIN /tmp/target/nichemarket-0.0.1-SNAPSHOT.jar /
COPY wait-for-it.sh /
RUN apt-get update; apt-get install -y fontconfig libfreetype6
RUN ln -sfn /usr/share/zoneinfo/Asia/Jakarta/etc/localtime
RUN chmod +x wait-for-it.sh
EXPOSE 8088
CMD ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/nichemarket-0.0.1-SNAPSHOT.jar"]