
    
--BASE LINE V23--

    create table m_account (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        account_code varchar(15) not null,
        app_code varchar(15) not null,
        email_address varchar(100) not null,
        is_account_activated boolean not null,
        is_active boolean not null,
        unique_activation_key varchar(30) not null,
        primary key (id)
    );

    create table m_address_type (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        address_type_code varchar(3) not null,
        description varchar(30) not null,
        is_active boolean not null,
        primary key (id)
    );

    create table m_agen_relation (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        agen_relation_code varchar(2) not null,
        description varchar(100) not null,
        is_active boolean not null,
        primary key (id)
    );

    create table m_answer (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        answer_code varchar(10) not null,
        description varchar(255) not null,
        is_active boolean not null,
        question_code varchar(10) not null,
        sequence int4 not null,
        primary key (id)
    );

    create table m_app (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        app_action_name varchar(50),
        app_code varchar(15) not null,
        app_description varchar(50) not null,
        is_active boolean not null,
        primary key (id)
    );

    create table m_appointment_time (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        appointment_time_code varchar(3) not null,
        description varchar(50) not null,
        is_active boolean not null,
        primary key (id)
    );

    create table m_as (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        as_code varchar(2) not null,
        as_code_string varchar(10) not null,
        description varchar(30) not null,
        is_active boolean not null,
        primary key (id)
    );

    create table m_bank_payment (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        bank_payment_code varchar(12) not null,
        description varchar(30) not null,
        is_active boolean not null,
        primary key (id)
    );

    create table m_card_issuer (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        card_issuer_code varchar(12) not null,
        description varchar(30) not null,
        is_active boolean not null,
        primary key (id)
    );

    create table m_card_type (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        card_type_code varchar(12) not null,
        description varchar(100) not null,
        is_active boolean not null,
        primary key (id)
    );

    create table m_certificate (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        certificate_code varchar(15) not null,
        certificate_desc varchar(15) not null,
        certificate_template_code varchar(15) not null,
        is_active boolean not null,
        primary key (id)
    );

    create table m_channel (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        channel_code varchar(10) not null,
        channel_code_mapp varchar(10),
        description varchar(30),
        is_active boolean,
        primary key (id)
    );

    create table m_citizen (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        citizen_code varchar(3) not null,
        description varchar(20) not null,
        is_active boolean not null,
        primary key (id)
    );

    create table m_country (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        country_code varchar(8) not null,
        description varchar(30) not null,
        is_active boolean not null,
        primary key (id)
    );

    create table m_currency (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        currency_code varchar(6) not null,
        description varchar(100) not null,
        is_active boolean not null,
        primary key (id)
    );

    create table m_document_checklist (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(100) not null,
        is_active boolean not null,
        payment_method_code varchar(60) not null,
        tmp_code varchar(6) not null,
        primary key (id)
    );

    create table m_document_mapping (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(11),
        document_mapping_code varchar(12),
        is_active boolean not null,
        mapping_code varchar(6),
        primary key (id)
    );

    create table m_education (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(30) not null,
        education_code varchar(12) not null,
        is_active boolean not null,
        primary key (id)
    );

    create table m_gender (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(100) not null,
        gender_code varchar(1) not null,
        is_active boolean not null,
        primary key (id)
    );

    create table m_gender_mgm (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        gender_code varchar(1) not null,
        gender_desc varchar(100) not null,
        is_active boolean not null,
        primary key (id)
    );

    create table m_genderrm (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(100),
        gender_code varchar(1) not null,
        is_active boolean,
        primary key (id)
    );

    create table m_gift (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        amount numeric(8, 2),
        gift_bonus_desc varchar(50) not null,
        gift_code varchar(15) not null,
        gift_redeem_desc varchar(50) not null,
        gift_type varchar(10) not null,
        is_active boolean not null,
        point_gained int4,
        point_used int4,
        ref_gift_code varchar(15) not null,
        primary key (id)
    );

    create table m_identity_type (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(100) not null,
        identity_type_code varchar(6) not null,
        is_active boolean not null,
        primary key (id)
    );

    create table m_image (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(50) not null,
        image_code varchar(10) not null,
        is_active boolean not null,
        object_image varchar(255) not null,
        primary key (id)
    );

    create table m_income (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(30) not null,
        income_code varchar(4) not null,
        is_active boolean not null,
        primary key (id)
    );

    create table m_industry (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(100) not null,
        industry_code varchar(6) not null,
        is_active boolean not null,
        primary key (id)
    );

    create table m_insured_relation (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(100) not null,
        insured_relation_code varchar(2) not null,
        is_active boolean not null,
        primary key (id)
    );

    create table m_invest_type (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(100) not null,
        invest_type_code varchar(10) not null,
        is_active boolean not null,
        primary key (id)
    );

    create table m_marital_status (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(30) not null,
        is_active boolean not null,
        marital_status_code varchar(2) not null,
        primary key (id)
    );

    create table m_mens_condition (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(100) not null,
        is_active boolean not null,
        mens_condition_code varchar(3) not null,
        primary key (id)
    );

    create table m_menu (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(30),
        is_active boolean,
        menu_code varchar(10) not null,
        parent_id int4,
        uri varchar(30),
        primary key (id)
    );

    create table m_occupation_group (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(100) not null,
        is_active boolean not null,
        occupation_group_code varchar(5) not null,
        primary key (id)
    );

    create table m_payment_method (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(100),
        is_active boolean,
        payment_method_code varchar(5) not null,
        primary key (id)
    );

    create table m_payment_period (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(30) not null,
        is_active boolean not null,
        payment_period_code varchar(3) not null,
        tot_month int4 not null,
        tot_year int4 not null,
        primary key (id)
    );

    create table m_product_channel (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        channel_code varchar(10) not null,
        is_active boolean not null,
        product_channel_code varchar(10) not null,
        product_code varchar(10) not null,
        primary key (id)
    );

    create table m_product_plan (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        is_active boolean not null,
        product_policy_code varchar(15) not null,
        partner_code_core varchar(15),
        product_policy_desc varchar(50) not null,
        primary key (id)
    );

    create table m_product_plan_policy (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        class_core_code varchar(15),
        is_active boolean not null,
        plan_code_core varchar(15) not null,
        product_policy_code varchar(15) not null,
        registration_type_code varchar(15) not null,
        type int4,
        primary key (id)
    );

    create table m_product_policy (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        email_code varchar(15),
        is_active boolean not null,
        partner_code_core varchar(15),
        policy_no varchar(15),
        product_policy_code varchar(15) not null,
        product_policy_desc varchar(50) not null,
        primary key (id)
    );

    create table m_products (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(30) not null,
        is_activate boolean not null,
        is_active boolean not null,
        is_basic boolean not null,
        is_class boolean not null,
        is_contract_period boolean not null,
        is_payor boolean not null,
        is_premium boolean not null,
        is_premium_period boolean not null,
        is_required boolean not null,
        is_risk boolean not null,
        is_sum_insured boolean not null,
        "order" int4 not null,
        parent_product_code varchar(8) not null,
        product_code varchar(10) not null,
        product_code_mapping varchar(10) not null,
        primary key (id)
    );

    create table m_proposal (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        is_active boolean,
        proposal_no varchar(20),
        primary key (id)
    );

    create table m_province (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(30) not null,
        is_active boolean not null,
        province_code varchar(4) not null,
        primary key (id)
    );

    create table m_purpose (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(30) not null,
        is_active boolean not null,
        purpose_code varchar(4) not null,
        primary key (id)
    );

    create table m_question (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(255) not null,
        is_active boolean not null,
        question_code varchar(10) not null,
        sequence int4 not null,
        primary key (id)
    );

    create table m_recipient (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        email_address varchar(100) not null,
        email_type_code varchar(3) not null,
        is_active boolean not null,
        partner_name varchar(100) not null,
        recipient_code varchar(10) not null,
        recipient_name varchar(255) not null,
        recipient_type_code varchar(3) not null,
        primary key (id)
    );

    create table m_recipient_received_msg (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        is_active boolean not null,
        message_type varchar(1) not null,
        recipient_code varchar(10) not null,
        recipient_received_msg_code varchar(12) not null,
        type_code varchar(3) not null,
        primary key (id)
    );

    create table m_recipient_type (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(50) not null,
        is_active boolean not null,
        is_internal boolean not null,
        recipient_type_code varchar(3) not null,
        primary key (id)
    );

    create table m_recurring_type (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(100) not null,
        is_active boolean not null,
        recurring_type_code varchar(12) not null,
        primary key (id)
    );

    create table m_registration_type (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        bill_amount numeric(12, 2),
        gift_code varchar(15),
        is_active boolean not null,
        max_age int4,
        max_customer int4,
        max_up numeric(12, 2),
        min_age int4,
        min_customer int4,
        point int4,
        product_policy_code varchar(15),
        registration_type_desc varchar(50) not null,
        registration_type_code varchar(15) not null,
        primary key (id)
    );

    create table m_relation_type (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(100),
        is_active boolean not null,
        relation_type_code varchar(1) not null,
        primary key (id)
    );

    create table m_relationship (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        is_active boolean not null,
        relationship_code varchar(1) not null,
        relationship_core_code varchar(1),
        relationship_desc varchar(100),
        primary key (id)
    );

    create table m_religion (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(100) not null,
        is_active boolean not null,
        religion_code varchar(1) not null,
        primary key (id)
    );

    create table m_residence_status (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(30) not null,
        is_active boolean not null,
        residence_status_code varchar(4) not null,
        primary key (id)
    );

    create table m_role (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(100),
        is_active boolean,
        role_code varchar(10) not null,
        primary key (id)
    );

    create table m_role_menu (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        is_active boolean,
        menu_code varchar(10),
        role_code varchar(10),
        role_menu_code varchar(10) not null,
        primary key (id)
    );

    create table m_rule_insured_age (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        is_active boolean not null,
        max_age int4,
        min_age int4,
        registration_type_code varchar(15) not null,
        relationship_code varchar(15) not null,
        rule_insured_age_code varchar(15) not null,
        primary key (id)
    );

    create table m_service_unit (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(30) not null,
        is_active boolean not null,
        service_unit_code varchar(6) not null,
        primary key (id)
    );

    create table m_source_fund (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(30) not null,
        is_active boolean not null,
        source_fund_code varchar(4) not null,
        primary key (id)
    );

    create table m_spaj (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        is_active boolean not null,
        spaj_code varchar(12),
        primary key (id)
    );

    create table m_spaj_additional_stat (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        domicile_status varchar(4),
        gross_income varchar(20),
        is_active boolean not null,
        other_domicile varchar(60),
        other_purpose_by varchar(30),
        other_source_of_fund varchar(30),
        purpose_buy varchar(4),
        source_of_fund varchar(4),
        spaj_additional_stat_code varchar(12),
        spaj_document_code varchar(12),
        spaj_no varchar(12),
        primary key (id)
    );

    create table m_spaj_agen_statement (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        agen_code varchar(20),
        agen_name varchar(60),
        is_active boolean not null,
        position varchar(5),
        spaj_agen_statement_code varchar(12),
        spaj_document_code varchar(12),
        spaj_no varchar(12),
        primary key (id)
    );

    create table m_spaj_beneficiary (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        beneficiary_1 varchar(60),
        date_of_birth_1 varchar(30),
        gender_1 varchar(2),
        is_active boolean not null,
        percentage_1 varchar(20),
        relationship_1 varchar(2),
        spaj_beneficiary_code varchar(12) not null,
        spaj_document_code varchar(12),
        spaj_no varchar(12) not null,
        primary key (id)
    );

    create table m_spaj_coverage_premium_detail (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        currency varchar(10),
        is_active boolean not null,
        p_unit_link_basic_plan varchar(50),
        p_unit_link_contract_period varchar(30),
        p_unit_link_premium varchar(50),
        p_unit_link_premium_payment_period varchar(2),
        p_unit_link_rider_1 varchar(60),
        p_unit_link_sum_insured varchar(50),
        p_unit_link_sum_insured_rider_1 varchar(50),
        p_unit_link_total_premium varchar(50),
        premium_payment_period varchar(2),
        spaj_coverage_premium_detail_code varchar(12),
        spaj_document_code varchar(12),
        spaj_no varchar(12),
        primary key (id)
    );

    create table m_spaj_document (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        document_type varchar(15) not null,
        marketing_office varchar(10) not null,
        marketing_program varchar(15),
        mode varchar(1) not null,
        policy_no varchar(30) not null,
        proposal_no varchar(11),
        reference varchar(12),
        soa_code varchar(15),
        source varchar(12) not null,
        spaj_document_code varchar(12),
        spaj_no varchar(11),
        underwriting_notes boolean,
        unique_id varchar(255) not null,
        primary key (id)
    );

    create table m_spaj_document_file (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        dms_code varchar(12) not null,
        document_name varchar(60),
        file varchar(255) not null,
        is_active boolean not null,
        spaj_document_code varchar(12) not null,
        spaj_document_file_code varchar(12),
        spaj_no varchar(12) not null,
        primary key (id)
    );

    create table m_spaj_document_history (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(60),
        is_active boolean,
        spaj_document_code varchar(12),
        spaj_document_history_code varchar(12),
        status varchar(20),
        primary key (id)
    );

    create table m_spaj_health (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        blood_type_insured varchar(30),
        blood_type_policy_holder varchar(30),
        height_insured varchar(60),
        height_policy_holder varchar(60),
        insured_month_question_14a varchar(10),
        insured_question_1 varchar(10),
        insured_question_2a varchar(10),
        insured_question_2b varchar(10),
        insured_question_2c varchar(10),
        insured_question_2d varchar(10),
        insured_question_2e varchar(10),
        insured_question_2f varchar(10),
        insured_question_2g varchar(10),
        insured_question_2h varchar(10),
        insured_question_2i varchar(10),
        insured_question_3 varchar(10),
        insured_question_4 varchar(10),
        insured_question_5 varchar(10),
        insured_question_6 varchar(10),
        insured_question_6a varchar(10),
        insured_question_6b varchar(10),
        insured_question_6c varchar(10),
        insured_question_6d varchar(10),
        insured_question_7 varchar(10),
        is_active boolean,
        notes_question varchar(10),
        policy_holder_month_question_14a varchar(10),
        policy_holder_question_1 varchar(10),
        policy_holder_question_2a varchar(10),
        policy_holder_question_2b varchar(10),
        policy_holder_question_2c varchar(10),
        policy_holder_question_2d varchar(10),
        policy_holder_question_2e varchar(10),
        policy_holder_question_2f varchar(10),
        policy_holder_question_2g varchar(10),
        policy_holder_question_2h varchar(10),
        policy_holder_question_2i varchar(10),
        policy_holder_question_3 varchar(10),
        policy_holder_question_4 varchar(10),
        policy_holder_question_5 varchar(10),
        policy_holder_question_6 varchar(10),
        policy_holder_question_6a varchar(10),
        policy_holder_question_6b varchar(10),
        policy_holder_question_6c varchar(10),
        policy_holder_question_6d varchar(10),
        policy_holder_question_7 varchar(10),
        policy_holder_total_question_3 varchar(10),
        spaj_document_code varchar(12),
        spaj_health_code varchar(12),
        spaj_no varchar(12),
        weight_insured varchar(15),
        weight_policy_holder varchar(15),
        primary key (id)
    );

    create table m_spaj_insured (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        citizenship varchar(3),
        correspondence_address varchar(225),
        date_of_birth date,
        email_address varchar(30),
        full_name varchar(60),
        gender varchar(1),
        home_address_1 varchar(225),
        home_address_2 varchar(225),
        home_address_3 varchar(225),
        home_city varchar(30),
        home_country varchar(4),
        home_handphone varchar(30),
        home_phone varchar(30),
        home_province varchar(4),
        home_zipcode varchar(10),
        identity_no varchar(30),
        identity_type varchar(6) not null,
        industry varchar(6),
        is_active boolean not null,
        job_level varchar(5),
        last_education varchar(30),
        marital_status varchar(2),
        npwp_no varchar(30),
        office varchar(30),
        office_address_1 varchar(225),
        office_address_2 varchar(225),
        office_address_3 varchar(225),
        office_city varchar(30),
        office_country varchar(8),
        office_handphone varchar(30),
        office_phone varchar(30),
        office_province varchar(4),
        office_zipcode varchar(10),
        other_correspondence_address_1 varchar(225),
        other_correspondence_address_2 varchar(225),
        other_correspondence_address_3 varchar(225),
        other_correspondence_city varchar(30),
        other_correspondence_country varchar(8),
        other_correspondence_handphone varchar(30),
        other_correspondence_phone varchar(30),
        other_correspondence_province varchar(4),
        other_correspondence_zipcode varchar(10),
        place_of_birth varchar(30),
        position varchar(30),
        religion varchar(1),
        religion_other varchar(15),
        spaj_document_code varchar(12),
        spaj_no varchar(12),
        spaj_policy_insured_code varchar(12),
        primary key (id)
    );

    create table m_spaj_policy_holder (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        citizenship varchar(3),
        correspondence_address varchar(225),
        date_of_birth date,
        email_address varchar(30),
        full_name varchar(60),
        gender varchar(1),
        home_address_1 varchar(225),
        home_address_2 varchar(225),
        home_address_3 varchar(225),
        home_city varchar(30),
        home_country varchar(4),
        home_handphone varchar(30),
        home_phone varchar(30),
        home_province varchar(4),
        home_zipcode varchar(10),
        identity_no varchar(30),
        identity_type varchar(6) not null,
        industry varchar(6),
        is_active boolean not null,
        job_level varchar(5),
        last_education varchar(30),
        marital_status varchar(2),
        npwp_no varchar(30),
        office varchar(30),
        office_address_1 varchar(225),
        office_address_2 varchar(225),
        office_address_3 varchar(225),
        office_city varchar(30),
        office_country varchar(8),
        office_handphone varchar(30),
        office_phone varchar(30),
        office_province varchar(4),
        office_zipcode varchar(10),
        other_correspondence_address_1 varchar(225),
        other_correspondence_address_2 varchar(225),
        other_correspondence_address_3 varchar(225),
        other_correspondence_city varchar(30),
        other_correspondence_country varchar(8),
        other_correspondence_handphone varchar(30),
        other_correspondence_phone varchar(30),
        other_correspondence_province varchar(4),
        other_correspondence_zipcode varchar(10),
        other_relationship varchar(30),
        place_of_birth varchar(30),
        position varchar(30),
        relationship varchar(2) not null,
        religion varchar(1),
        religion_other varchar(15),
        spaj_document_code varchar(12),
        spaj_no varchar(12),
        spaj_policy_holder_code varchar(12),
        primary key (id)
    );

    create table m_spaj_premium_payment (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        bank_name varchar(20) not null,
        is_active boolean not null,
        other_bank varchar(5),
        premium_payment_method varchar(5) not null,
        spaj_document_code varchar(12),
        spaj_no varchar(12),
        spaj_premium_payment_code varchar(12),
        primary key (id)
    );

    create table m_spaj_source_type (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        is_active boolean not null,
        spaj_source_type_code varchar(12) not null,
        spaj_source_type_name varchar(15) not null,
        primary key (id)
    );

    create table m_spaj_statement_letter (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        is_active boolean not null,
        spaj_document_code varchar(12),
        spaj_no varchar(12),
        spaj_statement_letter_code varchar(12),
        submission_date varchar(20),
        submission_place varchar(30),
        primary key (id)
    );

    create table m_user (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        email varchar(255) not null,
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(100) not null,
        user_type_code varchar(255),
        username varchar(100) not null,
        primary key (id)
    );

    create table m_user_channel (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        channel_code varchar(10),
        is_active boolean,
        user_channel_code varchar(10) not null,
        username varchar(30),
        primary key (id)
    );

    create table m_user_menu (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        is_active boolean,
        menu_code varchar(10),
        user_menu_code varchar(30) not null,
        username varchar(30),
        primary key (id)
    );

    create table m_user_role (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        is_active boolean,
        role_code varchar(10),
        user_role_code varchar(30) not null,
        username varchar(30),
        primary key (id)
    );

    create table m_user_type (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        code varchar(10),
        description varchar(30),
        is_active boolean,
        primary key (id)
    );

    create table m_valuta (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(30) not null,
        is_active boolean not null,
        symbol varchar(10) not null,
        valuta_code varchar(6) not null,
        primary key (id)
    );

    create table s_page (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(50) not null,
        is_active boolean not null,
        "order" int4 not null,
        page_code varchar(3) not null,
        primary key (id)
    );

    create table s_variable (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        block_name varchar(20) not null,
        data_type varchar(20) not null,
        description varchar(50) not null,
        is_active boolean not null,
        variable_code varchar(10) not null,
        primary key (id)
    );

    create table t_answer (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        answer_code varchar(10) not null,
        appointment_time_code varchar(3) not null,
        customer_code varchar(15) not null,
        extract_date timestamp,
        is_active boolean not null,
        is_processed boolean not null,
        question_1 varchar(60) not null,
        question_2 varchar(60) not null,
        question_3 varchar(60) not null,
        question_4 varchar(60) not null,
        question_5 varchar(60) not null,
        question_6 varchar(60) not null,
        question_7 varchar(60) not null,
        primary key (id)
    );

    create table t_base_rate (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        age int4 not null,
        age_th int4 not null,
        as_code varchar(2) not null,
        base_rate_code varchar(12) not null,
        category varchar(3) not null,
        contract_period int4 not null,
        divider int4 not null,
        is_active boolean not null,
        payment_period_code varchar(3) not null,
        premium_period int4 not null,
        product_code varchar(10) not null,
        rate int4 not null,
        valuta_code varchar(6) not null,
        year_th int4 not null,
        primary key (id)
    );

    create table t_base_rule_acquisition_fee (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        base_rule_acquisition_fee_code varchar(6) not null,
        is_active boolean not null,
        payment_period_code varchar(3) not null,
        product_code varchar(10) not null,
        rate float8 not null,
        valuta_code varchar(6) not null,
        year_th int4 not null,
        primary key (id)
    );

    create table t_base_rule_admin_fee (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        base_rule_admin_fee_code varchar(6) not null,
        is_active boolean,
        payment_period_code varchar(3),
        product_code varchar(10),
        rate int4,
        valuta_code varchar(6),
        primary key (id)
    );

    create table t_base_rule_as (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        as_code varchar(10),
        base_rule_as_code varchar(10) not null,
        is_active boolean,
        max_age int4,
        min_age int4,
        product_code varchar(10),
        primary key (id)
    );

    create table t_base_rule_client (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        as_code varchar(10) not null,
        base_rule_client_code varchar(6) not null,
        is_active boolean not null,
        is_required boolean not null,
        product_code varchar(10) not null,
        tot_client int4 not null,
        primary key (id)
    );

    create table t_base_rule_contract_period (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        base_rule_contract_period_code varchar(10) not null,
        is_active boolean not null,
        max int4 not null,
        max_age int4 not null,
        min int4 not null,
        payment_period_code varchar(10) not null,
        product_code varchar(10) not null,
        step int4 not null,
        primary key (id)
    );

    create table t_base_rule_debt_account (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        base_rule_debt_account_code varchar(6) not null,
        from_year_percen float8 not null,
        from_year_th int4 not null,
        is_active boolean not null,
        product_code varchar(10) not null,
        to_year_percen float8 not null,
        to_year_th int4 not null,
        primary key (id)
    );

    create table t_base_rule_entity (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        base_rule_entitas_code varchar(6) not null,
        interest_thp int4 not null,
        is_active boolean not null,
        is_allocation_fund boolean not null,
        is_contract_period boolean not null,
        is_interest_thp boolean not null,
        is_mutation_fund boolean not null,
        is_premium_period boolean not null,
        is_premium_periodic boolean not null,
        is_retirement boolean not null,
        is_single_topup boolean not null,
        is_topup_periodic boolean not null,
        product_code varchar(10) not null,
        retirement_age int4 not null,
        primary key (id)
    );

    create table t_base_rule_invest_type (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        base_rule_invest_type_code varchar(10) not null,
        high_interest float8 not null,
        invest_type_code varchar(10) not null,
        is_active boolean not null,
        low_interest float8 not null,
        medium_interest float8 not null,
        product_code varchar(10) not null,
        valuta_code varchar(6) not null,
        primary key (id)
    );

    create table t_base_rule_payment_period (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        base_rule_payment_period_code varchar(10) not null,
        factor int4 not null,
        is_active boolean not null,
        payment_period_code varchar(10) not null,
        product_code varchar(10) not null,
        valuta_code varchar(6) not null,
        primary key (id)
    );

    create table t_base_rule_periodic_topup (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        base_rule_periodic_topup_code varchar(10) not null,
        is_active boolean not null,
        max int4 not null,
        min int4 not null,
        payment_period_code varchar(10) not null,
        product_code varchar(10) not null,
        step int4 not null,
        valuta_code varchar(10) not null,
        primary key (id)
    );

    create table t_base_rule_policy_fee (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        base_rule_policy_fee_code varchar(6) not null,
        is_active boolean not null,
        payment_period_code varchar(3),
        product_code varchar(10),
        rate float8 not null,
        valuta_code varchar(6),
        primary key (id)
    );

    create table t_base_rule_premium_allocation (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        base_rule_premium_allocation_code varchar(6) not null,
        is_active boolean not null,
        payment_period_code varchar(3) not null,
        premium_percent float8 not null,
        product_code varchar(10) not null,
        topup_percent float8 not null,
        valuta_code varchar(6) not null,
        primary key (id)
    );

    create table t_base_rule_premium_period (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        base_rule_premium_period_code varchar(10) not null,
        is_active boolean not null,
        max int4 not null,
        max_age int4 not null,
        min int4 not null,
        payment_period_code varchar(10) not null,
        product_code varchar(10) not null,
        step int4 not null,
        primary key (id)
    );

    create table t_base_rule_premium_periodic (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        base_rule_premium_periodic_code varchar(10) not null,
        is_active boolean,
        max_premium float8,
        min_premium float8,
        payment_period_code varchar(10),
        product_code varchar(10),
        step_premium float8,
        valuta_code varchar(10),
        primary key (id)
    );

    create table t_base_rule_premium_receipt (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        base_rule_premium_receipt_code varchar(10) not null,
        is_active boolean,
        payment_period_code varchar(10),
        product_code varchar(10),
        rate float8,
        valuta_code varchar(10),
        primary key (id)
    );

    create table t_base_rule_redeem (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        age_th int4,
        base_rule_redeem_code varchar(6) not null,
        is_active boolean,
        percent float8,
        product_code varchar(10),
        year_th int4,
        primary key (id)
    );

    create table t_base_rule_redeem_fee (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        base_rule_redeem_fee_code varchar(6) not null,
        is_active boolean not null,
        payment_period_code varchar(3) not null,
        product_code varchar(10) not null,
        rate float8 not null,
        valuta_code varchar(6) not null,
        year_th int4 not null,
        primary key (id)
    );

    create table t_base_rule_relation (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        as_code varchar(10) not null,
        base_rule_relation_code varchar(10) not null,
        is_active boolean not null,
        product_code varchar(10) not null,
        relation_type_code varchar(10) not null,
        primary key (id)
    );

    create table t_base_rule_single_topup (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        base_rule_single_topup_code varchar(10) not null,
        is_active boolean,
        max int4,
        min int4,
        payment_period_code varchar(10),
        product_code varchar(10),
        step float8,
        valuta_code varchar(10),
        primary key (id)
    );

    create table t_base_rule_topup_fee (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        base_rule_topup_fee_code varchar(6) not null,
        is_active boolean not null,
        payment_period_code varchar(3) not null,
        product_code varchar(10) not null,
        rate float8 not null,
        valuta_code varchar(6) not null,
        primary key (id)
    );

    create table t_base_rule_valuta (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        base_rule_single_topup_code varchar(10) not null,
        is_active boolean,
        product_code varchar(10),
        valuta_code varchar(10),
        primary key (id)
    );

    create table t_bill (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        amount numeric(8, 2),
        bill_code varchar(15) not null,
        is_active boolean not null,
        registration_code varchar(15) not null,
        primary key (id)
    );

    create table t_customer (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        core_member_no varchar(15),
        customer_code varchar(15) not null,
        customer_name varchar(100) not null,
        customer_status varchar(5),
        date_of_birth varchar(100) not null,
        email_address varchar(100) not null,
        gender_code varchar(100) not null,
        is_active boolean not null,
        ktp_no varchar(100) not null,
        occupation varchar(100) not null,
        phone_no varchar(50) not null,
        ref_customer_code varchar(15),
        primary key (id)
    );

    create table t_customer_message (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        customer_message_code varchar(12) not null,
        customer_name varchar(100) not null,
        email_address varchar(50) not null,
        is_active boolean not null,
        message TEXT not null,
        phone_no varchar(100) not null,
        title varchar(100) not null,
        primary key (id)
    );

    create table t_customer_point (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        account_code varchar(15),
        current_point int4,
        customer_point_code varchar(15) not null,
        description varchar(50),
        is_active boolean not null,
        point_in int4,
        point_out int4,
        primary key (id)
    );

    create table t_customer_policy (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        customer_code varchar(12) not null,
        customer_policy_code varchar(12) not null,
        customer_policy_core_code varchar(50) not null,
        customer_reference_code varchar(12) not null,
        file_type_code varchar(3),
        insurance_period varchar(60),
        insurance_type varchar(100),
        is_active boolean not null,
        is_synced_to_core boolean not null,
        member_no varchar(15),
        policy_no varchar(12),
        ref_no varchar(20),
        registration_code varchar(15),
        sum_insured_ajb varchar(100),
        sum_insured_inpatient varchar(60),
        primary key (id)
    );

    create table t_customer_reference (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        customer_code varchar(15) not null,
        customer_reference_code varchar(12) not null,
        is_active boolean not null,
        unique_key varchar(15) not null,
        primary key (id)
    );

    create table t_customer_referral (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        account_code varchar(15),
        is_active boolean not null,
        referral_code varchar(15) not null,
        used_referral_code varchar(15) not null,
        primary key (id)
    );

    create table t_customer_registration (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        customer_code varchar(15) not null,
        is_active boolean not null,
        registration_code varchar(15) not null,
        primary key (id)
    );

    create table t_failed_async_jobs (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        cause_exception varchar(1000),
        exception varchar(5000),
        is_active boolean not null,
        method varchar(1000),
        payload varchar(1000),
        response varchar(1000),
        primary key (id)
    );

    create table t_interactive (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        appointment_time_code varchar(3) not null,
        customer_name varchar(100) not null,
        date_of_birth varchar(100) not null,
        email_address varchar(100) not null,
        gender_code varchar(1) not null,
        income varchar(100) not null,
        interactive_code varchar(12) not null,
        is_active boolean not null,
        occupation varchar(100) not null,
        phone_no varchar(50) not null,
        primary key (id)
    );

    create table t_interactive_needs (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(250) not null,
        interactive_code varchar(12) not null,
        interactive_needs_code varchar(12) not null,
        is_active boolean not null,
        primary key (id)
    );

    create table t_interactive_priority (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(250) not null,
        interactive_code varchar(12) not null,
        interactive_priority_code varchar(12) not null,
        is_active boolean not null,
        primary key (id)
    );

    create table t_jobs (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        count_retry int4,
        is_active boolean not null,
        method varchar(1000),
        payload varchar(1000),
        response varchar(1000),
        sequence int4,
        transaction_code_core varchar(500),
        transaction_id varchar(200),
        type varchar(50),
        primary key (id)
    );

    create table t_log_message (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        email_address varchar(150),
        gender_code varchar(2),
        is_active boolean not null,
        last_response_description varchar(150) not null,
        log_message_code varchar(10) not null,
        message_type varchar(1) not null,
        nav varchar(100),
        notes TEXT,
        phone_no varchar(100),
        send_date varchar(100) not null,
        sent_status boolean,
        type_code varchar(3) not null,
        va_bca varchar(20),
        va_permata varchar(20),
        valuta_id varchar(10),
        variable_date_1 varchar(100),
        variable_date_2 varchar(100),
        variable_name_1 varchar(100) not null,
        variable_name_2 varchar(100),
        variable_no_1 varchar(50) not null,
        variable_no_2 varchar(20),
        variable_no_3 varchar(50),
        variable_sum_1 varchar(100),
        variable_sum_2 varchar(100),
        variable_sum_3 varchar(100),
        primary key (id)
    );

    create table t_payment (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        amount numeric(15, 2),
        bill_code varchar(15) not null,
        is_active boolean not null,
        paid_amount numeric(15, 2),
        payment_code varchar(15) not null,
        payment_desc varchar(50),
        payment_ref_code varchar(60) not null,
        payment_status boolean not null,
        transaction_date date,
        primary key (id)
    );

    create table t_product_rate (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        age_max int4 not null,
        age_min int4 not null,
        as_code varchar(2) not null,
        category varchar(2) not null,
        classs int4 not null,
        contract_period int4 not null,
        gender_code varchar(1) not null,
        is_active boolean not null,
        payment_period_code varchar(3) not null,
        premium_period int4 not null,
        product_code varchar(10) not null,
        product_rate_code varchar(12) not null,
        rate float8 not null,
        risk int4 not null,
        valuta_code varchar(6) not null,
        primary key (id)
    );

    create table t_product_rule_activate (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        as_code varchar(10),
        gender_code varchar(1),
        is_active boolean,
        max int4,
        min int4,
        payment_period_code varchar(10),
        product_code varchar(10),
        product_rule_activate_code varchar(10) not null,
        relation_type_code varchar(10),
        rule_premium varchar(60),
        valuta_code varchar(6),
        primary key (id)
    );

    create table t_product_rule_benefit (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        footer_title varchar(120),
        footer_value varchar(120),
        header_title varchar(120),
        header_value varchar(120),
        is_active boolean,
        product_code varchar(10),
        product_rule_benefit_code varchar(10) not null,
        title varchar(120),
        valuta_code varchar(6),
        primary key (id)
    );

    create table t_product_rule_benefit_item (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(250),
        is_active boolean,
        "order" int4 not null,
        product_code varchar(10),
        product_rule_benefit_item_code varchar(10) not null,
        value varchar(120),
        primary key (id)
    );

    create table t_product_rule_benefit_item_rate (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        as_charge varchar(12),
        class_value varchar(12),
        "class" varchar(12),
        is_active boolean,
        "order" int4 not null,
        product_code varchar(10),
        product_rule_benefit_item_rate_code varchar(10) not null,
        rate float8 not null,
        use_max_sum_insured varchar(12),
        primary key (id)
    );

    create table t_product_rule_class (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        "class" float8 not null,
        is_active boolean,
        max_premium_periodic float8 not null,
        max_sum_insured_base float8 not null,
        max_tu_periodic float8 not null,
        payment_period_code varchar(10),
        product_code varchar(10),
        product_rule_class_code varchar(10) not null,
        primary key (id)
    );

    create table t_product_rule_contract_period (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        is_active boolean,
        max_age_contract_period int4 not null,
        max_contract_period varchar(30),
        min_contract_period varchar(30),
        product_code varchar(10),
        product_rule_contract_period_code varchar(10) not null,
        step_contract_period int4 not null,
        primary key (id)
    );

    create table t_product_rule_core (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        as_code varchar(2),
        is_active boolean,
        mapp_code varchar(20),
        payment_period_code varchar(10),
        product_code varchar(10),
        product_rule_core_code varchar(10) not null,
        risk varchar(10),
        sum_insured_class int4 not null,
        valuta_code varchar(6),
        primary key (id)
    );

    create table t_product_rule_grouping (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        grouping_code varchar(2),
        is_active boolean,
        mandatory int4,
        max int4,
        min int4,
        product_code varchar(10),
        product_code_ref varchar(10),
        product_rule_grouping_code varchar(10) not null,
        valuta_code varchar(6),
        primary key (id)
    );

    create table t_product_rule_grouping_sum_insured (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        grouping_code varchar(2),
        is_active boolean,
        max_sum_insured float8,
        product_code varchar(10),
        product_rule_grouping_sum_insured_code varchar(10) not null,
        primary key (id)
    );

    create table t_product_rule_image (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        image_code varchar(10),
        is_active boolean,
        product_code varchar(10),
        product_rule_image_code varchar(10) not null,
        title varchar(2),
        primary key (id)
    );

    create table t_product_rule_notes (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        description varchar(250),
        fill_h_d varchar(1),
        fill_ph_pb_pf varchar(2),
        is_active boolean,
        "order" int4 not null,
        other_description varchar(250),
        page_code varchar(3),
        product_code varchar(10),
        product_rule_note_code varchar(10) not null,
        valuta_code varchar(6),
        primary key (id)
    );

    create table t_product_rule_premium_period (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        is_active boolean,
        max_age_premium_period int4 not null,
        max_premium_period varchar(30),
        min_premium_period varchar(30),
        product_code varchar(10),
        product_rule_premium_period_code varchar(10) not null,
        step_premium_period int4 not null,
        primary key (id)
    );

    create table t_product_rule_required (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        as_code varchar(10),
        is_active boolean,
        product_code varchar(10),
        product_rule_required_code varchar(10) not null,
        relation_type_code varchar(10),
        primary key (id)
    );

    create table t_product_rule_risk (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        is_active boolean,
        product_code varchar(10),
        product_rule_risk_code varchar(10) not null,
        risk_name varchar(12),
        primary key (id)
    );

    create table t_product_rule_sum_insured (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        as_code varchar(2),
        "class" varchar(12),
        is_active boolean,
        max_age int4 not null,
        max_premium_periodic float8 not null,
        max_sum_insured_base float8 not null,
        max_sum_insured_formula varchar(100),
        min_age int4 not null,
        min_premium_periodic float8 not null,
        min_sum_insured_base float8 not null,
        min_sum_insured_formula varchar(100),
        payment_period_code varchar(10),
        product_code varchar(10),
        product_rule_sum_insured_code varchar(10) not null,
        risk varchar(12),
        step_sum_insured float8 not null,
        valuta_code varchar(6),
        primary key (id)
    );

    create table t_registration (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        account_code varchar(15),
        app_code varchar(15) not null,
        is_active boolean not null,
        registration_code varchar(15) not null,
        registration_type_code varchar(15) not null,
        app_id int8,
        primary key (id)
    );

    create table t_request_history (
       id  bigserial not null,
        created_by varchar(255),
        created_date timestamp,
        last_modified_by varchar(255),
        last_modified_date timestamp,
        code varchar(4) not null,
        customer_reference_code varchar(12) not null,
        is_active boolean not null,
        request_history_code varchar(12) not null,
        primary key (id)
    );

    alter table m_account 
       add constraint UK_ki7ytbeihcumecvah8h0h17nx unique (account_code);

    alter table m_account 
       add constraint UK_k0sfolvnmoleo2k5fbkbbeip9 unique (unique_activation_key);

    alter table m_address_type 
       add constraint UK_siyx1dka1jb2ib0uliuels5mq unique (address_type_code);

    alter table m_agen_relation 
       add constraint UK_sealbgt6woaroqtqbl78vfjug unique (agen_relation_code);

    alter table m_answer 
       add constraint UK_gnol1mo6mp97t790811043yll unique (answer_code);

    alter table m_app 
       add constraint UK_k6rpu6wy3ybia6upum4t4g1j4 unique (app_code);

    alter table m_appointment_time 
       add constraint UK_qco2ixniy6epe1aj2duy4h4hi unique (appointment_time_code);

    alter table m_as 
       add constraint UK_hcefge0yoenkvx5my5pd05cf6 unique (as_code);

    alter table m_bank_payment 
       add constraint UK_8acy78laajpvmyvicugabwgho unique (bank_payment_code);

    alter table m_card_issuer 
       add constraint UK_q5677dj45a6smqmyalg0dataj unique (card_issuer_code);

    alter table m_card_type 
       add constraint UK_7tcleuc9b9dj74m7ns23se7h5 unique (card_type_code);

    alter table m_certificate 
       add constraint UK_fxuf831um14kbbyjflucteegr unique (certificate_code);

    alter table m_certificate 
       add constraint UK_e0mmv6k2ei8x019xhhjx37osv unique (certificate_desc);

    alter table m_channel 
       add constraint UK_8g7k4cie64pq3v6ampcvk1olp unique (channel_code);

    alter table m_citizen 
       add constraint UK_kxo6caxylw5acc8ae9irqy90q unique (citizen_code);

    alter table m_country 
       add constraint UK_eqaqky9krm3flbx6akldkdq6b unique (country_code);

    alter table m_currency 
       add constraint UK_aaq3n1u6vlfyypx3c45dyhawa unique (currency_code);

    alter table m_document_checklist 
       add constraint UK_92y3vpdg1h3or8lm4mg83lkiv unique (payment_method_code);

    alter table m_document_mapping 
       add constraint UK_gcgdhxlr28utbsmmpo9gfypm unique (document_mapping_code);

    alter table m_education 
       add constraint UK_jn6dlw9sxkuar7npuyja9ojay unique (education_code);

    alter table m_gender 
       add constraint UK_j79d7qkw0uhnqqhik702eol0x unique (gender_code);

    alter table m_gender_mgm 
       add constraint UK_dm9qefaaluqmo889xou4o5wof unique (gender_code);

    alter table m_genderrm 
       add constraint UK_1xsfd1uq6puiv837tikytfbmn unique (gender_code);

    alter table m_gift 
       add constraint UK_412h3suvy5qvipfypbkod7fy7 unique (gift_code);

    alter table m_identity_type 
       add constraint UK_djrqocaxagp85nwr8h8n3wo8l unique (identity_type_code);

    alter table m_image 
       add constraint UK_2lfpkwawik6g7bnhpqimkk2hq unique (image_code);

    alter table m_income 
       add constraint UK_h132lkrgy1whra9c3hlw5hslh unique (income_code);

    alter table m_industry 
       add constraint UK_k1f51c65kyclefi89cbsyxu1i unique (industry_code);

    alter table m_insured_relation 
       add constraint UK_ctucuinjk0dhst9yhc51qtsel unique (insured_relation_code);

    alter table m_invest_type 
       add constraint UK_2sxuckm1v1bit7hg1qqy8mjhs unique (invest_type_code);

    alter table m_marital_status 
       add constraint UK_b1q0qlhhwi3qj53u7ddhlc24b unique (marital_status_code);

    alter table m_mens_condition 
       add constraint UK_l2swbigvm0n8sms8raa7a9lp6 unique (mens_condition_code);

    alter table m_menu 
       add constraint UK_rfwen4b5jhgbjsvakbp4nhtj1 unique (menu_code);

    alter table m_occupation_group 
       add constraint UK_smxiwyy62mu6v3idr41h876vh unique (occupation_group_code);

    alter table m_payment_method 
       add constraint UK_p261stvstlx59sr4r8e0a8sq3 unique (payment_method_code);

    alter table m_payment_period 
       add constraint UK_rh8sj3ar47lhijeet5xsbx3ft unique (payment_period_code);

    alter table m_product_channel 
       add constraint UK_s9jlv3cov6ee8xqq8olwjk9oh unique (product_channel_code);

    alter table m_product_plan 
       add constraint UK_kh290kmdyc5pkggivb3sv7fgp unique (product_policy_code);

    alter table m_product_policy 
       add constraint UK_7qn47ddpu627b2cnfmk72m971 unique (product_policy_code);

    alter table m_products 
       add constraint UK_jkb5ph79yavooj1gnapnmqfnw unique (product_code);

    alter table m_proposal 
       add constraint UK_rwus6u4gcw0ow1utgegxcc6b8 unique (proposal_no);

    alter table m_province 
       add constraint UK_en7iq503i3a2nq6dgvgn8ktxa unique (province_code);

    alter table m_purpose 
       add constraint UK_2bu9y2fgft1tg6hw10w4h1i8q unique (purpose_code);

    alter table m_question 
       add constraint UK_54p0udll5raverb9pcwslc17s unique (question_code);

    alter table m_recipient 
       add constraint UK_ogjefxcv2cpl0tx50pssv52s3 unique (recipient_code);

    alter table m_recipient_received_msg 
       add constraint UK87wvy8gj43my4cr7ny3byg2gs unique (recipient_code, type_code);

    alter table m_recipient_received_msg 
       add constraint UK_dmf89450htdvnu0evow70s26a unique (recipient_received_msg_code);

    alter table m_recipient_type 
       add constraint UK_d4g8iqhq8u6qntapowsgh41pj unique (recipient_type_code);

    alter table m_recurring_type 
       add constraint UK_3l4y8uocsgqy5rv3ae0prsui6 unique (recurring_type_code);

    alter table m_registration_type 
       add constraint UK_jdtpyfft29jfvxtn7ttyviyqx unique (registration_type_desc);

    alter table m_registration_type 
       add constraint UK_1j40s6ase8r8corrq9164tqa4 unique (registration_type_code);

    alter table m_relation_type 
       add constraint UK_hn76fw483q8297cu7chaapoh0 unique (relation_type_code);

    alter table m_relationship 
       add constraint UK_osy6phlmkmxdgturef77t5i9s unique (relationship_code);

    alter table m_religion 
       add constraint UK_ov62amhhepi9qaf9fhkuxj341 unique (religion_code);

    alter table m_residence_status 
       add constraint UK_q0xxqtl35u5oemv47bxtb0r0g unique (residence_status_code);

    alter table m_role 
       add constraint UK_6d1o5q8h9fx8jw70v7wn8rgrk unique (role_code);

    alter table m_role_menu 
       add constraint UK_rd5nwy3bm3sleo5x6ucbwwrbq unique (role_menu_code);

    alter table m_rule_insured_age 
       add constraint UK_clj6pof039c1mia9cjajbbmfv unique (rule_insured_age_code);

    alter table m_service_unit 
       add constraint UK_ar4y089pim05x6l2suordi12j unique (service_unit_code);

    alter table m_source_fund 
       add constraint UK_r3ajhhm1hp04qqwg7l1nc3ein unique (source_fund_code);

    alter table m_spaj 
       add constraint UK_hwcuqm50ej6ay62xv0i5tay0p unique (spaj_code);

    alter table m_spaj_additional_stat 
       add constraint UK_rxly68lk6wl2pa877co6ccb8u unique (spaj_additional_stat_code);

    alter table m_spaj_agen_statement 
       add constraint UK_gsdvucl1yvu13ppnld49e5cfb unique (spaj_agen_statement_code);

    alter table m_spaj_beneficiary 
       add constraint UK_c314sfs6blw0xagaon2p7sbmy unique (spaj_beneficiary_code);

    alter table m_spaj_coverage_premium_detail 
       add constraint UK_s8efup99fjfibukm2gnuqd4xl unique (spaj_coverage_premium_detail_code);

    alter table m_spaj_document 
       add constraint UK_b06vwvk4c83seud8arrvdsfxt unique (spaj_document_code);

    alter table m_spaj_document_file 
       add constraint UKm0kyu27icbad574pempuk69bs unique (spaj_no, dms_code);

    alter table m_spaj_document_file 
       add constraint UK_ao05d3klxhsxdnnw73tev7j0g unique (spaj_document_file_code);

    alter table m_spaj_document_history 
       add constraint UK_3wlgexissayynlurnx06sjlgw unique (spaj_document_history_code);

    alter table m_spaj_health 
       add constraint UK_tr0qmwmnn9l3jl5rmkjyhsl8k unique (spaj_health_code);

    alter table m_spaj_insured 
       add constraint UK_hs9ugbku38k50cgtp36aokq4u unique (spaj_policy_insured_code);

    alter table m_spaj_policy_holder 
       add constraint UK_4u9few3qcsln43htq1rofkwhm unique (spaj_policy_holder_code);

    alter table m_spaj_premium_payment 
       add constraint UK_4w26ntip90ygtuxb5pb21c6is unique (spaj_premium_payment_code);

    alter table m_spaj_source_type 
       add constraint UK_qtsfy0v7q2rrmusd14wdy7bit unique (spaj_source_type_code);

    alter table m_spaj_statement_letter 
       add constraint UK_ol2wx2dfusfnlgkakfytq0jhj unique (spaj_statement_letter_code);

    alter table m_user 
       add constraint UK_rycw44p7cruupkosx3ibmj9q3 unique (email);

    alter table m_user 
       add constraint UK_brmb54ld0gqstogmb812xw951 unique (username);

    alter table m_user_channel 
       add constraint UK_nccgfe58v7rew943kl3sxc13k unique (user_channel_code);

    alter table m_user_menu 
       add constraint UK_cpkaveasps0o84e1enh3g95yo unique (user_menu_code);

    alter table m_user_role 
       add constraint UK_5yl7sh9hfw9nqw4ej9p1y1b18 unique (user_role_code);

    alter table m_valuta 
       add constraint UK_cb27in85y2t0to59qrfh9dte8 unique (valuta_code);

    alter table s_page 
       add constraint UK_gax6enee8sc2dbxqdjc1btgq5 unique (page_code);

    alter table s_variable 
       add constraint UK_95kmasgh2w60s679mw6bqfo4m unique (variable_code);

    alter table t_answer 
       add constraint UK_hwqb8i6iotjrvy6xqj12u35ho unique (answer_code);

    alter table t_base_rate 
       add constraint UK_dos8igf69f20wpbwlm7caendg unique (base_rate_code);

    alter table t_base_rule_acquisition_fee 
       add constraint UK_a3w5yvl30jyb2wfxw4t6nvgc unique (base_rule_acquisition_fee_code);

    alter table t_base_rule_admin_fee 
       add constraint UK_1slkmbskdsfvih9889brh446i unique (base_rule_admin_fee_code);

    alter table t_base_rule_as 
       add constraint UK_4debenl78d53ce4wu07kofpp5 unique (base_rule_as_code);

    alter table t_base_rule_client 
       add constraint UK_bb7t23fbrf9lj2jlpkpssty1r unique (base_rule_client_code);

    alter table t_base_rule_contract_period 
       add constraint UK_nfo05almt68s2f1ricujir4al unique (base_rule_contract_period_code);

    alter table t_base_rule_debt_account 
       add constraint UK_k47rp9en52o0crbyeiyiui9l4 unique (base_rule_debt_account_code);

    alter table t_base_rule_entity 
       add constraint UK_jtfo6ub3lbr1cwa1bnf3hs88y unique (base_rule_entitas_code);

    alter table t_base_rule_payment_period 
       add constraint UK_kmfomwx35ce3jpm0obwhjdgr9 unique (base_rule_payment_period_code);

    alter table t_base_rule_periodic_topup 
       add constraint UK_ijhltsgt9ja9wvovvv2b7uqww unique (base_rule_periodic_topup_code);

    alter table t_base_rule_policy_fee 
       add constraint UK_i1ipmb4697bi50segqkpgffne unique (base_rule_policy_fee_code);

    alter table t_base_rule_premium_allocation 
       add constraint UK_fi3shgo1nl2uw3br71cfvljyh unique (base_rule_premium_allocation_code);

    alter table t_base_rule_premium_period 
       add constraint UK_5x3700klq9xq0cnj0tu97eyn6 unique (base_rule_premium_period_code);

    alter table t_base_rule_premium_periodic 
       add constraint UK_9bxm9py4d19evxi8bopvf0nuw unique (base_rule_premium_periodic_code);

    alter table t_base_rule_premium_receipt 
       add constraint UK_ncp5sbh8ja6hnoboi18dxf76e unique (base_rule_premium_receipt_code);

    alter table t_base_rule_redeem 
       add constraint UK_plmut5ccpdglnndhh2ntclai8 unique (base_rule_redeem_code);

    alter table t_base_rule_redeem_fee 
       add constraint UK_4dih422ycuqc1lt97bql6ml0 unique (base_rule_redeem_fee_code);

    alter table t_base_rule_relation 
       add constraint UK_bhbgaxyv5cfp5opstpqew503 unique (base_rule_relation_code);

    alter table t_base_rule_single_topup 
       add constraint UK_7r13gn3yvuxoxw7veeige1prh unique (base_rule_single_topup_code);

    alter table t_base_rule_topup_fee 
       add constraint UK_oyw0ji2txirdeux814o6va9di unique (base_rule_topup_fee_code);

    alter table t_base_rule_valuta 
       add constraint UK_dgnc2evs1dtc0sqh46w6960bo unique (base_rule_single_topup_code);

    alter table t_bill 
       add constraint UK_2lptefp1dnsi66g4if82lr40y unique (bill_code);

    alter table t_bill 
       add constraint UK_smcen282cjcmfrym50tp1h0av unique (registration_code);

    alter table t_customer 
       add constraint UK_ghbiil7iqujn7svnml9hcuglo unique (customer_code);

    alter table t_customer_message 
       add constraint UK_jl1cuqnbkvdjsuyjosm5j74oi unique (customer_message_code);

    alter table t_customer_point 
       add constraint UK_qnlq8x3gc1u0qa37ngs3ooitr unique (customer_point_code);

    alter table t_customer_policy 
       add constraint UK_6jbw3wycjfbdfhvxv5bg3co04 unique (customer_policy_code);

    alter table t_customer_policy 
       add constraint UK_kch14b1ll08v7p73t46m98scv unique (customer_policy_core_code);

    alter table t_customer_reference 
       add constraint UK_5yxlnob9jmoad722qql0stwh5 unique (unique_key);

    alter table t_customer_referral 
       add constraint UK_1vr3kocm8brr1yo23co4urrbo unique (referral_code);

    alter table t_jobs 
       add constraint UK_fq3861h3ivtxqxs5ge71yh3f1 unique (transaction_id);

    alter table t_log_message 
       add constraint UK_4fljm8jpradlmkoeq5rp116h7 unique (log_message_code);

    alter table t_payment 
       add constraint UK_hk6esa7xn773refx51dxbplf8 unique (payment_code);

    alter table t_product_rate 
       add constraint UK_t6cyoiamhdyh0rb1i94i3oi9a unique (product_rate_code);

    alter table t_product_rule_activate 
       add constraint UK_2i1tb162wybaclfgcxy5qcbtf unique (product_rule_activate_code);

    alter table t_product_rule_benefit 
       add constraint UK_hyd6t68qd0cpw002tddk7y7nd unique (product_rule_benefit_code);

    alter table t_product_rule_benefit_item 
       add constraint UK_ofytwu42m66edpvk8gk4o38g9 unique (product_rule_benefit_item_code);

    alter table t_product_rule_benefit_item_rate 
       add constraint UK_7k0maaa0mtq7iaosgx5199puo unique (product_rule_benefit_item_rate_code);

    alter table t_product_rule_class 
       add constraint UK_g6tlh7pwusgqd6wyd5y08mja4 unique (product_rule_class_code);

    alter table t_product_rule_contract_period 
       add constraint UK_oqeg68y6iv54qr4v7swvxtx7c unique (product_rule_contract_period_code);

    alter table t_product_rule_core 
       add constraint UK_6mto9n693ps2y2nqewrkwppfl unique (product_rule_core_code);

    alter table t_product_rule_grouping 
       add constraint UK_nd8la2oesucpj34s0eyflilhh unique (product_rule_grouping_code);

    alter table t_product_rule_grouping_sum_insured 
       add constraint UK_e4fkmkdlhcs4psvosnpncnw1o unique (product_rule_grouping_sum_insured_code);

    alter table t_product_rule_image 
       add constraint UK_gpfdmxtqfocsmnnd8ff9usee6 unique (product_rule_image_code);

    alter table t_product_rule_notes 
       add constraint UK_p2fpcqstgnnxh74ykxf3s911c unique (product_rule_note_code);

    alter table t_product_rule_premium_period 
       add constraint UK_rodd6nw5c1400rtxvgm952ccc unique (product_rule_premium_period_code);

    alter table t_product_rule_required 
       add constraint UK_2icrbei7dfjmpyr54y841asts unique (product_rule_required_code);

    alter table t_product_rule_risk 
       add constraint UK_qspsorbpbd1clw5ure5hrmmdr unique (product_rule_risk_code);

    alter table t_product_rule_sum_insured 
       add constraint UK_qm3lykkl7yrlr92vu4y6d2tjc unique (product_rule_sum_insured_code);

    alter table t_registration 
       add constraint UK_j0iq8j2w54xc02bdw0rq1p3d6 unique (registration_code);

    alter table t_registration 
       add constraint FKo2ysxkdeiiy4sgdxybq98yx0s
       foreign key (app_id) 
       references m_app;
