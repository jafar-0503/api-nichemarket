
/*TRIGGER REG CODE*/
CREATE OR REPLACE FUNCTION public.generate_account_code()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
DECLARE new_account varchar;
DECLARE check_account varchar;
DECLARE check_exist_date varchar;

BEGIN
    SELECT INTO check_account
        LPAD(cast(cast(substring(account_code, 10) AS integer) + 1 AS varchar), 4, '0')
    FROM m_account
    ORDER BY id
        DESC LIMIT 1;

    IF check_account IS NULL THEN
        new.account_code := concat('ACC', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    ELSE
        SELECT INTO check_exist_date
	    	CAST(SUBSTRING(account_code, 4, 6) AS varchar)
	    FROM m_account
	    ORDER BY id
	    DESC LIMIT 1;

	    CASE WHEN check_exist_date = CAST(to_char(now(), 'yymmdd') AS varchar) THEN
        	new.account_code := concat('ACC', to_char(now(), 'yymmdd'), LPAD(check_account, 4, '0'));

       		WHEN check_exist_date <> CAST (to_char(now(), 'yymmdd') AS varchar) THEN
       		new.account_code := concat('ACC', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    	END CASE ;
    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS generate_account_code
    ON public.m_account;
CREATE TRIGGER generate_account_code
    BEFORE INSERT
    ON public.m_account
    FOR EACH ROW
EXECUTE FUNCTION generate_account_code();



CREATE OR REPLACE FUNCTION public.calculate_current_point()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
DECLARE new_current_point integer;
DECLARE lastest_current_point integer;

BEGIN
    SELECT INTO lastest_current_point
        current_point
    FROM t_customer_point
    WHERE account_code = new.account_code
    ORDER BY id
        DESC LIMIT 1;

    IF lastest_current_point IS NULL THEN
        new.current_point := new.point_in;
    ELSE
        new.current_point := lastest_current_point + (new.point_in - new.point_out);
    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS calculate_current_point
    ON public.t_customer_point;
CREATE TRIGGER calculate_current_point
    BEFORE INSERT
    ON public.t_customer_point
    FOR EACH ROW
EXECUTE FUNCTION calculate_current_point();
