
/*TRIGGER REG CODE*/
CREATE OR REPLACE FUNCTION public.generate_rule_insured_age_code()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
DECLARE new_rule_insured_age varchar;
DECLARE check_rule_insured_age varchar;
DECLARE check_exist_date varchar;

BEGIN
    SELECT INTO check_rule_insured_age
        LPAD(cast(cast(substring(rule_insured_age_code, 10) AS integer) + 1 AS varchar), 4, '0')
    FROM m_rule_insured_age
    ORDER BY id
        DESC LIMIT 1;

    IF check_rule_insured_age IS NULL THEN
        new.rule_insured_age_code := concat('AGE', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    ELSE
        SELECT INTO check_exist_date
	    	CAST(SUBSTRING(rule_insured_age_code, 4, 6) AS varchar)
	    FROM m_rule_insured_age
	    ORDER BY id
	    DESC LIMIT 1;

	    CASE WHEN check_exist_date = CAST(to_char(now(), 'yymmdd') AS varchar) THEN
        	new.rule_insured_age_code := concat('AGE', to_char(now(), 'yymmdd'), LPAD(check_rule_insured_age, 4, '0'));

       		WHEN check_exist_date <> CAST (to_char(now(), 'yymmdd') AS varchar) THEN
       		new.rule_insured_age_code := concat('AGE', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    	END CASE ;
    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS generate_rule_insured_age_code
    ON public.m_rule_insured_age;
CREATE TRIGGER generate_rule_insured_age_code
    BEFORE INSERT
    ON public.m_rule_insured_age
    FOR EACH ROW
EXECUTE FUNCTION generate_rule_insured_age_code();
