INSERT INTO public.m_app(
	id, created_by, created_date, last_modified_by, last_modified_date, app_code, app_description, is_active)
	VALUES (1,'SYS', NOW(), 'SYS', NOW(), 'APP202007001', 'Niche Market Landing Page', true);

INSERT INTO public.m_gift(
	id, created_by, created_date, last_modified_by, last_modified_date, amount, gift_bonus_desc, gift_code, gift_redeem_desc, gift_type, is_active, point_gained, point_used, ref_gift_code)
	VALUES (1, 'SYS', NOW(), 'SYS', NOW(), 50000, '', 'GOPAY50K', 'gopay', 'redeem', true, 0, 5, '');

INSERT INTO public.m_gift(
	id, created_by, created_date, last_modified_by, last_modified_date, amount, gift_bonus_desc, gift_code, gift_redeem_desc, gift_type, is_active, point_gained, point_used, ref_gift_code)
	VALUES (2, 'SYS', NOW(), 'SYS', NOW(), 100000, '', 'GOPAY100K', 'gopay', 'redeem', true, 0, 10, '');
	
INSERT INTO public.m_gift(
	id, created_by, created_date, last_modified_by, last_modified_date, amount, gift_bonus_desc, gift_code, gift_redeem_desc, gift_type, is_active, point_gained, point_used, ref_gift_code)
	VALUES (3, 'SYS', NOW(), 'SYS', NOW(), 0, 'registration', 'REGPLAN1', '', 'gain', true, 1, 0, 'R2007001');

INSERT INTO public.m_gift(
	id, created_by, created_date, last_modified_by, last_modified_date, amount, gift_bonus_desc, gift_code, gift_redeem_desc, gift_type, is_active, point_gained, point_used, ref_gift_code)
	VALUES (4, 'SYS', NOW(), 'SYS', NOW(), 0, 'registration', 'REGPLAN2', '', 'gain', true, 2, 0, 'R2007002');

INSERT INTO public.m_gift(
	id, created_by, created_date, last_modified_by, last_modified_date, amount, gift_bonus_desc, gift_code, gift_redeem_desc, gift_type, is_active, point_gained, point_used, ref_gift_code)
	VALUES (5, 'SYS', NOW(), 'SYS', NOW(), 0, 'registration', 'REGPLAN3', '', 'gain', true, 3, 0, 'R2007003');

INSERT INTO public.m_gift(
	id, created_by, created_date, last_modified_by, last_modified_date, amount, gift_bonus_desc, gift_code, gift_redeem_desc, gift_type, is_active, point_gained, point_used, ref_gift_code)
	VALUES (6, 'SYS', NOW(), 'SYS', NOW(), 0, 'referencing', 'REF01', '', 'gain', true, 2, 0, '');
	
INSERT INTO public.m_gift(
	id, created_by, created_date, last_modified_by, last_modified_date, amount, gift_bonus_desc, gift_code, gift_redeem_desc, gift_type, is_active, point_gained, point_used, ref_gift_code)
	VALUES (7, 'SYS', NOW(), 'SYS', NOW(), 0, 'referenced', 'REF02', '', 'gain', true, 2, 0, '');
	
	
TRUNCATE TABLE m_gender_mgm;

INSERT INTO public.m_gender_mgm(							
	id, created_by, created_date, last_modified_by, last_modified_date, gender_code, gender_desc, is_active)	
VALUES (1, 'SYS', NOW(), 'SYS', NOW(), 'P', 'Pria', true);						    

INSERT INTO public.m_gender_mgm(							
	id, created_by, created_date, last_modified_by, last_modified_date, gender_code, gender_desc, is_active)						
	VALUES (2, 'SYS', NOW(), 'SYS', NOW(), 'W', 'Wanita', true);						
				
INSERT INTO public.m_product_policy(
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, policy_no, product_policy_code, product_policy_desc)
	VALUES (1, 'SYS', NOW(), 'SYS', NOW(), true, '', 'NCH20070001', 'Policy niche market');

TRUNCATE TABLE m_registration_type;
INSERT INTO public.m_registration_type(					
	id, created_by, created_date, last_modified_by, last_modified_date, bill_amount, is_active, registration_type_code, point, registration_type_desc, product_policy_code)				
	VALUES (1, 'SYS', NOW(), 'SYS', NOW(), 75000, True, 'R2007001', 1, 'Personal Plan', 'NCH20070001');		

INSERT INTO public.m_registration_type(					
	id, created_by, created_date, last_modified_by, last_modified_date, bill_amount, is_active, registration_type_code, point, registration_type_desc, product_policy_code)				
	VALUES (2, 'SYS', NOW(), 'SYS', NOW(), 140000, True, 'R2007002', 2, 'Spouse Plan', 'NCH20070001');						
					
INSERT INTO public.m_registration_type(					
	id, created_by, created_date, last_modified_by, last_modified_date, bill_amount, is_active, registration_type_code, point, registration_type_desc, product_policy_code)				
	VALUES (3, 'SYS', NOW(), 'SYS', NOW(), 275000, True, 'R2007003', 3, 'Family Plan', 'NCH20070001');