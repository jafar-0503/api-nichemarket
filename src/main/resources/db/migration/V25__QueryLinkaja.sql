--Set mapping core premi and bill linkaja
--Update bill Amount

ALTER TABLE m_registration_type ADD COLUMN premium NUMERIC(12,2);
ALTER TABLE m_registration_type ALTER COLUMN premium SET DEFAULT 0.00;

UPDATE m_registration_type SET premium = bill_amount;

UPDATE m_registration_type SET bill_amount = 15000 WHERE product_policy_code = 'NCH20100001';
UPDATE m_registration_type SET bill_amount =  32000 WHERE product_policy_code = 'NCH20100002';