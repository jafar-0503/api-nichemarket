INSERT INTO public.m_registration_type(					
	id, created_by, created_date, last_modified_by, last_modified_date, bill_amount, is_active, registration_type_code, point, registration_type_desc)				
	VALUES (1, 'SYS', NOW(), 'SYS', NOW(), 75000, True, 'R001', 1, 'Personal Plan');		

INSERT INTO public.m_registration_type(					
	id, created_by, created_date, last_modified_by, last_modified_date, bill_amount, is_active, registration_type_code, point, registration_type_desc)				
	VALUES (2, 'SYS', NOW(), 'SYS', NOW(), 150000, True, 'R002', 2, 'Spouse Plan');						
					
INSERT INTO public.m_registration_type(					
	id, created_by, created_date, last_modified_by, last_modified_date, bill_amount, is_active, registration_type_code, point, registration_type_desc)				
	VALUES (3, 'SYS', NOW(), 'SYS', NOW(), 150000, True, 'R003', 3, 'Family Plan');				

INSERT INTO public.m_relationship(							
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, relation_code, relationship_desc)						
	VALUES (1, 'SYS', NOW(), 'SYS', NOW(), true, '0', 'Diri Sendiri');						
							
INSERT INTO public.m_relationship(							
    id, created_by, created_date, last_modified_by, last_modified_date, is_active, relation_code, relationship_desc)							
    VALUES (2, 'SYS', NOW(), 'SYS', NOW(), true, '1', 'Istri');							
							
INSERT INTO public.m_relationship(							
id, created_by, created_date, last_modified_by, last_modified_date, is_active, relation_code, relationship_desc)							
VALUES (3, 'SYS', NOW(), 'SYS', NOW(), true, '2', 'Suami');							
							
INSERT INTO public.m_relationship(							
id, created_by, created_date, last_modified_by, last_modified_date, is_active, relation_code, relationship_desc)							
VALUES (4, 'SYS', NOW(), 'SYS', NOW(), true, '3', 'Anak');			


INSERT INTO public.m_gender_mgm(							
	id, created_by, created_date, last_modified_by, last_modified_date, gender_code, gender_desc, is_active)	
VALUES (1, 'SYS', NOW(), 'SYS', NOW(), 'M', 'Laki-laki', true);						    

INSERT INTO public.m_gender_mgm(							
	id, created_by, created_date, last_modified_by, last_modified_date, gender_code, gender_desc, is_active)						
	VALUES (2, 'SYS', NOW(), 'SYS', NOW(), 'F', 'Perempuan', true);						

INSERT INTO public.m_user_type(						
	id, created_by, created_date, last_modified_by, last_modified_date, code, description, is_active)					
	VALUES (1, 'SYS', NOW(), 'SYS', NOW(), 'C001', 'SiMedis Account', true);					
							