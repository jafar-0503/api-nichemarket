/*TRIGGER CUSTOMER CODE*/
CREATE OR REPLACE FUNCTION public.generate_customer_code()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
DECLARE new_customer varchar;
DECLARE check_customer varchar;
DECLARE check_exist_date varchar;

BEGIN
    SELECT INTO check_customer
        LPAD(cast(cast(substring(customer_code, 8) AS integer) + 1 AS varchar), 4, '0')
    FROM t_customer
    ORDER BY id
        DESC LIMIT 1;

    IF check_customer IS NULL THEN
        new.customer_code := concat('C', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    ELSE
        
        SELECT INTO check_exist_date
	    	CAST(SUBSTRING(customer_code, 2, 6) AS varchar)
	    FROM t_customer
	    ORDER BY id
	    DESC LIMIT 1;

	    CASE WHEN check_exist_date = CAST(to_char(now(), 'yymmdd') AS varchar) THEN
        	new.customer_code := concat('C', to_char(now(), 'yymmdd'), LPAD(check_customer, 4, '0'));

       		WHEN check_exist_date <> CAST (to_char(now(), 'yymmdd') AS varchar) THEN
       		new.customer_code := concat('C', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    	END CASE ;

    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS generate_customer_code
    ON public.t_customer;
CREATE TRIGGER generate_customer_code
    BEFORE INSERT
    ON public.t_customer
    FOR EACH ROW
EXECUTE FUNCTION generate_customer_code();


/*TRIGGER BILL CODE*/
CREATE OR REPLACE FUNCTION public.generate_bill_code()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
DECLARE new_bill varchar;
DECLARE check_bill varchar;
DECLARE check_exist_date varchar;

BEGIN
    SELECT INTO check_bill
        LPAD(cast(cast(substring(bill_code, 9) AS integer) + 1 AS varchar), 4, '0')
    FROM t_bill
    ORDER BY id
        DESC LIMIT 1;

    IF check_bill IS NULL THEN
        new.bill_code := concat('BI', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    ELSE
        
        SELECT INTO check_exist_date
	    	CAST(SUBSTRING(bill_code, 3, 6) AS varchar)
	    FROM t_bill
	    ORDER BY id
	    DESC LIMIT 1;

	    CASE WHEN check_exist_date = CAST(to_char(now(), 'yymmdd') AS varchar) THEN
        	new.bill_code := concat('BI', to_char(now(), 'yymmdd'), LPAD(check_bill, 4, '0'));

       		WHEN check_exist_date <> CAST (to_char(now(), 'yymmdd') AS varchar) THEN
       		new.bill_code := concat('BI', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    	END CASE ;
        
    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS generate_bill_code
    ON public.t_bill;
CREATE TRIGGER generate_bill_code
    BEFORE INSERT
    ON public.t_bill
    FOR EACH ROW
EXECUTE FUNCTION generate_bill_code();


/*TRIGGER REG CODE*/
CREATE OR REPLACE FUNCTION public.generate_registration_code()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
DECLARE new_registration varchar;
DECLARE check_registration varchar;
DECLARE check_exist_date varchar;

BEGIN
    SELECT INTO check_registration
        LPAD(cast(cast(substring(registration_code, 10) AS integer) + 1 AS varchar), 4, '0')
    FROM t_registration
    ORDER BY id
        DESC LIMIT 1;

    IF check_registration IS NULL THEN
        new.registration_code := concat('REG', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    ELSE
        SELECT INTO check_exist_date
	    	CAST(SUBSTRING(registration_code, 4, 6) AS varchar)
	    FROM t_registration
	    ORDER BY id
	    DESC LIMIT 1;

	    CASE WHEN check_exist_date = CAST(to_char(now(), 'yymmdd') AS varchar) THEN
        	new.registration_code := concat('REG', to_char(now(), 'yymmdd'), LPAD(check_registration, 4, '0'));

       		WHEN check_exist_date <> CAST (to_char(now(), 'yymmdd') AS varchar) THEN
       		new.registration_code := concat('REG', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    	END CASE ;
    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS generate_registration_code
    ON public.t_registration;
CREATE TRIGGER generate_registration_code
    BEFORE INSERT
    ON public.t_registration
    FOR EACH ROW
EXECUTE FUNCTION generate_registration_code();



/*TRIGGER PAYMENT CODE*/
CREATE OR REPLACE FUNCTION public.generate_payment_code()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
DECLARE new_payment varchar;
DECLARE check_payment varchar;
DECLARE check_exist_date varchar;

BEGIN
    SELECT INTO check_payment
        LPAD(cast(cast(substring(payment_code, 10) AS integer) + 1 AS varchar), 4, '0')
    FROM t_payment
    ORDER BY id
        DESC LIMIT 1;

    IF check_payment IS NULL THEN
        new.payment_code := concat('PAY', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    ELSE
        SELECT INTO check_exist_date
	    	CAST(SUBSTRING(payment_code, 4, 6) AS varchar)
	    FROM t_payment
	    ORDER BY id
	    DESC LIMIT 1;

	    CASE WHEN check_exist_date = CAST(to_char(now(), 'yymmdd') AS varchar) THEN
        	new.payment_code := concat('PAY', to_char(now(), 'yymmdd'), LPAD(check_payment, 4, '0'));

       		WHEN check_exist_date <> CAST (to_char(now(), 'yymmdd') AS varchar) THEN
       		new.payment_code := concat('PAY', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    	END CASE ;
    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS generate_payment_code
    ON public.t_payment;
CREATE TRIGGER generate_payment_code
    BEFORE INSERT
    ON public.t_payment
    FOR EACH ROW
EXECUTE FUNCTION generate_payment_code();



/*TRIGGER CUSTOMER POINT CODE*/
CREATE OR REPLACE FUNCTION public.generate_customer_point_code()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
DECLARE new_customer_point varchar;
DECLARE check_customer_point varchar;
DECLARE check_exist_date varchar;


BEGIN
    SELECT INTO check_customer_point
        LPAD(cast(cast(substring(customer_point_code, 9) AS integer) + 1 AS varchar), 4, '0')
    FROM t_customer_point
    ORDER BY id
        DESC LIMIT 1;

    IF check_customer_point IS NULL THEN
        new.customer_point_code := concat('CP', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    ELSE
        SELECT INTO check_exist_date
	    	CAST(SUBSTRING(customer_point_code, 3, 6) AS varchar)
	    FROM t_customer_point
	    ORDER BY id
	    DESC LIMIT 1;

	    CASE WHEN check_exist_date = CAST(to_char(now(), 'yymmdd') AS varchar) THEN
        	new.customer_point_code := concat('CP', to_char(now(), 'yymmdd'), LPAD(check_customer_point, 4, '0'));

       		WHEN check_exist_date <> CAST (to_char(now(), 'yymmdd') AS varchar) THEN
       		new.customer_point_code := concat('CP', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    	END CASE ;
    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS generate_customer_point_code
    ON public.t_customer_point;
CREATE TRIGGER generate_customer_point_code
    BEFORE INSERT
    ON public.t_customer_point
    FOR EACH ROW
EXECUTE FUNCTION generate_customer_point_code();


/*Continue V6*/
/*Trigger customer policy core code*/
CREATE OR REPLACE FUNCTION public.generate_customer_policy_core_code()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
DECLARE new_customer_policy_core varchar;
DECLARE check_customer_policy_core varchar;
DECLARE check_exist_date varchar;

BEGIN
    SELECT INTO check_customer_policy_core
        LPAD(cast(cast(substring(customer_policy_core_code, 7) AS integer) + 1 AS varchar), 4, '0')
    FROM t_customer_policy
    ORDER BY id
        DESC LIMIT 1;

    IF check_customer_policy_core IS NULL THEN
        new.customer_policy_core_code := concat('', to_char(now(), 'yymmdd') , LPAD('1', 4, '0'));
    ELSE
        SELECT INTO check_exist_date
	    	CAST(SUBSTRING(customer_policy_core_code, 1, 6) AS varchar)
	    FROM t_customer_policy
	    ORDER BY id
	    DESC LIMIT 1;

	    CASE WHEN check_exist_date = CAST(to_char(now(), 'yymmdd') AS varchar) THEN
        	new.customer_policy_core_code := concat('', to_char(now(), 'yymmdd'), LPAD(check_customer_policy_core, 4, '0'));

       		WHEN check_exist_date <> CAST (to_char(now(), 'yymmdd') AS varchar) THEN
       		new.customer_policy_core_code := concat('', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    	END CASE ;
    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS generate_customer_policy_core_code
    ON public.t_customer_policy;
CREATE TRIGGER generate_customer_policy_core_code
    BEFORE INSERT
    ON public.t_customer_policy
    FOR EACH ROW
EXECUTE FUNCTION generate_customer_policy_core_code();

