-- ============================================Insert m_app=========================
INSERT INTO public.m_app(
	id, created_by, created_date, last_modified_by, last_modified_date, app_code, app_description, is_active, app_action_name)
	VALUES (nextval('m_app_id_seq'), 'SYS', NOW(), 'SYS', NOW(), 'APP202010001', 'LinkAja Partner', true, 'registration');
	
-- =================================================INSERT m_product_policy===============================

INSERT INTO public.m_product_policy(
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, policy_no, product_policy_code, product_policy_desc, partner_code_core, email_code)
	VALUES (nextval('m_product_policy_id_seq'), 'SYS', NOW(), 'SYS', NOW(), true, '', 'NCH20100001', 'Policy niche market linkaja 1 month', 'linkaja_pro_1', 'E34');

INSERT INTO public.m_product_policy(
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, policy_no, product_policy_code, product_policy_desc, partner_code_core, email_code)
	VALUES (nextval('m_product_policy_id_seq'), 'SYS', NOW(), 'SYS', NOW(), true, '', 'NCH20100002', 'Policy niche market linkaja 3 month', 'linkaja_pro_2', 'E34');

-- ============================================Insert registration_code===========================
INSERT INTO public.m_registration_type(
	id, created_by, created_date, last_modified_by, last_modified_date, bill_amount, is_active, point, product_policy_code, registration_type_desc, registration_type_code, gift_code, max_age, min_age, max_up, max_customer, min_customer)
	VALUES (nextval('m_registration_type_id_seq'), 'SYS', NOW(), 'SYS', NOW(), 10500, True, 0, 'NCH20100001', 'LinkAja Covid One Month Plan', 'R2010001', null, 59, 17, 10000000, 1, 1);

INSERT INTO public.m_registration_type(
	id, created_by, created_date, last_modified_by, last_modified_date, bill_amount, is_active, point, product_policy_code, registration_type_desc, registration_type_code, gift_code, max_age, min_age, max_up, max_customer, min_customer)
	VALUES (nextval('m_registration_type_id_seq'), 'SYS', NOW(), 'SYS', NOW(), 26000, True, 0, 'NCH20100002', 'LinkAja Covid Three Month Plan', 'R2010002', null, 59, 17, 10000000, 1, 1);

-- ==============================================Insert m_rule_insured_age======================

INSERT INTO public.m_rule_insured_age(
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, max_age, min_age, registration_type_code, rule_insured_age_code, relationship_code)
	VALUES (nextval('m_rule_insured_age_id_seq'), 'SYS', NOW(), 'SYS', NOW(), True, 59, 17, 'R2010001', '', '0');

INSERT INTO public.m_rule_insured_age(
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, max_age, min_age, registration_type_code, rule_insured_age_code, relationship_code)
	VALUES (nextval('m_rule_insured_age_id_seq'), 'SYS', NOW(), 'SYS', NOW(), True, 59, 17, 'R2010002', '', '0');



-- ===============================================Inesrt m_product_plan_policy=============================
INSERT INTO public.m_product_plan_policy(
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, plan_code_core, product_policy_code, registration_type_code, type, class_core_code)
	VALUES (nextval('m_product_plan_policy_id_seq'), 'SYS', NOW(), 'SYS', NOW(), True, 'linkaja_1' , 'NCH20100001', 'R2010001', '1', 'I');
	
INSERT INTO public.m_product_plan_policy(
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, plan_code_core, product_policy_code, registration_type_code, type, class_core_code)
	VALUES (nextval('m_product_plan_policy_id_seq'), 'SYS', NOW(), 'SYS', NOW(), True, 'linkaja_2' , 'NCH20100002', 'R2010002', '1', 'I');



