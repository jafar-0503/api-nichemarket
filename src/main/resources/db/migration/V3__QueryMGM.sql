TRUNCATE TABLE m_registration_type;
TRUNCATE TABLE m_gender_mgm;

INSERT INTO public.m_registration_type(					
	id, created_by, created_date, last_modified_by, last_modified_date, bill_amount, is_active, registration_type_code, point, registration_type_desc)				
	VALUES (1, 'SYS', NOW(), 'SYS', NOW(), 75000, True, 'R001', 1, 'Personal Plan');		

INSERT INTO public.m_registration_type(					
	id, created_by, created_date, last_modified_by, last_modified_date, bill_amount, is_active, registration_type_code, point, registration_type_desc)				
	VALUES (2, 'SYS', NOW(), 'SYS', NOW(), 140000, True, 'R002', 2, 'Spouse Plan');						
					
INSERT INTO public.m_registration_type(					
	id, created_by, created_date, last_modified_by, last_modified_date, bill_amount, is_active, registration_type_code, point, registration_type_desc)				
	VALUES (3, 'SYS', NOW(), 'SYS', NOW(), 275000, True, 'R003', 3, 'Family Plan');				

INSERT INTO public.m_gender_mgm(							
	id, created_by, created_date, last_modified_by, last_modified_date, gender_code, gender_desc, is_active)	
VALUES (1, 'SYS', NOW(), 'SYS', NOW(), 'L', 'Laki-laki', true);						    

INSERT INTO public.m_gender_mgm(							
	id, created_by, created_date, last_modified_by, last_modified_date, gender_code, gender_desc, is_active)						
	VALUES (2, 'SYS', NOW(), 'SYS', NOW(), 'P', 'Perempuan', true);						
				
							