/*trigger Customer*/
/*Trigger Function*/
CREATE OR REPLACE FUNCTION public.generate_customer_code()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
DECLARE new_customer varchar;
DECLARE check_customer varchar;

BEGIN
    SELECT INTO check_customer
        LPAD(cast(cast(substring(customer_code, 8) AS integer) + 1 AS varchar), 4, '0')
    FROM t_customer
    ORDER BY id
        DESC LIMIT 1;

    IF check_customer IS NULL THEN
        new.customer_code := concat('C', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    ELSE
        new.customer_code := concat('C', to_char(now(), 'yymmdd' ), LPAD(check_customer, 4, '0'));
    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS generate_customer_code
    ON public.t_customer;
CREATE TRIGGER generate_customer_code
    BEFORE INSERT
    ON public.t_customer
    FOR EACH ROW
EXECUTE FUNCTION generate_customer_code();

/*trigger Answer*/
/*Trigger Function*/
CREATE OR REPLACE FUNCTION public.generate_answer_code()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
DECLARE new_answer varchar;
DECLARE check_answer varchar;

BEGIN
    SELECT INTO check_answer
        LPAD(cast(cast(substring(answer_code, 3) AS integer) + 1 AS varchar), 6, '0')
    FROM t_answer
    ORDER BY id
        DESC LIMIT 1;

    IF check_answer IS NULL THEN
        new.answer_code := concat('AN', LPAD('1', 6, '0'));
    ELSE
        new.answer_code := concat('AN', LPAD(check_answer, 6, '0'));
    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS generate_answer_code
    ON public.t_answer;
CREATE TRIGGER generate_answer_code
    BEFORE INSERT
    ON public.t_answer
    FOR EACH ROW
EXECUTE FUNCTION generate_answer_code();

/*trigger Log Message*/
/*Trigger Function*/
CREATE OR REPLACE FUNCTION public.generate_log_message_code()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
DECLARE new_log_message varchar;
DECLARE check_log_message varchar;

BEGIN
    SELECT INTO check_log_message
        LPAD(cast(cast(substring(log_message_code, 3) AS integer) + 1 AS varchar), 6, '0')
    FROM t_log_message
    ORDER BY id
        DESC LIMIT 1;

    IF check_log_message IS NULL THEN
        new.log_message_code := concat('LM', LPAD('1', 6, '0'));
    ELSE
        new.log_message_code := concat('LM', LPAD(check_log_message, 6, '0'));
    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS generate_log_message_code
    ON public.t_log_message;
CREATE TRIGGER generate_log_message_code
    BEFORE INSERT
    ON public.t_log_message
    FOR EACH ROW
EXECUTE FUNCTION generate_log_message_code();

/*trigger Customer Policy*/
/*Trigger Function*/
CREATE OR REPLACE FUNCTION public.generate_customer_policy_code()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
    DECLARE check_customer_policy varchar;
   check_exist_date varchar;

BEGIN
    SELECT INTO check_customer_policy
        LPAD(cast(cast(substring(customer_policy_code, 9) AS integer) + 1 AS varchar), 4, '0')
    FROM t_customer_policy
    ORDER BY id
        DESC LIMIT 1;


    IF check_customer_policy IS NULL THEN
        new.customer_policy_code := concat('CP', to_char(now(), 'yymmdd'), LPAD('1', 4, '0'));

    ELSE

	    SELECT INTO check_exist_date
	    	CAST(SUBSTRING(customer_policy_code, 3, 6) AS varchar)
	    FROM t_customer_policy
	    ORDER BY id
	    DESC LIMIT 1;

   		CASE WHEN check_exist_date = CAST(to_char(now(), 'yymmdd') AS varchar) THEN
        	new.customer_policy_code := concat('CP', to_char(now(), 'yymmdd'), LPAD(check_customer_policy, 4, '0'));

       		WHEN check_exist_date <> CAST (to_char(now(), 'yymmdd') AS varchar) THEN
       		new.customer_policy_code := concat('CP', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    	END CASE ;
    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS generate_customer_policy_code
  ON public.t_customer_policy;
CREATE TRIGGER generate_customer_policy_code
    BEFORE INSERT
    ON public.t_customer_policy
    FOR EACH ROW
EXECUTE FUNCTION generate_customer_policy_code();


/*trigger Customer Reference*/
/*Trigger Function*/
CREATE OR REPLACE FUNCTION public.generate_customer_reference_code()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
    DECLARE check_customer_reference varchar;
   check_exist_date varchar;

BEGIN
    SELECT INTO check_customer_reference
        LPAD(cast(cast(substring(customer_reference_code, 9) AS integer) + 1 AS varchar), 4, '0')
    FROM t_customer_reference
    ORDER BY id
        DESC LIMIT 1;


    IF check_customer_reference IS NULL THEN
        new.customer_reference_code := concat('CR', to_char(now(), 'yymmdd'), LPAD('1', 4, '0'));

    ELSE

	    SELECT INTO check_exist_date
	    	CAST(SUBSTRING(customer_reference_code, 3, 6) AS varchar)
	    FROM t_customer_reference
	    ORDER BY id
	    DESC LIMIT 1;

   		CASE WHEN check_exist_date = CAST(to_char(now(), 'yymmdd') AS varchar) THEN
        	new.customer_reference_code := concat('CR', to_char(now(), 'yymmdd'), LPAD(check_customer_reference, 4, '0'));

       		WHEN check_exist_date <> CAST (to_char(now(), 'yymmdd') AS varchar) THEN
       		new.customer_reference_code := concat('CR', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    	END CASE ;
    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS generate_customer_reference_code
  ON public.t_customer_reference;
CREATE TRIGGER generate_customer_reference_code
    BEFORE INSERT
    ON public.t_customer_reference
    FOR EACH ROW
EXECUTE FUNCTION generate_customer_reference_code();


/*trigger Request History*/
/*Trigger Function*/
CREATE OR REPLACE FUNCTION public.generate_request_history_code()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
    DECLARE check_request_history varchar;
   check_exist_date varchar;

BEGIN
    SELECT INTO check_request_history
        LPAD(cast(cast(substring(request_history_code, 9) AS integer) + 1 AS varchar), 4, '0')
    FROM t_request_history
    ORDER BY id
        DESC LIMIT 1;


    IF check_request_history IS NULL THEN
        new.request_history_code := concat('RH', to_char(now(), 'yymmdd'), LPAD('1', 4, '0'));

    ELSE

	    SELECT INTO check_exist_date
	    	CAST(SUBSTRING(request_history_code, 3, 6) AS varchar)
	    FROM t_request_history
	    ORDER BY id
	    DESC LIMIT 1;

   		CASE WHEN check_exist_date = CAST(to_char(now(), 'yymmdd') AS varchar) THEN
        	new.request_history_code := concat('RH', to_char(now(), 'yymmdd'), LPAD(check_request_history, 4, '0'));

       		WHEN check_exist_date <> CAST (to_char(now(), 'yymmdd') AS varchar) THEN
       		new.request_history_code := concat('RH', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    	END CASE ;
    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS generate_request_history_code
  ON public.t_request_history;
CREATE TRIGGER generate_request_history_code
    BEFORE INSERT
    ON public.t_request_history
    FOR EACH ROW
EXECUTE FUNCTION generate_request_history_code();


/*trigger Interactive*/
/*Trigger Function*/
CREATE OR REPLACE FUNCTION public.generate_interactive_code()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
    DECLARE check_interactive varchar;
   check_exist_date varchar;

BEGIN
    SELECT INTO check_interactive
        LPAD(cast(cast(substring(interactive_code, 8) AS integer) + 1 AS varchar), 4, '0')
    FROM t_interactive
    ORDER BY id
        DESC LIMIT 1;


    IF check_interactive IS NULL THEN
        new.interactive_code := concat('I', to_char(now(), 'yymmdd'), LPAD('1', 4, '0'));

    ELSE

	    SELECT INTO check_exist_date
	    	CAST(SUBSTRING(interactive_code, 2, 6) AS varchar)
	    FROM t_interactive
	    ORDER BY id
	    DESC LIMIT 1;

   		CASE WHEN check_exist_date = CAST(to_char(now(), 'yymmdd') AS varchar) THEN
        	new.interactive_code := concat('I', to_char(now(), 'yymmdd'), LPAD(check_interactive, 4, '0'));

       		WHEN check_exist_date <> CAST (to_char(now(), 'yymmdd') AS varchar) THEN
       		new.interactive_code := concat('I', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    	END CASE ;
    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS generate_interactive_code
  ON public.t_interactive;
CREATE TRIGGER generate_interactive_code
    BEFORE INSERT
    ON public.t_interactive
    FOR EACH ROW
EXECUTE FUNCTION generate_interactive_code();


/*trigger spaj*/
/*Trigger Function*/
CREATE OR REPLACE FUNCTION public.generate_interactive_priority_code()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
    DECLARE check_interactive_priority varchar;
   check_exist_date varchar;

BEGIN
    SELECT INTO check_interactive_priority
        LPAD(cast(cast(substring(interactive_priority_code, 9) AS integer) + 1 AS varchar), 4, '0')
    FROM t_interactive_priority
    ORDER BY id
        DESC LIMIT 1;


    IF check_interactive_priority IS NULL THEN
        new.interactive_priority_code := concat('IP', to_char(now(), 'yymmdd'), LPAD('1', 4, '0'));

    ELSE

	    SELECT INTO check_exist_date
	    	CAST(SUBSTRING(interactive_priority_code, 3, 6) AS varchar)
	    FROM t_interactive_priority
	    ORDER BY id
	    DESC LIMIT 1;

   		CASE WHEN check_exist_date = CAST(to_char(now(), 'yymmdd') AS varchar) THEN
        	new.interactive_priority_code := concat('IP', to_char(now(), 'yymmdd'), LPAD(check_interactive_priority, 4, '0'));

       		WHEN check_exist_date <> CAST (to_char(now(), 'yymmdd') AS varchar) THEN
       		new.interactive_priority_code := concat('IP', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    	END CASE ;
    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS generate_interactive_priority_code
  ON public.t_interactive_priority;
CREATE TRIGGER generate_interactive_priority_code
    BEFORE INSERT
    ON public.t_interactive_priority
    FOR EACH ROW
EXECUTE FUNCTION generate_interactive_priority_code();

/*trigger t_interactive_needs*/
/*Trigger Function*/
CREATE OR REPLACE FUNCTION public.generate_interactive_needs_code()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
    DECLARE check_interactive_needs varchar;
   check_exist_date varchar;

BEGIN
    SELECT INTO check_interactive_needs
        LPAD(cast(cast(substring(interactive_needs_code, 9) AS integer) + 1 AS varchar), 4, '0')
    FROM t_interactive_needs
    ORDER BY id
        DESC LIMIT 1;


    IF check_interactive_needs IS NULL THEN
        new.interactive_needs_code := concat('IN', to_char(now(), 'yymmdd'), LPAD('1', 4, '0'));

    ELSE

	    SELECT INTO check_exist_date
	    	CAST(SUBSTRING(interactive_needs_code, 3, 6) AS varchar)
	    FROM t_interactive_needs
	    ORDER BY id
	    DESC LIMIT 1;

   		CASE WHEN check_exist_date = CAST(to_char(now(), 'yymmdd') AS varchar) THEN
        	new.interactive_needs_code := concat('IN', to_char(now(), 'yymmdd'), LPAD(check_interactive_needs, 4, '0'));

       		WHEN check_exist_date <> CAST (to_char(now(), 'yymmdd') AS varchar) THEN
       		new.interactive_needs_code := concat('IN', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    	END CASE ;
    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS generate_interactive_needs_code
  ON public.t_interactive_needs;
CREATE TRIGGER generate_interactive_needs_code
    BEFORE INSERT
    ON public.t_interactive_needs
    FOR EACH ROW
EXECUTE FUNCTION generate_interactive_needs_code();


/*trigger t_customer_message*/
/*Trigger Function*/
CREATE OR REPLACE FUNCTION public.generate_customer_message_code()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
    DECLARE check_customer_message varchar;
   check_exist_date varchar;

BEGIN
    SELECT INTO check_customer_message
        LPAD(cast(cast(substring(customer_message_code, 9) AS integer) + 1 AS varchar), 4, '0')
    FROM t_customer_message
    ORDER BY id
        DESC LIMIT 1;


    IF check_customer_message IS NULL THEN
        new.customer_message_code := concat('CM', to_char(now(), 'yymmdd'), LPAD('1', 4, '0'));

    ELSE

	    SELECT INTO check_exist_date
	    	CAST(SUBSTRING(customer_message_code, 3, 6) AS varchar)
	    FROM t_customer_message
	    ORDER BY id
	    DESC LIMIT 1;

   		CASE WHEN check_exist_date = CAST(to_char(now(), 'yymmdd') AS varchar) THEN
        	new.customer_message_code := concat('CM', to_char(now(), 'yymmdd'), LPAD(check_customer_message, 4, '0'));

       		WHEN check_exist_date <> CAST (to_char(now(), 'yymmdd') AS varchar) THEN
       		new.customer_message_code := concat('CM', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    	END CASE ;
    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS generate_customer_message_code
  ON public.t_customer_message;
CREATE TRIGGER generate_customer_message_code
    BEFORE INSERT
    ON public.t_customer_message
    FOR EACH ROW
EXECUTE FUNCTION generate_customer_message_code();


/*trigger m_recipient_received_msg*/
/*Trigger Function*/
CREATE OR REPLACE FUNCTION public.generate_recipient_received_msg_code()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
    DECLARE check_recipient_received_msg varchar;
   check_exist_date varchar;

BEGIN
    SELECT INTO check_recipient_received_msg
        LPAD(cast(cast(substring(recipient_received_msg_code, 9) AS integer) + 1 AS varchar), 4, '0')
    FROM m_recipient_received_msg
    ORDER BY id
        DESC LIMIT 1;


    IF check_recipient_received_msg IS NULL THEN
        new.recipient_received_msg_code := concat('RR', to_char(now(), 'yymmdd'), LPAD('1', 4, '0'));

    ELSE

	    SELECT INTO check_exist_date
	    	CAST(SUBSTRING(recipient_received_msg_code, 3, 6) AS varchar)
	    FROM m_recipient_received_msg
	    ORDER BY id
	    DESC LIMIT 1;

   		CASE WHEN check_exist_date = CAST(to_char(now(), 'yymmdd') AS varchar) THEN
        	new.recipient_received_msg_code := concat('RR', to_char(now(), 'yymmdd'), LPAD(check_recipient_received_msg, 4, '0'));

       		WHEN check_exist_date <> CAST (to_char(now(), 'yymmdd') AS varchar) THEN
       		new.recipient_received_msg_code := concat('RR', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    	END CASE ;
    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS generate_recipient_received_msg_code
  ON public.m_recipient_received_msg;
CREATE TRIGGER generate_recipient_received_msg_code
    BEFORE INSERT
    ON public.m_recipient_received_msg
    FOR EACH ROW
EXECUTE FUNCTION generate_recipient_received_msg_code();



CREATE OR REPLACE FUNCTION public.generate_bill_code()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
DECLARE new_bill varchar;
DECLARE check_bill varchar;

BEGIN
    SELECT INTO check_bill
        LPAD(cast(cast(substring(bill_code, 9) AS integer) + 1 AS varchar), 4, '0')
    FROM t_bill
    ORDER BY id
        DESC LIMIT 1;

    IF check_bill IS NULL THEN
        new.bill_code := concat('BI', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    ELSE
        new.bill_code := concat('BI', to_char(now(), 'yymmdd' ), LPAD(check_bill, 4, '0'));
    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS generate_bill_code
    ON public.t_bill;
CREATE TRIGGER generate_bill_code
    BEFORE INSERT
    ON public.t_bill
    FOR EACH ROW
EXECUTE FUNCTION generate_bill_code();


CREATE OR REPLACE FUNCTION public.generate_registration_code()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
DECLARE new_registration varchar;
DECLARE check_registration varchar;

BEGIN
    SELECT INTO check_registration
        LPAD(cast(cast(substring(registration_code, 10) AS integer) + 1 AS varchar), 4, '0')
    FROM t_registration
    ORDER BY id
        DESC LIMIT 1;

    IF check_registration IS NULL THEN
        new.registration_code := concat('REG', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    ELSE
        new.registration_code := concat('REG', to_char(now(), 'yymmdd' ), LPAD(check_registration, 4, '0'));
    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS generate_registration_code
    ON public.t_registration;
CREATE TRIGGER generate_registration_code
    BEFORE INSERT
    ON public.t_registration
    FOR EACH ROW
EXECUTE FUNCTION generate_registration_code();

CREATE OR REPLACE FUNCTION public.generate_payment_code()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
DECLARE new_payment varchar;
DECLARE check_payment varchar;

BEGIN
    SELECT INTO check_payment
        LPAD(cast(cast(substring(payment_code, 10) AS integer) + 1 AS varchar), 4, '0')
    FROM t_payment
    ORDER BY id
        DESC LIMIT 1;

    IF check_payment IS NULL THEN
        new.payment_code := concat('PAY', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    ELSE
        new.payment_code := concat('PAY', to_char(now(), 'yymmdd' ), LPAD(check_payment, 4, '0'));
    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS generate_payment_code
    ON public.t_payment;
CREATE TRIGGER generate_payment_code
    BEFORE INSERT
    ON public.t_payment
    FOR EACH ROW
EXECUTE FUNCTION generate_payment_code();
