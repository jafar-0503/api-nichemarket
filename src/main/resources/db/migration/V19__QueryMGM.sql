UPDATE m_rule_insured_age
SET min_age = 17 WHERE registration_type_code IN ('R2007001','R2007002','R2007003') AND relationship_code <> '3';

ALTER TABLE m_account 
  DROP CONSTRAINT IF EXISTS uk_a3twtxben7gyjhi65q1eivw0a;