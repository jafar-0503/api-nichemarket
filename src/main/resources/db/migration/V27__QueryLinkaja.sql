--Drop Unique Constraint on registration_type_desc
ALTER TABLE IF EXISTS m_registration_type 
  DROP CONSTRAINT IF EXISTS UK_jdtpyfft29jfvxtn7ttyviyqx;

-- Update registration_type_desc for product name
UPDATE m_registration_type SET registration_type_desc = 'ELI COVID Protection' WHERE registration_type_code = 'R2010001';
UPDATE m_registration_type SET registration_type_desc = 'ELI COVID Protection' WHERE registration_type_code = 'R2010002';
