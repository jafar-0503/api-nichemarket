TRUNCATE TABLE m_product_policy;


INSERT INTO public.m_product_policy(
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, policy_no, product_policy_code, product_policy_desc, partner_code_core)
	VALUES (1, 'SYS', NOW(), 'SYS', NOW(), true, '', 'NCH20070001', 'Policy niche market', 'siMedis');

TRUNCATE TABLE m_registration_type;
INSERT INTO public.m_registration_type(					
	id, created_by, created_date, last_modified_by, last_modified_date, bill_amount, is_active, registration_type_code, point, registration_type_desc, product_policy_code, gift_code)				
	VALUES (1, 'SYS', NOW(), 'SYS', NOW(), 75000, True, 'R2007001', 1, 'Personal Plan', 'NCH20070001', 'REGPLAN1');		

INSERT INTO public.m_registration_type(					
	id, created_by, created_date, last_modified_by, last_modified_date, bill_amount, is_active, registration_type_code, point, registration_type_desc, product_policy_code, gift_code)				
	VALUES (2, 'SYS', NOW(), 'SYS', NOW(), 140000, True, 'R2007002', 2, 'Spouse Plan', 'NCH20070001', 'REGPLAN2');						
					
INSERT INTO public.m_registration_type(					
	id, created_by, created_date, last_modified_by, last_modified_date, bill_amount, is_active, registration_type_code, point, registration_type_desc, product_policy_code, gift_code)				
	VALUES (3, 'SYS', NOW(), 'SYS', NOW(), 275000, True, 'R2007003', 3, 'Family Plan', 'NCH20070001', 'REGPLAN3');

TRUNCATE TABLE m_relationship;
INSERT INTO public.m_relationship(							
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, relationship_code, relationship_desc, relationship_core_code)						
	VALUES (1, 'SYS', NOW(), 'SYS', NOW(), true, '0', 'Diri Sendiri', '0');						
							
INSERT INTO public.m_relationship(							
    id, created_by, created_date, last_modified_by, last_modified_date, is_active, relationship_code, relationship_desc, relationship_core_code)							
    VALUES (2, 'SYS', NOW(), 'SYS', NOW(), true, '1', 'Istri', '1');							
							
INSERT INTO public.m_relationship(							
id, created_by, created_date, last_modified_by, last_modified_date, is_active, relationship_code, relationship_desc, relationship_core_code)							
VALUES (3, 'SYS', NOW(), 'SYS', NOW(), true, '2', 'Suami' ,'1');							
							
INSERT INTO public.m_relationship(							
id, created_by, created_date, last_modified_by, last_modified_date, is_active, relationship_code, relationship_desc, relationship_core_code)							
VALUES (4, 'SYS', NOW(), 'SYS', NOW(), true, '3', 'Anak' ,'2');	