ALTER TABLE t_payment ALTER COLUMN payment_ref_code TYPE varchar(60);

INSERT INTO public.m_rule_insured_age(
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, max_age, min_age, registration_type_code, relationship_code, rule_insured_age_code)
	VALUES (1, 'SYS', NOW(), 'SYS', NOW(), true, 59, 1, 'R2007001', '0', '');
	
INSERT INTO public.m_rule_insured_age(
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, max_age, min_age, registration_type_code, relationship_code, rule_insured_age_code)
	VALUES (2, 'SYS', NOW(), 'SYS', NOW(), true, 59, 1, 'R2007002', '0', '');
	
INSERT INTO public.m_rule_insured_age(
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, max_age, min_age, registration_type_code, relationship_code, rule_insured_age_code)
	VALUES (3, 'SYS', NOW(), 'SYS', NOW(), true, 59, 1, 'R2007002', '1', '');

INSERT INTO public.m_rule_insured_age(
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, max_age, min_age, registration_type_code, relationship_code, rule_insured_age_code)
	VALUES (4, 'SYS', NOW(), 'SYS', NOW(), true, 59, 1, 'R2007002', '2', '');
	
INSERT INTO public.m_rule_insured_age(
id, created_by, created_date, last_modified_by, last_modified_date, is_active, max_age, min_age, registration_type_code, relationship_code, rule_insured_age_code)
VALUES (5, 'SYS', NOW(), 'SYS', NOW(), true, 59, 1, 'R2007003', '0', '');
	
INSERT INTO public.m_rule_insured_age(
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, max_age, min_age, registration_type_code, relationship_code, rule_insured_age_code)
	VALUES (6, 'SYS', NOW(), 'SYS', NOW(), true, 59, 1, 'R2007003', '1', '');

INSERT INTO public.m_rule_insured_age(
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, max_age, min_age, registration_type_code, relationship_code, rule_insured_age_code)
	VALUES (7, 'SYS', NOW(), 'SYS', NOW(), true, 59, 1, 'R2007003', '2', '');

INSERT INTO public.m_rule_insured_age(
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, max_age, min_age, registration_type_code, relationship_code, rule_insured_age_code)
	VALUES (8, 'SYS', NOW(), 'SYS', NOW(), true, 17, 1, 'R2007003', '3', '');