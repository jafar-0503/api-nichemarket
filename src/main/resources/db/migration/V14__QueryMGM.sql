UPDATE public.m_registration_type
	SET min_customer = 1, max_customer = 1
	WHERE registration_type_code = 'R2007001';

UPDATE public.m_registration_type
	SET min_customer = 2, max_customer = 2
	WHERE registration_type_code = 'R2007002';

UPDATE public.m_registration_type
	SET min_customer = 3, max_customer = 5
	WHERE registration_type_code = 'R2007003';
    
    