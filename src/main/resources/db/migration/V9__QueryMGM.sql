UPDATE public.m_app
	SET app_action_name = 'simedis2'
	WHERE id = 1;

UPDATE public.m_product_policy
	SET email_code='E27'
	WHERE id = 1;

TRUNCATE TABLE m_product_plan_policy;
INSERT INTO public.m_product_plan_policy(
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, plan_code_core, product_policy_code, registration_type_code, type)
	VALUES (1, 'SYS', NOW(), 'SYS', NOW(), true, 'SiMedis_1', 'NCH20070001', 'R2007001', 1);

INSERT INTO public.m_product_plan_policy(
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, plan_code_core, product_policy_code, registration_type_code, type)
	VALUES (2, 'SYS', NOW(), 'SYS', NOW(), true, 'SiMedis_2', 'NCH20070001', 'R2007002', 1);

INSERT INTO public.m_product_plan_policy(
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, plan_code_core, product_policy_code, registration_type_code, type)
	VALUES (3, 'SYS', NOW(), 'SYS', NOW(), true, 'SiMedis_4', 'NCH20070001', 'R2007002', 2);

INSERT INTO public.m_product_plan_policy(
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, plan_code_core, product_policy_code, registration_type_code, type)
	VALUES (4, 'SYS', NOW(), 'SYS', NOW(), true, 'SiMedis_3', 'NCH20070001', 'R2007003', 1);

INSERT INTO public.m_product_plan_policy(
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, plan_code_core, product_policy_code, registration_type_code, type)
	VALUES (5, 'SYS', NOW(), 'SYS', NOW(), true, 'SiMedis_4', 'NCH20070001', 'R2007003', 2); 