CREATE OR REPLACE FUNCTION public.generate_customer_point_code()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
DECLARE new_customer_point varchar;
DECLARE check_customer_point varchar;

BEGIN
    SELECT INTO check_customer_point
        LPAD(cast(cast(substring(customer_point_code, 9) AS integer) + 1 AS varchar), 4, '0')
    FROM t_customer_point
    ORDER BY id
        DESC LIMIT 1;

    IF check_customer_point IS NULL THEN
        new.customer_point_code := concat('CP', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    ELSE
        new.customer_point_code := concat('CP', to_char(now(), 'yymmdd' ), LPAD(check_customer_point, 4, '0'));
    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS generate_customer_point_code
    ON public.t_customer_point;
CREATE TRIGGER generate_customer_point_code
    BEFORE INSERT
    ON public.t_customer_point
    FOR EACH ROW
EXECUTE FUNCTION generate_customer_point_code();




CREATE OR REPLACE FUNCTION public.calculate_current_point()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
DECLARE new_current_point integer;
DECLARE lastest_current_point integer;

BEGIN
    SELECT INTO lastest_current_point
        current_point
    FROM t_customer_point
    WHERE customer_code = new.customer_code
    ORDER BY id
        DESC LIMIT 1;

    IF lastest_current_point IS NULL THEN
        new.current_point := new.point_in;
    ELSE
        new.current_point := lastest_current_point + (new.point_in - new.point_out);
    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS calculate_current_point
    ON public.t_customer_point;
CREATE TRIGGER calculate_current_point
    BEFORE INSERT
    ON public.t_customer_point
    FOR EACH ROW
EXECUTE FUNCTION calculate_current_point();

