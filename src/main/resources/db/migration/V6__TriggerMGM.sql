CREATE OR REPLACE FUNCTION public.generate_customer_policy_core_code()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
DECLARE new_customer_policy_core varchar;
DECLARE check_customer_policy_core varchar;

BEGIN
    SELECT INTO check_customer_policy_core
        LPAD(cast(cast(substring(customer_policy_core_code, 7) AS integer) + 1 AS varchar), 4, '0')
    FROM t_customer_policy
    ORDER BY id
        DESC LIMIT 1;

    IF check_customer_policy_core IS NULL THEN
        new.customer_policy_core_code := concat('', to_char(now(), 'yymmdd') , LPAD('1', 4, '0'));
    ELSE
        new.customer_policy_core_code := concat('', to_char(now(), 'yymmdd') , LPAD(check_customer_policy_core, 4, '0'));
    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS generate_customer_policy_core_code
    ON public.t_customer_policy;
CREATE TRIGGER generate_customer_policy_core_code
    BEFORE INSERT
    ON public.t_customer_policy
    FOR EACH ROW
EXECUTE FUNCTION generate_customer_policy_core_code();