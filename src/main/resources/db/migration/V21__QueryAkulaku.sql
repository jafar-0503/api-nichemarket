-- ============================================Insert m_app=========================
SELECT setval('m_app_id_seq', 1, true);
INSERT INTO public.m_app(
	id, created_by, created_date, last_modified_by, last_modified_date, app_code, app_description, is_active, app_action_name)
	VALUES (nextval('m_app_id_seq'), 'SYS', NOW(), 'SYS', NOW(), 'APP202009001', 'Akulaku Partner', true, 'akulaku');
	
-- =================================================Insert m_product_policy===============================
SELECT setval('m_product_policy_id_seq', 1, true);

INSERT INTO public.m_product_policy(
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, policy_no, product_policy_code, product_policy_desc, partner_code_core, email_code)
	VALUES (nextval('m_product_policy_id_seq'), 'SYS', NOW(), 'SYS', NOW(), true, '', 'NCH20090001', 'Policy niche market akulaku', 'akulaku', '');
	
-- ============================================Insert registration_code===========================
SELECT setval('m_registration_type_id_seq', 3, true);

INSERT INTO public.m_registration_type(
	id, created_by, created_date, last_modified_by, last_modified_date, bill_amount, is_active, point, product_policy_code, registration_type_desc, registration_type_code, gift_code, max_age, min_age, max_up, max_customer, min_customer)
	VALUES (nextval('m_registration_type_id_seq'), 'SYS', NOW(), 'SYS', NOW(), 15000, True, 0, 'NCH20090001', 'One Month Plan', 'R2009001', null, 59, 1, 10000000, 1, 1);

INSERT INTO public.m_registration_type(
	id, created_by, created_date, last_modified_by, last_modified_date, bill_amount, is_active, point, product_policy_code, registration_type_desc, registration_type_code, gift_code, max_age, min_age, max_up, max_customer, min_customer)
	VALUES (nextval('m_registration_type_id_seq'), 'SYS', NOW(), 'SYS', NOW(), 29950, True, 0, 'NCH20090001', 'Two Month Plan', 'R2009002', null, 59, 1, 10000000, 1, 1);

INSERT INTO public.m_registration_type(
	id, created_by, created_date, last_modified_by, last_modified_date, bill_amount, is_active, point, product_policy_code, registration_type_desc, registration_type_code, gift_code, max_age, min_age, max_up, max_customer, min_customer)
	VALUES (nextval('m_registration_type_id_seq'), 'SYS', NOW(), 'SYS', NOW(), 35300, True, 0, 'NCH20090001', 'Three Month Plan', 'R2009003', null, 59, 1, 10000000, 1, 1);

-- ==============================================Insert m_rule_insured_age======================
SELECT setval('m_rule_insured_age_id_seq', 8, true);

INSERT INTO public.m_rule_insured_age(
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, max_age, min_age, registration_type_code, rule_insured_age_code, relationship_code)
	VALUES (nextval('m_rule_insured_age_id_seq'), 'SYS', NOW(), 'SYS', NOW(), True, 59, 1, 'R2009001', '', '0');

INSERT INTO public.m_rule_insured_age(
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, max_age, min_age, registration_type_code, rule_insured_age_code, relationship_code)
	VALUES (nextval('m_rule_insured_age_id_seq'), 'SYS', NOW(), 'SYS', NOW(), True, 59, 1, 'R2009002', '', '0');

INSERT INTO public.m_rule_insured_age(
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, max_age, min_age, registration_type_code, rule_insured_age_code, relationship_code)
	VALUES (nextval('m_rule_insured_age_id_seq'), 'SYS', NOW(), 'SYS', NOW(), True, 59, 1, 'R2009003', '', '0');


-- ===============================================Inesrt m_product_plan_policy=============================

SELECT setval('m_product_plan_policy_id_seq', 5, true);

INSERT INTO public.m_product_plan_policy(
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, plan_code_core, product_policy_code, registration_type_code, type, class_core_code)
	VALUES (nextval('m_product_plan_policy_id_seq'), 'SYS', NOW(), 'SYS', NOW(), True, 'akulaku_1' , 'NCH20090001', 'R2009001', '1', 'I');
	
INSERT INTO public.m_product_plan_policy(
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, plan_code_core, product_policy_code, registration_type_code, type, class_core_code)
	VALUES (nextval('m_product_plan_policy_id_seq'), 'SYS', NOW(), 'SYS', NOW(), True, 'akulaku_2' , 'NCH20090001', 'R2009002', '1', 'I');
	
INSERT INTO public.m_product_plan_policy(
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, plan_code_core, product_policy_code, registration_type_code, type, class_core_code)
	VALUES (nextval('m_product_plan_policy_id_seq'), 'SYS', NOW(), 'SYS', NOW(), True, 'akulaku_3' , 'NCH20090001', 'R2009003', '1', 'I');

