ALTER TABLE t_registration
DROP COLUMN IF EXISTS unique_activation_key,
DROP COLUMN IF EXISTS is_account_activated;

ALTER TABLE t_customer_point
DROP COLUMN IF EXISTS customer_code;

ALTER TABLE t_customer_referral
DROP COLUMN IF EXISTS customer_code;

