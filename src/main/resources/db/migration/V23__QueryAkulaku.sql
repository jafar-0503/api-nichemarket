-- date created : 2020-10-15
-- purpose : adjust from product setting
-- =================================================Update m_product_policy===============================

UPDATE m_product_policy
SET product_policy_desc = 'Policy niche market akulaku 1 Month' , partner_code_core = 'akulaku_pro_1'
WHERE product_policy_code = 'NCH20090001';


-- =================================================INSERT m_product_policy===============================

INSERT INTO public.m_product_policy(
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, policy_no, product_policy_code, product_policy_desc, partner_code_core, email_code)
	VALUES (nextval('m_product_policy_id_seq'), 'SYS', NOW(), 'SYS', NOW(), true, '', 'NCH20090002', 'Policy niche market akulaku 2 month', 'akulaku_pro_2', 'E32');

INSERT INTO public.m_product_policy(
	id, created_by, created_date, last_modified_by, last_modified_date, is_active, policy_no, product_policy_code, product_policy_desc, partner_code_core, email_code)
	VALUES (nextval('m_product_policy_id_seq'), 'SYS', NOW(), 'SYS', NOW(), true, '', 'NCH20090003', 'Policy niche market akulaku 3 month', 'akulaku_pro_3', 'E32');

-- =================================================Update m_registration_type===============================

UPDATE m_registration_type
SET product_policy_code = 'NCH20090002'
WHERE registration_type_code = 'R2009002';

UPDATE m_registration_type
SET product_policy_code = 'NCH20090003'
WHERE registration_type_code = 'R2009003';

-- =================================================Update m_product_plan_policy==============================

UPDATE m_product_plan_policy
SET product_policy_code = 'NCH20090002'
WHERE plan_code_core = 'akulaku_2' AND registration_type_code = 'R2009002';

UPDATE m_product_plan_policy
SET product_policy_code = 'NCH20090003'
WHERE plan_code_core = 'akulaku_3' AND registration_type_code = 'R2009003';
