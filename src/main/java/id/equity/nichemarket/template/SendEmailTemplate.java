package id.equity.nichemarket.template;

import id.equity.nichemarket.dto.mgm.customerMessage.CreateCustomerMessage;
import id.equity.nichemarket.dto.mgm.dataFromLandingPage.DataFromLandingPage2;
import id.equity.nichemarket.dto.mgm.recipientReceivedMsg.RecipientReceivedMsgDto;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class SendEmailTemplate {

    //For Template E28
    public DataFromLandingPage2 convertToLandingPage2(CreateCustomerMessage customerMessageData,
                                                      RecipientReceivedMsgDto recipientData,
                                                      String messageType) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        DateTimeFormatter dtfHours = DateTimeFormatter.ofPattern("HHmm");
        LocalDateTime localDateNow = LocalDateTime.now();
        DataFromLandingPage2 newData = new DataFromLandingPage2();

        if (messageType.equals("E28")) {
            newData.setMessage_type("E");
            newData.setType_code(recipientData.getTypeCode() != null? recipientData.getTypeCode() : "");
            newData.setVariable_no_1(recipientData.getRecipientCode() != null?
                    recipientData.getRecipientCode() : "");
            newData.setVariable_name_1("recipient name"); //need to update after create db relation of recipient and recipient message
            newData.setVariable_date_1("");
            newData.setVariable_sum_1("");
            newData.setVariable_date_2("");
            newData.setVariable_sum_2("");
            newData.setSent_date(dtf.format(localDateNow));
            newData.setPhone_no(customerMessageData.getPhoneNo()); //need to recheck again because the recipient table doesn't have phone field
            newData.setEmail_address(customerMessageData.getEmailAddress()); //need to update after make relation of recipient and recipient message
            newData.setValuta_id("");
            newData.setVa_bca("");
            newData.setVa_permata("");
            newData.setNav("");
            newData.setGender_code("");
            newData.setVariable_no_2(customerMessageData.getPhoneNo()); //need to put phone number, but still need confirm whos phone to put
            newData.setVariable_sum_3("");
            newData.setVariable_name_2(customerMessageData.getTitle());
            newData.setVariable_no_3(customerMessageData.getCustomerName()  );
            newData.setNotes(customerMessageData.getMessage());
        }

        return newData;
    }
}
