package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.ProductRuleActivate.CreateProductRuleActivate;
import id.equity.nichemarket.dto.si.ProductRuleActivate.ProductRuleActivateDto;
import id.equity.nichemarket.model.si.ProductRuleActivate;
import id.equity.nichemarket.repository.si.ProductRuleActivateRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class ProductRuleActivateService {
	
	@Autowired
	private ProductRuleActivateRepository productRuleActivateRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List ProductRuleActivate
	public ResponseEntity<List<ProductRuleActivateDto>> listProductRuleActivate() {
		List<ProductRuleActivate> listproductRules = productRuleActivateRepository.findAll();
		Type targetType = new TypeToken<List<ProductRuleActivateDto>>() {}.getType();
		List<ProductRuleActivateDto> response = modelMapper.map(listproductRules, targetType);

		return ResponseEntity.ok(response);
	}
	
	//Get ProductRuleActivate By Id
	public ResponseEntity<ProductRuleActivateDto> getProductRuleActivateById(Long id) {
		ProductRuleActivate productRule = productRuleActivateRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Activate id " + id + " is not exist"));
		ProductRuleActivateDto response = modelMapper.map(productRule, ProductRuleActivateDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create ProductRuleActivate
	public ResponseEntity<ProductRuleActivateDto> addProductRuleActivate(CreateProductRuleActivate newRuleActivate) {
		ProductRuleActivate ruleActivate = modelMapper.map(newRuleActivate, ProductRuleActivate.class);
		ProductRuleActivate ruleActivateSaved = productRuleActivateRepository.save(ruleActivate);
		ProductRuleActivateDto response = modelMapper.map(ruleActivateSaved, ProductRuleActivateDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit ProductRuleActivate
	public ResponseEntity<ProductRuleActivateDto> editProductRuleActivate(CreateProductRuleActivate updateProductRule, Long id) {
		ProductRuleActivate productRule = productRuleActivateRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Activate id " + id + " is not exist"));
		
		//manual map
		productRule.setProductRuleActivateCode(updateProductRule.getProductRuleActivateCode());
		productRule.setProductCode(updateProductRule.getProductCode());
		productRule.setValutaCode(updateProductRule.getValutaCode());
		productRule.setPaymentPeriodCode(updateProductRule.getPaymentPeriodCode());
		productRule.setGenderCode(updateProductRule.getGenderCode());
		productRule.setAsCode(updateProductRule.getAsCode());
		productRule.setRelationTypeCode(updateProductRule.getRelationTypeCode());
		productRule.setMax(updateProductRule.getMax());
		productRule.setMin(updateProductRule.getMin());
		productRule.setRulePremium(updateProductRule.getRulePremium());
		productRule.setActive(updateProductRule.isActive());
		
		productRuleActivateRepository.save(productRule);
		ProductRuleActivateDto response = modelMapper.map(productRule, ProductRuleActivateDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete ProductRuleActivate
	public ResponseEntity<ProductRuleActivateDto> deleteProductRuleActivate(Long id) {
		ProductRuleActivate productRule = productRuleActivateRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Activate id " + id + " is not exist"));
		productRuleActivateRepository.deleteById(id);
		ProductRuleActivateDto response = modelMapper.map(productRule, ProductRuleActivateDto.class);

		return ResponseEntity.ok(response);
	}	
}