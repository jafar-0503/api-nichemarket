package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.si.BaseRuleDebtAccount.BaseRuleDebtAccountDto;
import id.equity.nichemarket.dto.si.BaseRuleDebtAccount.CreateBaseRuleDebtAccount;
import id.equity.nichemarket.model.si.BaseRuleDebtAccount;
import id.equity.nichemarket.repository.si.BaseRuleDebtAccountRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class BaseRuleDebtAccountService {

    @Autowired
    private BaseRuleDebtAccountRepository baseRuleDebtAccountRepository;

    @Autowired
    private ModelMapper modelMapper;

    //Get All Data
    public ResponseEntity<List<BaseRuleDebtAccountDto>> listBaseRuleDebtAccount() {
        List<BaseRuleDebtAccount> lsBaseRuleDebtAccount= baseRuleDebtAccountRepository.findAll();
        Type targetType = new TypeToken<List<BaseRuleDebtAccountDto>>() {}.getType();
        List<BaseRuleDebtAccountDto> response = modelMapper.map(lsBaseRuleDebtAccount, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<BaseRuleDebtAccountDto> getBaseRuleDebtAccountById(Long id) {
        BaseRuleDebtAccount baseRuleDebtAccount = baseRuleDebtAccountRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Debt Account id " + id + " is not exist"));
        BaseRuleDebtAccountDto response = modelMapper.map(baseRuleDebtAccount, BaseRuleDebtAccountDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<BaseRuleDebtAccountDto> addBaseRuleDebtAccount(CreateBaseRuleDebtAccount newBaseRuleDebtAccount) {
        BaseRuleDebtAccount baseRuleDebtAccount = modelMapper.map(newBaseRuleDebtAccount, BaseRuleDebtAccount.class);
        BaseRuleDebtAccount baseRuleDebtAccountSaved = baseRuleDebtAccountRepository.save(baseRuleDebtAccount);
        BaseRuleDebtAccountDto response = modelMapper.map(baseRuleDebtAccountSaved, BaseRuleDebtAccountDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<BaseRuleDebtAccountDto> editBaseRuleDebtAccount(CreateBaseRuleDebtAccount updateBaseRuleDebtAccount, Long id) {
        BaseRuleDebtAccount baseRuleDebtAccount = baseRuleDebtAccountRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Debt Account id " + id + " is not exist"));
        baseRuleDebtAccount.setBaseRuleDebtAccountCode(updateBaseRuleDebtAccount.getBaseRuleDebtAccountCode());
        baseRuleDebtAccount.setProductCode(updateBaseRuleDebtAccount.getProductCode());
        baseRuleDebtAccount.setFromYearTh(updateBaseRuleDebtAccount.getFromYearTh());
        baseRuleDebtAccount.setFromYearPercen(updateBaseRuleDebtAccount.getFromYearPercen());
        baseRuleDebtAccount.setToYearTh(updateBaseRuleDebtAccount.getToYearTh());
        baseRuleDebtAccount.setToYearPercen(updateBaseRuleDebtAccount.getToYearPercen());
        baseRuleDebtAccount.setActive(updateBaseRuleDebtAccount.isActive());
        baseRuleDebtAccountRepository.save(baseRuleDebtAccount);
        BaseRuleDebtAccountDto response = modelMapper.map(baseRuleDebtAccount, BaseRuleDebtAccountDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data
    public ResponseEntity<BaseRuleDebtAccountDto> deleteBaseRuleDebtAccount(Long id) {
        BaseRuleDebtAccount baseRuleDebtAccount = baseRuleDebtAccountRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Debt Account id " + id + " is not exist"));
        baseRuleDebtAccountRepository.deleteById(id);
        BaseRuleDebtAccountDto response = modelMapper.map(baseRuleDebtAccount, BaseRuleDebtAccountDto.class);

        return ResponseEntity.ok(response);
    }
}