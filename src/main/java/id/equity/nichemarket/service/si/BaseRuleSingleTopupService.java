package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.BaseRuleSingleTopup.BaseRuleSingleTopupDto;
import id.equity.nichemarket.dto.si.BaseRuleSingleTopup.CreateBaseRuleSingleTopup;
import id.equity.nichemarket.model.si.BaseRuleSingleTopup;
import id.equity.nichemarket.repository.si.BaseRuleSingleTopupRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class BaseRuleSingleTopupService {
	
	@Autowired
	private BaseRuleSingleTopupRepository baseRuleSingleTopupRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List BaseRuleSingleTopup
	public ResponseEntity<List<BaseRuleSingleTopupDto>> listBaseRuleSingleTopup() {
		List<BaseRuleSingleTopup> listSingleTopups = baseRuleSingleTopupRepository.findAll();
		Type targetType = new TypeToken<List<BaseRuleSingleTopupDto>>() {}.getType();
		List<BaseRuleSingleTopupDto> response = modelMapper.map(listSingleTopups, targetType);

		return ResponseEntity.ok(response);
	}
	
	//Get BaseRuleSingleTopup ById
	public ResponseEntity<BaseRuleSingleTopupDto> getBaseRuleSingleTopupById(Long id) {
		BaseRuleSingleTopup singleTopup = baseRuleSingleTopupRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Single Topup id " + id + " is not exist"));
		BaseRuleSingleTopupDto response = modelMapper.map(singleTopup, BaseRuleSingleTopupDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Post BaseRuleSingleTopup
	public ResponseEntity<BaseRuleSingleTopupDto> addBaseRuleSingleTopup(CreateBaseRuleSingleTopup newSingleTopup) {
		BaseRuleSingleTopup singleTopup = modelMapper.map(newSingleTopup, BaseRuleSingleTopup.class);
		BaseRuleSingleTopup singleTopupSaved = baseRuleSingleTopupRepository.save(singleTopup);
		BaseRuleSingleTopupDto response = modelMapper.map(singleTopupSaved, BaseRuleSingleTopupDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit BaseRuleSingleTopup
	public ResponseEntity<BaseRuleSingleTopupDto> editBaseRuleSingleTopup(CreateBaseRuleSingleTopup updateSingleTopup, Long id) {
		BaseRuleSingleTopup singleTopup = baseRuleSingleTopupRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Single Topup id " + id + " is not exist"));

		//manual map
		singleTopup.setBaseRuleSingleTopupCode(updateSingleTopup.getBaseRuleSingleTopupCode());
		singleTopup.setProductCode(updateSingleTopup.getProductCode());
		singleTopup.setValutaCode(updateSingleTopup.getValutaCode());
		singleTopup.setPaymentPeriodCode(updateSingleTopup.getPaymentPeriodCode());
		singleTopup.setMax(updateSingleTopup.getMax());
		singleTopup.setMin(updateSingleTopup.getMin());
		singleTopup.setStep(updateSingleTopup.getStep());
		singleTopup.setActive(updateSingleTopup.isActive());

		baseRuleSingleTopupRepository.save(singleTopup);
		BaseRuleSingleTopupDto response = modelMapper.map(singleTopup, BaseRuleSingleTopupDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete BaseRuleSingleTopup
	public ResponseEntity<BaseRuleSingleTopupDto> deleteBaseRuleSingleTopup(Long id) {
		BaseRuleSingleTopup singleTopup = baseRuleSingleTopupRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Single Topup id " + id + " is not exist"));
		baseRuleSingleTopupRepository.deleteById(id);
		BaseRuleSingleTopupDto response = modelMapper.map(singleTopup, BaseRuleSingleTopupDto.class);

		return ResponseEntity.ok(response);
	}
}