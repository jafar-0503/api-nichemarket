package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.Variable.CreateVariable;
import id.equity.nichemarket.dto.si.Variable.VariableDto;
import id.equity.nichemarket.model.si.Variable;
import id.equity.nichemarket.repository.si.VariableRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class VariableService {

	@Autowired
	private VariableRepository variableRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//Get All Data
	public ResponseEntity<List<VariableDto>> listVariable() {
		List<Variable> listVariable = variableRepository.findAll();
		Type targetType = new TypeToken<List<VariableDto>>() {}.getType();
		List<VariableDto> response = modelMapper.map(listVariable, targetType);

		return ResponseEntity.ok(response);
	}

	//Get Data By Id
	public ResponseEntity<VariableDto> getVariableById(Long id) {
		Variable variable = variableRepository.findById(id).orElseThrow(()-> new NotFoundException("Variable id " + id + " is not exist"));
		VariableDto response = modelMapper.map(variable, VariableDto.class);

		return ResponseEntity.ok(response);
	}

	//Post Data
	public ResponseEntity<VariableDto> addVariable(CreateVariable newVariable) {
		Variable variable = modelMapper.map(newVariable, Variable.class);
		Variable variableSaved = variableRepository.save(variable);
		VariableDto response = modelMapper.map(variableSaved, VariableDto.class);

		return ResponseEntity.ok(response);
	}

	//Update Data By Id
	public ResponseEntity<VariableDto> editVariable(CreateVariable updateVariable, Long id) {
		Variable variable = variableRepository.findById(id).orElseThrow(()-> new NotFoundException("Variable id " + id + " is not exist"));

		variable.setVariableCode(updateVariable.getVariableCode());
		variable.setDescription(updateVariable.getDescription());
		variable.setDataType(updateVariable.getDataType());
		variable.setBlockName(updateVariable.getBlockName());
		variable.setActive(updateVariable.isActive());
		variableRepository.save(variable);
		VariableDto response = modelMapper.map(variable, VariableDto.class);

		return ResponseEntity.ok(response);
	}

	public ResponseEntity<VariableDto> deleteVariable(Long id) {
		Variable findId = variableRepository.findById(id).orElseThrow(()-> new NotFoundException("Variable id " + id + " is not exist"));
		variableRepository.deleteById(id);
		VariableDto response = modelMapper.map(findId, VariableDto.class);

		return ResponseEntity.ok(response);
	}
}