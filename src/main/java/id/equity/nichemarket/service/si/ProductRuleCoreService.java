package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.si.ProductRuleCore.CreateProductRuleCore;
import id.equity.nichemarket.dto.si.ProductRuleCore.ProductRuleCoreDto;
import id.equity.nichemarket.model.si.ProductRuleCore;
import id.equity.nichemarket.repository.si.ProductRuleCoreRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class ProductRuleCoreService {
	
	@Autowired
	private ProductRuleCoreRepository productRuleCoreRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List ProductRuleCore
	public ResponseEntity<List<ProductRuleCoreDto>> listProductRuleCore() {
		List<ProductRuleCore> listProductRuleCores = productRuleCoreRepository.findAll();
		Type targetType = new TypeToken<List<ProductRuleCoreDto>>() {}.getType();
		List<ProductRuleCoreDto> response = modelMapper.map(listProductRuleCores, targetType);

		return ResponseEntity.ok(response);
	}
	
	//Get ProductRuleCore By Id
	public ResponseEntity<ProductRuleCoreDto> getProductRuleCoreById(Long id) {
		ProductRuleCore productRuleCore = productRuleCoreRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Core id " + id + " is not exist"));
		ProductRuleCoreDto response = modelMapper.map(productRuleCore, ProductRuleCoreDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create ProductRuleCore
	public ResponseEntity<ProductRuleCoreDto> addProductRuleCore(CreateProductRuleCore newProductRuleCore) {
		ProductRuleCore productRuleCore = modelMapper.map(newProductRuleCore, ProductRuleCore.class);
		ProductRuleCore productRuleCoreSaved = productRuleCoreRepository.save(productRuleCore);
		ProductRuleCoreDto response = modelMapper.map(productRuleCoreSaved, ProductRuleCoreDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit ProductRuleCore
	public ResponseEntity<ProductRuleCoreDto> editProductRuleCore(CreateProductRuleCore updateProductRuleCore, Long id) {
		ProductRuleCore productRuleCore = productRuleCoreRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Core id " + id + " is not exist"));
		
		//manual map
		productRuleCore.setProductRuleCoreCode(updateProductRuleCore.getProductRuleCoreCode());
		productRuleCore.setProductCode(updateProductRuleCore.getProductCode());
		productRuleCore.setValutaCode(updateProductRuleCore.getValutaCode());
		productRuleCore.setPaymentPeriodCode(updateProductRuleCore.getPaymentPeriodCode());
		productRuleCore.setSumInsuredClass(updateProductRuleCore.getSumInsuredClass());
		productRuleCore.setRisk(updateProductRuleCore.getRisk());
		productRuleCore.setAsCode(updateProductRuleCore.getAsCode());
		productRuleCore.setMappCode(updateProductRuleCore.getMappCode());
		productRuleCore.setActive(updateProductRuleCore.isActive());
		
		productRuleCoreRepository.save(productRuleCore);
		ProductRuleCoreDto response = modelMapper.map(productRuleCore, ProductRuleCoreDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete ProductRuleCore
	public ResponseEntity<ProductRuleCoreDto> deleteProductRuleCore(Long id) {
		ProductRuleCore productRuleCore = productRuleCoreRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Core id " + id + " is not exist"));
		productRuleCoreRepository.deleteById(id);
		ProductRuleCoreDto response = modelMapper.map(productRuleCore, ProductRuleCoreDto.class);

		return ResponseEntity.ok(response);
	}	
}