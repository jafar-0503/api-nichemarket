package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.BaseRulePremiumAllocation.CreateBaseRulePremiumAllocation;
import id.equity.nichemarket.dto.si.BaseRulePremiumAllocation.BaseRulePremiumAllocationDto;
import id.equity.nichemarket.model.si.BaseRulePremiumAllocation;
import id.equity.nichemarket.repository.si.BaseRulePremiumAllocationRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class BaseRulePremiumAllocationService {
	
	@Autowired
	private BaseRulePremiumAllocationRepository baseRulePremiumAllocationRepository;
	
	@Autowired
	private ModelMapper modelMapper;

	//Get All Data
	public ResponseEntity<List<BaseRulePremiumAllocationDto>> listBaseRulePremiumAllocation() {
		List<BaseRulePremiumAllocation> lsBaseRulePremiumAllocation= baseRulePremiumAllocationRepository.findAll();
		Type targetType = new TypeToken<List<BaseRulePremiumAllocationDto>>() {}.getType();
		List<BaseRulePremiumAllocationDto> response = modelMapper.map(lsBaseRulePremiumAllocation, targetType);

		return ResponseEntity.ok(response);
	}

	//Get Data By Id
	public ResponseEntity<BaseRulePremiumAllocationDto> getBaseRulePremiumAllocationById(Long id) {
		BaseRulePremiumAllocation bRPAlocation = baseRulePremiumAllocationRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Premium Allocation id " + id + " is not exist"));
		BaseRulePremiumAllocationDto response = modelMapper.map(bRPAlocation, BaseRulePremiumAllocationDto.class);

		return ResponseEntity.ok(response);
	}

	//Post Data
	public ResponseEntity<BaseRulePremiumAllocationDto> addBaseRulePremiumAllocation(CreateBaseRulePremiumAllocation newPremiumAllocation) {
		BaseRulePremiumAllocation bRPAlocation = modelMapper.map(newPremiumAllocation, BaseRulePremiumAllocation.class);
		BaseRulePremiumAllocation bRPAlocationSaved = baseRulePremiumAllocationRepository.save(bRPAlocation);
		BaseRulePremiumAllocationDto response = modelMapper.map(bRPAlocationSaved, BaseRulePremiumAllocationDto.class);

		return ResponseEntity.ok(response);
	}

	//Put Data By Id
	public ResponseEntity<BaseRulePremiumAllocationDto> editBaseRulePremiumAllocation(CreateBaseRulePremiumAllocation updatePremiumAllocation, Long id) {
		BaseRulePremiumAllocation bRPAlocation = baseRulePremiumAllocationRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Premium Allocation id " + id + " is not exist"));
		bRPAlocation.setBaseRulePremiumAllocationCode(updatePremiumAllocation.getBaseRulePremiumAllocationCode());
		bRPAlocation.setProductCode(updatePremiumAllocation.getProductCode());
		bRPAlocation.setValutaCode(updatePremiumAllocation.getValutaCode());
		bRPAlocation.setPaymentPeriodCode(updatePremiumAllocation.getPaymentPeriodCode());
		bRPAlocation.setPremiumPercent(updatePremiumAllocation.getPremiumPercent());
		bRPAlocation.setTopupPercent(updatePremiumAllocation.getTopupPercent());
		bRPAlocation.setActive(updatePremiumAllocation.isActive());
		baseRulePremiumAllocationRepository.save(bRPAlocation);
		BaseRulePremiumAllocationDto response = modelMapper.map(bRPAlocation, BaseRulePremiumAllocationDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete Data By Id
	public ResponseEntity<BaseRulePremiumAllocationDto> deleteBaseRulePremiumAllocation(Long id) {
		BaseRulePremiumAllocation bRPAlocation = baseRulePremiumAllocationRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Premium Allocation id " + id + " is not exist"));
		baseRulePremiumAllocationRepository.deleteById(id);
		BaseRulePremiumAllocationDto response = modelMapper.map(bRPAlocation, BaseRulePremiumAllocationDto.class);

		return ResponseEntity.ok(response);
	}
}