package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.Valuta.CreateValuta;
import id.equity.nichemarket.dto.si.Valuta.ValutaDto;
import id.equity.nichemarket.model.si.Valuta;
import id.equity.nichemarket.repository.si.ValutaRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;


@Service
public class ValutaService {
	
	@Autowired
	private ValutaRepository valutaRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List Valuta
	public ResponseEntity<List<ValutaDto>> listValuta() {
		List<Valuta> listValutas = valutaRepository.findAll();
		Type targetType = new TypeToken<List<ValutaDto>>() {}.getType();
		List<ValutaDto> response = modelMapper.map(listValutas, targetType);

		return ResponseEntity.ok(response);
	}

	//List Valuta By Id
	public ResponseEntity<ValutaDto> getValutaById(Long id) {
		Valuta valuta = valutaRepository.findById(id).orElseThrow(()-> new NotFoundException("Valuta id " + id + " is not exist"));
		ValutaDto response = modelMapper.map(valuta, ValutaDto.class);

		return ResponseEntity.ok(response);
	}

	//create valuta
	public ResponseEntity<ValutaDto> addValuta(CreateValuta newValuta) {
		Valuta valuta = modelMapper.map(newValuta, Valuta.class);
		Valuta valutaSaved = valutaRepository.save(valuta);
		ValutaDto response = modelMapper.map(valutaSaved, ValutaDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit Valuta
	public ResponseEntity<ValutaDto> editValuta(CreateValuta updateValuta, Long id) {
		Valuta valuta = valutaRepository.findById(id).orElseThrow(()-> new NotFoundException("Valuta id " + id + " is not Exist"));
		
		//manual map
		valuta.setValutaCode(updateValuta.getValutaCode());
		valuta.setDescription(updateValuta.getDescription());
		valuta.setSymbol(updateValuta.getSymbol());
		valuta.setActive(updateValuta.isActive());
		
		valutaRepository.save(valuta);
		ValutaDto response = modelMapper.map(valuta, ValutaDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete Data
	public ResponseEntity<ValutaDto> deleteValuta(Long id) {
		Valuta valuta = valutaRepository.findById(id).orElseThrow(()-> new NotFoundException("Valuta id " + id + " is not exist"));
		
		valutaRepository.deleteById(id);
		ValutaDto response = modelMapper.map(valuta, ValutaDto.class);

		return ResponseEntity.ok(response);
	}
}