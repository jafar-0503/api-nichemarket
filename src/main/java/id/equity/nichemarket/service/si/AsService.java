package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.As.AsDto;
import id.equity.nichemarket.dto.si.As.CreateAs;
import id.equity.nichemarket.model.si.As;
import id.equity.nichemarket.repository.si.AsRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class AsService {
	
	@Autowired
	private AsRepository asRepository;
	
	@Autowired
	private ModelMapper modelMapper;

	//List As
	public ResponseEntity<List<AsDto>> listAs() {
		List<As> listAss = asRepository.findAll();
		Type targetType = new TypeToken<List<AsDto>>() {}.getType();
		List<AsDto> response = modelMapper.map(listAss, targetType);

		return ResponseEntity.ok(response);
	}
	
	//Get As ById
	public ResponseEntity<AsDto> getAsById(Long id) {
		As as = asRepository.findById(id).orElseThrow(()-> new NotFoundException("As id " + id + " is not Exist"));
		AsDto response = modelMapper.map(as, AsDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create As
	public ResponseEntity<AsDto> addAs(CreateAs newAs) {
		As as = modelMapper.map(newAs, As.class);
		As asSaved = asRepository.save(as);
		AsDto response = modelMapper.map(asSaved, AsDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit As
	public ResponseEntity<AsDto> editAs(CreateAs updateAs, Long id) {
		As as = asRepository.findById(id).orElseThrow(()-> new NotFoundException("As id " + id + " is not Exist"));
		
		//manual map
		as.setAsCode(updateAs.getAsCode());
		as.setDescription(updateAs.getDescription());
		as.setAsCodeString(updateAs.getAsCodeString());
		as.setActive(updateAs.isActive());
		
		asRepository.save(as);
		AsDto response = modelMapper.map(as, AsDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete As
	public ResponseEntity<AsDto> deleteAs(Long id) {
		As as = asRepository.findById(id).orElseThrow(()-> new NotFoundException("As id " + id + " is not Exist"));
		
		asRepository.deleteById(id);
		AsDto response = modelMapper.map(as, AsDto.class);

		return ResponseEntity.ok(response);
	}
}