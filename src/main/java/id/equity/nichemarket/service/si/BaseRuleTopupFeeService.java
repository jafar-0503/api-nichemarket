package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.si.BaseRuleTopupFee.BaseRuleTopupFeeDto;
import id.equity.nichemarket.dto.si.BaseRuleTopupFee.CreateBaseRuleTopupFee;
import id.equity.nichemarket.model.si.BaseRuleTopupFee;
import id.equity.nichemarket.repository.si.BaseRuleTopupFeeRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class BaseRuleTopupFeeService {

    @Autowired
    private BaseRuleTopupFeeRepository baseRuleTopupFeeRepository;

    @Autowired
    private ModelMapper modelMapper;

    //Get All Data
    public ResponseEntity<List<BaseRuleTopupFeeDto>> listBaseRuleTopupFee() {
        List<BaseRuleTopupFee> ltBaseRuleTopupFee= baseRuleTopupFeeRepository.findAll();
        Type targetType = new TypeToken<List<BaseRuleTopupFeeDto>>() {}.getType();
        List<BaseRuleTopupFeeDto> response = modelMapper.map(ltBaseRuleTopupFee, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<BaseRuleTopupFeeDto> getBaseRuleTopupFeeById(Long id) {
        BaseRuleTopupFee BaseRuleTopupFee = baseRuleTopupFeeRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Topup Fee id " + id + " is not exist"));
        BaseRuleTopupFeeDto response = modelMapper.map(BaseRuleTopupFee, BaseRuleTopupFeeDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<BaseRuleTopupFeeDto> addBaseRuleTopupFee(CreateBaseRuleTopupFee newBaseRuleTopupFee) {
        BaseRuleTopupFee BaseRuleTopupFee = modelMapper.map(newBaseRuleTopupFee, BaseRuleTopupFee.class);
        BaseRuleTopupFee BaseRuleTopupFeeSaved = baseRuleTopupFeeRepository.save(BaseRuleTopupFee);
        BaseRuleTopupFeeDto response = modelMapper.map(BaseRuleTopupFeeSaved, BaseRuleTopupFeeDto.class);

        return ResponseEntity.ok(response);
    }

    //Put By Id
    public ResponseEntity<BaseRuleTopupFeeDto> editBaseRuleTopupFee(CreateBaseRuleTopupFee updateBaseRuleTopupFee, Long id) {
        BaseRuleTopupFee baseRuleTopupFee = baseRuleTopupFeeRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Topup Fee id " + id + " is not exist"));
        baseRuleTopupFee.setBaseRuleTopupFeeCode(updateBaseRuleTopupFee.getBaseRuleTopupFeeCode());
        baseRuleTopupFee.setProductCode(updateBaseRuleTopupFee.getProductCode());
        baseRuleTopupFee.setValutaCode(updateBaseRuleTopupFee.getValutaCode());
        baseRuleTopupFee.setPaymentPeriodCode(updateBaseRuleTopupFee.getPaymentPeriodCode());
        baseRuleTopupFee.setRate(updateBaseRuleTopupFee.getRate());
        baseRuleTopupFee.setActive(updateBaseRuleTopupFee.isActive());
        baseRuleTopupFeeRepository.save(baseRuleTopupFee);
        BaseRuleTopupFeeDto response = modelMapper.map(baseRuleTopupFee, BaseRuleTopupFeeDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete By Id
    public ResponseEntity<BaseRuleTopupFeeDto> deleteBaseRuleTopupFee(Long id) {
        BaseRuleTopupFee baseRuleTopupFee = baseRuleTopupFeeRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Topup Fee id " + id + " is not exist"));
        baseRuleTopupFeeRepository.deleteById(id);
        BaseRuleTopupFeeDto response = modelMapper.map(baseRuleTopupFee, BaseRuleTopupFeeDto.class);

        return ResponseEntity.ok(response);
    }
}