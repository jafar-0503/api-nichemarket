package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.BaseRuleAcquisitionFee.CreateBaseRuleAcquisitionFee;
import id.equity.nichemarket.dto.si.BaseRuleAcquisitionFee.BaseRuleAcquisitionFeeDto;
import id.equity.nichemarket.model.si.BaseRuleAcquisitionFee;
import id.equity.nichemarket.repository.si.BaseRuleAcquisitionFeeRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class BaseRuleAcquisitionFeeService {
	
	@Autowired
	private BaseRuleAcquisitionFeeRepository baseRuleAcquisitionFeeRepository;
	
	@Autowired
	private ModelMapper modelMapper;

	//Get All Data
	public ResponseEntity<List<BaseRuleAcquisitionFeeDto>> listBaseRuleAcquisitionFee() {
		List<BaseRuleAcquisitionFee> lsBRAFee= baseRuleAcquisitionFeeRepository.findAll();
		Type targetType = new TypeToken<List<BaseRuleAcquisitionFeeDto>>() {}.getType();
		List<BaseRuleAcquisitionFeeDto> response = modelMapper.map(lsBRAFee, targetType);

		return ResponseEntity.ok(response);
	}

	//Get Data By Id
	public ResponseEntity<BaseRuleAcquisitionFeeDto> getBaseRuleAcquisitionFeeById(Long id) {
		BaseRuleAcquisitionFee BRAFeeId = baseRuleAcquisitionFeeRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Acquisition Fee id " + id + " is not exist"));
		BaseRuleAcquisitionFeeDto response =  modelMapper.map(BRAFeeId, BaseRuleAcquisitionFeeDto.class);

		return ResponseEntity.ok(response);
	}

	//Post Data
	public ResponseEntity<BaseRuleAcquisitionFeeDto> addBaseRuleAcquisitionFee(CreateBaseRuleAcquisitionFee newBRAFee) {
		BaseRuleAcquisitionFee BRAFee = modelMapper.map(newBRAFee, BaseRuleAcquisitionFee.class);
		BaseRuleAcquisitionFee BRAFeeSaved = baseRuleAcquisitionFeeRepository.save(BRAFee);
		BaseRuleAcquisitionFeeDto response = modelMapper.map(BRAFeeSaved, BaseRuleAcquisitionFeeDto.class);

		return ResponseEntity.ok(response);
	}

	//Put Data By Id
	public ResponseEntity<BaseRuleAcquisitionFeeDto> editBaseRuleAcquisitionFee(CreateBaseRuleAcquisitionFee updateBRAFee, Long id) {
		BaseRuleAcquisitionFee BRAFee = baseRuleAcquisitionFeeRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Acquisition Fee id " + id + " is not exist"));
		BRAFee.setBaseRuleAcquisitionFeeCode(updateBRAFee.getBaseRuleAcquisitionFeeCode());
		BRAFee.setProductCode(updateBRAFee.getProductCode());
		BRAFee.setValutaCode(updateBRAFee.getValutaCode());
		BRAFee.setPaymentPeriodCode(updateBRAFee.getPaymentPeriodCode());
		BRAFee.setYearTh(updateBRAFee.getYearTh());
		BRAFee.setRate(updateBRAFee.getRate());
		BRAFee.setActive(updateBRAFee.isActive());
		baseRuleAcquisitionFeeRepository.save(BRAFee);
		BaseRuleAcquisitionFeeDto response = modelMapper.map(BRAFee, BaseRuleAcquisitionFeeDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete Data By Id
	public ResponseEntity<BaseRuleAcquisitionFeeDto> deleteBaseRuleAcquisitionFee(Long id) {
		BaseRuleAcquisitionFee BRAFee = baseRuleAcquisitionFeeRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Acquisition Fee id " + id + " is not exist"));
		baseRuleAcquisitionFeeRepository.deleteById(id);
		BaseRuleAcquisitionFeeDto response = modelMapper.map(BRAFee, BaseRuleAcquisitionFeeDto.class);

		return ResponseEntity.ok(response);
	}
}