package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.Pages.CreatePages;
import id.equity.nichemarket.dto.si.Pages.PagesDto;
import id.equity.nichemarket.model.si.Pages;
import id.equity.nichemarket.repository.si.PagesRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class PagesService {

	@Autowired
	private PagesRepository pagesRepository;
	
	@Autowired
	private ModelMapper modelMapper;

	//Get All Payment Period
	public ResponseEntity<List<PagesDto>> listPages() {
		List<Pages> lsPages = pagesRepository.findAll();
		Type targetType = new TypeToken<List<PagesDto>>() {}.getType();
		List<PagesDto> response = modelMapper.map(lsPages, targetType);

		return ResponseEntity.ok(response);
	}

	//Get Payment Period By Id
	public ResponseEntity<PagesDto> getPagesById(Long id) {
		Pages pages = pagesRepository.findById(id).orElseThrow(()-> new NotFoundException("Pages id " + id +" is not exist"));
		PagesDto response = modelMapper.map(pages, PagesDto.class);

		return ResponseEntity.ok(response);
	}

	//Post Payment Period
	public ResponseEntity<PagesDto> addPages(CreatePages newPges) {
		Pages pages = modelMapper.map(newPges, Pages.class);
		Pages pagesSaved = pagesRepository.save(pages);
		PagesDto response = modelMapper.map(pagesSaved, PagesDto.class);

		return ResponseEntity.ok(response);
	}

	//Put Payment Period
	public ResponseEntity<PagesDto> editPages(CreatePages updatePages, Long id) {
		Pages pages = pagesRepository.findById(id).orElseThrow(()-> new NotFoundException("Pages id " + id + " is not exist"));
		pages.setPageCode(updatePages.getPageCode());
		pages.setDescription(updatePages.getDescription());
		pages.setOrder(updatePages.getOrder());
		pages.setActive(updatePages.isActive());
		pagesRepository.save(pages);
		PagesDto response = modelMapper.map(pages, PagesDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete Data By Id
	public ResponseEntity<PagesDto> deletePages(Long id) {
		Pages findPages = pagesRepository.findById(id).orElseThrow(()-> new NotFoundException("Pages id " + id + " is not exist"));
		pagesRepository.deleteById(id);
		PagesDto response = modelMapper.map(findPages, PagesDto.class);

		return ResponseEntity.ok(response);
	}	
}