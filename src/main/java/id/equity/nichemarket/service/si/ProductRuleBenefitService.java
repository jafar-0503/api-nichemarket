package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.si.ProductRuleBenefit.CreateProductRuleBenefit;
import id.equity.nichemarket.dto.si.ProductRuleBenefit.ProductRuleBenefitDto;
import id.equity.nichemarket.model.si.ProductRuleBenefit;
import id.equity.nichemarket.repository.si.ProductRuleBenefitRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class ProductRuleBenefitService {
	
	@Autowired
	private ProductRuleBenefitRepository productRuleBenefitRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List ProductRuleBenefit
	public ResponseEntity<List<ProductRuleBenefitDto>> listProductRuleBenefit() {
		List<ProductRuleBenefit> listProductRuleBenefits = productRuleBenefitRepository.findAll();
		Type targetType = new TypeToken<List<ProductRuleBenefitDto>>() {}.getType();
		List<ProductRuleBenefitDto> response = modelMapper.map(listProductRuleBenefits, targetType);

		return ResponseEntity.ok(response);
	}
	
	//Get ProductRuleBenefit By Id
	public ResponseEntity<ProductRuleBenefitDto> getProductRuleBenefitById(Long id) {
		ProductRuleBenefit productRuleBenefit = productRuleBenefitRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Benefit id " + id + " is not exist"));
		ProductRuleBenefitDto response = modelMapper.map(productRuleBenefit, ProductRuleBenefitDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create ProductRuleBenefit
	public ResponseEntity<ProductRuleBenefitDto> addProductRuleBenefit(CreateProductRuleBenefit newProductRuleBenefit) {
		ProductRuleBenefit productRuleBenefit = modelMapper.map(newProductRuleBenefit, ProductRuleBenefit.class);
		ProductRuleBenefit productRuleBenefitSaved = productRuleBenefitRepository.save(productRuleBenefit);
		ProductRuleBenefitDto response = modelMapper.map(productRuleBenefitSaved, ProductRuleBenefitDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit ProductRuleBenefit
	public ResponseEntity<ProductRuleBenefitDto> editProductRuleBenefit(CreateProductRuleBenefit updateProductRuleBenefit, Long id) {
		ProductRuleBenefit productRuleBenefit = productRuleBenefitRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Benefit id " + id + " is not exist"));
		
		//manual map
		productRuleBenefit.setProductRuleBenefitCode(updateProductRuleBenefit.getProductRuleBenefitCode());
		productRuleBenefit.setProductCode(updateProductRuleBenefit.getProductCode());
		productRuleBenefit.setValutaCode(updateProductRuleBenefit.getValutaCode());
		productRuleBenefit.setTitle(updateProductRuleBenefit.getTitle());
		productRuleBenefit.setHeaderTitle(updateProductRuleBenefit.getHeaderTitle());
		productRuleBenefit.setHeaderValue(updateProductRuleBenefit.getHeaderValue());
		productRuleBenefit.setFooterTitle(updateProductRuleBenefit.getFooterTitle());
		productRuleBenefit.setFooterValue(updateProductRuleBenefit.getFooterValue());
		productRuleBenefit.setActive(updateProductRuleBenefit.isActive());
		
		productRuleBenefitRepository.save(productRuleBenefit);
		ProductRuleBenefitDto response = modelMapper.map(productRuleBenefit, ProductRuleBenefitDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete ProductRuleBenefit
	public ResponseEntity<ProductRuleBenefitDto> deleteProductRuleBenefit(Long id) {
		ProductRuleBenefit productRuleBenefit = productRuleBenefitRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Benefit id " + id + " is not exist"));
		productRuleBenefitRepository.deleteById(id);
		ProductRuleBenefitDto response = modelMapper.map(productRuleBenefit, ProductRuleBenefitDto.class);

		return ResponseEntity.ok(response);
	}	
}