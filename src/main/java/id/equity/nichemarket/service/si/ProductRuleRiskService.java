package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.si.ProductRuleRisk.CreateProductRuleRisk;
import id.equity.nichemarket.dto.si.ProductRuleRisk.ProductRuleRiskDto;
import id.equity.nichemarket.model.si.ProductRuleRisk;
import id.equity.nichemarket.repository.si.ProductRuleRiskRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class ProductRuleRiskService {
	
	@Autowired
	private ProductRuleRiskRepository productRuleRiskRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List ProductRuleRisk
	public ResponseEntity<List<ProductRuleRiskDto>> listProductRuleRisk() {
		List<ProductRuleRisk> listProductRuleRisks = productRuleRiskRepository.findAll();
		Type targetType = new TypeToken<List<ProductRuleRiskDto>>() {}.getType();
		List<ProductRuleRiskDto> response = modelMapper.map(listProductRuleRisks, targetType);

		return ResponseEntity.ok(response);
	}
	
	//Get ProductRuleRisk By Id
	public ResponseEntity<ProductRuleRiskDto> getProductRuleRiskById(Long id) {
		ProductRuleRisk productRuleRisk = productRuleRiskRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Risk id " + id + " is not exist"));
		ProductRuleRiskDto response = modelMapper.map(productRuleRisk, ProductRuleRiskDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create ProductRuleRisk
	public ResponseEntity<ProductRuleRiskDto> addProductRuleRisk(CreateProductRuleRisk newProductRuleRisk) {
		ProductRuleRisk productRuleRisk = modelMapper.map(newProductRuleRisk, ProductRuleRisk.class);
		ProductRuleRisk productRuleRiskSaved = productRuleRiskRepository.save(productRuleRisk);
		ProductRuleRiskDto response = modelMapper.map(productRuleRiskSaved, ProductRuleRiskDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit ProductRuleRisk
	public ResponseEntity<ProductRuleRiskDto> editProductRuleRisk(CreateProductRuleRisk updateProductRuleRisk, Long id) {
		ProductRuleRisk productRuleRisk = productRuleRiskRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Risk id " + id + " is not exist"));
		
		//manual map
		productRuleRisk.setProductRuleRiskCode(updateProductRuleRisk.getProductRuleRiskCode());
		productRuleRisk.setProductCode(updateProductRuleRisk.getProductCode());
		productRuleRisk.setRiskName(updateProductRuleRisk.getRiskName());
		productRuleRisk.setActive(updateProductRuleRisk.isActive());
		
		productRuleRiskRepository.save(productRuleRisk);
		ProductRuleRiskDto response = modelMapper.map(productRuleRisk, ProductRuleRiskDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete ProductRuleRisk
	public ResponseEntity<ProductRuleRiskDto> deleteProductRuleRisk(Long id) {
		ProductRuleRisk productRuleRisk = productRuleRiskRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Risk id " + id + " is not exist"));
		productRuleRiskRepository.deleteById(id);
		ProductRuleRiskDto response = modelMapper.map(productRuleRisk, ProductRuleRiskDto.class);

		return ResponseEntity.ok(response);
	}	
}