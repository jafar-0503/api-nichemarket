package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.ServiceUnit.CreateServiceUnit;
import id.equity.nichemarket.dto.si.ServiceUnit.ServiceUnitDto;
import id.equity.nichemarket.model.si.ServiceUnit;
import id.equity.nichemarket.repository.si.ServiceUnitRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class ServiceUnitService {

	@Autowired
	private ServiceUnitRepository serviceUnitRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//Get All Service Unit
	public ResponseEntity<List<ServiceUnitDto>> listServiceUnit() {
		List<ServiceUnit> lsServiceUnit = serviceUnitRepository.findAll();
		Type targetType = new TypeToken<List<ServiceUnitDto>>() {}.getType();
		List<ServiceUnitDto> response = modelMapper.map(lsServiceUnit, targetType);

		return ResponseEntity.ok(response);
	}

	//Get Service Unit By Id
	public ResponseEntity<ServiceUnitDto> getServiceUnitById(Long id) {
		ServiceUnit serviceUnit = serviceUnitRepository.findById(id).orElseThrow(()->new NotFoundException("Service Unit id "+ id + " is not exist"));
		ServiceUnitDto response = modelMapper.map(serviceUnit, ServiceUnitDto.class);

		return ResponseEntity.ok(response);
	}

	//Post Service Unit
	public ResponseEntity<ServiceUnitDto> addServiceUnit(CreateServiceUnit newServiceUnit) {
		ServiceUnit serviceUnit = modelMapper.map(newServiceUnit, ServiceUnit.class);
		ServiceUnit serviceUnitSaved = serviceUnitRepository.save(serviceUnit);
		ServiceUnitDto response = modelMapper.map(serviceUnitSaved, ServiceUnitDto.class);

		return ResponseEntity.ok(response);
	}

	//Put Service Unit
	public ResponseEntity<ServiceUnitDto> editServiceUnit(CreateServiceUnit updateServiceUnit, Long id) {
		ServiceUnit serviceUnit = serviceUnitRepository.findById(id).orElseThrow(()-> new NotFoundException("Service Unit id " + id + " is not exist"));
		serviceUnit.setServiceUnitCode(updateServiceUnit.getServiceUnitCode());
		serviceUnit.setDescription(updateServiceUnit.getDescription());
		serviceUnit.setActive(updateServiceUnit.isActive());

		serviceUnitRepository.save(serviceUnit);
		ServiceUnitDto response = modelMapper.map(serviceUnit, ServiceUnitDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete Service Unit
	public ResponseEntity<ServiceUnitDto> deleteServiceUnit(Long id) {
		ServiceUnit serviceUnit = serviceUnitRepository.findById(id).orElseThrow(()-> new NotFoundException("Service Unit id " + id + " is not exist"));
		serviceUnitRepository.deleteById(id);
		ServiceUnitDto response = modelMapper.map(serviceUnit, ServiceUnitDto.class);

		return ResponseEntity.ok(response);
	}
}