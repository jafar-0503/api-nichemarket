package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.si.BaseRulePolicyFee.BaseRulePolicyFeeDto;
import id.equity.nichemarket.dto.si.BaseRulePolicyFee.CreateBaseRulePolicyFee;
import id.equity.nichemarket.model.si.BaseRulePolicyFee;
import id.equity.nichemarket.repository.si.BaseRulePolicyRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class BaseRulePolicyFeeService {
	
	@Autowired
	private BaseRulePolicyRepository baseRulePolicyRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List BaseRulePolicy
	public ResponseEntity<List<BaseRulePolicyFeeDto>> listBaseRulePolicyFee() {
		List<BaseRulePolicyFee> listBaseRulesPolicy = baseRulePolicyRepository.findAll();
		Type targetType = new TypeToken <List<BaseRulePolicyFeeDto>>() {}.getType();
		List<BaseRulePolicyFeeDto> response = modelMapper.map(listBaseRulesPolicy, targetType);

		return ResponseEntity.ok(response);
	}
	
	//List BaseRulePolicy By Id
	public ResponseEntity<BaseRulePolicyFeeDto> getBaseRulePolicyFeeById(Long id) {
		BaseRulePolicyFee baseRulePolicyFee = baseRulePolicyRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Policy Fee id " + id + " is not exist"));
		BaseRulePolicyFeeDto response = modelMapper.map(baseRulePolicyFee, BaseRulePolicyFeeDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create BaseRulePolicy
	public ResponseEntity<BaseRulePolicyFeeDto> addBaseRulePolicyFee(CreateBaseRulePolicyFee newBaseRulePolicy) {
		BaseRulePolicyFee baseRulePolicyFee = modelMapper.map(newBaseRulePolicy, BaseRulePolicyFee.class);
		BaseRulePolicyFee baseRulePolicyFeeSaved = baseRulePolicyRepository.save(baseRulePolicyFee);
		BaseRulePolicyFeeDto response = modelMapper.map(baseRulePolicyFeeSaved, BaseRulePolicyFeeDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit BaseRulePolicy
	public ResponseEntity<BaseRulePolicyFeeDto> editBaseRulePolicyFee(CreateBaseRulePolicyFee updateBaseRulePolicy, Long id) {
		BaseRulePolicyFee baseRulePolicyFee = baseRulePolicyRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Policy Fee id " + id + " is not Exist"));
		
		//manual map
		baseRulePolicyFee.setBaseRulePolicyFeeCode(updateBaseRulePolicy.getBaseRulePolicyFeeCode());
		baseRulePolicyFee.setProductCode(updateBaseRulePolicy.getProductCode());
		baseRulePolicyFee.setValutaCode(updateBaseRulePolicy.getValutaCode());
		baseRulePolicyFee.setPaymentPeriodCode(updateBaseRulePolicy.getPaymentPeriodCode());
		baseRulePolicyFee.setRate(updateBaseRulePolicy.getRate());
		baseRulePolicyFee.setActive(updateBaseRulePolicy.isActive());
		
		baseRulePolicyRepository.save(baseRulePolicyFee);
		BaseRulePolicyFeeDto response = modelMapper.map(baseRulePolicyFee, BaseRulePolicyFeeDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete BaseRulePolicy
	public ResponseEntity<BaseRulePolicyFeeDto> deleteBaseRulePolicyFee(Long id) {
		BaseRulePolicyFee baseRulePolicyFee = baseRulePolicyRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Policy Fee id " + id + " is not exist"));
		baseRulePolicyRepository.deleteById(id);
		BaseRulePolicyFeeDto response = modelMapper.map(baseRulePolicyFee, BaseRulePolicyFeeDto.class);

		return ResponseEntity.ok(response);
	}
}