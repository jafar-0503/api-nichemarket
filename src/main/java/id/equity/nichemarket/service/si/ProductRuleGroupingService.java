package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.ProductRuleGrouping.CreateProductRuleGrouping;
import id.equity.nichemarket.dto.si.ProductRuleGrouping.ProductRuleGroupingDto;
import id.equity.nichemarket.model.si.ProductRuleGrouping;
import id.equity.nichemarket.repository.si.ProductRuleGroupingRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class ProductRuleGroupingService {
	
	@Autowired
	private ProductRuleGroupingRepository productRuleGroupingRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List ProductRuleGrouping
	public ResponseEntity<List<ProductRuleGroupingDto>> listProductRuleGrouping() {
		List<ProductRuleGrouping> listproductRuleGroupings = productRuleGroupingRepository.findAll();
		Type targetType = new TypeToken<List<ProductRuleGroupingDto>>() {}.getType();
		List<ProductRuleGroupingDto> response = modelMapper.map(listproductRuleGroupings, targetType);

		return ResponseEntity.ok(response);
	}
	//Get ProductRuleGrouping By Id
	public ResponseEntity<ProductRuleGroupingDto> getProductRuleGroupingById(Long id) {
		ProductRuleGrouping ruleGrouping = productRuleGroupingRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Grouping id " + id + " is not exist"));
		ProductRuleGroupingDto response = modelMapper.map(ruleGrouping, ProductRuleGroupingDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create ProductRuleGrouping
	public ResponseEntity<ProductRuleGroupingDto> addProductRuleGrouping(CreateProductRuleGrouping newRuleGrouping) {
		ProductRuleGrouping ruleGrouping = modelMapper.map(newRuleGrouping, ProductRuleGrouping.class);
		ProductRuleGrouping ruleGroupingSaved = productRuleGroupingRepository.save(ruleGrouping);
		ProductRuleGroupingDto response = modelMapper.map(ruleGroupingSaved, ProductRuleGroupingDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit ProductRuleGrouping
	public ResponseEntity<ProductRuleGroupingDto> editProductRuleGrouping(CreateProductRuleGrouping updateRuleGrouping, Long id) {
		ProductRuleGrouping ruleGrouping = productRuleGroupingRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Grouping id " + id + " is not exist"));
		
		//manual map
		ruleGrouping.setProductRuleGroupingCode(updateRuleGrouping.getProductRuleGroupingCode());
		ruleGrouping.setProductCode(updateRuleGrouping.getProductCode());
		ruleGrouping.setValutaCode(updateRuleGrouping.getValutaCode());
		ruleGrouping.setProductCodeRef(updateRuleGrouping.getProductCodeRef());
		ruleGrouping.setMin(updateRuleGrouping.getMin());
		ruleGrouping.setMax(updateRuleGrouping.getMax());
		ruleGrouping.setMandatory(updateRuleGrouping.getMandatory());
		ruleGrouping.setGroupingCode(updateRuleGrouping.getGroupingCode());
		ruleGrouping.setActive(updateRuleGrouping.isActive());
		
		productRuleGroupingRepository.save(ruleGrouping);
		ProductRuleGroupingDto response = modelMapper.map(ruleGrouping, ProductRuleGroupingDto.class);

		return ResponseEntity.ok(response);
	}
	//Delete ProductRuleGrouping
	public ResponseEntity<ProductRuleGroupingDto> deleteProductRuleGrouping(Long id) {
		ProductRuleGrouping ruleGrouping = productRuleGroupingRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Grouping id " + id + " is not exist"));
		productRuleGroupingRepository.deleteById(id);
		ProductRuleGroupingDto response = modelMapper.map(ruleGrouping, ProductRuleGroupingDto.class);

		return ResponseEntity.ok(response);
	}
}