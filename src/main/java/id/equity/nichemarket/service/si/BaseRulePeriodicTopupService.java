package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.si.BaseRulePeriodicTopup.BaseRulePeriodicTopupDto;
import id.equity.nichemarket.dto.si.BaseRulePeriodicTopup.CreateBaseRulePeriodicTopup;
import id.equity.nichemarket.model.si.BaseRulePeriodicTopup;
import id.equity.nichemarket.repository.si.BaseRulePeriodicTopupRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class BaseRulePeriodicTopupService {
	
	@Autowired
	private BaseRulePeriodicTopupRepository baseRulePeriodicTopupRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//Get All PeriodicTopup
	public ResponseEntity<List<BaseRulePeriodicTopupDto>> listBaseRulePeriodicTopup() {
		List<BaseRulePeriodicTopup> listPeriodicTopups = baseRulePeriodicTopupRepository.findAll();
		Type targetType = new TypeToken<List<BaseRulePeriodicTopupDto>>() {}.getType();
		List<BaseRulePeriodicTopupDto> response = modelMapper.map(listPeriodicTopups, targetType);

		return ResponseEntity.ok(response);
	}
	
	//List PeriodicTopup ById
	public ResponseEntity<BaseRulePeriodicTopupDto> getBaseRulePeriodicTopupById(Long id) {
		BaseRulePeriodicTopup baseRulePeriodicTopup = baseRulePeriodicTopupRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Periodic Topup id " + id + " is not exist"));
		BaseRulePeriodicTopupDto response = modelMapper.map(baseRulePeriodicTopup, BaseRulePeriodicTopupDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create PeriodicTopup
	public ResponseEntity<BaseRulePeriodicTopupDto> addBaseRulePeriodicTopup(CreateBaseRulePeriodicTopup newperiodicTopup) {
		BaseRulePeriodicTopup baseRulePeriodicTopup = modelMapper.map(newperiodicTopup, BaseRulePeriodicTopup.class);
		BaseRulePeriodicTopup periodicTopupSaved = baseRulePeriodicTopupRepository.save(baseRulePeriodicTopup);
		BaseRulePeriodicTopupDto response = modelMapper.map(periodicTopupSaved, BaseRulePeriodicTopupDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit PeriodicTopup
	public ResponseEntity<BaseRulePeriodicTopupDto> editBaseRulePeriodicTopup(CreateBaseRulePeriodicTopup updatePeriodicTopup,
			Long id) {
		BaseRulePeriodicTopup baseRulePeriodicTopup = baseRulePeriodicTopupRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Periodic Topup id " + id + " is not exist"));
		
		//manual map
		baseRulePeriodicTopup.setBaseRulePeriodicTopupCode(updatePeriodicTopup.getBaseRulePeriodicTopupCode());
		baseRulePeriodicTopup.setProductCode(updatePeriodicTopup.getProductCode());
		baseRulePeriodicTopup.setValutaCode(updatePeriodicTopup.getValutaCode());
		baseRulePeriodicTopup.setPaymentPeriodCode(updatePeriodicTopup.getPaymentPeriodCode());
		baseRulePeriodicTopup.setMax(updatePeriodicTopup.getMax());
		baseRulePeriodicTopup.setMin(updatePeriodicTopup.getMin());
		baseRulePeriodicTopup.setStep(updatePeriodicTopup.getStep());
		baseRulePeriodicTopup.setActive(updatePeriodicTopup.isActive());
		
		baseRulePeriodicTopupRepository.save(baseRulePeriodicTopup);
		BaseRulePeriodicTopupDto response = modelMapper.map(baseRulePeriodicTopup, BaseRulePeriodicTopupDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete PeriodicTopup
	public ResponseEntity<BaseRulePeriodicTopupDto> deleteBaseRulePeriodicTopup(Long id) {
		BaseRulePeriodicTopup baseRulePeriodicTopup = baseRulePeriodicTopupRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Periodic Topup id " + id + " is not exist"));
		baseRulePeriodicTopupRepository.deleteById(id);
		BaseRulePeriodicTopupDto response = modelMapper.map(baseRulePeriodicTopup, BaseRulePeriodicTopupDto.class);

		return ResponseEntity.ok(response);
	}	
}