package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.si.ProductRuleContractPeriod.CreateProductRuleContractPeriod;
import id.equity.nichemarket.dto.si.ProductRuleContractPeriod.ProductRuleContractPeriodDto;
import id.equity.nichemarket.model.si.ProductRuleContractPeriod;
import id.equity.nichemarket.repository.si.ProductRuleContractPeriodRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class ProductRuleContractPeriodService {
	
	@Autowired
	private ProductRuleContractPeriodRepository productRuleContractPeriodRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List listProductRuleContractPeriods
	public ResponseEntity<List<ProductRuleContractPeriodDto>> listProductRuleContractPeriod() {
		List<ProductRuleContractPeriod> listProductRuleContractPeriods = productRuleContractPeriodRepository.findAll();
		Type targetType = new TypeToken<List<ProductRuleContractPeriodDto>>() {}.getType();
		List<ProductRuleContractPeriodDto> response = modelMapper.map(listProductRuleContractPeriods, targetType);

		return ResponseEntity.ok(response);
	}
	
	//Get ProductRuleContractPeriod By Id
	public ResponseEntity<ProductRuleContractPeriodDto> getProductRuleContractPeriodById(Long id) {
		ProductRuleContractPeriod productRuleContractPeriod = productRuleContractPeriodRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Contract Period id " + id + " is not exist"));
		ProductRuleContractPeriodDto response = modelMapper.map(productRuleContractPeriod, ProductRuleContractPeriodDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create ProductRuleContractPeriod
	public ResponseEntity<ProductRuleContractPeriodDto> addProductRuleContractPeriod(CreateProductRuleContractPeriod newProductRuleContractPeriod) {
		ProductRuleContractPeriod productRuleContractPeriod = modelMapper.map(newProductRuleContractPeriod, ProductRuleContractPeriod.class);
		ProductRuleContractPeriod productRuleContractPeriodSaved = productRuleContractPeriodRepository.save(productRuleContractPeriod);
		ProductRuleContractPeriodDto response = modelMapper.map(productRuleContractPeriodSaved, ProductRuleContractPeriodDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit ProductRuleContractPeriod
	public ResponseEntity<ProductRuleContractPeriodDto> editProductRuleContractPeriod(CreateProductRuleContractPeriod updateProductRuleContractPeriod, Long id) {
		ProductRuleContractPeriod productRuleContractPeriod = productRuleContractPeriodRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Contract Period id " + id + " is not exist"));
		
		//manual map
		productRuleContractPeriod.setProductRuleContractPeriodCode(updateProductRuleContractPeriod.getProductRuleContractPeriodCode());
		productRuleContractPeriod.setProductCode(updateProductRuleContractPeriod.getProductCode());
		productRuleContractPeriod.setMinContractPeriod(updateProductRuleContractPeriod.getMinContractPeriod());
		productRuleContractPeriod.setMaxContractPeriod(updateProductRuleContractPeriod.getMaxContractPeriod());
		productRuleContractPeriod.setStepContractPeriod(updateProductRuleContractPeriod.getStepContractPeriod());
		productRuleContractPeriod.setMaxAgeContractPeriod(updateProductRuleContractPeriod.getMaxAgeContractPeriod());
		productRuleContractPeriod.setActive(updateProductRuleContractPeriod.isActive());
		
		productRuleContractPeriodRepository.save(productRuleContractPeriod);
		ProductRuleContractPeriodDto response = modelMapper.map(productRuleContractPeriod, ProductRuleContractPeriodDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete ProductRuleContractPeriod
	public ResponseEntity<ProductRuleContractPeriodDto> deleteProductRuleContractPeriod(Long id) {
		ProductRuleContractPeriod productRuleContractPeriod = productRuleContractPeriodRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Contract Period id " + id + " is not exist"));
		productRuleContractPeriodRepository.deleteById(id);
		ProductRuleContractPeriodDto response = modelMapper.map(productRuleContractPeriod, ProductRuleContractPeriodDto.class);

		return ResponseEntity.ok(response);
	}	
}