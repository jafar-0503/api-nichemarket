package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.si.BaseRuleContractPeriod.BaseRuleContractPeriodDto;
import id.equity.nichemarket.dto.si.BaseRuleContractPeriod.CreateBaseRuleContractPeriod;
import id.equity.nichemarket.model.si.BaseRuleContractPeriod;
import id.equity.nichemarket.repository.si.BaseRuleContractPeriodRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class BaseRuleContractPeriodService {
    @Autowired
    private BaseRuleContractPeriodRepository baseRuleContractPeriodRepository;

    @Autowired
    private ModelMapper modelMapper;

    //Get All Data
    public ResponseEntity<List<BaseRuleContractPeriodDto>> listBaseRuleContractPeriod() {
        List<BaseRuleContractPeriod> lsBaseRuleContractPeriod= baseRuleContractPeriodRepository.findAll();
        Type targetType = new TypeToken<List<BaseRuleContractPeriodDto>>() {}.getType();
        List<BaseRuleContractPeriodDto> response = modelMapper.map(lsBaseRuleContractPeriod, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<BaseRuleContractPeriodDto> getBaseRuleContractPeriodyId(Long id) {
        BaseRuleContractPeriod baseRuleContractPeriod = baseRuleContractPeriodRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Contract Period id " + id + " is not exist"));
        BaseRuleContractPeriodDto response = modelMapper.map(baseRuleContractPeriod, BaseRuleContractPeriodDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<BaseRuleContractPeriodDto> addBaseRuleContractPeriod(CreateBaseRuleContractPeriod newBaseRuleContractPeriod) {
        BaseRuleContractPeriod bRIType = modelMapper.map(newBaseRuleContractPeriod, BaseRuleContractPeriod.class);
        BaseRuleContractPeriod bRITypeSaved = baseRuleContractPeriodRepository.save(bRIType);
        BaseRuleContractPeriodDto response = modelMapper.map(bRITypeSaved, BaseRuleContractPeriodDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<BaseRuleContractPeriodDto> editBaseRuleContractPeriod(CreateBaseRuleContractPeriod updateBaseRuleContractPeriod, Long id) {
        BaseRuleContractPeriod baseRuleContractPeriod = baseRuleContractPeriodRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Contract Period id " + id + " is not exist"));
        baseRuleContractPeriod.setBaseRuleContractPeriodCode(updateBaseRuleContractPeriod.getBaseRuleContractPeriodCode());
        baseRuleContractPeriod.setProductCode(updateBaseRuleContractPeriod.getProductCode());
        baseRuleContractPeriod.setPaymentPeriodCode(updateBaseRuleContractPeriod.getPaymentPeriodCode());
        baseRuleContractPeriod.setMin(updateBaseRuleContractPeriod.getMin());
        baseRuleContractPeriod.setMax(updateBaseRuleContractPeriod.getMax());
        baseRuleContractPeriod.setStep(updateBaseRuleContractPeriod.getStep());
        baseRuleContractPeriod.setMaxAge(updateBaseRuleContractPeriod.getMaxAge());
        baseRuleContractPeriod.setActive(updateBaseRuleContractPeriod.isActive());
        baseRuleContractPeriodRepository.save(baseRuleContractPeriod);
        BaseRuleContractPeriodDto response = modelMapper.map(baseRuleContractPeriod, BaseRuleContractPeriodDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data By Id
    public ResponseEntity<BaseRuleContractPeriodDto> deleteBaseRuleContractPeriod(Long id) {
        BaseRuleContractPeriod baseRuleContractPeriod = baseRuleContractPeriodRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Contract Period id " + id + " is not exist"));
        baseRuleContractPeriodRepository.deleteById(id);
        BaseRuleContractPeriodDto response = modelMapper.map(baseRuleContractPeriod, BaseRuleContractPeriodDto.class);

        return ResponseEntity.ok(response);
    }
}