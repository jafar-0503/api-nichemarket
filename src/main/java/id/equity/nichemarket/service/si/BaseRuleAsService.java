package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.si.BaseRuleAs.BaseRuleAsDto;
import id.equity.nichemarket.dto.si.BaseRuleAs.CreateBaseRuleAs;
import id.equity.nichemarket.model.si.BaseRuleAs;
import id.equity.nichemarket.repository.si.BaseRuleAsRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class BaseRuleAsService {
	
	@Autowired
	private BaseRuleAsRepository baseRuleAsRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List BaseRuleAs
	public ResponseEntity<List<BaseRuleAsDto>> listBaseRuleAs() {
		List<BaseRuleAs> listBaseRuleAss = baseRuleAsRepository.findAll();
		Type targetType = new TypeToken<List<BaseRuleAsDto>>() {}.getType();
		List<BaseRuleAsDto> response = modelMapper.map(listBaseRuleAss, targetType);

		return ResponseEntity.ok(response);
	}
	
	//Get BaseRuleAs By Id
	public ResponseEntity<BaseRuleAsDto> getBaseRuleAsById(Long id) {
		BaseRuleAs baseRuleAs = baseRuleAsRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule As id ®" + id + " is not exist"));
		BaseRuleAsDto response = modelMapper.map(baseRuleAs, BaseRuleAsDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create BaseRuleAs
	public ResponseEntity<BaseRuleAsDto> addBaseRuleAs(CreateBaseRuleAs newBaseRuleAs) {
		BaseRuleAs baseRuleAs = modelMapper.map(newBaseRuleAs, BaseRuleAs.class);
		BaseRuleAs baseRuleAsSaved = baseRuleAsRepository.save(baseRuleAs);
		BaseRuleAsDto response = modelMapper.map(baseRuleAsSaved, BaseRuleAsDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit BaseRuleAs
	public ResponseEntity<BaseRuleAsDto> editBaseRuleAs(CreateBaseRuleAs updateBaseruleAs, Long id) {
		BaseRuleAs baseRuleAs = baseRuleAsRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule As id " + id + " is not exist"));

		//manual map
		baseRuleAs.setBaseRuleAsCode(updateBaseruleAs.getBaseRuleAsCode());
		baseRuleAs.setProductCode(updateBaseruleAs.getProductCode());
		baseRuleAs.setAsCode(updateBaseruleAs.getAsCode());
		baseRuleAs.setMinAge(updateBaseruleAs.getMinAge());
		baseRuleAs.setMaxAge(updateBaseruleAs.getMaxAge());
		baseRuleAs.setActive(updateBaseruleAs.isActive());
		
		
		baseRuleAsRepository.save(baseRuleAs);
		BaseRuleAsDto response = modelMapper.map(baseRuleAs, BaseRuleAsDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete BaseRuleAs
	public ResponseEntity<BaseRuleAsDto> deleteBaseRuleAs(Long id) {
		BaseRuleAs baseRuleAs = baseRuleAsRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule As id " + id + " is not exist"));
		baseRuleAsRepository.deleteById(id);
		BaseRuleAsDto response = modelMapper.map(baseRuleAs, BaseRuleAsDto.class);

		return ResponseEntity.ok(response);
	}
}