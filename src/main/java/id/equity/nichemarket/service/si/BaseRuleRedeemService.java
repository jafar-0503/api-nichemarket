package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.BaseRuleRedeem.BaseRuleRedeemDto;
import id.equity.nichemarket.dto.si.BaseRuleRedeem.CreateBaseRuleRedeem;
import id.equity.nichemarket.model.si.BaseRuleRedeem;
import id.equity.nichemarket.repository.si.BaseRuleRedeemRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class BaseRuleRedeemService {
	
	@Autowired
	private BaseRuleRedeemRepository baseRuleRedeemRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List Redeem
	public ResponseEntity<List<BaseRuleRedeemDto>> listBaseRuleRedeem() {
		List<BaseRuleRedeem> listBaseRuleRedeems = baseRuleRedeemRepository.findAll();
		Type targetType = new TypeToken<List<BaseRuleRedeemDto>>() {}.getType();
		List<BaseRuleRedeemDto> response = modelMapper.map(listBaseRuleRedeems, targetType);

		return ResponseEntity.ok(response);
	}
	
	//Get Redeem By Id
	public ResponseEntity<BaseRuleRedeemDto> getBaseRuleRedeemById(Long id) {
		BaseRuleRedeem baseRuleRedeem = baseRuleRedeemRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Redeem id " + id + " is not exist"));
		BaseRuleRedeemDto response = modelMapper.map(baseRuleRedeem, BaseRuleRedeemDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create Redeem
	public ResponseEntity<BaseRuleRedeemDto> addBaseRuleRedeem(CreateBaseRuleRedeem newRedeem) {
		BaseRuleRedeem baseRuleRedeem = modelMapper.map(newRedeem, BaseRuleRedeem.class);
		BaseRuleRedeem BaseRuleRedeemSaved = baseRuleRedeemRepository.save(baseRuleRedeem);
		BaseRuleRedeemDto response = modelMapper.map(BaseRuleRedeemSaved, BaseRuleRedeemDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit Redeem
	public ResponseEntity<BaseRuleRedeemDto> editBaseRuleRedeem(CreateBaseRuleRedeem updateRedeem, Long id) {
		BaseRuleRedeem baseRuleRedeem = baseRuleRedeemRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Redeem id " + id + " is not exist"));

		//manual map
		baseRuleRedeem.setBaseRuleRedeemCode(updateRedeem.getBaseRuleRedeemCode());
		baseRuleRedeem.setProductCode(updateRedeem.getProductCode());
		baseRuleRedeem.setYearTh(updateRedeem.getYearTh());
		baseRuleRedeem.setAgeTh(updateRedeem.getAgeTh());
		baseRuleRedeem.setPercent(updateRedeem.getPercent());
		baseRuleRedeem.setActive(updateRedeem.isActive());
		
		baseRuleRedeemRepository.save(baseRuleRedeem);
		BaseRuleRedeemDto response = modelMapper.map(baseRuleRedeem, BaseRuleRedeemDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete Redeem
	public ResponseEntity<BaseRuleRedeemDto> deleteBaseRuleRedeem(Long id) {
		BaseRuleRedeem baseRuleRedeem = baseRuleRedeemRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Redeem id " + id + " is not exist"));
		baseRuleRedeemRepository.deleteById(id);
		BaseRuleRedeemDto response = modelMapper.map(baseRuleRedeem, BaseRuleRedeemDto.class);

		return ResponseEntity.ok(response);
	}
}