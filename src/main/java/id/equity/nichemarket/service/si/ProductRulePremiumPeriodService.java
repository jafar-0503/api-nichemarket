package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.si.ProductRulePremiumPeriod.CreateProductRulePremiumPeriod;
import id.equity.nichemarket.dto.si.ProductRulePremiumPeriod.ProductRulePremiumPeriodDto;
import id.equity.nichemarket.model.si.ProductRulePremiumPeriod;
import id.equity.nichemarket.repository.si.ProductRulePremiumPeriodRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class ProductRulePremiumPeriodService {
	
	@Autowired
	private ProductRulePremiumPeriodRepository productRulePremiumPeriodRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List ProductRulePremiumPeriod
	public ResponseEntity<List<ProductRulePremiumPeriodDto>> listProductRulePremiumPeriod() {
		List<ProductRulePremiumPeriod> listProductRulePremiumPeriods = productRulePremiumPeriodRepository.findAll();
		Type targetType = new TypeToken<List<ProductRulePremiumPeriodDto>>() {}.getType();
		List<ProductRulePremiumPeriodDto> response = modelMapper.map(listProductRulePremiumPeriods, targetType);

		return ResponseEntity.ok(response);
	}
	
	//Get ProductRulePremiumPeriod By Id
	public ResponseEntity<ProductRulePremiumPeriodDto> getProductRulePremiumPeriodById(Long id) {
		ProductRulePremiumPeriod productRulePremiumPeriod = productRulePremiumPeriodRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Premium Period id " + id + " is not exist"));
		ProductRulePremiumPeriodDto response = modelMapper.map(productRulePremiumPeriod, ProductRulePremiumPeriodDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create ProductRulePremiumPeriod
	public ResponseEntity<ProductRulePremiumPeriodDto> addProductRulePremiumPeriod(CreateProductRulePremiumPeriod newProductRulePremiumPeriod) {
		ProductRulePremiumPeriod productRulePremiumPeriod = modelMapper.map(newProductRulePremiumPeriod, ProductRulePremiumPeriod.class);
		ProductRulePremiumPeriod productRulePremiumPeriodSaved = productRulePremiumPeriodRepository.save(productRulePremiumPeriod);
		ProductRulePremiumPeriodDto response = modelMapper.map(productRulePremiumPeriodSaved, ProductRulePremiumPeriodDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit ProductRulePremiumPeriod
	public ResponseEntity<ProductRulePremiumPeriodDto> editProductRulePremiumPeriod(CreateProductRulePremiumPeriod updateProductRulePremiumPeriod, Long id) {
		ProductRulePremiumPeriod productRulePremiumPeriod = productRulePremiumPeriodRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Premium Period id " + id + " is not exist"));
		
		//manual map
		productRulePremiumPeriod.setProductRulePremiumPeriodCode(updateProductRulePremiumPeriod.getProductRulePremiumPeriodCode());
		productRulePremiumPeriod.setProductCode(updateProductRulePremiumPeriod.getProductCode());
		productRulePremiumPeriod.setMinPremiumPeriod(updateProductRulePremiumPeriod.getMinPremiumPeriod());
		productRulePremiumPeriod.setMaxPremiumPeriod(updateProductRulePremiumPeriod.getMaxPremiumPeriod());
		productRulePremiumPeriod.setStepPremiumPeriod(updateProductRulePremiumPeriod.getStepPremiumPeriod());
		productRulePremiumPeriod.setMaxAgePremiumPeriod(updateProductRulePremiumPeriod.getMaxAgePremiumPeriod());
		productRulePremiumPeriod.setActive(updateProductRulePremiumPeriod.isActive());
		
		productRulePremiumPeriodRepository.save(productRulePremiumPeriod);
		ProductRulePremiumPeriodDto response = modelMapper.map(productRulePremiumPeriod, ProductRulePremiumPeriodDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete ProductRuleContractPeriod
	public ResponseEntity<ProductRulePremiumPeriodDto> deleteProductRulePremiumPeriod(Long id) {
		ProductRulePremiumPeriod productRulePremiumPeriod = productRulePremiumPeriodRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Premium Period id " + id + " is not exist"));
		productRulePremiumPeriodRepository.deleteById(id);
		ProductRulePremiumPeriodDto response = modelMapper.map(productRulePremiumPeriod, ProductRulePremiumPeriodDto.class);

		return ResponseEntity.ok(response);
	}	
}