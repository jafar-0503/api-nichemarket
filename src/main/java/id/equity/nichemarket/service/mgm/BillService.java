package id.equity.nichemarket.service.mgm;

import java.util.List;
import java.lang.reflect.Type;


import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.mgm.bill.BillDto;
import id.equity.nichemarket.exception.ErrorsException;
import id.equity.nichemarket.model.mgm.Bill;
import id.equity.nichemarket.repository.mgm.BillRepository;

@Service
public class BillService {

	@Autowired
	private BillRepository billRepository;
	
	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private EntityManager em;
	
	//List Bill
	public ResponseEntity<List<BillDto>> listBill(){
		List<Bill> lsBill= billRepository.findAll();
		Type targetType = new TypeToken<List<BillDto>>() {}.getType();
		List<BillDto> response = modelMapper.map(lsBill, targetType);

		return ResponseEntity.ok(response);
	}

	//Get Bill By Id
	public ResponseEntity<BillDto> getBillById(Long id) {
		Bill bill = billRepository.findById(id).orElseThrow(()-> new NotFoundException("Bill id " + id + " is not exist"));
		BillDto response = modelMapper.map(bill, BillDto.class);

		return ResponseEntity.ok(response);
	}

	

	//Post Bill
	@Transactional
	public ResponseEntity<BillDto> addBill(BillDto newBill) {
		Bill bill = modelMapper.map(newBill, Bill.class);
		Bill billSaved = billRepository.save(bill);
		em.refresh(billSaved);
		BillDto response = modelMapper.map(billSaved, BillDto.class);
		return ResponseEntity.ok(response);
	}
	
	@Transactional
	public BillDto saveBill(BillDto newBill) {
		Bill bill = modelMapper.map(newBill, Bill.class);
		bill.setActive(true);
		Bill billSaved = billRepository.save(bill);
		em.refresh(billSaved);
		BillDto response = modelMapper.map(billSaved, BillDto.class);
		return response;
	}

	//Put Bill
	public ResponseEntity<BillDto> editBill(BillDto updateBill, Long id) {
		Bill bill = billRepository.findById(id).orElseThrow(()-> new NotFoundException("Bill id " + id + " is not exist"));
		bill.setBillCode(updateBill.getBillCode());
		bill.setRegistrationCode(updateBill.getRegistrationCode());
		bill.setAmount(updateBill.getAmount());
		bill.setActive(updateBill.isActive());
		billRepository.save(bill);
		BillDto response = modelMapper.map(bill, BillDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete Bill	
	public ResponseEntity<BillDto> deleteBill(Long id) {
		Bill findBill = billRepository.findById(id).orElseThrow(()-> new NotFoundException("Bill id " + id + " is not exist"));
		billRepository.deleteById(id);
		BillDto response = modelMapper.map(findBill, BillDto.class);

		return ResponseEntity.ok(response);
	}

	//Get Bill By billCode
	public BillDto getBillByCode(String billCode) {
		Bill bill = billRepository.findByBillCode(billCode);
		if(bill == null) {
			throw new ErrorsException("Bill code " + billCode + " is not exist");
		}
		
		return modelMapper.map(bill, BillDto.class);
	}

	
}