package id.equity.nichemarket.service.mgm;
import java.util.List;
import java.lang.reflect.Type;


import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.mgm.gift.GiftDto;
import id.equity.nichemarket.exception.ErrorsException;
import id.equity.nichemarket.model.mgm.Gift;
import id.equity.nichemarket.repository.mgm.GiftRepository;

@Service
public class GiftService {

	private static final String REFERENCING_GIFT_CODE = "REF01";
	private static final String REFERENCED_GIFT_CODE = "REF02";

	@Autowired
	private GiftRepository giftRepository;
	
	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private EntityManager em;
	
	//List Gift
	public ResponseEntity<List<GiftDto>> listGift(){
		List<Gift> lsGift= giftRepository.findAll();
		Type targetType = new TypeToken<List<GiftDto>>() {}.getType();
		List<GiftDto> response = modelMapper.map(lsGift, targetType);

		return ResponseEntity.ok(response);
	}

	//Get Gift By Id
	public ResponseEntity<GiftDto> getGiftById(Long id) {
		Gift gift = giftRepository.findById(id).orElseThrow(()-> new NotFoundException("Gift id " + id + " is not exist"));
		GiftDto response = modelMapper.map(gift, GiftDto.class);

		return ResponseEntity.ok(response);
	}	

	//Post Gift
	@Transactional
	public ResponseEntity<GiftDto> addGift(GiftDto newGift) {
		Gift gift = modelMapper.map(newGift, Gift.class);
		Gift giftSaved = giftRepository.save(gift);
		em.refresh(giftSaved);
		GiftDto response = modelMapper.map(giftSaved, GiftDto.class);
		return ResponseEntity.ok(response);
	}
	
	@Transactional
	public GiftDto saveGift(GiftDto newGift) {
		Gift gift = modelMapper.map(newGift, Gift.class);
		gift.setActive(true);
		Gift giftSaved = giftRepository.save(gift);
		em.refresh(giftSaved);
		GiftDto response = modelMapper.map(giftSaved, GiftDto.class);
		return response;
	}

	//Put Gift
	public ResponseEntity<GiftDto> editGift(GiftDto updateGift, Long id) {
		Gift gift = giftRepository.findById(id).orElseThrow(()-> new NotFoundException("Gift id " + id + " is not exist"));
		gift.setGiftCode(updateGift.getGiftCode());
		gift.setAmount(updateGift.getAmount());
		gift.setActive(updateGift.isActive());
		giftRepository.save(gift);
		GiftDto response = modelMapper.map(gift, GiftDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete Gift	
	public ResponseEntity<GiftDto> deleteGift(Long id) {
		Gift findGift = giftRepository.findById(id).orElseThrow(()-> new NotFoundException("Gift id " + id + " is not exist"));
		giftRepository.deleteById(id);
		GiftDto response = modelMapper.map(findGift, GiftDto.class);

		return ResponseEntity.ok(response);
	}

	//Get Gift By giftCode
	public GiftDto getGiftByCode(String giftCode) {
		Gift gift = giftRepository.findByGiftCode(giftCode);
		if(gift == null) {
			throw new ErrorsException("Gift code " + giftCode + " is not exit");
		}
		
		return modelMapper.map(gift, GiftDto.class);
	}

	public GiftDto getReferencingGift() {
		// TODO Auto-generated method stub
		Gift gift = giftRepository.findByGiftCode(REFERENCING_GIFT_CODE);
		return modelMapper.map(gift, GiftDto.class);
	}
	
	public GiftDto getReferencedGift() {
		// TODO Auto-generated method stub
		Gift gift = giftRepository.findByGiftCode(REFERENCED_GIFT_CODE);
		return modelMapper.map(gift, GiftDto.class);
	}

	
}