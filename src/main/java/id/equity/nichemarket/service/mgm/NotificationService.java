package id.equity.nichemarket.service.mgm;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.google.gson.JsonObject;

import id.equity.nichemarket.dto.mgm.account.AccountCustomerDto;
import id.equity.nichemarket.dto.mgm.customer.CustomerDto;
import id.equity.nichemarket.model.mgm.FailedAsyncJobs;
import id.equity.nichemarket.retrofit.mgm.EmailNotificationResponse;
import id.equity.nichemarket.retrofit.mgm.EmailNotificationService;

@Service
public class NotificationService {
    private static final Logger logger = LoggerFactory.getLogger(NotificationService.class);

	
	@Autowired
	private EmailObjGeneratorService emailObjGeneratorService;
	
	@Autowired
	private EmailNotificationService emailNotificationService;
	
	@Autowired
	private AsyncJobService asyncJobService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Async
	public EmailNotificationResponse activationAccountNotif(CustomerDto customer , AccountCustomerDto account) throws InterruptedException {
		logger.info("SENDING EMAIL..." );
	    // Artificial delay of 1s for demonstration purposes
	   JsonObject emailObjParam = emailObjGeneratorService.generateActivationAccount(customer, account.getUniqueActivationKey());
	   EmailNotificationResponse emailSend = null;
	   try {
		   emailSend = emailNotificationService.sendEmailNotif(emailObjParam);

	   } catch (Exception e ) {
		   e.printStackTrace();
		   
		   //Save If any exception here
		   StringWriter sw = new StringWriter();
           e.printStackTrace(new PrintWriter(sw));           
           String exceptionAsString = sw.toString();
		   
		   FailedAsyncJobs failedAsyncJobs = new FailedAsyncJobs();
		   failedAsyncJobs.setPayload(emailObjParam.toString());
		   failedAsyncJobs.setResponse("failed");
		   failedAsyncJobs.setException(exceptionAsString);
		   failedAsyncJobs.setCauseException(e.getCause().toString());
		   failedAsyncJobs.setMethod("activationAccountNotif");
		   
		   asyncJobService.saveFailedJob(failedAsyncJobs);
	   }
	   

	   
	   return emailSend;
	}
	
}
