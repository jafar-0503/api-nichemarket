package id.equity.nichemarket.service.mgm;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.dto.mgm.account.AccountCustomerDto;
import id.equity.nichemarket.dto.mgm.account.RedeemationPointRequestDto;
import id.equity.nichemarket.dto.mgm.account.RedeemationPointResponseDto;
import id.equity.nichemarket.dto.mgm.account.ValidationRedeemResponseDto;
import id.equity.nichemarket.dto.mgm.customer.CustomerDto;
import id.equity.nichemarket.dto.mgm.customerPoint.CustomerPointDto;
import id.equity.nichemarket.dto.mgm.gift.GiftDto;
import id.equity.nichemarket.exception.ErrorsException;

@Service
public class RedeemService {

	@Autowired
	private CustomerPointService customerPointService;
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private GiftService giftService;
	
	@Transactional
	public ResponseEntity<RedeemationPointResponseDto> redeem(RedeemationPointRequestDto redeemationPoint) {

		
		//Get Customer 
		AccountCustomerDto account = accountService.getAccountByEmailAddress(redeemationPoint.getEmailAddress());
		//Get Current Point & Point Needed		
		CustomerPointDto currCustomerPoint = customerPointService.getCustomerPointByAccountCode(account.getAccountCode());
		GiftDto gift = giftService.getGiftByCode(redeemationPoint.getGiftCode());					
		if(currCustomerPoint.getCurrentPoint() < gift.getPointUsed()) {
			throw new ErrorsException("Insufficient Point");
		}
		
		//Insert Into Customer Point Out
		CustomerPointDto customerPoint = new CustomerPointDto();
		customerPoint.setAccountCode(account.getAccountCode());
		customerPoint.setPointOut(gift.getPointUsed());
		customerPoint.setDescription(redeemationPoint.getGiftCode());
		customerPoint.setActive(true);
				
		CustomerPointDto customerPointSaved = customerPointService.addCustomerPoint(customerPoint);
		
		//Create Header For Calling IRIS API
			//Generate header unique uuid
			//Set transactionpointcode
			//Save header point transaction
		
		//Create IRIS Benificary into t_jobs transaction 
			//Set header uuid
			//Set idempotent id 
			//Set payload
				//Create Payload 
					//				{
					//				    "name": "ferry 2",
					//				    "account": "3223462784",
					//				    "bank": "gopay",
					//				    "alias_name": "tekg",
					//				    "email": "beneficiary@example.com"
					//				}
			//Set status
		
		//Create IRIS Payout into t_jobs transaction 
			//Set header uuid
			//Set idempotent id 
			//Set payload
				//Create Payload
					//		{
					//			  "payouts": [
					//			    {
					//			      "beneficiary_name": "endiaz",
					//			      "beneficiary_account": "08568585234",
					//			      "beneficiary_bank": "gopay",
					//			      "beneficiary_email": "beneficiary@example.com",
					//			      "amount": "10000.00",
					//			      "notes": "Test payout"
					//			    }
					//			  ]
					//		}
			//Set status

		//Create IRIS Approve Payout into t_jobs transaction
			//Set header uuid
			//Set idempotent id 
			//Set payload
					//		{
					//		    "reference_nos": [
					//		        "kuhpvtmf9ybydjmeey"       
					//		    ],
					//		    "otp": "33232"
					//		}
			//Set status

		//====================== CALL WITH ASYNC ================
		//Call API IRIS Benificiary
			//If success or failed update t_jobs
		
		//Call API IRIS Payout		
			//If success or failed update t_jobs
		
		//Call API IRIS Approve
			//If success or failed update t_jobs
				
		RedeemationPointResponseDto response = new RedeemationPointResponseDto();
		response.setGiftCode(gift.getGiftCode());
		response.setCurrentPoint(customerPointSaved.getCurrentPoint());
		response.setUsedPoint(gift.getPointUsed());
		response.setEmailAddress(redeemationPoint.getEmailAddress());		
				
		return ResponseEntity.ok(response);
	}
	
	@Transactional
	public ResponseEntity<ValidationRedeemResponseDto> validateRedeem(RedeemationPointRequestDto redeemationPoint) {
		//Get Customer 
		AccountCustomerDto account = accountService.getAccountByEmailAddress(redeemationPoint.getEmailAddress());
		//Get Current Point & Point Needed		
		CustomerPointDto currCustomerPoint = customerPointService.getCustomerPointByAccountCode(account.getAccountCode());
		GiftDto gift = giftService.getGiftByCode(redeemationPoint.getGiftCode());					
		if(currCustomerPoint.getCurrentPoint() < gift.getPointUsed()) {
			throw new ErrorsException("Insufficient Point");
		}
		
		ValidationRedeemResponseDto response = new ValidationRedeemResponseDto();
		response.setValid(true);
		
		return ResponseEntity.ok(response);
	}
	
	

}
