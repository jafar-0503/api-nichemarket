package id.equity.nichemarket.service.mgm;

import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.mgm.productPlanPolicy.ProductPlanPolicyDto;
import id.equity.nichemarket.exception.ErrorsException;
import id.equity.nichemarket.model.mgm.ProductPlanPolicy;
import id.equity.nichemarket.repository.mgm.ProductPlanPolicyRepository;

@Service
public class ProductPlanPolicyService {
	@Autowired
	private ProductPlanPolicyRepository productPlanPolicyRepository;
	
	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private EntityManager em;
	
	//List ProductPlanPolicy
	public ResponseEntity<List<ProductPlanPolicyDto>> listProductPlanPolicy(){
		List<ProductPlanPolicy> lsProductPlanPolicy= productPlanPolicyRepository.findAll();
		Type targetType = new TypeToken<List<ProductPlanPolicyDto>>() {}.getType();
		List<ProductPlanPolicyDto> response = modelMapper.map(lsProductPlanPolicy, targetType);

		return ResponseEntity.ok(response);
	}

	//Get ProductPlanPolicy By Id
	public ResponseEntity<ProductPlanPolicyDto> getProductPlanPolicyById(Long id) {
		ProductPlanPolicy productPlanPolicy = productPlanPolicyRepository.findById(id).orElseThrow(()-> new NotFoundException("ProductPlanPolicy id " + id + " is not exist"));
		ProductPlanPolicyDto response = modelMapper.map(productPlanPolicy, ProductPlanPolicyDto.class);

		return ResponseEntity.ok(response);
	}

	

	//Post ProductPlanPolicy
	@Transactional
	public ResponseEntity<ProductPlanPolicyDto> addProductPlanPolicy(ProductPlanPolicyDto newProductPlanPolicy) {
		ProductPlanPolicy productPlanPolicy = modelMapper.map(newProductPlanPolicy, ProductPlanPolicy.class);
		ProductPlanPolicy productPlanPolicySaved = productPlanPolicyRepository.save(productPlanPolicy);
		em.refresh(productPlanPolicySaved);
		ProductPlanPolicyDto response = modelMapper.map(productPlanPolicySaved, ProductPlanPolicyDto.class);
		return ResponseEntity.ok(response);
	}
	
	@Transactional
	public ProductPlanPolicyDto saveProductPlanPolicy(ProductPlanPolicyDto newProductPlanPolicy) {
		ProductPlanPolicy productPlanPolicy = modelMapper.map(newProductPlanPolicy, ProductPlanPolicy.class);
		productPlanPolicy.setActive(true);
		ProductPlanPolicy productPlanPolicySaved = productPlanPolicyRepository.save(productPlanPolicy);
		em.refresh(productPlanPolicySaved);
		ProductPlanPolicyDto response = modelMapper.map(productPlanPolicySaved, ProductPlanPolicyDto.class);
		return response;
	}

	//Put ProductPlanPolicy
	public ResponseEntity<ProductPlanPolicyDto> editProductPlanPolicy(ProductPlanPolicyDto updateProductPlanPolicy, Long id) {
		ProductPlanPolicy productPlanPolicy = productPlanPolicyRepository.findById(id).orElseThrow(()-> new NotFoundException("ProductPlanPolicy id " + id + " is not exist"));

		productPlanPolicy.setActive(updateProductPlanPolicy.isActive());
		productPlanPolicyRepository.save(productPlanPolicy);
		ProductPlanPolicyDto response = modelMapper.map(productPlanPolicy, ProductPlanPolicyDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete ProductPlanPolicy	
	public ResponseEntity<ProductPlanPolicyDto> deleteProductPlanPolicy(Long id) {
		ProductPlanPolicy findProductPlanPolicy = productPlanPolicyRepository.findById(id).orElseThrow(()-> new NotFoundException("ProductPlanPolicy id " + id + " is not exist"));
		productPlanPolicyRepository.deleteById(id);
		ProductPlanPolicyDto response = modelMapper.map(findProductPlanPolicy, ProductPlanPolicyDto.class);

		return ResponseEntity.ok(response);
	}

	//Get ProductPlanPolicy By productPlanPolicyCode
	public ProductPlanPolicyDto getProductPlanPolicyByRegTypeCodeAndType(String registrationTypeCode, Integer type) {
		ProductPlanPolicy productPlanPolicy = productPlanPolicyRepository.findByRegTypeCodeAndType(registrationTypeCode, type);
		if(productPlanPolicy == null) {
			throw new NotFoundException("ProductPlanPolicy code " + registrationTypeCode + " and type " + type + "is not exist");
		}
		
		return modelMapper.map(productPlanPolicy, ProductPlanPolicyDto.class);
	}
}
