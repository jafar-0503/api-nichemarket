package id.equity.nichemarket.service.mgm;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.mgm.recipient.CreateRecipient;
import id.equity.nichemarket.dto.mgm.recipient.RecipientDto;
import id.equity.nichemarket.model.mgm.Recipient;
import id.equity.nichemarket.repository.mgm.RecipientRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class RecipientService {

	@Autowired
	private RecipientRepository recipientRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List Recipient
	public ResponseEntity<List<RecipientDto>> listRecipient(){
		List<Recipient> lsRecipient= recipientRepository.findAll();
		Type targetType = new TypeToken<List<RecipientDto>>() {}.getType();
		List<RecipientDto> response = modelMapper.map(lsRecipient, targetType);

		return ResponseEntity.ok(response);
	}

	//Get Recipient By Id
	public ResponseEntity<RecipientDto> getRecipientById(Long id) {
		Recipient recipient = recipientRepository.findById(id).orElseThrow(()-> new NotFoundException("Recipient id " + id + " is not exist"));
		RecipientDto response = modelMapper.map(recipient, RecipientDto.class);

		return ResponseEntity.ok(response);
	}

	//Post Recipient
	public ResponseEntity<RecipientDto> addRecipient(CreateRecipient newRecipient) {
		Recipient recipient = modelMapper.map(newRecipient, Recipient.class);
		Recipient recipientSaved = recipientRepository.save(recipient);
		RecipientDto response = modelMapper.map(recipientSaved, RecipientDto.class);

		return ResponseEntity.ok(response);
	}

	//Put Recipient
	public ResponseEntity<RecipientDto> editRecipient(CreateRecipient updateRecipient, Long id) {
		Recipient recipient = recipientRepository.findById(id).orElseThrow(()-> new NotFoundException("Recipient id " + id + " is not exist"));
		recipient.setRecipientCode(updateRecipient.getRecipientCode());
		recipient.setRecipientName(updateRecipient.getRecipientName());
		recipient.setEmailAddress(updateRecipient.getEmailAddress());
		recipient.setRecipientTypeCode(updateRecipient.getRecipientTypeCode());
		recipient.setPartnerName(updateRecipient.getPartnerName());
		recipient.setEmailTypeCode(updateRecipient.getEmailTypeCode());
		recipientRepository.save(recipient);
		RecipientDto response = modelMapper.map(recipient, RecipientDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete Recipient	
	public ResponseEntity<RecipientDto> deleteRecipient(Long id) {
		Recipient findRecipient = recipientRepository.findById(id).orElseThrow(()-> new NotFoundException("Recipient id " + id + " is not exist"));
		recipientRepository.deleteById(id);
		RecipientDto response = modelMapper.map(findRecipient, RecipientDto.class);

		return ResponseEntity.ok(response);
	}
}