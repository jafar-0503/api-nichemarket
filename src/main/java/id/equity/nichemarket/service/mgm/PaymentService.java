package id.equity.nichemarket.service.mgm;

import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.mgm.account.AccountCustomerDto;
import id.equity.nichemarket.dto.mgm.bill.BillDto;
import id.equity.nichemarket.dto.mgm.customer.CustomerDto;
import id.equity.nichemarket.dto.mgm.customerPoint.CustomerPointDto;
import id.equity.nichemarket.dto.mgm.customerPolicy.CustomerPolicyDto;
import id.equity.nichemarket.dto.mgm.customerReferral.CustomerReferralDto;
import id.equity.nichemarket.dto.mgm.gift.GiftDto;
import id.equity.nichemarket.dto.mgm.payment.PaymentDto;
import id.equity.nichemarket.dto.mgm.payment.PaymentRequestDto;
import id.equity.nichemarket.dto.mgm.payment.PaymentResponseDto;
import id.equity.nichemarket.dto.mgm.payment.PaymentSuccessResponseDto;
import id.equity.nichemarket.dto.mgm.registration.RegistrationDto;
import id.equity.nichemarket.dto.mgm.registrationType.RegistrationTypeDto;
import id.equity.nichemarket.exception.ErrorsException;
import id.equity.nichemarket.model.mgm.Customer;
import id.equity.nichemarket.model.mgm.CustomerReferral;
import id.equity.nichemarket.model.mgm.Payment;
import id.equity.nichemarket.repository.mgm.PaymentRepository;
import id.equity.nichemarket.retrofit.mgm.EmailNotificationResponse;
import id.equity.nichemarket.retrofit.mgm.freeCovid.BaseFreeCovidResponse;
import id.equity.nichemarket.retrofit.mgm.freeCovid.FreeCovidCertificateResponse;


@Service
public class PaymentService {

	@Autowired
	private PaymentRepository paymentRepository;
	
	@Autowired
	private CustomerService customerService;

	@Autowired
	private CustomerRegistrationService customerRegistrationService;
	
	@Autowired
	private RegistrationService registrationService;
	
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private CustomerPolicyService customerPolicyService;
	
	@Autowired
	private GroupInsuranceService groupInsuranceService;
	
	@Autowired
	private RegistrationTypeService registrationTypeService;
	
	@Autowired
	private CustomerReferralService customerReferralService;
	
	@Autowired
	private CustomerPointService customerPointService;
	
	@Autowired
	private BillService billService;
	
	@Autowired
	private NotificationService notificationService;
	
	@Autowired
	private GiftService giftService;
	
	@Autowired
	private AppService appService;
	
	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private EntityManager em;
	
    private static final Logger logger = LoggerFactory.getLogger(PaymentService.class);

	
	//List Payment
	public ResponseEntity<List<PaymentDto>> listPayment(){
		List<Payment> lsPayment= paymentRepository.findAll();
		Type targetType = new TypeToken<List<PaymentDto>>() {}.getType();
		List<PaymentDto> response = modelMapper.map(lsPayment, targetType);

		return ResponseEntity.ok(response);
	}

	//Get Payment By Id
	public ResponseEntity<PaymentDto> getPaymentById(Long id) {
		Payment payment = paymentRepository.findById(id).orElseThrow(()-> new NotFoundException("Payment id " + id + " is not exist"));
		PaymentDto response = modelMapper.map(payment, PaymentDto.class);

		return ResponseEntity.ok(response);
	}


	//Post Payment
//	@Transactional
	public ResponseEntity<PaymentSuccessResponseDto> addPayment(PaymentRequestDto newPayment) throws Exception {
		
		//Check If Bill Exist 
		BillDto bill = billService.getBillByCode(newPayment.getBillCode());
		
		//Check If Payment Haven't paid
		Payment existPayment = paymentRepository.findByBillCode(newPayment.getBillCode());
		if(existPayment != null) {
			logger.warn("Bill " + newPayment.getBillCode() + " already paid");
			throw new ErrorsException("Bill already paid");
		}
		
		//Validate Amount 
		validatePaymentAmount(newPayment);
		
		//Get Account, Bill, Registration , & Customer Primary By RegCode
		RegistrationDto registration = registrationService.getRegistrationByCode(bill.getRegistrationCode());
		AccountCustomerDto account = accountService.getAccountByCode(registration.getAccountCode());
		List<CustomerDto> allCustomer = customerRegistrationService.getAllCustomerByRegCode(bill.getRegistrationCode());
		
		CustomerDto customer = allCustomer.stream()
										.filter( c -> c.getCustomerStatus().equals("0"))
										.findAny()                            
						                .orElse(null);
		
		if(customer == null) {
			logger.error("Primary Customer With Reg Code " + bill.getRegistrationCode() + " Is Not Found");
			throw new NotFoundException("Primary Customer With Reg Code " + bill.getRegistrationCode() + " Is Not Found");
		}
				
		//Save Payment To DB
		PaymentDto paymentResponse = this.addNewPayment(newPayment);		
		logger.info("Payment = " + paymentResponse);
		
		//Map to Response object
		PaymentResponseDto paymentResponseFinal = modelMapper.map(paymentResponse, PaymentResponseDto.class);
		
		//Update Account to active
		AccountCustomerDto updateAccount = new AccountCustomerDto();
		updateAccount.setActive(true);
		AccountCustomerDto updatedAccount = accountService.updateAccountStatus(updateAccount, account.getAccountCode());
		logger.info("Set Account : " + account.getAccountCode() + " To Active");


		//Save To Create Transaction To Core
		List<CustomerPolicyDto> customerPolicyList = customerPolicyService.saveAllCustomerPolicy(allCustomer, bill.getRegistrationCode());	
		logger.info("Customer Policy = " + customerPolicyList);
		
		//Submit All Customer To Core & Save To Policy  
		List<BaseFreeCovidResponse> responseGrpList = groupInsuranceService.saveToCoreGrp(registration, customerPolicyList, paymentResponse);
			
		//Generate Certificate				
		//Result is not unique !!!!!!	
		FreeCovidCertificateResponse freeCovidResponse = null;
		//Disable Generate Auto Certificate 
//		CustomerPolicyDto customerPolicyDto = customerPolicyService.getPolicyByCoreCode(customerPolicyList.get(0).getCustomerPolicyCoreCode());
//      freeCovidResponse = customerPolicyService.getCovidCertificate(customerPolicyDto);
        
		//Save & Generate Referral Code
        CustomerReferralDto customerReferralSaved = customerReferralService.generateAndSaveCustomerReferall(account, newPayment);
		logger.info("Customer Referral = " + customerReferralSaved);
		
        //Get Registration Point And Gift By RegistrationCode
        RegistrationTypeDto registrationType = registrationTypeService.getRegistrationTypeByCode(registration.getRegistrationTypeCode());
        GiftDto registrationGift = giftService.getGiftByCode(registrationType.getGiftCode());        
        
        CustomerPointDto customerPointSaved = customerPointService.transactionPoint(account.getAccountCode(), registrationGift);       
		logger.info("Customer Point = " + customerPointSaved);			
		
	                        
        //If any referral add bonus +2
        if(!newPayment.getUsedReferralCode().equals("")) {
        	//Check If Referral Code is exist
        	CustomerReferral customerReferralSource = customerReferralService.getCustomerReferralByReferralCode(newPayment.getUsedReferralCode());       	        	
        	
        	if(customerReferralSource != null) {
        		
        		//Get Gift from referencing & referenced
            	GiftDto referencingGift = giftService.getReferencingGift();
            	GiftDto referencedGift = giftService.getReferencedGift();                	    
        		
        		//Add To Source Customer Referral
                CustomerPointDto customerPointByReferralSaved = customerPointService.transactionPoint(account.getAccountCode(), referencingGift);       

                CustomerPointDto customerPointFromReferralSaved = customerPointService.transactionPoint(customerReferralSource.getAccountCode(), referencedGift);       
        		
        	}      	        	
        }
		
		//Send Email Activation
		EmailNotificationResponse emailStatus = null;
		if(true) {
			//Async Send Email
			emailStatus = notificationService.activationAccountNotif(customer , account);
		}		 
		
		PaymentSuccessResponseDto response = new PaymentSuccessResponseDto();
		response.setRegistrationCode(bill.getRegistrationCode());
		response.setReferralCode(customerReferralSaved.getReferralCode());
		response.setPayment(paymentResponseFinal);
		response.setMembers(responseGrpList);
		response.setCertificate(freeCovidResponse);
		response.setEmailStatus(emailStatus);
		
		return ResponseEntity.ok(response);
	}

	private void validatePaymentAmount(PaymentRequestDto newPayment) {
		//Check Amount & Paid Amount if 0		
		if(newPayment.getAmount().intValue() != newPayment.getPaidAmount().intValue()) {
			logger.warn("Invalid Amount / Paid Amount" + newPayment.getAmount() + "!=" + newPayment.getPaidAmount());
			throw new ErrorsException("Invalid Amount / Paid Amount");
		}
	}

	@Transactional
	public PaymentDto addNewPayment(PaymentRequestDto newPayment) {
		Payment payment = modelMapper.map(newPayment, Payment.class);
		Payment paymentSaved = paymentRepository.save(payment);
		paymentRepository.refresh(paymentSaved);
		PaymentDto paymentResponse = modelMapper.map(paymentSaved, PaymentDto.class);
		return paymentResponse;
	}

	//Put Payment
	public ResponseEntity<PaymentDto> editPayment(PaymentDto updatePayment, Long id) {
		Payment payment = paymentRepository.findById(id).orElseThrow(()-> new NotFoundException("Payment id " + id + " is not exist"));
		payment.setPaymentCode(updatePayment.getPaymentCode());
		
		payment.setActive(updatePayment.isActive());
		paymentRepository.save(payment);
		PaymentDto response = modelMapper.map(payment, PaymentDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete Payment	
	public ResponseEntity<PaymentDto> deletePayment(Long id) {
		Payment findPayment = paymentRepository.findById(id).orElseThrow(()-> new NotFoundException("Payment id " + id + " is not exist"));
		paymentRepository.deleteById(id);
		PaymentDto response = modelMapper.map(findPayment, PaymentDto.class);

		return ResponseEntity.ok(response);
	}

	//Get Payment By paymentCode
	public PaymentDto getPaymentByCode(String paymentCode) {
		Payment payment = paymentRepository.findByPaymentCode(paymentCode);
		return modelMapper.map(payment, PaymentDto.class);
	}

	
}