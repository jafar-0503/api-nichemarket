package id.equity.nichemarket.service.mgm;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.dto.mgm.CustomerValidationDto;
import id.equity.nichemarket.dto.mgm.customer.CustomerAgeValidationDto;
import id.equity.nichemarket.dto.mgm.registrationType.RegistrationTypeDto;
import id.equity.nichemarket.dto.mgm.ruleInsuredAge.RuleInsuredAgeDto;
import id.equity.nichemarket.exception.CustomerValidationException;
import id.equity.nichemarket.exception.ErrorsException;
import id.equity.nichemarket.model.mgm.Customer;
import id.equity.nichemarket.retrofit.mgm.freeCovid.FreeCovidService;
import id.equity.nichemarket.validation.partner.BaseRegistrationValidation;

@Service
public class SiMedisValidatorService extends BaseRegistrationValidation<Customer, RegistrationTypeDto>{

    private static final Logger logger = LoggerFactory.getLogger(SiMedisValidatorService.class);

	
	@Autowired
	private RegistrationService registrationService;
	
	@Autowired
	private SiMedisValidatorCoreService siMedisValidatorCoreService;
	
	@Autowired
	private RuleInsuredAgeService ruleInsuredAgeService;
	
	
	@Autowired
	private RegistrationTypeService registrationTypeService;
	
	@Autowired
	private ProductPolicyService productPolicyService;
	
	@Autowired
	private AccountService accountService;

	@Autowired
	private ProductPlanPolicyService productPlanPolicyService;

	@Autowired
	private FreeCovidService freeCovidService;


	@Autowired
	private UtilitiesService utilitiesService;
	
	public boolean isValidAge(CustomerAgeValidationDto customerAgeValidation) {
		Integer customerAge = 0;	
		
		//Get Rule Min Max Age From Reg Type
//		RegistrationTypeDto registrationType = registrationTypeService.getRegistrationTypeByCode(customerAgeValidation.getRegistrationTypeCode());
		RuleInsuredAgeDto ruleInsuredAge = ruleInsuredAgeService.getRuleInsuredAgeByRegTypeCodeAndCustomerStatus(customerAgeValidation.getRegistrationTypeCode(),customerAgeValidation.getCustomerStatus());
		
		//Calculate Age
		customerAge = utilitiesService.calculateAge(customerAgeValidation.getDateOfBirth(), "");
		
		if(customerAge >= ruleInsuredAge.getMinAge() && customerAge <= ruleInsuredAge.getMaxAge()) {
			logger.info("Current Age " + customerAge + " Is Valid");
			return true;
		}else {
			logger.warn("Current Age " + customerAge + " Is Not Valid");
			return false;
		}
		
	}

	public boolean isNotDuplicateKtp(String ktpNO, String productPolicyCode) {
		Integer totalActiveKtpNo = registrationService.countActiveKtpNo(ktpNO, productPolicyCode);
		if(totalActiveKtpNo > 0) {
			logger.warn("Duplicate Ktp No " + ktpNO + " found: " + totalActiveKtpNo);
			return false;
		}else {
			logger.info("Non Duplicate Ktp No " + ktpNO );
			return true;
		}		
	}
	
	
	/**
	 * Fix this to count by type (general , simedis , others)
	 * @author Ferry
	 */
	public boolean isNotDuplicateEmailAddress(String emailAddress) {
		Integer totalActiveEmailAddress = accountService.countActiveEmailAddress(emailAddress.toLowerCase());
		if(totalActiveEmailAddress > 0) {
			logger.warn("Duplicate Email Address On Backend " + emailAddress + " found: " + totalActiveEmailAddress);
			return false;
		}else {
			logger.info("Non Duplicate Email Address On Backend :" + emailAddress);
			return true;
		}
	}

	public ResponseEntity<?> validateKtpNoAndDob(CustomerValidationDto customerValidation) {
		String validationMessage = "";
		
		List<String> errors = new ArrayList<>();
		
		boolean isKtpCoreValid = false;
		boolean isKtpAppValid = false;
		
		//Check Core KTP validation
		isKtpCoreValid = siMedisValidatorCoreService.isValidKtp(customerValidation);
		
		//Check Backend KTP validation
			//Get ProductPolicyCode
		RegistrationTypeDto registrationType = registrationTypeService.getRegistrationTypeByCode(customerValidation.getRegistrationTypeCode());
		
		isKtpAppValid = this.isNotDuplicateKtp(customerValidation.getKtpNo() , registrationType.getProductPolicyCode());
						
        if(!isKtpCoreValid) {        	
			errors.add("KtpNo Already Exist And Active");
        }
        
        if(!isKtpAppValid) {
        	errors.add("KtpNo Already Registered");
        }

		//Check Umur
		CustomerAgeValidationDto customerAgeValidation = new CustomerAgeValidationDto();
		customerAgeValidation.setDateOfBirth(customerValidation.getDateOfBirth());
		customerAgeValidation.setRegistrationTypeCode(customerValidation.getRegistrationTypeCode());
		customerAgeValidation.setCustomerStatus(customerValidation.getCustomerStatus());		
		
		boolean isAgeValid = this.isValidAge(customerAgeValidation);
		if(!isAgeValid) {
			errors.add("Date Of Birth Is Not Valid");
		}	
	
		
		if(isKtpCoreValid && isKtpAppValid && isAgeValid) {
			return ResponseEntity.ok("");			
		}else {
			validationMessage = "Invalid Data";   
			throw new CustomerValidationException(validationMessage, errors);
		}
	}

	public boolean isValidTotalCustomer(int totalCustomer, RegistrationTypeDto currentRegistrationType) {
		// TODO Auto-generated method stub
		if(totalCustomer >= currentRegistrationType.getMinCustomer() && totalCustomer <= currentRegistrationType.getMaxCustomer()) {
			return true;
		}else {
    		logger.warn("Total Registered Customer Is Invalid");
			return false;
		}		
	}

	@Override
	public boolean isValidKtpCore(CustomerValidationDto customerValidation) {
		return siMedisValidatorCoreService.isValidKtp(customerValidation);
	}

	@Override
	public List<String> validate(List<Customer> customerList, RegistrationTypeDto currentRegistrationType) {
		List<String> errors = new ArrayList<>();
		
		//Validate Total Customer Based On Registration Type
		if(!(this.isValidTotalCustomer(customerList.size(), currentRegistrationType))) {
			throw new ErrorsException("Total Registered Customer Is Invalid");
		}
		
		//Validate Multiple Ktp each Registration
		if(customerList.stream().map(Customer::getKtpNO).distinct().count() != customerList.size()) {
			throw new ErrorsException("Duplicate Ktp No On Registration");
		}
		
		//Validate Multiple Customer with customerStatus is 0
		if(customerList.stream().filter(c -> c.getCustomerStatus().equals("0")).count() != 1) throw new ErrorsException("Customer Status is Invalid");
				
		//Validate App Code,KTP,Email Address
		boolean isNotDuplicateKtpNO = true;
		boolean isNotDuplicateEmailAddress = true;
		boolean isValidAge = true;
		
		
		String errValidationMessage = "";
		for(Customer customer : customerList){			
			isNotDuplicateKtpNO = true;
			isNotDuplicateEmailAddress = true;
			isValidAge = true;
			
			//Check KTP On Backend			
			isNotDuplicateKtpNO = this.isNotDuplicateKtp(customer.getKtpNO(), currentRegistrationType.getProductPolicyCode());
			
			//Check KTP On Core
			CustomerValidationDto customerValidation = new CustomerValidationDto();
			customerValidation.setKtpNo(customer.getKtpNO());
			customerValidation.setRegistrationTypeCode(currentRegistrationType.getRegistrationTypeCode());		
			boolean isKtpCoreValid = this.isValidKtpCore(customerValidation);
			
			//If Primary Customer Check Email Not Null And Not Empty
			if(customer.getCustomerStatus().equals("0")) {
				if(customer.getEmailAddress() == "" || customer.getEmailAddress() == null) {
					errors.add("Primary Email Address Is Empty");
					errValidationMessage = "Invalid Customer Data";
					throw new CustomerValidationException(errValidationMessage, errors);
				}
				//Check Paid Account With Spesific Email Address
				isNotDuplicateEmailAddress = this.isNotDuplicateEmailAddress(customer.getEmailAddress());
			}
			
			//Check Age		
			CustomerAgeValidationDto customerAgeValidation = new CustomerAgeValidationDto();
			customerAgeValidation.setDateOfBirth(customer.getDateOfBirth());
			customerAgeValidation.setRegistrationTypeCode(currentRegistrationType.getRegistrationTypeCode());
			customerAgeValidation.setCustomerStatus(customer.getCustomerStatus());			
			isValidAge = this.isValidAge(customerAgeValidation);			

			
			if(isNotDuplicateKtpNO == false) {
				errors.add("Duplicate KTP No : " + customer.getKtpNO());
			}
			
			if(isKtpCoreValid == false) {
				errors.add("Duplicate KTP No On Core System: " + customer.getKtpNO());
			}			
			
			if(isNotDuplicateEmailAddress == false) {
				errors.add("Duplicate Email Address : " + customer.getEmailAddress());				
			}
			
			if(isValidAge == false) {
				errors.add("Invalid Age For: " + customer.getCustomerName());
			}			
						
		}
		return errors;
	}

	@Override
	public String getType() {
		return "simedis";
	}

}
