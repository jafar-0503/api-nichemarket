package id.equity.nichemarket.service.mgm;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.JsonObject;

import id.equity.nichemarket.dto.mgm.CustomerValidationDto;
import id.equity.nichemarket.dto.mgm.product.ProductPolicyDto;
import id.equity.nichemarket.dto.mgm.productPlanPolicy.ProductPlanPolicyDto;
import id.equity.nichemarket.dto.mgm.registrationType.RegistrationTypeDto;
import id.equity.nichemarket.exception.APICoreException;
import id.equity.nichemarket.exception.ErrorsException;
import id.equity.nichemarket.retrofit.mgm.freeCovid.BaseValidationCovidResponse;
import id.equity.nichemarket.retrofit.mgm.freeCovid.FreeCovidService;

@Service
public class SiMedisValidatorCoreService {
	
	@Autowired
	private RegistrationService registrationService;
	
	@Autowired
	private RuleInsuredAgeService ruleInsuredAgeService;
	
	
	@Autowired
	private RegistrationTypeService registrationTypeService;
	
	@Autowired
	private ProductPolicyService productPolicyService;
	
	@Autowired
	private AccountService accountService;

	@Autowired
	private ProductPlanPolicyService productPlanPolicyService;

	@Autowired
	private FreeCovidService freeCovidService;
	
    private static final Logger logger = LoggerFactory.getLogger(SiMedisValidatorCoreService.class);

	public boolean isValidKtp(CustomerValidationDto customerValidation) {
		String validationMessage = "";
		//Get Product Policy Code
		RegistrationTypeDto registrationType = registrationTypeService.getRegistrationTypeByCode(customerValidation.getRegistrationTypeCode());
		
		//Get Partner Core Code
		ProductPolicyDto productPolicy = productPolicyService.getProductPolicyByCode(registrationType.getProductPolicyCode());
		
		//Get Product Plan Policy
		ProductPlanPolicyDto productPlanPolicy = productPlanPolicyService.getProductPlanPolicyByRegTypeCodeAndType(customerValidation.getRegistrationTypeCode(), 1);
		//Check to Core
		boolean isKtpValid = true;
		

		JsonObject customerData = new JsonObject();
		customerData.addProperty("card_no", customerValidation.getKtpNo());
		customerData.addProperty("partner_code", productPolicy.getPartnerCodeCore());
		customerData.addProperty("plan_code", productPlanPolicy.getPlanCodeCore());

		JsonObject allCustomerData = new JsonObject();
		allCustomerData.addProperty("username", "API_GRP");
		allCustomerData.addProperty("signature", "ItsfaDMljP+ZNw4KTqxWa7XHKAXtzs7+CH3jjiHrd/Y=");
		allCustomerData.addProperty("action","checkActiveUP");
		allCustomerData.add("value", customerData);
		
		logger.info("Sending Primary Customer Data :" + allCustomerData);			
		BaseValidationCovidResponse response = freeCovidService.checkUPByKtpNo(allCustomerData);
        logger.info("Result Validating To Core : " + response);	        

        // Check & Compare Active UP VS Maks UP
        if(response == null) {
        	throw new APICoreException("Error validating to core");
        }
        
        if(response.getData().getSum_insured_ajb() == null) {
        	throw new APICoreException("Error validating to core");
        }
        
        //If UP Is Not 0
        if(!response.getData().getSum_insured_ajb().equals("0")) {
    		logger.info("Active UP :" + response.getData().getSum_insured_ajb());	
    		logger.info("Maks UP :" + registrationType.getMaxUP());
    		
    		// Comparing
    		String activeUP = response.getData().getSum_insured_ajb().split(",")[0].replace(".", "");
    		BigDecimal maxUP = registrationType.getMaxUP();
    		
    		if(Integer.parseInt(activeUP) >= maxUP.doubleValue()){
    			isKtpValid = false;
    		}    		    	    		
        }
        
        return isKtpValid;
		        
	}
}
