package id.equity.nichemarket.service.mgm;

import id.equity.nichemarket.dto.mgm.customer.CustomerDto;
import id.equity.nichemarket.dto.mgm.customerReference.CustomerReferenceDto;
import id.equity.nichemarket.exception.ErrorsException;
import id.equity.nichemarket.model.mgm.Customer;
import id.equity.nichemarket.model.mgm.CustomerReference;
import id.equity.nichemarket.repository.mgm.CustomerReferenceRepository;
import id.equity.nichemarket.repository.mgm.CustomerRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Random;

@Service
public class CustomerReferenceService {
    @Autowired
    private CustomerService customerService;
    @Autowired
    private CustomerReferenceRepository customerReferenceRepository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private EntityManager em;
    private String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    public String generateUniqueKey(){
        int codeLength = 15;
        Random random = new Random();
        StringBuilder builder = new StringBuilder(codeLength);
        CustomerReference checkExistKey= null;
        int trying = 0;
        do {
             for (int i = 0; i < codeLength; i++) {
                 builder.append(this.alphabet.charAt(random.nextInt(this.alphabet.length())));
             }
             checkExistKey = customerReferenceRepository.findByKey(builder.toString());
             trying++;
         } while (checkExistKey != null && trying < 3);
        return builder.toString();
    }

    @Transactional
    public CustomerReferenceDto savingCustomerReference(CustomerDto data, String uniqueKey){
        CustomerReference customerReference = modelMapper.map(data, CustomerReference.class);
        customerReference.setCustomerCode(data.getCustomerCode());
        customerReference.setUniqueKey(uniqueKey);
        customerReference.setActive(true);
        CustomerReference customerReferenceSaved = customerReferenceRepository.save(customerReference);
        em.refresh(customerReferenceSaved);
        CustomerReferenceDto response = modelMapper.map(customerReferenceSaved, CustomerReferenceDto.class);
        return response;
    }

    public CustomerReferenceDto checkUniqueKey(String email, String phoneNo, String key){
        CustomerReferenceDto checkKey = null;
        try{
            CustomerReference checkExist = customerReferenceRepository.findByKey(key);
            Customer checkCustomer = customerRepository.findByCustomerCodeAndPhoneNo(checkExist.getCustomerCode(), phoneNo, email);
            if (checkCustomer != null) {
                checkKey = modelMapper.map(checkExist, CustomerReferenceDto.class);
            }
        } catch (NullPointerException e){
            e.printStackTrace();
        }
        return checkKey;
    }

    public ResponseEntity<List<CustomerReferenceDto>> listCustomerReference() {
        List<CustomerReference> lsCustomerReference= customerReferenceRepository.findAll();
        Type targetType = new TypeToken<List<CustomerReferenceDto>>() {}.getType();
        List<CustomerReferenceDto> response = modelMapper.map(lsCustomerReference, targetType);

        return ResponseEntity.ok(response);
    }

    public CustomerDto getCustomer(String customerReferenceCode) {
        CustomerReference customerReference = customerReferenceRepository.findByCustomerReferenceCode(customerReferenceCode);
        CustomerDto customerDto = customerService.getCustomerByCode(customerReference.getCustomerCode());
        return customerDto;
    }

    public CustomerReferenceDto checkByUniqueKey(String key) {
        CustomerReference customerReference = customerReferenceRepository.findByKey(key);
        if (customerReference == null){
            throw new ErrorsException("key is not exist");
        }
        CustomerReferenceDto customerReferenceDto = modelMapper.map(customerReference, CustomerReferenceDto.class);
        return customerReferenceDto;
    }
}
