package id.equity.nichemarket.service.mgm;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.mgm.question.QuestionDto;
import id.equity.nichemarket.dto.mgm.question.CreateQuestion;
import id.equity.nichemarket.model.mgm.Question;
import id.equity.nichemarket.repository.mgm.QuestionRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class QuestionService {

	@Autowired
	private QuestionRepository questionRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List Question
	public ResponseEntity<List<QuestionDto>> listQuestion(){
		List<Question> lsQuestion= questionRepository.findAll();
		Type targetType = new TypeToken<List<QuestionDto>>() {}.getType();
		List<QuestionDto> response = modelMapper.map(lsQuestion, targetType);

		return ResponseEntity.ok(response);
	}

	//Get Question By Id
	public ResponseEntity<QuestionDto> getQuestionById(Long id) {
		Question question = questionRepository.findById(id).orElseThrow(()-> new NotFoundException("Question id " + id + " is not exist"));
		QuestionDto response = modelMapper.map(question, QuestionDto.class);

		return ResponseEntity.ok(response);
	}

	//Post Question
	public ResponseEntity<QuestionDto> addQuestion(CreateQuestion newQuestion) {
		Question question = modelMapper.map(newQuestion, Question.class);
		Question questionSaved = questionRepository.save(question);
		QuestionDto response = modelMapper.map(questionSaved, QuestionDto.class);

		return ResponseEntity.ok(response);
	}

	//Put Question
	public ResponseEntity<QuestionDto> editQuestion(CreateQuestion updateQuestion, Long id) {
		Question question = questionRepository.findById(id).orElseThrow(()-> new NotFoundException("Question id " + id + " is not exist"));
		question.setQuestionCode(updateQuestion.getQuestionCode());
		question.setDescription(updateQuestion.getDescription());
		question.setSequence(updateQuestion.getSequence());
		question.setActive(updateQuestion.isActive());
		questionRepository.save(question);
		QuestionDto response = modelMapper.map(question, QuestionDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete Question	
	public ResponseEntity<QuestionDto> deleteQuestion(Long id) {
		Question findQuestion = questionRepository.findById(id).orElseThrow(()-> new NotFoundException("Question id " + id + " is not exist"));
		questionRepository.deleteById(id);
		QuestionDto response = modelMapper.map(findQuestion, QuestionDto.class);

		return ResponseEntity.ok(response);
	}
}