package id.equity.nichemarket.service.mgm;

import id.equity.nichemarket.dto.es.SpajPolicyHolder.SpajPolicyHolderDto;
import id.equity.nichemarket.dto.mgm.recipientReceivedMsg.CreateRecipientReceivedMsg;
import id.equity.nichemarket.dto.mgm.recipientReceivedMsg.RecipientReceivedMsgDto;
import id.equity.nichemarket.model.mgm.RecipientReceivedMsg;
import id.equity.nichemarket.repository.mgm.RecipientReceivedMsgRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.lang.reflect.Type;
import java.util.List;

@Service
public class RecipientReceivedMsgService {
    @Autowired
    private RecipientReceivedMsgRepository recipientReceivedMsgRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private EntityManager em;

    @Transactional
    public ResponseEntity addRecipientReceived(CreateRecipientReceivedMsg createRecipientReceivedMsg) {
        RecipientReceivedMsg recipientReceivedMsg = modelMapper.map(createRecipientReceivedMsg, RecipientReceivedMsg.class);
        RecipientReceivedMsg recipientReceivedMsgSaved = recipientReceivedMsgRepository.save(recipientReceivedMsg);
        em.refresh(recipientReceivedMsgSaved);
        RecipientReceivedMsgDto response = modelMapper.map(recipientReceivedMsgSaved, RecipientReceivedMsgDto.class);
        return ResponseEntity.ok(response);
    }

    public List<RecipientReceivedMsgDto> getRecipientByTypeCode(String messageType) {
        List<RecipientReceivedMsg> recipientReceivedMsgs = recipientReceivedMsgRepository.findByTypeCode(messageType);
        Type targetType = new TypeToken<List<RecipientReceivedMsgDto>>() {}.getType();
        List<RecipientReceivedMsgDto> response = modelMapper.map(recipientReceivedMsgs, targetType);
        return response;
    }
}
