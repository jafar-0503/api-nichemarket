package id.equity.nichemarket.service.mgm;

import id.equity.nichemarket.dto.mgm.interactive.InteractiveDto;
import id.equity.nichemarket.model.mgm.Interactive;
import id.equity.nichemarket.model.mgm.InteractivePriority;
import id.equity.nichemarket.repository.mgm.InteractivePriorityRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InteractivePriorityService {
    @Autowired
    private InteractivePriorityRepository interactivePriorityRepository;
    @Autowired
    private ModelMapper modelMapper;

    private static final Logger logger = LoggerFactory.getLogger(InteractivePriorityService.class);

    public Boolean saveInteractivePriority(List<String> data, String interactiveCode) {
        int total = 0;
        for (String savingData : data){
            InteractivePriority interactivePriority = modelMapper.map(savingData, InteractivePriority.class);
            interactivePriority.setInteractiveCode(interactiveCode);
            interactivePriority.setDescription(savingData);
            interactivePriority.setActive(true);
            interactivePriorityRepository.save(interactivePriority);
            total++;
        }
        if (total != data.size()){
            logger.info("Failed to save interactive priority");
            return false;
        }
        return true;
    }

    //Get Interactive By Email
    public InteractiveDto getInteractiveByEmail(String email) {
        Interactive interactive = interactivePriorityRepository.findByEmail(email);
        InteractiveDto interactiveDto = null;
        try {
            interactiveDto = modelMapper.map(interactive, InteractiveDto.class);
        }finally {
            return interactiveDto;
        }
    }

    //Get Interactive By Phone
    public InteractiveDto getInteractiveByPhone(String phone) {
        Interactive interactive = interactivePriorityRepository.findByPhone(phone);
        InteractiveDto interactiveDto = null;
        try {
            interactiveDto = modelMapper.map(interactive, InteractiveDto.class);
        } finally {
            return interactiveDto;
        }
    }

}
