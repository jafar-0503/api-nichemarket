package id.equity.nichemarket.service.mgm;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.JsonObject;

import id.equity.nichemarket.dto.mgm.app.AppDto;
import id.equity.nichemarket.dto.mgm.customer.CustomerCoreDataDto;
import id.equity.nichemarket.dto.mgm.customer.CustomerDetailPolicyDto;
import id.equity.nichemarket.dto.mgm.customerPolicy.CustomerPolicyDto;
import id.equity.nichemarket.dto.mgm.payment.PaymentDto;
import id.equity.nichemarket.dto.mgm.product.ProductPolicyDto;
import id.equity.nichemarket.dto.mgm.productPlanPolicy.ProductPlanPolicyDto;
import id.equity.nichemarket.dto.mgm.registration.RegistrationDto;
import id.equity.nichemarket.dto.mgm.registrationType.RegistrationTypeDto;
import id.equity.nichemarket.exception.ErrorsException;
import id.equity.nichemarket.exception.LinkajaInformPaymentException;
import id.equity.nichemarket.model.mgm.Customer;
import id.equity.nichemarket.model.mgm.Jobs;
import id.equity.nichemarket.retrofit.mgm.freeCovid.BaseFreeCovidResponse;
import id.equity.nichemarket.retrofit.mgm.freeCovid.FreeCovidService;

@Service
public class GroupInsuranceService {
	@Autowired
    private FreeCovidService freeCovidService;
	
	@Autowired
    private CustomerPolicyService customerPolicyService;
	
	@Autowired
    private CustomerService customerService;

	@Autowired
    private RegistrationTypeService registrationTypeService;
	
	@Autowired
    private RelationshipService relationshipService;
	
	@Autowired
    private ProductPolicyService productPolicyService;
	
	@Autowired
    private ProductPlanPolicyService productPlanPolicyService;	
	
	@Autowired
    private AppService appService;	
	
	@Autowired
    private JobsService jobsService;	
	
	@Autowired
    private ModelMapper modelMapper;
	
	@Autowired
	private EntityManager em;
	
    private static final Logger logger = LoggerFactory.getLogger(GroupInsuranceService.class);

	
//	@Transactional
	public List<BaseFreeCovidResponse> saveToCoreGrp(RegistrationDto registration, List<CustomerPolicyDto> customerPolicyList, PaymentDto payment) {
			//Get App Detail
			AppDto currAppDto = appService.getAppByAppCode(registration.getAppCode());		
		
			//Loop to get customer code
			List<String> customerCodeList = new ArrayList<>();
			for(CustomerPolicyDto customerPolicy : customerPolicyList) {
				customerCodeList.add(customerPolicy.getCustomerCode());
			}			

			
			//Get Detail Customer By Customer Code
			List<Customer> customerList = null;
			
			List<CustomerDetailPolicyDto> customerDetailPolicy = customerService.getAllCustomerInCustomerCode(customerCodeList);
			
			
			//Get partnerCode by registration
			RegistrationTypeDto registrationType = registrationTypeService.getRegistrationTypeByCode(registration.getRegistrationTypeCode());
			ProductPolicyDto productPolicyDto = productPolicyService.getProductPolicyByCode(registrationType.getProductPolicyCode());
		        
	        ProductPlanPolicyDto productPlanPolicy = _getInsuredPlanCodeCore(registration.getRegistrationTypeCode(),1);	        

	        //Get Primary Customer
			CustomerDetailPolicyDto primaryCustomer = customerDetailPolicy.stream()
											.filter( c -> c.getCustomerStatus().equals("0"))
											.findAny()                            
							                .orElse(null);
			//Get Other Customer 
			List<CustomerDetailPolicyDto> otherCustomerList = customerDetailPolicy.stream()
												    .filter( c -> !c.getCustomerStatus().equals("0"))
												    .collect(Collectors.toList());
		
			
			if(primaryCustomer == null) {
				logger.error("Primary Customer In List " + customerCodeList + " Is Not Found");
				throw new ErrorsException("Primary Customer In List " + customerCodeList + " Is Not Found");
			}			
			
			ArrayList<JsonObject> otherCustomerDataList = new ArrayList<>();
			
			//Generate Post Data To Core
			JsonObject primaryCustomerData = generateDataForCore(primaryCustomer, productPolicyDto, productPlanPolicy, currAppDto, payment);
			
			//Save to jobs table
			Integer sequence = 1;
			createJobs(primaryCustomerData, primaryCustomer.getCustomerPolicyCoreCode(), sequence);
		
			
			for(CustomerDetailPolicyDto customer : otherCustomerList) {
				sequence++;
				ProductPlanPolicyDto otherProductPlanPolicy = _getInsuredPlanCodeCore(registration.getRegistrationTypeCode(),2);
				JsonObject otherCustomerData = generateDataForCore(customer, productPolicyDto, otherProductPlanPolicy, currAppDto, payment);
				otherCustomerDataList.add(otherCustomerData);
				
				//Add Transaction Partner Code to jobs table
								
				
				//Save to jobs table
				createJobs(otherCustomerData, customer.getCustomerPolicyCoreCode() , sequence);
			}			
			
	        //Send To Core The Primary Customer
			logger.info("Sending Primary Customer Data :" + primaryCustomerData);			
	        BaseFreeCovidResponse primaryResponse = freeCovidService.sendToCoreGrp(primaryCustomerData);
	        logger.info("Result Sending To Core : " + primaryResponse);	        
	        
	        List<BaseFreeCovidResponse> freeCovidResponse = new ArrayList<>();
	        if (primaryResponse == null){
	        	logger.error("Failed to send primary customer to core with id: " + primaryCustomer.getCustomerCode());
	        	BaseFreeCovidResponse failedPrimaryResponse = new BaseFreeCovidResponse();
	        	failedPrimaryResponse.setError_code("2");
	        	failedPrimaryResponse.setData(null);
	        	failedPrimaryResponse.setError_description("Error when submit primary customer to core transaction code :" + primaryCustomer.getCustomerPolicyCoreCode());
	        	freeCovidResponse.add(failedPrimaryResponse);
	        	return freeCovidResponse;
//	            throw new ErrorsException("Failed to send primary customer to core with id: " + primaryCustomer.getCustomerCode());	            
	        }
	        
	        if(!primaryResponse.getError_code().equals("1")) {
	        	logger.error("Failed to send to core error code : " + primaryResponse.getError_code() + " , " + primaryResponse.getError_description());	        	
	        	freeCovidResponse.add(primaryResponse);
	        	return freeCovidResponse;
//	        	throw new ErrorsException(primaryResponse.getError_description());	    
	        }
	        
	        freeCovidResponse.add(primaryResponse);                        	
	        
	        //Update To Customer Policy If Succeed To Core
	        CustomerPolicyDto customerPolicyDto = customerPolicyService.updateFromFreeCovidResponse(primaryResponse.getData());	        	        	     
			
			if(otherCustomerDataList != null && !otherCustomerDataList.isEmpty()){		
				for (JsonObject otherCustomerData : otherCustomerDataList) {

					logger.info("Sending Others Customer Data :" + otherCustomerData);					
				    BaseFreeCovidResponse OtherFreeCovidResponse = freeCovidService.sendToCoreGrp(otherCustomerData);			        
				    logger.info("Result Sending To Core : " + OtherFreeCovidResponse);	
				    
				    
				    if (OtherFreeCovidResponse == null){
			        	logger.error("Failed to send other customer to core with customer code : " + String.valueOf(otherCustomerData.get("customer_code")));
			        	BaseFreeCovidResponse failedOtherResponse = new BaseFreeCovidResponse();
			        	failedOtherResponse.setError_code("2");
			        	failedOtherResponse.setData(null);
			        	failedOtherResponse.setError_description("Error when submit other customer with customer policy code : " + String.valueOf(otherCustomerData.get("transaction_code")));
					    freeCovidResponse.add(failedOtherResponse);    			    
			        }else {
			        	if(OtherFreeCovidResponse.getError_code().equals("1")) {
			    	        //Update To Customer Policy If Succeed To Core
						    CustomerPolicyDto OtherCustomerPolicyDto = customerPolicyService.updateFromFreeCovidResponse(OtherFreeCovidResponse.getData());
						} 
					    freeCovidResponse.add(OtherFreeCovidResponse);    			    
			        }	    				   			    
				    
				}	
				
			}		        
			
			
        return freeCovidResponse;
	}


	private ProductPlanPolicyDto _getInsuredPlanCodeCore(String registrationTypeCode, Integer type) {
		ProductPlanPolicyDto productPlanPolicyDto = productPlanPolicyService.getProductPlanPolicyByRegTypeCodeAndType(registrationTypeCode,type);
		return productPlanPolicyDto;
	}
	
	
	/**
	 * Version 1
	 * <p>Generate JSON format for core API<p>
	 * 
	 * @param customerDetailPolicy
	 * @param productPolicy
	 * @param productPlanPolicy
	 * @param app
	 * @param payment
	 * @return jsonObject
	 */
	private JsonObject generateDataForCore(CustomerDetailPolicyDto customerDetailPolicy,
		ProductPolicyDto productPolicy, ProductPlanPolicyDto productPlanPolicy , AppDto app, PaymentDto payment) {
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        DateTimeFormatter dtfHours = DateTimeFormatter.ofPattern("HHmm");
        LocalDateTime localDateNow = LocalDateTime.now();
		
		String partnerCode = productPolicy.getPartnerCodeCore();
		String emailCode = productPolicy.getEmailCode();
		String mappedPrimaryCustomerStatus = relationshipService.getRelationshipCoreCodeByCustomerStatus(customerDetailPolicy.getCustomerStatus());

		JsonObject contentEmail = new JsonObject();
        JsonObject allEmailContent = new JsonObject();
        contentEmail.addProperty("fullname", customerDetailPolicy.getCustomerName());
        contentEmail.addProperty("card_no", customerDetailPolicy.getKtpNO());
        contentEmail.addProperty("date_of_birth", customerDetailPolicy.getDateOfBirth());
        contentEmail.addProperty("email_address", customerDetailPolicy.getEmailAddress());
        contentEmail.addProperty("gender", customerDetailPolicy.getGenderCode());
        contentEmail.addProperty("phone_no", customerDetailPolicy.getPhoneNo());
        contentEmail.addProperty("customer_status", mappedPrimaryCustomerStatus);
        contentEmail.addProperty("customer_code", customerDetailPolicy.getCustomerCode());
        contentEmail.addProperty("customer_ref_code", customerDetailPolicy.getRefCustomerCode());
        contentEmail.addProperty("transaction_code", customerDetailPolicy.getCustomerPolicyCoreCode());
        contentEmail.addProperty("partner_code", partnerCode);
        contentEmail.addProperty("payment_type", payment.getPaymentType() == null ? "" : payment.getPaymentType()); //Payment type
        contentEmail.addProperty("order_id", payment.getPaymentRefCode()); //Payment ref code
        contentEmail.addProperty("plan_code", productPlanPolicy.getPlanCodeCore());
        contentEmail.addProperty("start_date", dtf.format(localDateNow));
        contentEmail.addProperty("class", productPlanPolicy.getClassCoreCode());
        contentEmail.addProperty("email_code", emailCode);
		
		allEmailContent.addProperty("username", "API_GRP");
        allEmailContent.addProperty("signature", "ItsfaDMljP+ZNw4KTqxWa7XHKAXtzs7+CH3jjiHrd/Y=");
        allEmailContent.addProperty("action",app.getAppActionName());
        allEmailContent.add("value", contentEmail);
		return allEmailContent;
	}
	
	private Jobs createJobs(JsonObject payload, String transactionCodeCore,Integer sequence) {
		UUID uuid = UUID.randomUUID();			
		Jobs job = new Jobs();
		job.setPayload(payload.toString());
		job.setSequence(sequence);
		job.setTransactionCodeCore(transactionCodeCore);
		job.setTransactionId(uuid.toString());
		Jobs jobsSaved = jobsService.addJob(job);
		return jobsSaved;		
	}
	
	
	//Execute this every one hour and select only today
	public List<BaseFreeCovidResponse> resyncToCore(List<CustomerCoreDataDto> customerCoreDataList) {
		//Get Primary Customer
		CustomerCoreDataDto primaryCustomer = customerCoreDataList.stream()
										.filter( c -> c.getCustomerStatus().equals("0"))
										.findAny()                            
						                .orElse(null);
        List<BaseFreeCovidResponse> freeCovidResponse = new ArrayList<>();

		
		//Submit to Core Primary Customer
		if (primaryCustomer != null) {
			JsonObject generatedCustomerData = generateDataForCoreFromJobs(primaryCustomer);
	        BaseFreeCovidResponse primaryResponse = freeCovidService.sendToCoreGrp(generatedCustomerData);

	        if (primaryResponse == null){
	        	logger.error("Failed to send primary customer to core with id: " + primaryCustomer.getCustomerCode());
	        	BaseFreeCovidResponse failedPrimaryResponse = new BaseFreeCovidResponse();
	        	failedPrimaryResponse.setError_code("2");
	        	failedPrimaryResponse.setData(null);
	        	failedPrimaryResponse.setError_description("Error when submit primary customer to core transaction code :" + primaryCustomer.getTransactionCode());
	        	freeCovidResponse.add(failedPrimaryResponse);
	        	return freeCovidResponse;
	        }
	        freeCovidResponse.add(primaryResponse);                        	
		       
	        //Update To Customer Policy If Succeed To Core
	        CustomerPolicyDto customerPolicyDto = customerPolicyService.updateFromFreeCovidResponse(primaryResponse.getData());	 			
		}	
		
		
        //Get Other Customer 
		List<CustomerCoreDataDto> otherCustomerList = customerCoreDataList.stream()
											    .filter( c -> !c.getCustomerStatus().equals("0"))
											    .collect(Collectors.toList());
		
		//Submit to Core Other
		for(CustomerCoreDataDto customerData : otherCustomerList) {
			JsonObject generatedCustomerData = generateDataForCoreFromJobs(customerData);
	        BaseFreeCovidResponse OtherFreeCovidResponse = freeCovidService.sendToCoreGrp(generatedCustomerData);
		    
		    if (OtherFreeCovidResponse == null){
	        	logger.error("Failed to send other customer to core with customer code : " + customerData.getCustomerCode());
	        	BaseFreeCovidResponse failedOtherResponse = new BaseFreeCovidResponse();
	        	failedOtherResponse.setError_code("2");
	        	failedOtherResponse.setData(null);
	        	failedOtherResponse.setError_description("Error when submit other customer with customer policy code : " + customerData.getCustomerCode());
			    freeCovidResponse.add(failedOtherResponse);    			    
	        }else {
	        	if(OtherFreeCovidResponse.getError_code().equals("1")) {
				    CustomerPolicyDto OtherCustomerPolicyDto = customerPolicyService.updateFromFreeCovidResponse(OtherFreeCovidResponse.getData());
				}
			    freeCovidResponse.add(OtherFreeCovidResponse);    			    
	        }	    	
		}		
		
		//Loop Response From Core To Update
        for(BaseFreeCovidResponse baseFreeCovidResponse : freeCovidResponse) {
        	if(baseFreeCovidResponse.getError_code().equals("1")) {
            	//Update Customer Policy
			    CustomerPolicyDto customerPolicyDto = customerPolicyService.updateFromFreeCovidResponse(baseFreeCovidResponse.getData());	           
        	}
        	
        	 //Update Jobs By Transaction Code
		    Jobs jobs = jobsService.updateJobs(baseFreeCovidResponse.getData().getTransaction_code());       	
        }        
        	
        return freeCovidResponse;
        
	}


	private JsonObject generateDataForCoreFromJobs(CustomerCoreDataDto customerData) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        DateTimeFormatter dtfHours = DateTimeFormatter.ofPattern("HHmm");
        LocalDateTime localDateNow = LocalDateTime.now();		
	
		JsonObject contentEmail = new JsonObject();
        JsonObject allEmailContent = new JsonObject();
        contentEmail.addProperty("fullname", customerData.getCustomerName());
        contentEmail.addProperty("card_no", customerData.getKtpNO());
        contentEmail.addProperty("date_of_birth", customerData.getDateOfBirth());
        contentEmail.addProperty("email_address", customerData.getEmailAddress());
        contentEmail.addProperty("gender", customerData.getGender());
        contentEmail.addProperty("phone_no", customerData.getPhoneNo());
        contentEmail.addProperty("customer_status", customerData.getCustomerStatus());
        contentEmail.addProperty("customer_code", customerData.getCustomerCode());
        contentEmail.addProperty("customer_ref_code", customerData.getCustomerRefCode());
        contentEmail.addProperty("transaction_code", customerData.getTransactionCode());
        contentEmail.addProperty("partner_code", customerData.getPartnerCode());
        contentEmail.addProperty("plan_code", customerData.getPlanCode());
        contentEmail.addProperty("start_date", dtf.format(localDateNow));
        contentEmail.addProperty("class", customerData.getClassType());
        contentEmail.addProperty("email_code", customerData.getEmailCode());
		
		allEmailContent.addProperty("username", "API_GRP");
        allEmailContent.addProperty("signature", "ItsfaDMljP+ZNw4KTqxWa7XHKAXtzs7+CH3jjiHrd/Y=");
        allEmailContent.addProperty("action", "register");
        allEmailContent.add("value", contentEmail);
		return allEmailContent;
	}
}
