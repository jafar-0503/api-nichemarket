package id.equity.nichemarket.service.mgm;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.JsonObject;

import id.equity.nichemarket.dto.mgm.customer.CustomerDto;

@Service
public class EmailObjGeneratorService {
	
	@Value("${frontend.data.activation-url}")
	private String activationUrl;
	
	public JsonObject generateActivationAccount(CustomerDto customer, String accountActivationKey) {
	    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        DateTimeFormatter dtfHours = DateTimeFormatter.ofPattern("HHmm");
        LocalDateTime localDateNow = LocalDateTime.now();
        String url = activationUrl + accountActivationKey;
        String convertedGenderCode = (customer.getGenderCode().equals("W")) ? "P" : "L" ; 
        JsonObject jsonEmail = new JsonObject();
        JsonObject jsonValue = new JsonObject();
        jsonValue.addProperty("message_type", "E");
        jsonValue.addProperty("type_code", "E29");
        jsonValue.addProperty("variable_no_1", customer.getCustomerCode());
        jsonValue.addProperty("variable_name_1", customer.getCustomerName());
        jsonValue.addProperty("variable_date_1", customer.getDateOfBirth());
        jsonValue.addProperty("variable_sum_1", "");
        jsonValue.addProperty("variable_date_2", "");
        jsonValue.addProperty("variable_sum_2", "");
        jsonValue.addProperty("sent_date", dtf.format(localDateNow));
        jsonValue.addProperty("phone_no", customer.getPhoneNo());
        jsonValue.addProperty("email_address", customer.getEmailAddress());
        jsonValue.addProperty("valuta_id", "");
        jsonValue.addProperty("va_bca", "");
        jsonValue.addProperty("va_permata", "");
        jsonValue.addProperty("nav", "");
        jsonValue.addProperty("gender_code", convertedGenderCode);
        jsonValue.addProperty("variable_no_2", "");
        jsonValue.addProperty("variable_sum_3", "");
        jsonValue.addProperty("variable_name_2", url );
        jsonValue.addProperty("notes", "");
        jsonValue.addProperty("variable_no_3", dtfHours.format(localDateNow));

        jsonEmail.addProperty("username", "API_MGM");
        jsonEmail.addProperty("signature", "I/vWqxM3DmodUjCALPJOhEjzwbo8kWx2EChS1GEHelU=");
        jsonEmail.addProperty("action", "notification");
        jsonEmail.add("value", jsonValue);

        return jsonEmail;
	}
}
