package id.equity.nichemarket.service.mgm;
import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.mgm.relationship.RelationshipDto;
import id.equity.nichemarket.model.mgm.Relationship;
import id.equity.nichemarket.repository.mgm.RelationshipRepository;

@Service
public class RelationshipService {

	@Autowired
	private RelationshipRepository relationshipRepository;
	
	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private EntityManager em;
	
	//List Relationship
	public ResponseEntity<List<RelationshipDto>> listRelationship(){
		List<Relationship> lsRelationship= relationshipRepository.findAll();
		Type targetType = new TypeToken<List<RelationshipDto>>() {}.getType();
		List<RelationshipDto> response = modelMapper.map(lsRelationship, targetType);

		return ResponseEntity.ok(response);
	}

	//Get Relationship By Id
	public ResponseEntity<RelationshipDto> getRelationshipById(Long id) {
		Relationship relationship = relationshipRepository.findById(id).orElseThrow(()-> new NotFoundException("Relationship id " + id + " is not exist"));
		RelationshipDto response = modelMapper.map(relationship, RelationshipDto.class);

		return ResponseEntity.ok(response);
	}


	//Post Relationship
	@Transactional
	public ResponseEntity<RelationshipDto> addRelationship(RelationshipDto newRelationship) {
		Relationship relationship = modelMapper.map(newRelationship, Relationship.class);
		Relationship relationshipSaved = relationshipRepository.save(relationship);
		em.refresh(relationshipSaved);
		RelationshipDto response = modelMapper.map(relationshipSaved, RelationshipDto.class);
		return ResponseEntity.ok(response);
	}

	//Put Relationship
	public ResponseEntity<RelationshipDto> editRelationship(RelationshipDto updateRelationship, Long id) {
		Relationship relationship = relationshipRepository.findById(id).orElseThrow(()-> new NotFoundException("Relationship id " + id + " is not exist"));
//		relationship.setRelationshipCode(updateRelationship.getRelationshipCode());
//		relationship.setRelationshipName(updateRelationship.getRelationshipName());
//		relationship.setDateOfBirth(updateRelationship.getDateOfBirth());
//		relationship.setOccupation(updateRelationship.getOccupation());
//		relationship.setPhoneNo(updateRelationship.getPhoneNo());
//		relationship.setEmailAddress(updateRelationship.getEmailAddress());
//		relationship.setActive(updateRelationship.isActive());
		relationshipRepository.save(relationship);
		RelationshipDto response = modelMapper.map(relationship, RelationshipDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete Relationship	
	public ResponseEntity<RelationshipDto> deleteRelationship(Long id) {
		Relationship findRelationship = relationshipRepository.findById(id).orElseThrow(()-> new NotFoundException("Relationship id " + id + " is not exist"));
		relationshipRepository.deleteById(id);
		RelationshipDto response = modelMapper.map(findRelationship, RelationshipDto.class);

		return ResponseEntity.ok(response);
	}

	//Get Relationship By relationshipCode
	public RelationshipDto getRelationshipByCode(String relationshipCode) {
		Relationship relationship = relationshipRepository.findByRelationshipCode(relationshipCode);
		return modelMapper.map(relationship, RelationshipDto.class);
	}

	public String getRelationshipCoreCodeByCustomerStatus(String relationshipCode) {
		Relationship relationship = relationshipRepository.findByRelationshipCode(relationshipCode);
		return relationship.getRelationshipCoreCode();
	}

	
}