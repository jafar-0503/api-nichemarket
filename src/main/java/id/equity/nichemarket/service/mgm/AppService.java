package id.equity.nichemarket.service.mgm;
import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.mgm.app.AppDto;
import id.equity.nichemarket.exception.ErrorsException;
import id.equity.nichemarket.model.mgm.App;
import id.equity.nichemarket.repository.mgm.AppRepository;

@Service
public class AppService {

	@Autowired
	private AppRepository appRepository;
	
	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private EntityManager em;
	
	//List App
	public ResponseEntity<List<AppDto>> listApp(){
		List<App> lsApp= appRepository.findAll();
		Type targetType = new TypeToken<List<AppDto>>() {}.getType();
		List<AppDto> response = modelMapper.map(lsApp, targetType);

		return ResponseEntity.ok(response);
	}

	//Get App By Id
	public ResponseEntity<AppDto> getAppById(Long id) {
		App app = appRepository.findById(id).orElseThrow(()-> new NotFoundException("App id " + id + " is not exist"));
		AppDto response = modelMapper.map(app, AppDto.class);

		return ResponseEntity.ok(response);
	}

	

	//Post App
	@Transactional
	public ResponseEntity<AppDto> addApp(AppDto newApp) {
		App app = modelMapper.map(newApp, App.class);
		App appSaved = appRepository.save(app);
		em.refresh(appSaved);
		AppDto response = modelMapper.map(appSaved, AppDto.class);
		return ResponseEntity.ok(response);
	}
	
	@Transactional
	public AppDto saveApp(AppDto newApp) {
		App app = modelMapper.map(newApp, App.class);
		app.setActive(true);
		App appSaved = appRepository.save(app);
		em.refresh(appSaved);
		AppDto response = modelMapper.map(appSaved, AppDto.class);
		return response;
	}

	//Put App
	public ResponseEntity<AppDto> editApp(AppDto updateApp, Long id) {
		App app = appRepository.findById(id).orElseThrow(()-> new NotFoundException("App id " + id + " is not exist"));
		app.setAppCode(updateApp.getAppCode());
		app.setActive(updateApp.isActive());
		appRepository.save(app);
		AppDto response = modelMapper.map(app, AppDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete App	
	public ResponseEntity<AppDto> deleteApp(Long id) {
		App findApp = appRepository.findById(id).orElseThrow(()-> new NotFoundException("App id " + id + " is not exist"));
		appRepository.deleteById(id);
		AppDto response = modelMapper.map(findApp, AppDto.class);

		return ResponseEntity.ok(response);
	}

	//Get App By appCode
	public AppDto getAppByCode(String appCode) {
		App app = appRepository.findByAppCode(appCode);
		if(app == null) {
			throw new ErrorsException("App code " + appCode + " is not exit");
		}
		
		return modelMapper.map(app, AppDto.class);
	}

	public AppDto getAppByAppCode(String appCode) {
		App app = appRepository.findByAppCode(appCode);		
		if(app == null) {
			throw new ErrorsException("App code " + appCode + " is not exit");
		}
		return modelMapper.map(app, AppDto.class);
	}

	
}