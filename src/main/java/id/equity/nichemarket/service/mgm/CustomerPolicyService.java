package id.equity.nichemarket.service.mgm;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.google.gson.JsonObject;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.mgm.customer.CustomerDetailPolicyDto;
import id.equity.nichemarket.dto.mgm.customer.CustomerDto;
import id.equity.nichemarket.dto.mgm.customerPolicy.CustomerPolicyDto;
import id.equity.nichemarket.dto.mgm.customerRegistration.CustomerCertificateReqDto;
import id.equity.nichemarket.exception.ErrorsException;
import id.equity.nichemarket.model.mgm.CustomerPolicy;
import id.equity.nichemarket.repository.mgm.CustomerPolicyRepository;
import id.equity.nichemarket.retrofit.mgm.freeCovid.FreeCovidCertificateResponse;
import id.equity.nichemarket.retrofit.mgm.freeCovid.FreeCovidResponse;
import id.equity.nichemarket.retrofit.mgm.freeCovid.FreeCovidService;

@Service
public class CustomerPolicyService {
    @Autowired
    private FreeCovidService freeCovidService;
    @Autowired
    private CustomerPolicyRepository customerPolicyRepository;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private CustomerRegistrationService customerRegistrationService;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private EntityManager em;

    private static final Logger logger = LoggerFactory.getLogger(CustomerPolicyService.class);

    public CustomerPolicyDto savingFromFreeCovidResponse(FreeCovidResponse freeCovidResponse, String customerCode) {
        CustomerPolicy customerPolicy = modelMapper.map(freeCovidResponse, CustomerPolicy.class);
        customerPolicy.setCustomerReferenceCode("");
        customerPolicy.setCustomerCode(customerCode);
        customerPolicy.setPolicyNo(freeCovidResponse.getPolicy_no());
        customerPolicy.setMemberNo(freeCovidResponse.getMember_no());
        customerPolicy.setInsuranceType(freeCovidResponse.getInsurance_type());
        customerPolicy.setSumInsuredAjb(freeCovidResponse.getSum_insured_ajb());
        customerPolicy.setSumInsuredInpatient(freeCovidResponse.getSum_insured_inpatient());
        customerPolicy.setInsuranceType(freeCovidResponse.getInsurance_type());
        customerPolicy.setRefNo(freeCovidResponse.getRef_no());
        customerPolicy.setFileTypeCode(freeCovidResponse.getFile_type_code());
        customerPolicy.setActive(true);
        CustomerPolicy customerPolicySaved =  customerPolicyRepository.save(customerPolicy);
        em.refresh(customerPolicySaved);
		CustomerPolicyDto customerPolicyDto = modelMapper.map(customerPolicySaved, CustomerPolicyDto.class);
		return customerPolicyDto;
    }

    public CustomerPolicyDto checkCustomerPolicy(String referencCode) {
        CustomerPolicy customerPolicy = customerPolicyRepository.findByCustomerReference(referencCode);
        if (customerPolicy == null){
            throw new ErrorsException("Customer reference code " + referencCode + " is not exist");
        }
        return modelMapper.map(customerPolicy, CustomerPolicyDto.class);
    }

    public FreeCovidCertificateResponse getCovidCertificate(CustomerPolicyDto customerPolicyDto) {
    	try {
            JsonObject contentEmail = new JsonObject();
            JsonObject allEmailContent = new JsonObject();
            contentEmail.addProperty("policy_no", customerPolicyDto.getPolicyNo());
            contentEmail.addProperty("member_no", customerPolicyDto.getMemberNo());
            contentEmail.addProperty("ref_no", customerPolicyDto.getRefNo());

            allEmailContent.addProperty("username", "API_GRP");
            allEmailContent.addProperty("signature", "ItsfaDMljP+ZNw4KTqxWa7XHKAXtzs7+CH3jjiHrd/Y=");
            allEmailContent.addProperty("action", "certificate");
            allEmailContent.add("value", contentEmail);

            FreeCovidCertificateResponse freeCovidResponse = freeCovidService.downloadCertificate(allEmailContent);
            if (freeCovidResponse == null){
                throw new ErrorsException("Failed to send regist certificate");
            }
            return freeCovidResponse;

		} catch (Exception e) {
			logger.error(e.getMessage());
			System.out.println(e.getMessage()); 
			return null;
		}
    }

	public CustomerPolicyDto getPolicyByCustomerCode(String customerCode) {
		CustomerPolicy customerPolicy = customerPolicyRepository.findByCustomerCode(customerCode);
		if (customerPolicy == null){
            throw new ErrorsException("Customer code " + customerCode + " is not exist");
        }
		CustomerPolicyDto customerPolicyDto = modelMapper.map(customerPolicy, CustomerPolicyDto.class);
		return customerPolicyDto;
	}
	
	/**
	 * Version 1
	 * <p>Used on [simedis , linkaja] controller </p>
	 * @author Ferry
	 * @param registrationCode
	 * @return
	 */
	public ResponseEntity<List<CustomerDetailPolicyDto>> getAllPolicyByRegistrationCode(String registrationCode){
		//Get List Of Customer Code By Registration Code
		List<CustomerDto> customer = customerRegistrationService.getAllCustomerByRegCode(registrationCode);
		
		ArrayList<String> customerCodeList = new ArrayList<>();
		for (CustomerDto customerDto : customer) {
			customerCodeList.add(customerDto.getCustomerCode());
		}		
		
		//Get Policy By Customer Code
		List<CustomerDetailPolicyDto> customerPolicyDtoList = customerPolicyRepository.findByMultipleCustomerCode(customerCodeList); 
		logger.info("Customer Policy List : " + customerPolicyDtoList);
		
		return ResponseEntity.ok(customerPolicyDtoList);		
	}
	
	public ResponseEntity<FreeCovidCertificateResponse> generateCertificateByCustomerCode(CustomerCertificateReqDto customerRegistrationReq) {
		CustomerPolicy customerPolicy = customerPolicyRepository.findByCustomerCodeAndRegistrationCode(customerRegistrationReq.getCustomerCode() , customerRegistrationReq.getRegistrationCode());
		if(customerPolicy == null) {
			throw new NotFoundException("Customer Policy with customer code: " + customerRegistrationReq.getCustomerCode() + " And registration code: " + customerRegistrationReq.getRegistrationCode() + " is not found");
		}
		CustomerPolicyDto customerPolicyDto = modelMapper.map(customerPolicy, CustomerPolicyDto.class);
		FreeCovidCertificateResponse response = getCovidCertificate(customerPolicyDto);
		if(response == null) {
			throw new ErrorsException("Failed Generate Certificate");
		}
		
		if(!response.getError_code().equals("1")) {
			throw new ErrorsException("Failed Generate Certificate " + "caused : " + response.getError_description());
		}
		
		return ResponseEntity.ok(response);		
	}

	@Transactional
	public List<CustomerPolicyDto> saveAllCustomerPolicy(List<CustomerDto> allCustomer, String registrationCode) {
		List<CustomerPolicy> customerPolicyList = new ArrayList<>();
		for(CustomerDto customer : allCustomer) {
			CustomerPolicy customerPolicy = new CustomerPolicy();
			customerPolicy.setCustomerReferenceCode("");
			customerPolicy.setRegistrationCode(registrationCode);
	        customerPolicy.setCustomerCode(customer.getCustomerCode());
	        customerPolicy.setPolicyNo("");
	        customerPolicy.setMemberNo("");
	        customerPolicy.setInsuranceType("");
	        customerPolicy.setSumInsuredAjb("");
	        customerPolicy.setSumInsuredInpatient("");
	        customerPolicy.setInsuranceType("");
	        customerPolicy.setRefNo("");
	        customerPolicy.setFileTypeCode("");
	        customerPolicy.setActive(true);
	        customerPolicy.setSyncedToCore(false);
	        customerPolicyList.add(customerPolicy);

		}
        List<CustomerPolicy> customerPolicySaved =  customerPolicyRepository.saveAll(customerPolicyList);
        for(CustomerPolicy customerPolicy : customerPolicySaved) {
        	em.refresh(customerPolicy);
        }
        
		Type targetType = new TypeToken<List<CustomerPolicyDto>>() {}.getType();
		List<CustomerPolicyDto> customerPolicyDto = modelMapper.map(customerPolicySaved, targetType);

		return customerPolicyDto;
	}

	public CustomerPolicyDto updateFromFreeCovidResponse(FreeCovidResponse data) {
		CustomerPolicy customerPolicy = customerPolicyRepository.findByCustomerPolicyCoreCode(data.getTransaction_code())
																		.orElseThrow(()-> new NotFoundException("Customer Policy Core Code " + data.getTransaction_code() + " is not exist"));
		
		customerPolicy.setPolicyNo(data.getPolicy_no());
		customerPolicy.setMemberNo(data.getMember_no());
		customerPolicy.setInsuranceType(data.getInsurance_type());
		customerPolicy.setInsurancePeriod(data.getInsurance_period());
		customerPolicy.setSumInsuredAjb(data.getSum_insured_ajb());
		customerPolicy.setSumInsuredInpatient(data.getSum_insured_inpatient());
		customerPolicy.setRefNo(data.getRef_no());
		customerPolicy.setFileTypeCode(data.getFile_type_code());
		customerPolicy.setSyncedToCore(true);
		customerPolicyRepository.save(customerPolicy);
		
		CustomerPolicyDto response = modelMapper.map(customerPolicy, CustomerPolicyDto.class);

		return response;
	}


	public CustomerPolicyDto getPolicyByCoreCode(String customerPolicyCoreCode) {
		CustomerPolicy customerPolicy = customerPolicyRepository.findByCustomerPolicyCoreCode(customerPolicyCoreCode)
																.orElseThrow(()-> new NotFoundException("Customer policy Core Code " + customerPolicyCoreCode + " is not exist"));
		CustomerPolicyDto response = modelMapper.map(customerPolicy , CustomerPolicyDto.class);
		return response;
	}

	public int getTotalMemberInsuredByRegistrationCode(String registrationCode) {
		int totalMemberInsured = 0;
		totalMemberInsured = customerPolicyRepository.countMemberInsuredByRegCode(registrationCode);		
		return totalMemberInsured;
	}

	public List<CustomerPolicyDto> getFailedSyncCustomerPolicy() {
		List<CustomerPolicy> customerPolicy = customerPolicyRepository.findByStatusSyncCore(false);
		Type targetType = new TypeToken<List<CustomerPolicyDto>>() {}.getType();

		List<CustomerPolicyDto> response = modelMapper.map(customerPolicy , targetType);
		return response;
	}
}
