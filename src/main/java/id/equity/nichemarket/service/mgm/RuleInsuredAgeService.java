package id.equity.nichemarket.service.mgm;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.mgm.ruleInsuredAge.RuleInsuredAgeDto;
import id.equity.nichemarket.exception.CustomerValidationException;
import id.equity.nichemarket.exception.ErrorsException;
import id.equity.nichemarket.model.mgm.RuleInsuredAge;
import id.equity.nichemarket.repository.mgm.RuleInsuredAgeRepository;

@Service
public class RuleInsuredAgeService {

	@Autowired
	private RuleInsuredAgeRepository ruleInsuredAgeRepository;
	
	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private EntityManager em;
	
	//List RuleInsuredAge
	public ResponseEntity<List<RuleInsuredAgeDto>> listRuleInsuredAge(){
		List<RuleInsuredAge> lsRuleInsuredAge= ruleInsuredAgeRepository.findAll();
		Type targetType = new TypeToken<List<RuleInsuredAgeDto>>() {}.getType();
		List<RuleInsuredAgeDto> response = modelMapper.map(lsRuleInsuredAge, targetType);

		return ResponseEntity.ok(response);
	}

	//Get RuleInsuredAge By Id
	public ResponseEntity<RuleInsuredAgeDto> getRuleInsuredAgeById(Long id) {
		RuleInsuredAge ruleInsuredAge = ruleInsuredAgeRepository.findById(id).orElseThrow(()-> new NotFoundException("RuleInsuredAge id " + id + " is not exist"));
		RuleInsuredAgeDto response = modelMapper.map(ruleInsuredAge, RuleInsuredAgeDto.class);

		return ResponseEntity.ok(response);
	}

	

	//Post RuleInsuredAge
	@Transactional
	public ResponseEntity<RuleInsuredAgeDto> addRuleInsuredAge(RuleInsuredAgeDto newRuleInsuredAge) {
		RuleInsuredAge ruleInsuredAge = modelMapper.map(newRuleInsuredAge, RuleInsuredAge.class);
		RuleInsuredAge ruleInsuredAgeSaved = ruleInsuredAgeRepository.save(ruleInsuredAge);
		em.refresh(ruleInsuredAgeSaved);
		RuleInsuredAgeDto response = modelMapper.map(ruleInsuredAgeSaved, RuleInsuredAgeDto.class);
		return ResponseEntity.ok(response);
	}
	
	@Transactional
	public RuleInsuredAgeDto saveRuleInsuredAge(RuleInsuredAgeDto newRuleInsuredAge) {
		RuleInsuredAge ruleInsuredAge = modelMapper.map(newRuleInsuredAge, RuleInsuredAge.class);
		ruleInsuredAge.setActive(true);
		RuleInsuredAge ruleInsuredAgeSaved = ruleInsuredAgeRepository.save(ruleInsuredAge);
		em.refresh(ruleInsuredAgeSaved);
		RuleInsuredAgeDto response = modelMapper.map(ruleInsuredAgeSaved, RuleInsuredAgeDto.class);
		return response;
	}

	//Put RuleInsuredAge
	public ResponseEntity<RuleInsuredAgeDto> editRuleInsuredAge(RuleInsuredAgeDto updateRuleInsuredAge, Long id) {
		RuleInsuredAge ruleInsuredAge = ruleInsuredAgeRepository.findById(id).orElseThrow(()-> new NotFoundException("RuleInsuredAge id " + id + " is not exist"));
		ruleInsuredAge.setRelationshipCode(updateRuleInsuredAge.getRelationshipCode());
		ruleInsuredAge.setActive(updateRuleInsuredAge.isActive());
		ruleInsuredAgeRepository.save(ruleInsuredAge);
		RuleInsuredAgeDto response = modelMapper.map(ruleInsuredAge, RuleInsuredAgeDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete RuleInsuredAge	
	public ResponseEntity<RuleInsuredAgeDto> deleteRuleInsuredAge(Long id) {
		RuleInsuredAge findRuleInsuredAge = ruleInsuredAgeRepository.findById(id).orElseThrow(()-> new NotFoundException("RuleInsuredAge id " + id + " is not exist"));
		ruleInsuredAgeRepository.deleteById(id);
		RuleInsuredAgeDto response = modelMapper.map(findRuleInsuredAge, RuleInsuredAgeDto.class);

		return ResponseEntity.ok(response);
	}

	//Get RuleInsuredAge By ruleInsuredAgeCode
	public RuleInsuredAgeDto getRuleInsuredAgeByCode(String ruleInsuredAgeCode) {
		RuleInsuredAge ruleInsuredAge = ruleInsuredAgeRepository.findByRuleInsuredAgeCode(ruleInsuredAgeCode);
		if(ruleInsuredAge == null) {
			throw new ErrorsException("RuleInsuredAge code " + ruleInsuredAgeCode + " is not exit");
		}
		
		return modelMapper.map(ruleInsuredAge, RuleInsuredAgeDto.class);
	}

	public RuleInsuredAgeDto getRuleInsuredAgeByRegTypeCodeAndCustomerStatus(String registrationTypeCode,
			String customerStatus) {
		RuleInsuredAge ruleInsuredAge = ruleInsuredAgeRepository.findByRegistrationTypeCodeAndCustomerStatus(registrationTypeCode, customerStatus);
		if(ruleInsuredAge == null) {
			List<String> errors = new ArrayList<>();
			errors.add("Date Of Birth is Invalid");
			throw new CustomerValidationException("Invalid Customer Data" , errors);
		}
		
		return modelMapper.map(ruleInsuredAge, RuleInsuredAgeDto.class);
	}

	
}