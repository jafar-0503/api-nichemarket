package id.equity.nichemarket.service.mgm;

import id.equity.nichemarket.dto.mgm.dataFromLandingPage.DataFromLandingPage2;
import id.equity.nichemarket.dto.mgm.interactive.CreateInteractiveNeedAndPriority;
import id.equity.nichemarket.dto.mgm.interactive.InteractiveDto;
import id.equity.nichemarket.exception.ErrorsException;
import id.equity.nichemarket.model.mgm.Interactive;
import id.equity.nichemarket.repository.mgm.InteractiveRepository;
import id.equity.nichemarket.retrofit.mgm.EmailNotificationResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class InteractiveService {
    @Autowired
    private InteractivePriorityService interactivePriorityService;
    @Autowired
    private InteractiveNeedsService interactiveNeedsService;
    @Autowired
    private LandingPageService landingPageService;
    @Autowired
    private InteractiveRepository interactiveRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private EntityManager em;

    @Transactional
    public InteractiveDto savingData(CreateInteractiveNeedAndPriority createInteractiveNeedAndPriority) {
        InteractiveDto interactiveDto = savingInteractive(createInteractiveNeedAndPriority);
        List<String> interactivePriority = new ArrayList(Arrays.asList(createInteractiveNeedAndPriority.getCustomerPriority()));
        List<String> interactiveNeeds = new ArrayList(Arrays.asList(createInteractiveNeedAndPriority.getCustomerNeeds()));
        interactivePriorityService.saveInteractivePriority(interactivePriority, interactiveDto.getInteractiveCode());
        interactiveNeedsService.saveInteractiveNeeds(interactiveNeeds, interactiveDto.getInteractiveCode());

        return interactiveDto;
    }


    @Transactional
    public EmailNotificationResponse sendEmailNotification(
            CreateInteractiveNeedAndPriority createInteractiveNeedAndPriority,
            String[] customerPriority) {
        InteractiveDto checkEmail = interactivePriorityService.getInteractiveByEmail(
                createInteractiveNeedAndPriority.getEmailAddress());
        if (checkEmail != null){throw new ErrorsException("Email already registered!");}
        InteractiveDto checkPhone = interactivePriorityService.getInteractiveByPhone(
                createInteractiveNeedAndPriority.getPhoneNo());
        if (checkPhone != null){throw new ErrorsException("Phone Number already registered!");}

        String newCustomerPriority = "";
        for (int i = 1; i <= customerPriority.length; i++){
            if (i == 1){
                newCustomerPriority = newCustomerPriority + "<br>" + i + ". " + customerPriority[i-1] + "<br>";
            } else {
                newCustomerPriority = newCustomerPriority + i + ". " + customerPriority[i-1] + "<br>";
            }
        }
        DataFromLandingPage2 dataToSendNotif = migrateInteractiveToLandingPage2(createInteractiveNeedAndPriority);
        EmailNotificationResponse emailResponse = landingPageService.sendNotif(dataToSendNotif, newCustomerPriority);

        return emailResponse;
    }

    public InteractiveDto savingInteractive(CreateInteractiveNeedAndPriority data){
        Interactive interactive = modelMapper.map(data, Interactive.class);
        Interactive interactiveSaved = interactiveRepository.save(interactive);
        em.refresh(interactiveSaved);
        InteractiveDto interactiveDtoSaved = modelMapper.map(interactiveSaved, InteractiveDto.class);
        return interactiveDtoSaved;
    }

    public DataFromLandingPage2 migrateInteractiveToLandingPage2(CreateInteractiveNeedAndPriority data){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        DateTimeFormatter dtfHours = DateTimeFormatter.ofPattern("HHmm");
        LocalDateTime localDateNow = LocalDateTime.now();

        DataFromLandingPage2 newData =  new DataFromLandingPage2();
        newData.setMessage_type("E");
        newData.setType_code("E26");
        newData.setVariable_no_1("");
        newData.setVariable_name_1(data.getCustomerName());
        newData.setVariable_date_1(data.getDateOfBirth());
        newData.setVariable_sum_1("");
        newData.setVariable_date_2("");
        newData.setVariable_sum_2("");
        newData.setSent_date(dtf.format(localDateNow));
        newData.setPhone_no(data.getPhoneNo());
        newData.setEmail_address(data.getEmailAddress());
        newData.setValuta_id("");
        newData.setVa_bca("");
        newData.setVa_permata("");
        newData.setNav("");
        newData.setGender_code(data.getGenderCode());
        newData.setVariable_no_2("");
        newData.setVariable_sum_3("");
        newData.setVariable_name_2("");
        newData.setVariable_no_3(dtfHours.format(localDateNow));
        newData.setNotes("");
        return newData;
    }

}
