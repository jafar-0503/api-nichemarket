package id.equity.nichemarket.service.mgm;

import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.mgm.product.ProductPolicyDto;
import id.equity.nichemarket.exception.ErrorsException;
import id.equity.nichemarket.model.mgm.ProductPolicy;
import id.equity.nichemarket.repository.mgm.ProductPolicyRepository;

@Service
public class ProductPolicyService {

	@Autowired
	private ProductPolicyRepository productPolicyRepository;
	
	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private EntityManager em;
	
	//List ProductPolicy
	public ResponseEntity<List<ProductPolicyDto>> listProductPolicy(){
		List<ProductPolicy> lsProductPolicy= productPolicyRepository.findAll();
		Type targetType = new TypeToken<List<ProductPolicyDto>>() {}.getType();
		List<ProductPolicyDto> response = modelMapper.map(lsProductPolicy, targetType);

		return ResponseEntity.ok(response);
	}

	//Get ProductPolicy By Id
	public ResponseEntity<ProductPolicyDto> getProductPolicyById(Long id) {
		ProductPolicy productPolicy = productPolicyRepository.findById(id).orElseThrow(()-> new NotFoundException("ProductPolicy id " + id + " is not exist"));
		ProductPolicyDto response = modelMapper.map(productPolicy, ProductPolicyDto.class);

		return ResponseEntity.ok(response);
	}

	

	//Post ProductPolicy
	@Transactional
	public ResponseEntity<ProductPolicyDto> addProductPolicy(ProductPolicyDto newProductPolicy) {
		ProductPolicy productPolicy = modelMapper.map(newProductPolicy, ProductPolicy.class);
		ProductPolicy productPolicySaved = productPolicyRepository.save(productPolicy);
		em.refresh(productPolicySaved);
		ProductPolicyDto response = modelMapper.map(productPolicySaved, ProductPolicyDto.class);
		return ResponseEntity.ok(response);
	}
	
	@Transactional
	public ProductPolicyDto saveProductPolicy(ProductPolicyDto newProductPolicy) {
		ProductPolicy productPolicy = modelMapper.map(newProductPolicy, ProductPolicy.class);
		ProductPolicy productPolicySaved = productPolicyRepository.save(productPolicy);
		em.refresh(productPolicySaved);
		ProductPolicyDto response = modelMapper.map(productPolicySaved, ProductPolicyDto.class);
		return response;
	}

	//Put ProductPolicy
	public ResponseEntity<ProductPolicyDto> editProductPolicy(ProductPolicyDto updateProductPolicy, Long id) {
		ProductPolicy productPolicy = productPolicyRepository.findById(id).orElseThrow(()-> new NotFoundException("ProductPolicy id " + id + " is not exist"));
		productPolicyRepository.save(productPolicy);
		ProductPolicyDto response = modelMapper.map(productPolicy, ProductPolicyDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete ProductPolicy	
	public ResponseEntity<ProductPolicyDto> deleteProductPolicy(Long id) {
		ProductPolicy findProductPolicy = productPolicyRepository.findById(id).orElseThrow(()-> new NotFoundException("ProductPolicy id " + id + " is not exist"));
		productPolicyRepository.deleteById(id);
		ProductPolicyDto response = modelMapper.map(findProductPolicy, ProductPolicyDto.class);

		return ResponseEntity.ok(response);
	}

	public ProductPolicyDto getProductPolicyByCode(String productPolicyCode) {
		ProductPolicy findProductPolicy = productPolicyRepository.findByProductPolicyCode(productPolicyCode);
		ProductPolicyDto response = modelMapper.map(findProductPolicy, ProductPolicyDto.class);
		return response;
	}

	//Get ProductPolicy By productPolicyCode
//	public ProductPolicyDto getProductPolicyByProductCode(String productPolicyCode) {
//		ProductPolicy productPolicy = productPolicyRepository.findByProductCode(productPolicyCode);
//		if(productPolicy == null) {
//			throw new ErrorsException("ProductPolicy code " + productPolicyCode + " is not exit");
//		}
//		
//		return modelMapper.map(productPolicy, ProductPolicyDto.class);
//	}

	
}


