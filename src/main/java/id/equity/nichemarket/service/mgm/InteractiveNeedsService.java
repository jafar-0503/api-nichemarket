package id.equity.nichemarket.service.mgm;

import id.equity.nichemarket.model.mgm.InteractiveNeeds;
import id.equity.nichemarket.repository.mgm.InteractiveNeedsRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InteractiveNeedsService {
    @Autowired
    private InteractiveNeedsRepository interactiveNeedsRepository;
    @Autowired
    private ModelMapper modelMapper;

    private static final Logger logger = LoggerFactory.getLogger(InteractiveNeedsService.class);

    public Boolean saveInteractiveNeeds(List<String> data, String interactiveCode) {
        int total = 0;
        for (String savingData : data){
            InteractiveNeeds interactiveNeeds = modelMapper.map(savingData, InteractiveNeeds.class);
            interactiveNeeds.setInteractiveCode(interactiveCode);
            interactiveNeeds.setDescription(savingData);
            interactiveNeeds.setActive(true);
            interactiveNeedsRepository.save(interactiveNeeds);
            total++;
        }
        if (total != data.size()){
            logger.info("Failed to save interactive priority");
            return false;
        }
        return true;
    }
}
