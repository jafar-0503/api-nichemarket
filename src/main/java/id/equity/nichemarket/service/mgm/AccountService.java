package id.equity.nichemarket.service.mgm;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.mgm.account.AccountActivationDto;
import id.equity.nichemarket.dto.mgm.account.AccountActivationResponseDto;
import id.equity.nichemarket.dto.mgm.account.AccountCustomerDto;
import id.equity.nichemarket.dto.mgm.account.AccountDetailPolicyResponseDto;
import id.equity.nichemarket.dto.mgm.account.AccountPointResponseDto;
import id.equity.nichemarket.dto.mgm.account.AccountProfileResponseDto;
import id.equity.nichemarket.dto.mgm.account.AccountValidationResponseDto;
import id.equity.nichemarket.dto.mgm.customer.CustomerDto;
import id.equity.nichemarket.dto.mgm.customerPoint.CustomerPointDto;
import id.equity.nichemarket.dto.mgm.customerPolicy.CustomerPolicyDto;
import id.equity.nichemarket.dto.mgm.projection.ICountActiveEmailAddressDto;
import id.equity.nichemarket.dto.mgm.projection.ISummaryProductionDataDto;
import id.equity.nichemarket.dto.mgm.registration.RegistrationDto;
import id.equity.nichemarket.dto.mgm.registrationType.RegistrationTypeDto;
import id.equity.nichemarket.dto.rm.user.CreateUserDto;
import id.equity.nichemarket.exception.ErrorsException;
import id.equity.nichemarket.model.mgm.Account;
import id.equity.nichemarket.model.rm.User;
import id.equity.nichemarket.repository.mgm.AccountRepository;
import id.equity.nichemarket.repository.mgm.CustomerRepository;
import id.equity.nichemarket.service.rm.UserService;

@Service
public class AccountService {

	@Autowired
	private UserService userService;
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@Autowired
	private AccountRepository accountRepository;
	
	@Autowired
	private CustomerService customerService;
	
	
	@Autowired
	private CustomerRegistrationService customerRegistrationService;
	
	@Autowired
	private CustomerPointService customerPointService;
	
	@Autowired
	private RegistrationService registrationService;
	
	@Autowired
	private RegistrationTypeService registrationTypeService;

	@Autowired
	private CustomerReferralService customerReferralService;
	
	@Autowired
	private CustomerPolicyService customerPolicyService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private EntityManager em;
	
	
	private String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    public String generateUniqueKey(){
        int codeLength = 15;
        Random random = new Random();
        StringBuilder builder = new StringBuilder(codeLength);
        Account checkExistKey= null;
        int trying = 0;
        do {
             for (int i = 0; i < codeLength; i++) {
                 builder.append(this.alphabet.charAt(random.nextInt(this.alphabet.length())));
             }
             checkExistKey = accountRepository.findByKey(builder.toString());
             trying++;
         } while (checkExistKey != null && trying < 3);
        return builder.toString();
    }
	
	//List Account
	public ResponseEntity<List<AccountCustomerDto>> listAccount(){
		List<Account> lsAccount= accountRepository.findAll();
		Type targetType = new TypeToken<List<AccountCustomerDto>>() {}.getType();
		List<AccountCustomerDto> response = modelMapper.map(lsAccount, targetType);

		return ResponseEntity.ok(response);
	}

	//Get Account By Id
	public ResponseEntity<AccountCustomerDto> getAccountById(Long id) {
		Account account = accountRepository.findById(id).orElseThrow(()-> new NotFoundException("Account id " + id + " is not exist"));
		AccountCustomerDto response = modelMapper.map(account, AccountCustomerDto.class);

		return ResponseEntity.ok(response);
	}

	

	//Post Account
	@Transactional
	public ResponseEntity<AccountCustomerDto> addAccount(AccountCustomerDto newAccount) {
		Account account = modelMapper.map(newAccount, Account.class);
		Account accountSaved = accountRepository.save(account);
		em.refresh(accountSaved);
		AccountCustomerDto response = modelMapper.map(accountSaved, AccountCustomerDto.class);
		return ResponseEntity.ok(response);
	}
	
	@Transactional
	public AccountCustomerDto saveAccount(AccountCustomerDto newAccount) {
		Account account = modelMapper.map(newAccount, Account.class);
		Account accountSaved = accountRepository.save(account);
		em.refresh(accountSaved);
		AccountCustomerDto response = modelMapper.map(accountSaved, AccountCustomerDto.class);
		return response;
	}
	
	//Get Account By accountCode
	public AccountCustomerDto getAccountByCode(String accountCode) {
		Account account = accountRepository.findByAccountCode(accountCode);
		if(account == null) {
			throw new ErrorsException("Account code " + accountCode + " is not exit");
		}
		
		return modelMapper.map(account, AccountCustomerDto.class);
	}
	
	//Get Account By key
	public AccountCustomerDto _getAccountByActivationKey(String uniqueActivationKey) {
		Account account = accountRepository.findByKey(uniqueActivationKey);
		if(account == null) {
			throw new ErrorsException("Activation Key " + uniqueActivationKey + " is not found");
		}
		
		return modelMapper.map(account, AccountCustomerDto.class);
	}

	//Put Account
	public ResponseEntity<AccountCustomerDto> editAccount(AccountCustomerDto updateAccount, Long id) {
		Account account = accountRepository.findById(id).orElseThrow(()-> new NotFoundException("Account id " + id + " is not exist"));
		account.setAccountCode(updateAccount.getAccountCode());		
		account.setActive(updateAccount.isActive());
		accountRepository.save(account);
		AccountCustomerDto response = modelMapper.map(account, AccountCustomerDto.class);

		return ResponseEntity.ok(response);
	}
	
	public ResponseEntity<AccountValidationResponseDto> validateActivationKey(String uniqueActivationKey) {
		//Check if activationKey is exist
		AccountCustomerDto account = this._getAccountByActivationKey(uniqueActivationKey);
		
		if(account == null) {
			throw new NotFoundException("key is not exist");
		}
		
		if(account.isAccountActivated()) {
			throw new ErrorsException("Account Is already active");
		}
		
		//Get Primary Customer By Registration Code
//		CustomerDto customer = customerRegistrationService.getPrimaryCustomerByRegistrationCode(registration.getRegistrationCode());	
		AccountValidationResponseDto response = new AccountValidationResponseDto();
		response.setAccountCode(account.getAccountCode());
		response.setEmailAddress(account.getEmailAddress());
		return ResponseEntity.ok(response);
	}
	
	@Transactional
	public ResponseEntity<AccountActivationResponseDto> activateAccount(AccountActivationDto account) {
		//Check if account already actived
		Account currAccount = accountRepository.findByAccountCode(account.getAccountCode());	
		
		
		if(currAccount.isAccountActivated()) {
			throw new ErrorsException("Account Already Active");
		}
		
		if(!(currAccount.isActive())) {
			throw new ErrorsException("Account Haven't Pay Bill");
		}	
		
		//Get First Registration Code
		RegistrationDto registration = registrationService.getFirstRegistrationByAccountCode(currAccount.getAccountCode());
		
		
		//Get Detail Customer With 0 status by Registration Code
//		AccountActivationDto accountToActivate = modelMapper.map(account, AccountActivationDto.class);		
		CustomerDto customer = customerRegistrationService.getPrimaryCustomerByRegistrationCode(registration.getRegistrationCode());		
		
		//Register User Service
		CreateUserDto user = new CreateUserDto();
		user.setEmail(customer.getEmailAddress());
		user.setFirstName(customer.getCustomerName());
		user.setLastName("");
		user.setUserTypeCode("C001");
		user.setUsername(customer.getEmailAddress());
		user.setPassword(account.getPassword());
		
		//Save User Account
		User userSaved = userService.save(user);
		em.refresh(userSaved);
		
		//Update Account is Actived
//		RegistrationDto updateRegistration = new RegistrationDto();
//		updateRegistration.setAccountActivated(true);
//		RegistrationDto registration = registrationService.updateRegistrationStatus(updateRegistration, accountToActivate.getRegistrationCode());
		
		AccountCustomerDto updateAccount = new AccountCustomerDto();
		updateAccount.setAccountActivated(true);
		updateAccount.setActive(true);
		AccountCustomerDto updatedAccount = this.updateAccountStatus(updateAccount, account.getAccountCode());
		
		
		AccountActivationResponseDto response = modelMapper.map(userSaved, AccountActivationResponseDto.class);
		response.setAccountCode(account.getAccountCode());		
		
		return ResponseEntity.ok(response);
	}
	
	//Put Account
	public AccountCustomerDto updateAccountStatus(AccountCustomerDto updateAccount, String accountCode) {
		Account account = accountRepository.findByAccountCode(accountCode);			
		account.setAccountActivated(updateAccount.isAccountActivated());
		account.setActive(updateAccount.isActive());
		accountRepository.save(account);
		AccountCustomerDto response = modelMapper.map(account, AccountCustomerDto.class);

		return response;
	}
	
	
	
	
	public ResponseEntity<List<AccountDetailPolicyResponseDto>> getListAccountPolicy(String emailAddress) {		
		//Get Account  By Email Customer
		AccountCustomerDto account = this.getAccountByEmailAddress(emailAddress);
		
		//Get Registration type code By Active Registration Code
		List<RegistrationDto> registration = registrationService.getRegistrationByAccountCode(account.getAccountCode());

		List<AccountDetailPolicyResponseDto> accountDetailPolicyList = new ArrayList<>();
		
		//Loop registration
		for (RegistrationDto myRegistration : registration) {
			AccountDetailPolicyResponseDto accountDetailPolicyResponse = new AccountDetailPolicyResponseDto();
			//Get Registration 
			
			
			//Get Registration Type
			RegistrationTypeDto registrationType = registrationTypeService.getRegistrationTypeByCode(myRegistration.getRegistrationTypeCode());
			
			//Get Primary Customer By RegistrationCode
			CustomerDto primaryCustomer = customerRegistrationService.getPrimaryCustomerByRegistrationCode(myRegistration.getRegistrationCode()); 
			
			//Get Customer Policy
			CustomerPolicyDto customerPolicy = customerPolicyService.getPolicyByCustomerCode(primaryCustomer.getCustomerCode());			

			
			//Get Total Member Insured
			int totalMemberInsured = customerPolicyService.getTotalMemberInsuredByRegistrationCode(myRegistration.getRegistrationCode());

			accountDetailPolicyResponse.setRegistrationCode(myRegistration.getRegistrationCode());
			accountDetailPolicyResponse.setProductPolicy(null);
			accountDetailPolicyResponse.setRegistrationType(registrationType);		
			accountDetailPolicyResponse.setCustomerPolicy(customerPolicy);
			accountDetailPolicyResponse.setTotalMemberInsured(totalMemberInsured);
			accountDetailPolicyList.add(accountDetailPolicyResponse);
		}				
		
		
		return ResponseEntity.ok(accountDetailPolicyList);
	}

	public AccountCustomerDto getAccountByEmailAddress(String emailAddress) {
		Account account = accountRepository.findByEmailAddressAndActive(emailAddress);
		if(account == null) {
			throw new ErrorsException("Email Address " + emailAddress + " is not exist");
		}
		
		return modelMapper.map(account, AccountCustomerDto.class);
	}
	
	
	public ResponseEntity<AccountPointResponseDto> getAccountPoint(String emailAddress) {
		//Get Account  By Email Customer
		AccountCustomerDto account = this.getAccountByEmailAddress(emailAddress);
		
		//Get Point
		CustomerPointDto customerPoint = customerPointService.getCustomerPointByAccountCode(account.getAccountCode());
		
		//Total Referenced
		Integer totalReferenced = 0;
		String referralCode = customerReferralService.getReferralCodeByAccountCode(account.getAccountCode());
		
		if(!referralCode.equals("")) {
			totalReferenced = customerReferralService.getTotalReferencedByReferralCode(referralCode);
		}				
		
		AccountPointResponseDto response = new AccountPointResponseDto();
		response.setPoint(customerPoint.getCurrentPoint());
		response.setTotalReferenced(totalReferenced);
		response.setReferralCode(referralCode);
		
		return ResponseEntity.ok(response);
	}
	
	public ResponseEntity<AccountProfileResponseDto> getAccountProfile(String emailAddress) {
		//Get Account  By Email Customer
		AccountCustomerDto account = this.getAccountByEmailAddress(emailAddress);
		
		//Get First Registration By Account
		RegistrationDto firstRegistration = registrationService.getFirstRegistrationByAccountCode(account.getAccountCode());
		
		//Get Primary Customer
		CustomerDto primaryCustomer = customerRegistrationService.getPrimaryCustomerByRegistrationCode(firstRegistration.getRegistrationCode());							
		
		AccountProfileResponseDto response = new AccountProfileResponseDto();
		response.setCustomerName(primaryCustomer.getCustomerName());
		response.setGenderCode(primaryCustomer.getGenderCode());
		response.setDateOfBirth(primaryCustomer.getDateOfBirth());
		response.setPhoneNo(primaryCustomer.getPhoneNo());
		
		return ResponseEntity.ok(response);
	}


	public Integer countActiveEmailAddress(String emailAddress) {
		ICountActiveEmailAddressDto countActiveKtpCustomer = accountRepository.countEmailAddressPaidPayment(emailAddress);
		Integer totalActiveKtpNo = countActiveKtpCustomer.getCount();
		return totalActiveKtpNo;
	}
	
	public List<ISummaryProductionDataDto> getProductionData() {
		List<ISummaryProductionDataDto> summary = accountRepository.getProductionData();
		return summary;
	}	
	
}
