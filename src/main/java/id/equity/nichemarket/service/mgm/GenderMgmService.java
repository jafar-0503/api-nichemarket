package id.equity.nichemarket.service.mgm;

import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.mgm.gender.GenderDto;
import id.equity.nichemarket.model.mgm.GenderMgm;
import id.equity.nichemarket.repository.mgm.GenderMgmRepository;

@Service
public class GenderMgmService {
	@Autowired
	private GenderMgmRepository genderRepository;
	
	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private EntityManager em;
	
	//List Gender
	public ResponseEntity<List<GenderDto>> listGender(){
		List<GenderMgm> lsGender= genderRepository.findAll();
		Type targetType = new TypeToken<List<GenderDto>>() {}.getType();
		List<GenderDto> response = modelMapper.map(lsGender, targetType);

		return ResponseEntity.ok(response);
	}

	//Get Gender By Id
	public ResponseEntity<GenderDto> getGenderById(Long id) {
		GenderMgm gender = genderRepository.findById(id).orElseThrow(()-> new NotFoundException("Gender id " + id + " is not exist"));
		GenderDto response = modelMapper.map(gender, GenderDto.class);

		return ResponseEntity.ok(response);
	}

	

	//Post Gender
	@Transactional
	public ResponseEntity<GenderDto> addGender(GenderDto newGender) {
		GenderMgm gender = modelMapper.map(newGender, GenderMgm.class);
		GenderMgm genderSaved = genderRepository.save(gender);
		em.refresh(genderSaved);
		GenderDto response = modelMapper.map(genderSaved, GenderDto.class);
		return ResponseEntity.ok(response);
	}

	//Put Gender
	public ResponseEntity<GenderDto> editGender(GenderDto updateGender, Long id) {
		GenderMgm gender = genderRepository.findById(id).orElseThrow(()-> new NotFoundException("Gender id " + id + " is not exist"));
		gender.setGenderCode(updateGender.getGenderCode());
		gender.setGenderDesc(updateGender.getGenderDesc());
		gender.setActive(updateGender.isActive());
		genderRepository.save(gender);
		GenderDto response = modelMapper.map(gender, GenderDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete Gender	
	public ResponseEntity<GenderDto> deleteGender(Long id) {
		GenderMgm findGender = genderRepository.findById(id).orElseThrow(()-> new NotFoundException("Gender id " + id + " is not exist"));
		genderRepository.deleteById(id);
		GenderDto response = modelMapper.map(findGender, GenderDto.class);

		return ResponseEntity.ok(response);
	}

	//Get Gender By genderCode
	public GenderDto getGenderByCode(String genderCode) {
		GenderMgm gender = genderRepository.findByGenderCode(genderCode);
		return modelMapper.map(gender, GenderDto.class);
	}

}
