package id.equity.nichemarket.service.mgm;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.mgm.logMessage.CreateLogMessage;
import id.equity.nichemarket.dto.mgm.logMessage.LogMessageDto;
import id.equity.nichemarket.dto.mgm.dataFromLandingPage.DataFromLandingPage2;
import id.equity.nichemarket.model.mgm.LogMessage;
import id.equity.nichemarket.repository.mgm.LogMessageRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class LogMessageService {

	@Autowired
	private LogMessageRepository logMessageRepository;
	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	private EntityManager em;
	
	//List LogMessage
	public ResponseEntity<List<LogMessageDto>> listLogMessage(){
		List<LogMessage> lsLogMessage= logMessageRepository.findAll();
		Type targetType = new TypeToken<List<LogMessageDto>>() {}.getType();
		List<LogMessageDto> response = modelMapper.map(lsLogMessage, targetType);

		return ResponseEntity.ok(response);
	}

	//Get LogMessage By Id
	public ResponseEntity<LogMessageDto> getLogMessageById(Long id) {
		LogMessage logMessage = logMessageRepository.findById(id).orElseThrow(()-> new NotFoundException("Log Message id " + id + " is not exist"));
		LogMessageDto response = modelMapper.map(logMessage, LogMessageDto.class);

		return ResponseEntity.ok(response);
	}

	//Post LogMessage
	public ResponseEntity<LogMessageDto> addLogMessage(CreateLogMessage newLogMessage) {
		LogMessage logMessage = modelMapper.map(newLogMessage, LogMessage.class);
		LogMessage logMessageSaved = logMessageRepository.save(logMessage);
		LogMessageDto response = modelMapper.map(logMessageSaved, LogMessageDto.class);

		return ResponseEntity.ok(response);
	}

	//Put LogMessage
	public ResponseEntity<LogMessageDto> editLogMessage(CreateLogMessage updateLogMessage, Long id) {
		LogMessage logMessage = logMessageRepository.findById(id).orElseThrow(()-> new NotFoundException("Log Message id " + id + " is not exist"));
		logMessage.setLogMessageCode(updateLogMessage.getLogMessageCode());
		logMessage.setMessageType(updateLogMessage.getMessageType());
		logMessage.setTypeCode(updateLogMessage.getTypeCode());
		logMessage.setVariableNo1(updateLogMessage.getVariableNo1());
		logMessage.setVariableName1(updateLogMessage.getVariableName1());
		logMessage.setVariableDate1(updateLogMessage.getVariableDate1());
		logMessage.setVariableSum1(updateLogMessage.getVariableSum1());
		logMessage.setVariableDate2(updateLogMessage.getVariableDate2());
		logMessage.setVariableSum2(updateLogMessage.getVariableSum2());
		logMessage.setSendDate(updateLogMessage.getSendDate());
		logMessage.setPhoneNo(updateLogMessage.getPhoneNo());
		logMessage.setEmailAddress(updateLogMessage.getEmailAddress());
		logMessage.setValutaId(updateLogMessage.getValutaId());
		logMessage.setVaBca(updateLogMessage.getVaBca());
		logMessage.setVaPermata(updateLogMessage.getVaPermata());
		logMessage.setNav(updateLogMessage.getNav());
		logMessage.setGenderCode(updateLogMessage.getGenderCode());
		logMessage.setNotes(updateLogMessage.getNotes());
		logMessage.setVariableNo2(updateLogMessage.getVariableNo2());
		logMessage.setVariableSum3(updateLogMessage.getVariableSum3());
		logMessage.setVariableName2(updateLogMessage.getVariableName2());
		logMessage.setVariableNo3(updateLogMessage.getVariableNo3());
		logMessage.setSentStatus(updateLogMessage.isActive());
		logMessage.setLastResponseDescription(updateLogMessage.getLastResponseDescription());
		logMessageRepository.save(logMessage);
		LogMessageDto response = modelMapper.map(logMessage, LogMessageDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete LogMessage	
	public ResponseEntity<LogMessageDto> deleteLogMessage(Long id) {
		LogMessage findLogMessage = logMessageRepository.findById(id).orElseThrow(()-> new NotFoundException("Log Message id " + id + " is not exist"));
		logMessageRepository.deleteById(id);
		LogMessageDto response = modelMapper.map(findLogMessage, LogMessageDto.class);

		return ResponseEntity.ok(response);
	}


	//Saving log data from landing page
	public LogMessageDto savingFE2(DataFromLandingPage2 data, String sendingDesc, Boolean sentStatus) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDateTime localDateNow = LocalDateTime.now();
		LogMessage logMessage = modelMapper.map(data, LogMessage.class);
		logMessage.setMessageType(data.getMessage_type());
		logMessage.setTypeCode(data.getType_code());
		logMessage.setVariableNo1(data.getVariable_no_1());
		logMessage.setVariableName1(data.getVariable_name_1());
		logMessage.setVariableDate1(data.getVariable_date_1());
		logMessage.setVariableSum1(data.getVariable_sum_1());
		logMessage.setVariableDate2(data.getVariable_date_2());
		logMessage.setVariableSum2(data.getVariable_sum_2());
		logMessage.setSendDate(dtf.format(localDateNow));
		logMessage.setPhoneNo(data.getPhone_no());
		logMessage.setEmailAddress(data.getEmail_address());
		logMessage.setValutaId(data.getValuta_id());
		logMessage.setVaBca(data.getVa_bca());
		logMessage.setVaPermata(data.getVa_permata());
		logMessage.setNav(data.getNav());
		logMessage.setGenderCode(data.getGender_code());
		logMessage.setNotes(data.getNotes());
		logMessage.setVariableNo2(data.getVariable_no_2());
		logMessage.setVariableSum3(data.getVariable_sum_3());
		logMessage.setVariableName2(data.getVariable_name_2());
		logMessage.setVariableNo3(data.getVariable_no_3());
		logMessage.setSentStatus(sentStatus);
		logMessage.setLastResponseDescription(sendingDesc);
		logMessage.setActive(true);
		LogMessage logMessageSaved = logMessageRepository.save(logMessage);
		em.refresh(logMessageSaved);
		LogMessageDto response = modelMapper.map(logMessageSaved, LogMessageDto.class);
		return response;
	}

	@Transactional
	public LogMessageDto savingRecipientNotif(CreateLogMessage createLogMessage){
		LogMessage logMessage = modelMapper.map(createLogMessage, LogMessage.class);
		logMessage.setMessageType(createLogMessage.getMessageType());
		logMessage.setTypeCode(createLogMessage.getTypeCode());
		logMessage.setVariableNo1(createLogMessage.getVariableNo1());
		logMessage.setVariableName1(createLogMessage.getVariableName1());
		logMessage.setVariableDate1(createLogMessage.getVariableDate1());
		logMessage.setVariableSum1(createLogMessage.getVariableSum1());
		logMessage.setVariableDate2(createLogMessage.getVariableDate2());
		logMessage.setVariableSum2(createLogMessage.getVariableSum2());
		logMessage.setSendDate(createLogMessage.getSendDate());
		logMessage.setPhoneNo(createLogMessage.getPhoneNo());
		logMessage.setEmailAddress(createLogMessage.getEmailAddress());
		logMessage.setValutaId(createLogMessage.getValutaId());
		logMessage.setVaBca(createLogMessage.getVaBca());
		logMessage.setVaPermata(createLogMessage.getVaPermata());
		logMessage.setNav(createLogMessage.getNav());
		logMessage.setGenderCode(createLogMessage.getGenderCode());
		logMessage.setNotes(createLogMessage.getNotes());
		logMessage.setVariableNo2(createLogMessage.getVariableNo2());
		logMessage.setVariableSum3(createLogMessage.getVariableSum3());
		logMessage.setVariableName2(createLogMessage.getVariableName2());
		logMessage.setVariableNo3(createLogMessage.getVariableNo3());
		logMessage.setSentStatus(createLogMessage.isSentStatus());
		logMessage.setLastResponseDescription(createLogMessage.getLastResponseDescription());
		logMessage.setActive(true);
		LogMessage logMessageSaved = logMessageRepository.save(logMessage);
		em.refresh(logMessageSaved);
		LogMessageDto response = modelMapper.map(logMessageSaved, LogMessageDto.class);
		return response;
	}
}