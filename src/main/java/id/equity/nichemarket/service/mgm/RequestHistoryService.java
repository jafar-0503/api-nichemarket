package id.equity.nichemarket.service.mgm;

import com.google.gson.JsonObject;
import id.equity.nichemarket.dto.mgm.customer.CustomerDto;
import id.equity.nichemarket.dto.mgm.customerReference.CustomerReferenceDto;
import id.equity.nichemarket.dto.mgm.dataFromLandingPage.DataFromLandingPage2;
import id.equity.nichemarket.dto.mgm.requestHistory.CreateRequestHistory;
import id.equity.nichemarket.dto.mgm.requestHistory.RequestHistoryDto;
import id.equity.nichemarket.exception.ErrorsException;
import id.equity.nichemarket.model.mgm.Customer;
import id.equity.nichemarket.model.mgm.CustomerReference;
import id.equity.nichemarket.model.mgm.RequestHistory;
import id.equity.nichemarket.repository.mgm.CustomerReferenceRepository;
import id.equity.nichemarket.repository.mgm.CustomerRepository;
import id.equity.nichemarket.repository.mgm.RequestHistoryRepository;
import id.equity.nichemarket.retrofit.mgm.EmailNotificationResponse;
import id.equity.nichemarket.retrofit.mgm.freeCovid.BaseFreeCovidResponse;
import id.equity.nichemarket.retrofit.mgm.freeCovid.FreeCovidResponse;
import id.equity.nichemarket.retrofit.mgm.freeCovid.FreeCovidService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Random;

@Service
public class RequestHistoryService {
    @Autowired
    private CustomerReferenceService customerReferenceService;
    @Autowired
    private LandingPageService landingPageService;
    @Autowired
    private FreeCovidService freeCovidService;
    @Autowired
    private CustomerPolicyService customerPolicyService;
    @Autowired
    private RequestHistoryRepository requestHistoryRepository;
    @Autowired
    private CustomerReferenceRepository customerReferenceRepository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private EntityManager em;

    public CustomerReferenceDto checkKey(String email, String phoneNo, String key){
        CustomerReferenceDto checkKey = customerReferenceService.checkUniqueKey(email, phoneNo, key);
        return checkKey;
    }

    @Transactional
    public RequestHistoryDto requestOtp(String customerReferenceCode) {
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        Random random = new Random();
        String otp = "";
        RequestHistory checkExistOtp = null;
        int trying = 0;
        do {
            StringBuilder builder = new StringBuilder(1);
            builder.append(alphabet.charAt(random.nextInt(alphabet.length())));
            otp = String.valueOf(100 + random.nextInt(900)) + builder;
            checkExistOtp = requestHistoryRepository.findByOtp(otp);;
            trying++;
        } while (checkExistOtp != null && trying < 3);
        RequestHistoryDto requestHistoryDto = convertToRequestHistory(customerReferenceCode, otp);

        return requestHistoryDto;
    }

    private RequestHistoryDto convertToRequestHistory(String customerReferenceCode, String otp) throws NullPointerException {
        RequestHistory requestHistory = requestHistoryRepository.findByCustomerReference(customerReferenceCode);
        if (requestHistory != null){
            requestHistory.setCode(otp);
            requestHistoryRepository.save(requestHistory);
        } else {
            CreateRequestHistory createRequestHistory = new CreateRequestHistory();
            createRequestHistory.setCustomerReferenceCode(customerReferenceCode);
            createRequestHistory.setCode(otp);
            createRequestHistory.setActive(true);
            requestHistory = modelMapper.map(createRequestHistory, RequestHistory.class);
            requestHistoryRepository.save(requestHistory);
        }
        RequestHistoryDto response = modelMapper.map(requestHistory, RequestHistoryDto.class);

        return response;
    }

    @Transactional
    public RequestHistoryDto savingOtp(CreateRequestHistory data){
        RequestHistory requestHistory = modelMapper.map(data, RequestHistory.class);
        RequestHistory requestHistorySaved = requestHistoryRepository.save(requestHistory);
        em.refresh(requestHistorySaved);
        RequestHistoryDto response = modelMapper.map(requestHistorySaved, RequestHistoryDto.class);
        return response;
    }

    public EmailNotificationResponse sendOtp(CustomerDto customerData,
                                             RequestHistoryDto otpData){
        DataFromLandingPage2 dataToSendOtp = convertToNotificationFormat(customerData, otpData.getCode());
        EmailNotificationResponse sendNotif = landingPageService.sendNotif(dataToSendOtp, null);
        return sendNotif;
    }

    public DataFromLandingPage2 convertToNotificationFormat(CustomerDto customer, String otp){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime localDateNow = LocalDateTime.now();

        DataFromLandingPage2 dataToSendOtp = new DataFromLandingPage2();
        dataToSendOtp.setMessage_type("S");
        dataToSendOtp.setType_code("N30");
        dataToSendOtp.setVariable_no_1(customer.getCustomerCode());
        dataToSendOtp.setVariable_name_1(customer.getCustomerName());
        dataToSendOtp.setVariable_date_1(customer.getDateOfBirth());
        dataToSendOtp.setVariable_sum_1("");
        dataToSendOtp.setVariable_date_2("");
        dataToSendOtp.setVariable_sum_2("");
        dataToSendOtp.setSent_date(dtf.format(localDateNow));
        dataToSendOtp.setPhone_no(customer.getPhoneNo());
        dataToSendOtp.setEmail_address(customer.getEmailAddress());
        dataToSendOtp.setValuta_id("");
        dataToSendOtp.setVa_bca("");
        dataToSendOtp.setVa_permata("");
        dataToSendOtp.setNav("");
        dataToSendOtp.setGender_code(customer.getGenderCode());
        dataToSendOtp.setVariable_no_2("");
        dataToSendOtp.setVariable_sum_3("");
        dataToSendOtp.setVariable_name_2("Sending OTP");
        dataToSendOtp.setNotes(otp);
        dataToSendOtp.setVariable_no_3("");

        return dataToSendOtp;
    }

    public RequestHistoryDto checkOtp(String otp) {
        RequestHistory requestHistory = requestHistoryRepository.findByOtp(otp);
        if (requestHistory == null){
            throw new ErrorsException("OTP is not valid");
        }
        RequestHistoryDto requestHistoryDto = modelMapper.map(requestHistory, RequestHistoryDto.class);
        return requestHistoryDto;
    }

    public ResponseEntity<List<RequestHistoryDto>> listRequestHistory() {
        List<RequestHistory> requestHistories = requestHistoryRepository.findAll();
        Type targetType = new TypeToken<List<RequestHistoryDto>>() {}.getType();
        List<RequestHistoryDto> response = modelMapper.map(requestHistories, targetType);

        return ResponseEntity.ok(response);
    }

    public ResponseEntity<RequestHistoryDto> addRequestHistory(CreateRequestHistory createRequestHistory) {
        RequestHistory requestHistory = modelMapper.map(createRequestHistory, RequestHistory.class);
        RequestHistory requestHistorySaved = requestHistoryRepository.save(requestHistory);
        RequestHistoryDto response = modelMapper.map(requestHistorySaved, RequestHistoryDto.class);

        return ResponseEntity.ok(response);
    }

    public FreeCovidResponse registerCertificate(CustomerDto customerDto, String customerReferenceCode) {
        JsonObject contentEmail = new JsonObject();
        JsonObject allEmailContent = new JsonObject();
        contentEmail.addProperty("policy_no","2020-000.10");
        contentEmail.addProperty("fullname", customerDto.getCustomerName());
        contentEmail.addProperty("date_of_birth", customerDto.getDateOfBirth());
        contentEmail.addProperty("gender", customerDto.getGenderCode());
        contentEmail.addProperty("email_addrress", customerDto.getEmailAddress());
        contentEmail.addProperty("phone_no", customerDto.getPhoneNo());

        allEmailContent.addProperty("username", "API_GRP");
        allEmailContent.addProperty("signature", "ItsfaDMljP+ZNw4KTqxWa7XHKAXtzs7+CH3jjiHrd/Y=");
        allEmailContent.addProperty("action", "register");
        allEmailContent.add("value", contentEmail);

        BaseFreeCovidResponse freeCovidResponse = freeCovidService.sendCertficate(allEmailContent);
        if (freeCovidResponse == null){
            throw new ErrorsException("Failed to send regist certificate");
        }
        customerPolicyService.savingFromFreeCovidResponse(freeCovidResponse.getData(), customerReferenceCode);
        return freeCovidResponse.getData();
    }
}
