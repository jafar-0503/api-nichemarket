package id.equity.nichemarket.service.mgm;

import java.util.Random;

import javax.persistence.EntityManager;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.dto.mgm.account.AccountCustomerDto;
import id.equity.nichemarket.dto.mgm.customerReferral.CustomerReferralDto;
import id.equity.nichemarket.dto.mgm.payment.PaymentRequestDto;
import id.equity.nichemarket.model.mgm.CustomerReferral;
import id.equity.nichemarket.repository.mgm.CustomerReferralRepository;

@Service
public class CustomerReferralService {
	
	@Autowired
	private CustomerReferralRepository customerReferralRepository;
	
	@Autowired
	private EntityManager em;
	
	@Autowired
	private ModelMapper modelMapper;
	
	 private String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	 public String generateReferralCode(){
	        int codeLength = 7;
	        Random random = new Random();
	        StringBuilder builder = new StringBuilder(codeLength);
	        CustomerReferral checkExistKey= null;
	        int trying = 0;
	        do {
	             for (int i = 0; i < codeLength; i++) {
	                 builder.append(this.alphabet.charAt(random.nextInt(this.alphabet.length())));
	             }
	             checkExistKey = customerReferralRepository.findByReferralCode(builder.toString());
	             trying++;
	         } while (checkExistKey != null && trying < 3);
	        return builder.toString();
	    }


	public CustomerReferral getCustomerReferralByReferralCode(String referralCode) {
		CustomerReferral customerReferral = customerReferralRepository.findByReferralCode(referralCode);
		return customerReferral;
	}
	
	public CustomerReferralDto addCustomerReferral(CustomerReferral newCustomerReferral) {
		CustomerReferral customerReferralSaved = customerReferralRepository.save(newCustomerReferral);
		CustomerReferralDto customerReferral = modelMapper.map(customerReferralSaved, CustomerReferralDto.class);
		return customerReferral;
	}
	
	public String getReferralCodeByAccountCode(String accountCode) {
		String referralCode = "";
		CustomerReferral customerReferral = customerReferralRepository.findByAccountCode(accountCode);
		if(customerReferral != null) {
			referralCode = customerReferral.getReferralCode();
		}
		return referralCode;
	}


	public Integer getTotalReferencedByReferralCode(String referralCode) {
		Integer totalReferenced = 0;
		totalReferenced = customerReferralRepository.sumReferencedByReferralCode(referralCode);
		return totalReferenced;
	}

	public CustomerReferralDto generateAndSaveCustomerReferall(AccountCustomerDto account, PaymentRequestDto newPayment) {
		String referralCode = this.generateReferralCode();
		CustomerReferral customerReferral = new CustomerReferral();
		customerReferral.setAccountCode(account.getAccountCode());
		customerReferral.setReferralCode(referralCode);
		customerReferral.setUsedReferralCode(newPayment.getUsedReferralCode());
		CustomerReferralDto customerReferralSaved = this.addCustomerReferral(customerReferral);
		return customerReferralSaved;
	}


}
