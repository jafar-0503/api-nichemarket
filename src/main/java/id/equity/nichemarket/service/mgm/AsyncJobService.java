package id.equity.nichemarket.service.mgm;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.model.mgm.FailedAsyncJobs;
import id.equity.nichemarket.repository.mgm.FailedAsyncJobsRepository;

@Service
public class AsyncJobService {
	
	@Autowired
	private FailedAsyncJobsRepository failedAsyncJobsRepository;
	
	@Autowired 
	private EntityManager em;
	
	
	
	public FailedAsyncJobs saveFailedJob(FailedAsyncJobs failedAsyncJobs) {
		FailedAsyncJobs failedAsyncJobsSaved = failedAsyncJobsRepository.save(failedAsyncJobs);
		em.refresh(failedAsyncJobsSaved);
		return failedAsyncJobsSaved;
	}
	

}
