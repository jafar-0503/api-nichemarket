package id.equity.nichemarket.service.mgm;

import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.mgm.registrationType.RegistrationTypeDto;
import id.equity.nichemarket.model.mgm.RegistrationType;
import id.equity.nichemarket.repository.mgm.RegistrationTypeRepository;


@Service
public class RegistrationTypeService {

	@Autowired
	private RegistrationTypeRepository registrationTypeRepository;
	
	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private EntityManager em;
	
	//List RegistrationType
	public ResponseEntity<List<RegistrationTypeDto>> listRegistrationType(){
		List<RegistrationType> lsRegistrationType= registrationTypeRepository.findAll();
		Type targetType = new TypeToken<List<RegistrationTypeDto>>() {}.getType();
		List<RegistrationTypeDto> response = modelMapper.map(lsRegistrationType, targetType);

		return ResponseEntity.ok(response);
	}

	//Get RegistrationType By Id
	public ResponseEntity<RegistrationTypeDto> getRegistrationTypeById(Long id) {
		RegistrationType registrationType = registrationTypeRepository.findById(id).orElseThrow(()-> new NotFoundException("RegistrationType id " + id + " is not exist"));
		RegistrationTypeDto response = modelMapper.map(registrationType, RegistrationTypeDto.class);

		return ResponseEntity.ok(response);
	}

	

	//Post RegistrationType
	@Transactional
	public ResponseEntity<RegistrationTypeDto> addRegistrationType(RegistrationTypeDto newRegistrationType) {
		RegistrationType registrationType = modelMapper.map(newRegistrationType, RegistrationType.class);
		RegistrationType registrationTypeSaved = registrationTypeRepository.save(registrationType);
		em.refresh(registrationTypeSaved);
		RegistrationTypeDto response = modelMapper.map(registrationTypeSaved, RegistrationTypeDto.class);
		return ResponseEntity.ok(response);
	}

	//Put RegistrationType
	public ResponseEntity<RegistrationTypeDto> editRegistrationType(RegistrationTypeDto updateRegistrationType, Long id) {
		RegistrationType registrationType = registrationTypeRepository.findById(id).orElseThrow(()-> new NotFoundException("RegistrationType id " + id + " is not exist"));
		registrationType.setRegistrationTypeCode(updateRegistrationType.getRegistrationTypeCode());
		registrationType.setRegistrationDesc(updateRegistrationType.getRegistrationDesc());
		registrationType.setBillAmount(updateRegistrationType.getBillAmount());
		registrationType.setPoint(updateRegistrationType.getPoint());
		registrationType.setActive(updateRegistrationType.isActive());
		registrationTypeRepository.save(registrationType);
		RegistrationTypeDto response = modelMapper.map(registrationType, RegistrationTypeDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete RegistrationType	
	public ResponseEntity<RegistrationTypeDto> deleteRegistrationType(Long id) {
		RegistrationType findRegistrationType = registrationTypeRepository.findById(id).orElseThrow(()-> new NotFoundException("RegistrationType id " + id + " is not exist"));
		registrationTypeRepository.deleteById(id);
		RegistrationTypeDto response = modelMapper.map(findRegistrationType, RegistrationTypeDto.class);

		return ResponseEntity.ok(response);
	}

	//Get RegistrationType By registrationTypeCode
	public RegistrationTypeDto getRegistrationTypeByCode(String registrationTypeCode) {
		RegistrationType registrationType = registrationTypeRepository.findByRegistrationTypeCode(registrationTypeCode).orElseThrow(()-> new NotFoundException("RegistrationType code " + registrationTypeCode + " is not exist"));;
		return modelMapper.map(registrationType, RegistrationTypeDto.class);
	}


	
}