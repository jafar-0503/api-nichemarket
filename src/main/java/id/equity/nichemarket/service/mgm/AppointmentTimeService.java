package id.equity.nichemarket.service.mgm;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.mgm.appointmentTime.CreateAppointmentTime;
import id.equity.nichemarket.dto.mgm.appointmentTime.AppointmentTimeDto;
import id.equity.nichemarket.model.mgm.AppointmentTime;
import id.equity.nichemarket.repository.mgm.AppointmentTimeRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class AppointmentTimeService {

	@Autowired
	private AppointmentTimeRepository appointmentTimeRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List AppointmentTime
	public ResponseEntity<List<AppointmentTimeDto>> listAppointmentTime(){
		List<AppointmentTime> lsAppointmentTime= appointmentTimeRepository.findAll();
		Type targetType = new TypeToken<List<AppointmentTimeDto>>() {}.getType();
		List<AppointmentTimeDto> response = modelMapper.map(lsAppointmentTime, targetType);

		return ResponseEntity.ok(response);
	}

	//Get AppointmentTime By Id
	public ResponseEntity<AppointmentTimeDto> getAppointmentTimeById(Long id) {
		AppointmentTime appointmentTime = appointmentTimeRepository.findById(id).orElseThrow(()-> new NotFoundException("Appointment Time id " + id + " is not exist"));
		AppointmentTimeDto response = modelMapper.map(appointmentTime, AppointmentTimeDto.class);

		return ResponseEntity.ok(response);
	}

	//Post AppointmentTime
	public ResponseEntity<AppointmentTimeDto> addAppointmentTime(CreateAppointmentTime newAppointmentTime) {
		AppointmentTime appointmentTime = modelMapper.map(newAppointmentTime, AppointmentTime.class);
		AppointmentTime appointmentTimeSaved = appointmentTimeRepository.save(appointmentTime);
		AppointmentTimeDto response = modelMapper.map(appointmentTimeSaved, AppointmentTimeDto.class);

		return ResponseEntity.ok(response);
	}

	//Put AppointmentTime
	public ResponseEntity<AppointmentTimeDto> editAppointmentTime(CreateAppointmentTime updateAppointmentTime, Long id) {
		AppointmentTime appointmentTime = appointmentTimeRepository.findById(id).orElseThrow(()-> new NotFoundException("Appointment Time id " + id + " is not exist"));
		appointmentTime.setAppointmentTimeCode(updateAppointmentTime.getAppointmentTimeCode());
		appointmentTime.setDescription(updateAppointmentTime.getDescription());
		appointmentTime.setActive(updateAppointmentTime.isActive());
		appointmentTimeRepository.save(appointmentTime);
		AppointmentTimeDto response = modelMapper.map(appointmentTime, AppointmentTimeDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete AppointmentTime	
	public ResponseEntity<AppointmentTimeDto> deleteAppointmentTime(Long id) {
		AppointmentTime findAppointmentTime = appointmentTimeRepository.findById(id).orElseThrow(()-> new NotFoundException("Appointment Time id " + id + " is not exist"));
		appointmentTimeRepository.deleteById(id);
		AppointmentTimeDto response = modelMapper.map(findAppointmentTime, AppointmentTimeDto.class);

		return ResponseEntity.ok(response);
	}
}