package id.equity.nichemarket.service.mgm;

import id.equity.nichemarket.dto.mgm.customer.CustomerDto;
import id.equity.nichemarket.dto.mgm.registrationType.RegistrationTypeDto;

public interface IProductPolicyValidatorService {	
	boolean isValidAge(CustomerDto customer);
}
