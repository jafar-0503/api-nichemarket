package id.equity.nichemarket.service.mgm;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.exception.CustomerValidationException;


@Service
public class UtilitiesService {
    private static final Logger logger = LoggerFactory.getLogger(UtilitiesService.class);

	
	public Integer calculateAge(String sDate1, String date2) {	
		try {			
			Integer year;
			Integer month;
			Integer day;	
			
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			
			LocalDate date1 = LocalDate.parse(sDate1, formatter);
			LocalDateTime now = LocalDateTime.now();  
			
			year = now.getYear()-date1.getYear();
			month = now.getMonthValue()-date1.getMonthValue();
			day = now.getDayOfMonth()-date1.getDayOfMonth();
			
			if(month < 0) {
				year = year-1;
				month = 12 + month;
			}
			
			if(day<0) {
				month = month-1;
				day = 30 - date1.getDayOfMonth() + now.getDayOfMonth();
			}
			
			if(day > 15) {
				month = month + 1;
			}
			
			if(month > 5) {
				year = year + 1;
			}
			
			if(year == 0) {
				year = 0;
			}

			return year;
		}catch(Exception e) {
			logger.error(sDate1 + " Date of Birth Format Is Invalid");
			e.printStackTrace();
			List<String> errors = new ArrayList<>();
			errors.add("Date of Birth Format Is Invalid");
			throw new CustomerValidationException("Invalid Data" , errors);
		}
	}
}
