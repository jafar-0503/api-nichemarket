package id.equity.nichemarket.service.mgm;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.mgm.CustomerValidationDto;
import id.equity.nichemarket.dto.mgm.account.AccountCustomerDto;
import id.equity.nichemarket.dto.mgm.app.AppDto;
import id.equity.nichemarket.dto.mgm.bill.BillDto;
import id.equity.nichemarket.dto.mgm.custom.CreateCustomerRegistrationDto;
import id.equity.nichemarket.dto.mgm.customer.CreateCustomer;
import id.equity.nichemarket.dto.mgm.customer.CustomerAgeValidationDto;
import id.equity.nichemarket.dto.mgm.customer.CustomerDetailPolicyDto;
import id.equity.nichemarket.dto.mgm.customer.CustomerDto;
import id.equity.nichemarket.dto.mgm.customer.CustomerWhatsappDto;
import id.equity.nichemarket.dto.mgm.customerRegistration.CustomerRegistrationDto;
import id.equity.nichemarket.dto.mgm.customerRegistration.CustomerRegistrationResponseDto;
import id.equity.nichemarket.dto.mgm.dataFromLandingPage.DataFromLandingPage;
import id.equity.nichemarket.dto.mgm.registrationType.RegistrationTypeDto;
import id.equity.nichemarket.exception.CustomerValidationException;
import id.equity.nichemarket.exception.ErrorsException;
import id.equity.nichemarket.model.mgm.App;
import id.equity.nichemarket.model.mgm.Customer;
import id.equity.nichemarket.model.mgm.Registration;
import id.equity.nichemarket.model.mgm.RegistrationType;
import id.equity.nichemarket.repository.mgm.AccountRepository;
import id.equity.nichemarket.repository.mgm.CustomerRepository;
import id.equity.nichemarket.repository.mgm.RegistrationRepository;
import id.equity.nichemarket.service.rm.UserService;
import id.equity.nichemarket.validation.partner.BaseRegistrationValidation;
import id.equity.nichemarket.validation.partner.RegistrationValidationFactory;

@Service
public class CustomerService {

	@Autowired
	private RegistrationService registrationService;
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private AccountService accountService;

		
	@Autowired
	private CustomerRegistrationService customerRegistrationService;
	
	@Autowired
	private RegistrationRepository registrationRepository;
	
	@Autowired
	private RegistrationTypeService registrationTypeService;
	
	@Autowired
	private BillService billService;
	
	@Autowired
	private AppService appService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private SiMedisValidatorService siMedisValidatorService;
	
	@Autowired
	private SiMedisValidatorCoreService siMedisValidatorCoreService;

	@Autowired
	private EntityManager em;
	
    private static final Logger logger = LoggerFactory.getLogger(CustomerService.class);

	
	//List Customer
	public ResponseEntity<List<CustomerDto>> listCustomer(){
		List<Customer> lsCustomer= customerRepository.findAll();
		Type targetType = new TypeToken<List<CustomerDto>>() {}.getType();
		List<CustomerDto> response = modelMapper.map(lsCustomer, targetType);

		return ResponseEntity.ok(response);
	}

	//Get Customer By Id
	public ResponseEntity<CustomerDto> getCustomerById(Long id) {
		Customer customer = customerRepository.findById(id).orElseThrow(()-> new NotFoundException("Customer id " + id + " is not exist"));
		CustomerDto response = modelMapper.map(customer, CustomerDto.class);

		return ResponseEntity.ok(response);
	}

	
	//Get Customer By Email Address
	public CustomerDto getCustomerByEmailAddress(String emailAddress) {
		Customer customer = customerRepository.findByEmailAddress(emailAddress).orElseThrow(()-> new NotFoundException("Customer email " + emailAddress + " is not exist"));
		CustomerDto response = modelMapper.map(customer, CustomerDto.class);

		return response;
	}
	//Get Customer By Key
	public ResponseEntity<CustomerDto> getCustomerByKey(String key) {
		Customer customer = customerRepository.findByUniqueKey(key);
		if (customer == null){
			throw new NotFoundException("Key " + key + " is not exist");
		}
		CustomerDto response = modelMapper.map(customer, CustomerDto.class);

		return ResponseEntity.ok(response);
	}
	
	
	//Post Customer
	@Transactional
	public ResponseEntity<CustomerDto> addCustomer(CreateCustomer newCustomer) {
		Customer customer = modelMapper.map(newCustomer, Customer.class);
		Customer customerSaved = customerRepository.save(customer);
		em.refresh(customerSaved);
		CustomerDto response = modelMapper.map(customerSaved, CustomerDto.class);
		return ResponseEntity.ok(response);
	}
	
	@Transactional
	public List<Customer> addAllCustomer(List<Customer> allCustomer) {		
        List<Customer> CustomerSaved =  customerRepository.saveAll(allCustomer);
        for(Customer Customer : CustomerSaved) {
        	em.refresh(Customer);
        }
        
		Type targetType = new TypeToken<List<Customer>>() {}.getType();
		List<Customer> customer = modelMapper.map(CustomerSaved, targetType);

		return customer;
	}

	//Put Customer
	public ResponseEntity<CustomerDto> editCustomer(CreateCustomer updateCustomer, Long id) {
		Customer customer = customerRepository.findById(id).orElseThrow(()-> new NotFoundException("Customer id " + id + " is not exist"));
		customer.setCustomerCode(updateCustomer.getCustomerCode());
		customer.setCustomerName(updateCustomer.getCustomerName());
		customer.setDateOfBirth(updateCustomer.getDateOfBirth());
		customer.setOccupation(updateCustomer.getOccupation());
		customer.setPhoneNo(updateCustomer.getPhoneNo());
		customer.setEmailAddress(updateCustomer.getEmailAddress());
		customer.setActive(updateCustomer.isActive());
		customerRepository.save(customer);
		CustomerDto response = modelMapper.map(customer, CustomerDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete Customer	
	public ResponseEntity<CustomerDto> deleteCustomer(Long id) {
		Customer findCustomer = customerRepository.findById(id).orElseThrow(()-> new NotFoundException("Customer id " + id + " is not exist"));
		customerRepository.deleteById(id);
		CustomerDto response = modelMapper.map(findCustomer, CustomerDto.class);

		return ResponseEntity.ok(response);
	}

	//saving data from Landing Page
	@Transactional
	public CustomerDto savingCustomerData(DataFromLandingPage data){
		Customer customer = modelMapper.map(data, Customer.class);
		customer.setCustomerName(data.getCustomerName());
		customer.setGenderCode(data.getGenderCode());
		customer.setDateOfBirth(data.getDateOfBirth());
		customer.setOccupation(data.getOccupation());
		customer.setPhoneNo(data.getPhoneNo());
		customer.setEmailAddress(data.getEmailAddress());
		customer.setActive(true);
		Customer customerSaved = customerRepository.save(customer);
		em.refresh(customerSaved);
		CustomerDto response = modelMapper.map(customer, CustomerDto.class);
		return response;
	}

	//Get Customer By customerCode
	public CustomerDto getCustomerByCode(String customerCode) {
		Customer customer = customerRepository.findByCustomerCode(customerCode);
		return modelMapper.map(customer, CustomerDto.class);
	}

	//get Customer By Email and code or By Phone and code
	public CustomerDto getCustomerByEmailAndCodeOrPhoneAndCode(String customerCode, String email, String phone) {
		Customer customer = customerRepository.findByEmailAndCodeOrPhoneAndCode(
				customerCode, phone, email);
		return modelMapper.map(customer, CustomerDto.class);
	}
	


	@Transactional
	public ResponseEntity<CustomerRegistrationResponseDto> registerCustomer(CustomerRegistrationDto newCustomer) {
		Type listType = new TypeToken<List<Customer>>() {}.getType();
		List<Customer> customerList = modelMapper.map(newCustomer.getCustomers(), listType);
		
		//Get Registration Type Code
		RegistrationType registrationType = modelMapper.map(newCustomer, RegistrationType.class);
		
		//Save Bills & Get billDetail by registration type code	
		RegistrationTypeDto currentRegistrationType = registrationTypeService.getRegistrationTypeByCode(registrationType.getRegistrationTypeCode());
		
		//===========================Validate Begin===========================//
		String errValidationMessage = "";
		
		BaseRegistrationValidation validationFactory = RegistrationValidationFactory.getService("simedis"); 
		List<String> validationResult = validationFactory.validate(customerList, currentRegistrationType);
		
		if(!validationResult.isEmpty()) {
			logger.warn(validationResult.toString());
			errValidationMessage = "Invalid Customer Data";
			throw new CustomerValidationException(errValidationMessage, validationResult);
		}
		//===========================Validate End===========================//
		
		//Get Customer Parent
		Customer customerParent = customerList.stream()
									    .filter( c -> c.getCustomerStatus().equals("0"))
									    .map(c -> { 
								    			c.setActive(true);
								    			c.setEmailAddress(c.getEmailAddress().toLowerCase());
								    			return c;
								    	})
									    .findAny()                            
						                .orElse(null);
		
		//Check Username & Email On User
		if(userService.isUsernameExist(customerParent.getEmailAddress(), "C001")) {
			throw new ErrorsException("Email " + customerParent.getEmailAddress() + " is already exist");
		}
		
		
		//Get App Detail
		AppDto currAppDto = appService.getAppByAppCode(newCustomer.getAppCode());		
		App currApp = modelMapper.map(currAppDto ,App.class);
		
		//Save Account 
		AccountCustomerDto account = new AccountCustomerDto();
		account.setAppCode(newCustomer.getAppCode());
		account.setEmailAddress(customerParent.getEmailAddress().toLowerCase());
		account.setUniqueActivationKey(accountService.generateUniqueKey());
		account.setAccountActivated(false);	
		account.setActive(false);
		AccountCustomerDto accountSaved = accountService.saveAccount(account);
		
		//Save Registration with type code
		Registration registration = new Registration();
		registration.setRegistrationTypeCode(registrationType.getRegistrationTypeCode());
		registration.setAppCode(newCustomer.getAppCode());
		registration.setApp(currApp);	
		registration.setAccountCode(accountSaved.getAccountCode());
		Registration registrationSaved = registrationRepository.save(registration);		
		em.refresh(registrationSaved);				
			
	
		//Save & Get Unique Code Customer Parent
		Customer parentCustomerSaved = customerRepository.save(customerParent);	
		em.refresh(parentCustomerSaved);
		
		//Save Customer Registration Of Customer Parent
		CreateCustomerRegistrationDto parentCustomerRegistration = new CreateCustomerRegistrationDto();
		parentCustomerRegistration.setRegistrationCode(registrationSaved.getRegistrationCode());
		parentCustomerRegistration.setCustomerCode(parentCustomerSaved.getCustomerCode());
		CreateCustomerRegistrationDto customerRegistrationSaved = customerRegistrationService.addCustomerRegistration(parentCustomerRegistration);
		
		//Save The Others Using Saved Customer Parent Code
		List<Customer> otherCustomerList = customerList.stream()
											    .filter( c -> !c.getCustomerStatus().equals("0"))
											    .map(c -> { 
											    			c.setRefCustomerCode(parentCustomerSaved.getCustomerCode());
											    			c.setEmailAddress(c.getEmailAddress().toLowerCase());
											    			c.setActive(true);
											    			return c;
											    	})
											    .collect(Collectors.toList());
											    		
		List<Customer> otherCustomerSaved = customerRepository.saveAll(otherCustomerList);
		

		CreateCustomerRegistrationDto otherCustomerRegistration = new CreateCustomerRegistrationDto();
		for (Customer otherCustomer : otherCustomerSaved) {
			em.refresh(otherCustomer);
			otherCustomerRegistration.setRegistrationCode(registrationSaved.getRegistrationCode());
			otherCustomerRegistration.setCustomerCode(otherCustomer.getCustomerCode());
			CreateCustomerRegistrationDto otherCustomerRegistrationSaved = customerRegistrationService.addCustomerRegistration(otherCustomerRegistration);
		}		
		
		
		
		BillDto bill = new BillDto();
		bill.setAmount(currentRegistrationType.getBillAmount());	
		bill.setRegistrationCode(registrationSaved.getRegistrationCode());
		BillDto billSaved = billService.saveBill(bill);
		
		//Create Response Object
		CustomerRegistrationResponseDto response = new CustomerRegistrationResponseDto();
		response.setBillAmount(bill.getAmount());
		response.setRegistrationCode(bill.getRegistrationCode());
		response.setBillCode(billSaved.getBillCode());	
		response.setUsedReferralCode(newCustomer.getUsedReferralCode());
		return ResponseEntity.ok(response);
//		return ResponseEntity.ok(BaseResponse<CustomerRegistrationResponseDto>(
//                true,
//                response,
//                "Success add customer")
//        );	
	}

	public ResponseEntity<List<CustomerDto>> getCustomerByRefCustomerCode(String refCustomerCode) {
		List<Customer> customerList = customerRepository.findByRefCustomerCode(refCustomerCode);
		Type listType = new TypeToken<List<Customer>>() {}.getType();
		List<CustomerDto> response = modelMapper.map(customerList, listType);
		return ResponseEntity.ok(response);
	}

	public ResponseEntity<CustomerDto> getCustomerByCustomerCode(String customerCode) {
		Customer customerList = customerRepository.findByCustomerCode(customerCode);
		CustomerDto response = modelMapper.map(customerList, CustomerDto.class);
		return ResponseEntity.ok(response);
	}

	

	public List<CustomerDetailPolicyDto> getAllCustomerInCustomerCode(List<String> customerCodeList) {
		List<CustomerDetailPolicyDto> customerList = customerRepository.findAllByCustomerCodeIn(customerCodeList);
		return customerList;
	}

	public void getByCustomerPolicyCoreCode(List<String> customerCodeList) {
		Object[] customerList = customerRepository.findAllByCustomerPolicyCoreCode(customerCodeList);
		System.out.println(customerList[0].toString());
		
		throw new ErrorsException("error");
		
	}
	
	public ResponseEntity<BaseResponse<Object>> validateCustomerInfo(CustomerWhatsappDto customerWhatsapp){
		
		
		return null;
	}
	


}