package id.equity.nichemarket.service.mgm;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.aspect.BaseResponseMgmAspect;
import id.equity.nichemarket.dto.mgm.projection.ISummaryProductionDataDto;

@Service
public class ReportService {

	@Autowired
	private AccountService accountService;
	

    
    private static final Logger logger = LoggerFactory.getLogger(ReportService.class);

    //Function for create daily report to stakeholder
	public boolean createDailyProductionReport(String fileLocation){
		logger.info("===================Generating Begin====================");
		List<ISummaryProductionDataDto> summaries = accountService.getProductionData();
        int i = 1;
        try (Workbook workbook = new XSSFWorkbook()) {
            SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy");
            Sheet sheet = workbook.createSheet("Data_" + dateFormat2.format(new Date()));
            
            CellStyle cellStyle = workbook.createCellStyle();
            
            String[] COLUMNs = {"Transasction Date", "Transaction No", "Customer Name"
            					, "Account ID", "Referral Code", "Used Referral Code" , "Product Type"
            					, "Latest Point", "Premi", "Policy No", "Member No" };
            
            //Header Style
            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setColor(IndexedColors.BLACK.getIndex());
         
            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);
            
            // Row for Header
            Row headerRow = sheet.createRow(0);
            
            // Header
            for (int col = 0; col < COLUMNs.length; col++) {
              Cell cell = headerRow.createCell(col);
              cell.setCellValue(COLUMNs[col]);
              cell.setCellStyle(headerCellStyle);
            }
         
            int rowIdx = 1;

            //looping new row per data
            for (ISummaryProductionDataDto summary : summaries) {
            	 Row row = sheet.createRow(rowIdx++);
            	   
            	 row.createCell(0).setCellValue(summary.getTransactionDate());
            	 row.createCell(1).setCellValue(summary.getTransactionNo());
            	 row.createCell(2).setCellValue(summary.getCustomerName());
            	 row.createCell(3).setCellValue(summary.getAccountId());
            	 row.createCell(4).setCellValue(summary.getReferralCode());
            	 row.createCell(5).setCellValue(summary.getUsedReferralCode());
            	 row.createCell(6).setCellValue(summary.getPlanType());
            	 row.createCell(7).setCellValue(summary.getLatestPoint());
            	 row.createCell(8).setCellValue(summary.getPremi());
            	 row.createCell(9).setCellValue(summary.getPolicyNo());
            	 row.createCell(10).setCellValue(summary.getMemberNo());
            
			}                  


               
            //making size of column
            sheet.autoSizeColumn(0);
            sheet.autoSizeColumn(1);
            sheet.autoSizeColumn(2);
            sheet.autoSizeColumn(3);
            sheet.autoSizeColumn(4);
            sheet.autoSizeColumn(5);
            sheet.autoSizeColumn(6);
            sheet.autoSizeColumn(7);
            sheet.autoSizeColumn(8);
            sheet.autoSizeColumn(9);
            sheet.autoSizeColumn(10);
            sheet.autoSizeColumn(11);
            sheet.autoSizeColumn(12);
            sheet.autoSizeColumn(13);
            sheet.autoSizeColumn(14);
           
            System.out.print(System.getProperty("java.io.tmpdir"));
            FileOutputStream outputStream = new FileOutputStream(fileLocation);
            workbook.write(outputStream);
            workbook.close();
    		logger.info("Success Generate");
        } catch (IOException e){
    		logger.error("Fail to Generate");
    		logger.error("Error :" + e);
    		logger.info("===================Generating End====================");
            e.printStackTrace();
            return false;
        }
		logger.info("===================Generating End====================");
        return true;
    }
}
