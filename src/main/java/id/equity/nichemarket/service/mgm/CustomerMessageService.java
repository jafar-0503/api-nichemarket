package id.equity.nichemarket.service.mgm;

import id.equity.nichemarket.dto.mgm.customerMessage.CreateCustomerMessage;
import id.equity.nichemarket.dto.mgm.customerMessage.CustomerMessageDto;
import id.equity.nichemarket.dto.mgm.dataFromLandingPage.DataFromLandingPage2;
import id.equity.nichemarket.dto.mgm.recipientReceivedMsg.RecipientReceivedMsgDto;
import id.equity.nichemarket.model.mgm.CustomerMessage;
import id.equity.nichemarket.repository.mgm.CustomerMessageRepository;
import id.equity.nichemarket.retrofit.mgm.EmailNotificationResponse;
import id.equity.nichemarket.template.SendEmailTemplate;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public class CustomerMessageService {
    @Autowired
    private LandingPageService landingPageService;
    @Autowired
    private RecipientReceivedMsgService recipientReceivedMsgService;
    @Autowired
    private CustomerMessageRepository customerMessageRepository;
    @Autowired
    private SendEmailTemplate sendEmailTemplate;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private EntityManager em;

    //Post New Customer Message Data
    public CustomerMessageDto addCustomerMessage(CreateCustomerMessage createCustomerMessage) {
        CustomerMessage customerMessage = modelMapper.map(createCustomerMessage, CustomerMessage.class);
        CustomerMessage customerMessageSaved = customerMessageRepository.save(customerMessage);
        em.refresh(customerMessageSaved);
        CustomerMessageDto response = modelMapper.map(customerMessageSaved, CustomerMessageDto.class);

        //pick recipient data with message_code = E28
        List<RecipientReceivedMsgDto> recipientList = recipientReceivedMsgService.getRecipientByTypeCode("E28");
        //convert data to email template

        //sending email with added signature inside the function
        //input null for customer priority if not necessary
        for (RecipientReceivedMsgDto recipient : recipientList) {
            DataFromLandingPage2 dataToSendEmail = sendEmailTemplate.convertToLandingPage2(createCustomerMessage,
                    recipient,
                    "E28");
            EmailNotificationResponse SendingEmailResponse = landingPageService.sendNotif(dataToSendEmail, null);
        }

        return response;
    }
}
