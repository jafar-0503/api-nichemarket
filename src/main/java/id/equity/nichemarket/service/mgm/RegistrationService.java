package id.equity.nichemarket.service.mgm;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Random;

import javax.persistence.EntityManager;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.mgm.projection.ICountActiveEmailAddressDto;
import id.equity.nichemarket.dto.mgm.projection.ICountActiveKtpCustomerDto;
import id.equity.nichemarket.dto.mgm.registration.RegistrationDto;
import id.equity.nichemarket.model.mgm.Registration;
import id.equity.nichemarket.repository.mgm.RegistrationRepository;

@Service
public class RegistrationService {
	
	@Autowired
	private RegistrationRepository registrationRepository;
	
	@Autowired
	private ModelMapper modelMapper;


	@Autowired
	private EntityManager em;
	
	    
		//List Registration
		public ResponseEntity<List<RegistrationDto>> listRegistration(){
			List<Registration> lsRegistration= registrationRepository.findAll();
			Type targetType = new TypeToken<List<RegistrationDto>>() {}.getType();
			List<RegistrationDto> response = modelMapper.map(lsRegistration, targetType);

			return ResponseEntity.ok(response);
		}

		//Get Registration By Id
		public ResponseEntity<RegistrationDto> getRegistrationById(Long id) {
			Registration registration = registrationRepository.findById(id).orElseThrow(()-> new NotFoundException("Registration id " + id + " is not exist"));
			RegistrationDto response = modelMapper.map(registration, RegistrationDto.class);

			return ResponseEntity.ok(response);
		}


		
		//Put Registration
		public ResponseEntity<RegistrationDto> editRegistration(RegistrationDto updateRegistration, Long id) {
			Registration registration = registrationRepository.findById(id).orElseThrow(()-> new NotFoundException("Registration id " + id + " is not exist"));
			registration.setRegistrationCode(updateRegistration.getRegistrationCode());			
			registrationRepository.save(registration);
			RegistrationDto response = modelMapper.map(registration, RegistrationDto.class);

			return ResponseEntity.ok(response);
		}

		//Put Registration
		public RegistrationDto updateRegistrationStatus(RegistrationDto updateRegistration, String registrationCode) {
			Registration registration = registrationRepository.findByRegistrationCode(registrationCode);;			
			registrationRepository.save(registration);
			RegistrationDto response = modelMapper.map(registration, RegistrationDto.class);

			return response;
		}
		
		//Delete Registration	
		public ResponseEntity<RegistrationDto> deleteRegistration(Long id) {
			Registration findRegistration = registrationRepository.findById(id).orElseThrow(()-> new NotFoundException("Registration id " + id + " is not exist"));
			registrationRepository.deleteById(id);
			RegistrationDto response = modelMapper.map(findRegistration, RegistrationDto.class);

			return ResponseEntity.ok(response);
		}

		//Get Registration By registrationCode
		public RegistrationDto getRegistrationByCode(String registrationCode) {
			Registration registration = registrationRepository.findByRegistrationCode(registrationCode);
			if(registration == null) {
				throw new NotFoundException("Registration Code " + registrationCode + " Is Not Found");
			}
			return modelMapper.map(registration, RegistrationDto.class);
		}

	


		public Integer countActiveKtpNo(String ktpNO, String productPolicyCode) {
			ICountActiveKtpCustomerDto countActiveKtpCustomer = registrationRepository.countKtpNoPaidPayment(ktpNO, productPolicyCode);
			
			System.out.println(countActiveKtpCustomer);
			Integer totalActiveKtpNo = countActiveKtpCustomer.getCount();
			return totalActiveKtpNo;
		}



		public Integer countActiveEmailAddress(String emailAddress) {
			ICountActiveEmailAddressDto countActiveKtpCustomer = registrationRepository.countEmailAddressPaidPayment(emailAddress);
			Integer totalActiveKtpNo = countActiveKtpCustomer.getCount();
			return totalActiveKtpNo;
		}



		public RegistrationDto getFirstRegistrationByAccountCode(String accountCode) {
			List<Registration> registrationList = registrationRepository.findByAccountCode(accountCode);
			if(registrationList == null) {
				throw new NotFoundException("Account Code " + accountCode + " Is Not Found");
			}
			Registration firstRegistration = registrationList.get(0);
			return modelMapper.map(firstRegistration, RegistrationDto.class);
		}



		public List<RegistrationDto> getRegistrationByAccountCode(String accountCode) {
			List<Registration> registrationList = registrationRepository.findByAccountCode(accountCode);
			if(registrationList == null) {
				throw new NotFoundException("Account Code " + accountCode + " Is Not Found");
			}
			Type targetType = new TypeToken<List<RegistrationDto>>() {}.getType();
			return modelMapper.map(registrationList, targetType);
		}
		

}
