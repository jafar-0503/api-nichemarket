package id.equity.nichemarket.service.mgm;

import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;

import org.modelmapper.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.mgm.customerPolicy.CustomerPolicyDto;
import id.equity.nichemarket.model.mgm.Jobs;
import id.equity.nichemarket.repository.mgm.JobsRepository;
import id.equity.nichemarket.tasks.ResendToCore;

@Service
public class JobsService {
	
	@Autowired
	private JobsRepository jobsRepository;
	
	@Autowired
	private EntityManager em;
	
	private static final Logger logger = LoggerFactory.getLogger(JobsService.class);

	
	public Jobs addJob(Jobs jobs) {
		Jobs jobsSaved = jobsRepository.save(jobs);
//		em.refresh(jobsSaved);	
		return jobsSaved;
	}

	public Jobs updateJobs(String transactionCode) {
		Jobs job = jobsRepository.findByTransactionCoreCode(transactionCode)
				.orElseThrow(()-> new NotFoundException("Transaction Code " + transactionCode + " is not exist"));
		Integer countRetry = (job.getCountRetry() == null) ? 0 : job.getCountRetry();
		job.setCountRetry(countRetry + 1);		
		jobsRepository.save(job);
		
		return job;
	}
	
	public List<Jobs> getFailedJobsWithRetryLessThan(Integer limit, List<String> transactionCode){
		List<Jobs> jobs = jobsRepository.findByRetryLessThan(limit , transactionCode);
		return jobs;
	}
}
