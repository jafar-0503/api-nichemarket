package id.equity.nichemarket.service.mgm;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.mgm.answer.CreateAnswer;
import id.equity.nichemarket.dto.mgm.answer.AnswerDto;
import id.equity.nichemarket.model.mgm.Answer;
import id.equity.nichemarket.repository.mgm.AnswerRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class AnswerService {

	@Autowired
	private AnswerRepository answerRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List Answer
	public ResponseEntity<List<AnswerDto>> listAnswer(){
		List<Answer> lsAnswer= answerRepository.findAll();
		Type targetType = new TypeToken<List<AnswerDto>>() {}.getType();
		List<AnswerDto> response = modelMapper.map(lsAnswer, targetType);

		return ResponseEntity.ok(response);
	}

	//Get Answer By Id
	public ResponseEntity<AnswerDto> getAnswerById(Long id) {
		Answer answer = answerRepository.findById(id).orElseThrow(()-> new NotFoundException("Answer id " + id + " is not exist"));
		AnswerDto response = modelMapper.map(answer, AnswerDto.class);

		return ResponseEntity.ok(response);
	}

	//Post Answer
	public ResponseEntity<AnswerDto> addAnswer(CreateAnswer newAnswer) {
		Answer answer = modelMapper.map(newAnswer, Answer.class);
		Answer answerSaved = answerRepository.save(answer);
		AnswerDto response = modelMapper.map(answerSaved, AnswerDto.class);

		return ResponseEntity.ok(response);
	}

	//Put Answer
	public ResponseEntity<AnswerDto> editAnswer(CreateAnswer updateAnswer, Long id) {
		Answer answer = answerRepository.findById(id).orElseThrow(()-> new NotFoundException("Answer id " + id + " is not exist"));
		answer.setAnswerCode(updateAnswer.getAnswerCode());
		answer.setDescription(updateAnswer.getDescription());
		answer.setQuestionCode(updateAnswer.getQuestionCode());
		answer.setSequence(updateAnswer.getSequence());
		answer.setActive(updateAnswer.isActive());
		answerRepository.save(answer);
		AnswerDto response = modelMapper.map(answer, AnswerDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete Answer	
	public ResponseEntity<AnswerDto> deleteAnswer(Long id) {
		Answer findAnswer = answerRepository.findById(id).orElseThrow(()-> new NotFoundException("Answer id " + id + " is not exist"));
		answerRepository.deleteById(id);
		AnswerDto response = modelMapper.map(findAnswer, AnswerDto.class);

		return ResponseEntity.ok(response);
	}
}