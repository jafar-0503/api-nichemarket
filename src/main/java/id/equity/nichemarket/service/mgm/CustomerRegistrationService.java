package id.equity.nichemarket.service.mgm;

import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.mgm.custom.CreateCustomerRegistrationDto;
import id.equity.nichemarket.dto.mgm.customer.CustomerDto;
import id.equity.nichemarket.dto.mgm.registration.RegistrationDto;
import id.equity.nichemarket.model.mgm.Customer;
import id.equity.nichemarket.model.mgm.CustomerRegistration;
import id.equity.nichemarket.model.mgm.Registration;
import id.equity.nichemarket.repository.mgm.CustomerRegistrationRepository;

@Service
public class CustomerRegistrationService {
	
	@Autowired
	private CustomerRegistrationRepository customerRegistrationRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private EntityManager em;
	
    private static final Logger logger = LoggerFactory.getLogger(CustomerRegistrationService.class);

	
	public CreateCustomerRegistrationDto addCustomerRegistration(CreateCustomerRegistrationDto newCustomerRegistration) {
		CustomerRegistration customerRegistration = modelMapper.map(newCustomerRegistration, CustomerRegistration.class);
		customerRegistration.setActive(true);
		CustomerRegistration customerRegistrationSaved = customerRegistrationRepository.save(customerRegistration);
		em.refresh(customerRegistrationSaved);
		CreateCustomerRegistrationDto response = modelMapper.map(customerRegistrationSaved, CreateCustomerRegistrationDto.class);
		return response;		
	}

	public RegistrationDto getActiveRegistrationByCustomerCode(String customerCode) {
		Registration registration = customerRegistrationRepository.findActiveCustomerRegistrationByCustomerCode(customerCode)
				.orElseThrow(()-> new NotFoundException("Customer Registration with Customer Code" + customerCode + " is not exist"));
		RegistrationDto response = modelMapper.map(registration, RegistrationDto.class);
		return response;
	}
	
	public List<CustomerDto> getAllCustomerByRegCode(String registrationCode) {
		List<Customer> customerList = customerRegistrationRepository.findAllCustomerByRegistrationCode(registrationCode);
		Type listType = new TypeToken<List<CustomerDto>>() {}.getType();
		List<CustomerDto> customerListDto = modelMapper.map(customerList, listType);
		return customerListDto;	
	}

	//For Activation Account After Payment
	public CustomerDto getPrimaryCustomerByRegistrationCode(String registrationCode) {
		Customer customer = customerRegistrationRepository.findPrimaryCustomerByRegistrationCode(registrationCode)
				.orElseGet(()->{
					logger.warn("Primary Customer with registration Code" + registrationCode + " is not exist");
					throw new NotFoundException("Primary Customer with registration Code" + registrationCode + " is not exist");
				});
		CustomerDto response = modelMapper.map(customer, CustomerDto.class);
		return response;	
	}
}
