package id.equity.nichemarket.service.mgm;

import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.mgm.customer.CustomerDto;
import id.equity.nichemarket.dto.mgm.customerPoint.CustomerPointDto;
import id.equity.nichemarket.dto.mgm.gift.GiftDto;
import id.equity.nichemarket.model.mgm.CustomerPoint;
import id.equity.nichemarket.repository.mgm.CustomerPointRepository;

@Service
public class CustomerPointService {

	@Autowired
	private CustomerPointRepository customerPointRepository;
	
	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private EntityManager em;
	
	//List CustomerPoint
	public ResponseEntity<List<CustomerPointDto>> listCustomerPoint(){
		List<CustomerPoint> lsCustomerPoint= customerPointRepository.findAll();
		Type targetType = new TypeToken<List<CustomerPointDto>>() {}.getType();
		List<CustomerPointDto> response = modelMapper.map(lsCustomerPoint, targetType);

		return ResponseEntity.ok(response);
	}

	//Get CustomerPoint By Id
	public ResponseEntity<CustomerPointDto> getCustomerPointById(Long id) {
		CustomerPoint customerPoint = customerPointRepository.findById(id).orElseThrow(()-> new NotFoundException("CustomerPoint id " + id + " is not exist"));
		CustomerPointDto response = modelMapper.map(customerPoint, CustomerPointDto.class);

		return ResponseEntity.ok(response);
	}
	

	//Post CustomerPoint
	@Transactional
	public CustomerPointDto addCustomerPoint(CustomerPointDto newCustomerPoint) {
		CustomerPoint customerPoint = modelMapper.map(newCustomerPoint, CustomerPoint.class);
		customerPoint.setActive(true);
		CustomerPoint customerPointSaved = customerPointRepository.save(customerPoint);
		customerPointRepository.refresh(customerPointSaved);
		CustomerPointDto response = modelMapper.map(customerPointSaved, CustomerPointDto.class);
		return response;
	}
	
	

	//Put CustomerPoint
	public ResponseEntity<CustomerPointDto> editCustomerPoint(CustomerPointDto updateCustomerPoint, Long id) {
		CustomerPoint customerPoint = customerPointRepository.findById(id).orElseThrow(()-> new NotFoundException("CustomerPoint id " + id + " is not exist"));
		customerPoint.setCustomerPointCode(updateCustomerPoint.getCustomerPointCode());		
		customerPointRepository.save(customerPoint);
		CustomerPointDto response = modelMapper.map(customerPoint, CustomerPointDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete CustomerPoint	
	public ResponseEntity<CustomerPointDto> deleteCustomerPoint(Long id) {
		CustomerPoint findCustomerPoint = customerPointRepository.findById(id).orElseThrow(()-> new NotFoundException("CustomerPoint id " + id + " is not exist"));
		customerPointRepository.deleteById(id);
		CustomerPointDto response = modelMapper.map(findCustomerPoint, CustomerPointDto.class);

		return ResponseEntity.ok(response);
	}

	//Get CustomerPoint By customerPointCode
	public CustomerPointDto getCustomerPointByCode(String customerPointCode) {
		CustomerPoint customerPoint = customerPointRepository.findByCustomerPointCode(customerPointCode).orElseThrow(()-> new NotFoundException("Customer Point Code " + customerPointCode + " is not exist"));;
		return modelMapper.map(customerPoint, CustomerPointDto.class);
	}
	

	
	public CustomerPointDto transactionPoint(String accountCode, GiftDto gift) {
		CustomerPointDto customerPoint = new CustomerPointDto();
        customerPoint.setPointIn(gift.getPointGained());
        customerPoint.setAccountCode(accountCode);
        customerPoint.setDescription(gift.getGiftCode());
        CustomerPointDto customerPointSaved = this.addCustomerPoint(customerPoint); 
        return customerPointSaved;
	}

	public CustomerPointDto getCustomerPointByAccountCode(String accountCode) {
		CustomerPoint customerPoint = customerPointRepository.findTop1ByAccountCodeOrderByIdDesc(accountCode ).orElseThrow(()-> new NotFoundException("Account Code " + accountCode + " is not exist"));;
		CustomerPointDto response = modelMapper.map(customerPoint, CustomerPointDto.class);
		return response;
	}


	
}
