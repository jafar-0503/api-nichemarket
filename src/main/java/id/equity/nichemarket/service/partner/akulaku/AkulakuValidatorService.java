package id.equity.nichemarket.service.partner.akulaku;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import id.equity.nichemarket.dto.mgm.CustomerValidationDto;
import id.equity.nichemarket.dto.mgm.customer.CustomerAgeValidationDto;
import id.equity.nichemarket.dto.mgm.registrationType.RegistrationTypeDto;
import id.equity.nichemarket.dto.mgm.ruleInsuredAge.RuleInsuredAgeDto;
import id.equity.nichemarket.exception.CustomerValidationException;
import id.equity.nichemarket.exception.ErrorsException;
import id.equity.nichemarket.model.mgm.Customer;
import id.equity.nichemarket.retrofit.mgm.freeCovid.FreeCovidService;
import id.equity.nichemarket.service.mgm.AccountService;
import id.equity.nichemarket.service.mgm.ProductPlanPolicyService;
import id.equity.nichemarket.service.mgm.ProductPolicyService;
import id.equity.nichemarket.service.mgm.RegistrationService;
import id.equity.nichemarket.service.mgm.RegistrationTypeService;
import id.equity.nichemarket.service.mgm.RuleInsuredAgeService;
import id.equity.nichemarket.service.mgm.SiMedisValidatorCoreService;
import id.equity.nichemarket.service.mgm.UtilitiesService;
import id.equity.nichemarket.validation.partner.BaseRegistrationValidation;

@Component
public class AkulakuValidatorService extends BaseRegistrationValidation<Customer, RegistrationTypeDto>{
    private static final Logger logger = LoggerFactory.getLogger(AkulakuValidatorService.class);

	@Autowired
	private SiMedisValidatorCoreService siMedisValidatorCoreService;
	
	
	@Override
	public boolean isValidKtpCore(CustomerValidationDto customerValidation) {
		return siMedisValidatorCoreService.isValidKtp(customerValidation);
	}
	
	
	@Override
	public List<String> validate(List<Customer> customerList, RegistrationTypeDto currRegistrationType) {
		List<String> errors = new ArrayList<>();
		
		//Validate total customer
		if(!(this.isValidTotalCustomer(customerList.size(), currRegistrationType))) {
			throw new ErrorsException("Total Registered Customer Is Invalid");
		}		
		
		//Validate App Code,KTP,Email Address
		boolean isNotDuplicateKtpNO = true;
		boolean isValidAge = true;
		
		//Validate KTP (Core / BackEnd) , Age 
		for(Customer customer : customerList){			
			isNotDuplicateKtpNO = true;
			isValidAge = true;
			
			//Check KTP On Backend			
			isNotDuplicateKtpNO = this.isNotDuplicateKtp(customer.getKtpNO(), currRegistrationType.getProductPolicyCode());
			System.out.println("DUPLICATE KTP NO STATUS = " + isNotDuplicateKtpNO);
			
			//Check KTP On Core
			CustomerValidationDto customerValidation = new CustomerValidationDto();
			customerValidation.setKtpNo(customer.getKtpNO());
			customerValidation.setRegistrationTypeCode(currRegistrationType.getRegistrationTypeCode());		
			boolean isKtpCoreValid = this.isValidKtpCore(customerValidation);			
		
			//Check Age		
			CustomerAgeValidationDto customerAgeValidation = new CustomerAgeValidationDto();
			customerAgeValidation.setDateOfBirth(customer.getDateOfBirth());
			customerAgeValidation.setRegistrationTypeCode(currRegistrationType.getRegistrationTypeCode());
			customerAgeValidation.setCustomerStatus(customer.getCustomerStatus());			
			isValidAge = this.isValidAge(customerAgeValidation);	
			
			if(isNotDuplicateKtpNO == false) {
				errors.add("Duplicate KTP No : " + customer.getKtpNO());
			}
			
			if(isKtpCoreValid == false) {
				errors.add("Duplicate KTP No On Core System: " + customer.getKtpNO());
			}			
					
			if(isValidAge == false) {
				errors.add("Invalid Age For: " + customer.getCustomerName());
			}	
			
		}
		
		return errors;	
		
	}

	@Override
	public String getType() {		
		return "akulaku";
	}


}
