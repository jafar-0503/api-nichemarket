package id.equity.nichemarket.service.partner.linkaja;

import lombok.Data;
import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.constant.url.GlobalConstantLinkaja;
import id.equity.nichemarket.exception.ErrorsException;


@Data
@Service("linkajaAuthorizationService")
public class LinkajaAuthorizationService {

	private String username = GlobalConstantLinkaja.USERNAME_LINKAJA;
	private String password = GlobalConstantLinkaja.PASSWORD_LINKAJA;
	
    private static final Logger logger = LoggerFactory.getLogger(LinkajaAuthorizationService.class);

	
	public boolean isValidCredentials(String headerAuth) {
		
		try {
			String token = headerAuth.split(" ")[1];
			String credential = new String(Base64.getDecoder().decode(token));
			
			String currUsername = credential.split(":")[0];
			String currPassword = credential.split(":")[1];	
			logger.info("Login info : " + currUsername + "-" + currPassword);
			if(currUsername.equals(this.username) && currPassword.equals(this.password) ) {
				return true;
			}else {
				return false;
			}
		}catch(Exception e) {
			e.printStackTrace();
			throw new ErrorsException("Error Validate Authorization");
		}
		
	}
	
}
