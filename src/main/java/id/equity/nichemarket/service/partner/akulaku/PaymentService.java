package id.equity.nichemarket.service.partner.akulaku;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.mgm.account.AccountCustomerDto;
import id.equity.nichemarket.dto.mgm.bill.BillDto;
import id.equity.nichemarket.dto.mgm.customer.CustomerDto;
import id.equity.nichemarket.dto.mgm.customerPolicy.CustomerPolicyDto;
import id.equity.nichemarket.dto.mgm.payment.PaymentDto;
import id.equity.nichemarket.dto.mgm.payment.PaymentRequestDto;
import id.equity.nichemarket.dto.mgm.payment.PaymentResponseDto;
import id.equity.nichemarket.dto.mgm.payment.PaymentSuccessResponseDto;
import id.equity.nichemarket.dto.mgm.registration.RegistrationDto;
import id.equity.nichemarket.exception.ErrorsException;
import id.equity.nichemarket.model.mgm.Payment;
import id.equity.nichemarket.repository.mgm.PaymentRepository;
import id.equity.nichemarket.retrofit.mgm.freeCovid.BaseFreeCovidResponse;
import id.equity.nichemarket.retrofit.mgm.freeCovid.FreeCovidCertificateResponse;
import id.equity.nichemarket.service.mgm.AccountService;
import id.equity.nichemarket.service.mgm.AppService;
import id.equity.nichemarket.service.mgm.BillService;
import id.equity.nichemarket.service.mgm.CustomerPointService;
import id.equity.nichemarket.service.mgm.CustomerPolicyService;
import id.equity.nichemarket.service.mgm.CustomerReferralService;
import id.equity.nichemarket.service.mgm.CustomerRegistrationService;
import id.equity.nichemarket.service.mgm.CustomerService;
import id.equity.nichemarket.service.mgm.GiftService;
import id.equity.nichemarket.service.mgm.GroupInsuranceService;
import id.equity.nichemarket.service.mgm.NotificationService;
import id.equity.nichemarket.service.mgm.RegistrationService;
import id.equity.nichemarket.service.mgm.RegistrationTypeService;

@Service("akulakuPaymentService")
public class PaymentService {
	@Autowired
	private PaymentRepository paymentRepository;
	
	@Autowired
	private CustomerService customerService;

	@Autowired
	private CustomerRegistrationService customerRegistrationService;
	
	@Autowired
	private RegistrationService registrationService;
	
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private CustomerPolicyService customerPolicyService;
	
	@Autowired
	private GroupInsuranceService groupInsuranceService;
	
	@Autowired
	private RegistrationTypeService registrationTypeService;
	
	@Autowired
	private CustomerReferralService customerReferralService;
	
	@Autowired
	private CustomerPointService customerPointService;
	
	@Autowired
	private BillService billService;
	
	@Autowired
	private NotificationService notificationService;
	
	@Autowired
	private GiftService giftService;
	
	@Autowired
	private AppService appService;
	
	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private EntityManager em;
	
    private static final Logger logger = LoggerFactory.getLogger(PaymentService.class);
    

	//Post Payment
    //@Transactional
	public ResponseEntity<PaymentSuccessResponseDto> addPayment(PaymentRequestDto newPayment) throws Exception {	
		//Check if payment status is false
		if(newPayment.isPaymentStatus() == false) {
			throw new ErrorsException("Payment Status Is False, Data Will Not Recorded");
		}
		
		//Check If Bill Exist 
		BillDto bill = billService.getBillByCode(newPayment.getBillCode());
		
		//Check If Payment Haven't paid
		Payment existPayment = paymentRepository.findByBillCode(newPayment.getBillCode());
		if(existPayment != null) {
			logger.warn("Bill " + newPayment.getBillCode() + " already paid");
			throw new ErrorsException("Bill already paid");
		}
		
		//Validate Amount 
		validatePaymentAmount(newPayment);
		
		//Get Account, Bill, Registration , & Customer Primary By RegCode
		RegistrationDto registration = registrationService.getRegistrationByCode(bill.getRegistrationCode());
		AccountCustomerDto account = accountService.getAccountByCode(registration.getAccountCode());
		List<CustomerDto> allCustomer = customerRegistrationService.getAllCustomerByRegCode(bill.getRegistrationCode());
		
		CustomerDto customer = allCustomer.stream()
										.filter( c -> c.getCustomerStatus().equals("0"))
										.findAny()                            
						                .orElse(null);
		
		if(customer == null) {
			logger.error("Primary Customer With Reg Code " + bill.getRegistrationCode() + " Is Not Found");
			throw new NotFoundException("Primary Customer With Reg Code " + bill.getRegistrationCode() + " Is Not Found");
		}
				
		//Save Payment To DB
		PaymentDto paymentResponse = this.addNewPayment(newPayment);		
		logger.info("Payment = " + paymentResponse);
		
		//Map to Response object
		PaymentResponseDto paymentResponseFinal = modelMapper.map(paymentResponse, PaymentResponseDto.class);
		
		//Update Account to active
		AccountCustomerDto updateAccount = new AccountCustomerDto();
		updateAccount.setActive(true);
		AccountCustomerDto updatedAccount = accountService.updateAccountStatus(updateAccount, account.getAccountCode());
		logger.info("Set Account : " + account.getAccountCode() + " To Active");

		//Save To Create Transaction To Core
		List<CustomerPolicyDto> customerPolicyList = customerPolicyService.saveAllCustomerPolicy(allCustomer, bill.getRegistrationCode());	
		logger.info("Customer Policy = " + customerPolicyList);
		
		//Submit All Customer To Core & Save To Policy  
		List<BaseFreeCovidResponse> responseGrpList = groupInsuranceService.saveToCoreGrp(registration, customerPolicyList, paymentResponse);
			
		//Generate Certificate				
		//Result is not unique !!!!!!	
		FreeCovidCertificateResponse freeCovidResponse = null;        
		
		PaymentSuccessResponseDto response = new PaymentSuccessResponseDto();
		response.setRegistrationCode(bill.getRegistrationCode());
		response.setReferralCode("");
		response.setPayment(paymentResponseFinal);
		response.setMembers(responseGrpList);
		response.setCertificate(freeCovidResponse);
		response.setEmailStatus(null);
		
		return ResponseEntity.ok(response);
	}
	
	private void validatePaymentAmount(PaymentRequestDto newPayment) {
		//Check Amount & Paid Amount if 0		
		if(newPayment.getAmount().intValue() != newPayment.getPaidAmount().intValue()) {
			logger.warn("Invalid Amount / Paid Amount" + newPayment.getAmount() + "!=" + newPayment.getPaidAmount());
			throw new ErrorsException("Invalid Amount / Paid Amount");
		}
	}
	
	@Transactional
	public PaymentDto addNewPayment(PaymentRequestDto newPayment) {
		Payment payment = modelMapper.map(newPayment, Payment.class);
		Payment paymentSaved = paymentRepository.save(payment);
		paymentRepository.refresh(paymentSaved);
		PaymentDto paymentResponse = modelMapper.map(paymentSaved, PaymentDto.class);
		return paymentResponse;
	}
}
