package id.equity.nichemarket.service.partner.linkaja;
import java.sql.Timestamp;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.constant.url.GlobalConstantLinkaja;
import id.equity.nichemarket.exception.ErrorsException;

@Service
public class EncryptionAESService {

	private static final String key = GlobalConstantLinkaja.SECRET_KEY;
    private static String initVector = GlobalConstantLinkaja.HEADER_TIMESTAMP;
	
    private static final Logger logger = LoggerFactory.getLogger(EncryptionAESService.class);
        
    public static String encrypt(String value) {
    	try { 	  
    		generateIV();
    		IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
    		SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
    
    		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
    		cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
    
    		byte[] encrypted = cipher.doFinal(value.getBytes("UTF-8"));
    		return Base64.getEncoder().encodeToString(encrypted);
    	} catch (Exception ex) {
            logger.error("Error :" + ex);
    		ex.printStackTrace();
    		throw new ErrorsException("Error encrypt process");
    	}
    }
    
    public static String decrypt(String value, String headerInitVector) {
    	try {
    		IvParameterSpec iv = new IvParameterSpec(headerInitVector.getBytes("UTF-8"));
    		SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
    
    		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
    		cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
    
    		byte[] encrypted = cipher.doFinal(Base64.getDecoder().decode(value));
    		return new String(encrypted);
    	} catch (Exception ex) {
    		ex.printStackTrace();
            logger.error("Error :" + ex);
    		throw new ErrorsException("Error decrypt process");
    	}
    }
    
    
    /**
     * Generating initVector to global variable on Class GlobalConstantLinkaja 
     */
    public static void generateIV() {
	   Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	   GlobalConstantLinkaja.HEADER_TIMESTAMP = timestamp.getTime() + "000";
       
       initVector = GlobalConstantLinkaja.HEADER_TIMESTAMP;
       logger.info("Used Init Vector :" + initVector);
    }
    
    public static String encodeBase64Auth(String username, String password) {
    	try{
    		String usernamePassword = username + ":" + password;
            byte[] bytes = usernamePassword.getBytes("UTF-8");
            return Base64.getEncoder().encodeToString(bytes);
        }catch(Exception e){
            e.printStackTrace();       
            logger.error("Error :" + e);
            throw new ErrorsException("Error when encoding username,password");
        }
    }
    
    public static String decodeBase64Auth(String base64) {
    	return new String(Base64.getDecoder().decode(base64));
    }
}
