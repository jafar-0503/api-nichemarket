package id.equity.nichemarket.service.partner.linkaja;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.json.JSONException;
import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.json.JsonReadFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.mgm.account.AccountCustomerDto;
import id.equity.nichemarket.dto.mgm.bill.BillDto;
import id.equity.nichemarket.dto.mgm.customer.CustomerDto;
import id.equity.nichemarket.dto.mgm.customerPolicy.CustomerPolicyDto;
import id.equity.nichemarket.dto.mgm.payment.PaymentDto;
import id.equity.nichemarket.dto.mgm.payment.v2.PaymentRequestDto;
import id.equity.nichemarket.dto.mgm.payment.v2.PaymentSuccessResponseDto;
import id.equity.nichemarket.dto.mgm.registration.RegistrationDto;
import id.equity.nichemarket.dto.partner.linkaja.ApplinkResponseDto;
import id.equity.nichemarket.dto.partner.linkaja.InformPaymentDto;
import id.equity.nichemarket.dto.partner.linkaja.InformPaymentResponseDto;
import id.equity.nichemarket.exception.ErrorsException;
import id.equity.nichemarket.exception.LinkajaInformPaymentException;
import id.equity.nichemarket.model.mgm.Payment;
import id.equity.nichemarket.repository.mgm.PaymentRepository;
import id.equity.nichemarket.retrofit.mgm.freeCovid.BaseFreeCovidResponse;
import id.equity.nichemarket.retrofit.mgm.freeCovid.FreeCovidCertificateResponse;
import id.equity.nichemarket.retrofit.partner.linkaja.IntegrationLinkAjaService;
import id.equity.nichemarket.service.mgm.AccountService;
import id.equity.nichemarket.service.mgm.BillService;
import id.equity.nichemarket.service.mgm.CustomerPolicyService;
import id.equity.nichemarket.service.mgm.CustomerRegistrationService;
import id.equity.nichemarket.service.mgm.GroupInsuranceService;
import id.equity.nichemarket.service.mgm.RegistrationService;


@Service("linkajaPaymentService")
public class PaymentService {
	
	
	@Autowired
	private HttpServletRequest request;	
	
	@Autowired
	private PaymentRepository paymentRepository;
	
	@Autowired
	private IntegrationLinkAjaService integrationLinkAjaService;	

	@Autowired
	private EncryptionAESService encryptionAESService;	

	@Autowired
	private CustomerRegistrationService customerRegistrationService;
	
	@Autowired
	private RegistrationService registrationService;
	
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private CustomerPolicyService customerPolicyService;
	
	@Autowired
	private GroupInsuranceService groupInsuranceService;
	
	@Autowired
	private BillService billService;
	
	
	@Autowired
	private ModelMapper modelMapper;
	
    private static final Logger logger = LoggerFactory.getLogger(PaymentService.class);
	
	public ResponseEntity<ApplinkResponseDto> testAPI() {
		ApplinkResponseDto applinkResponseDto = integrationLinkAjaService.generateApplink("b8HsgiHoXzZ9aeba4XaGWXdMJQq8cR7wEDaMGmv5GxL0fPjqOdtKFzMhpJ9urQz+o1OMyqlB5BUhB31YARlVYZvJUNgUtse0iC98F7S36M771VhtiLGTJHF9sOGj1CJ42PnUy4X1vYXPL45nKTIeaWEhEGStXGBMK8p88D90IkgdWSH1sXtXwzBTdx2I0E3wWLGuHMA5meZInER+KNT0M9S6Z7W4Ol5FL7gkffkkW3A0pazxtWzLo9IwkxY8YFrjwFtNQkmcBx1T4f/SmeQMM/hvl4j1S8hVoDkNolsIwBX9rbU8ee07dUH2XpNo1k8CuLIlITO4vQPzDYHP//w+bozmaec0Qn3tlwb5USJGOBlJKqXasKw7deIiGE6SbFyWmjF97LmtGWVkRTYZ63I6Mw6sVnKSQ9h+0VQ76yJIUu/8w6o+7VQK2ju5/ZCj6BNccIFUj45jqjnLmaQXimcGScZROjLsUxSxNVRJontiIwbdQ1tVxAX0ws2YiutwCzRtg9pvaNfdoLLqnVu3BjcNZM9RL8x8SORoZcDuTqGIdhYlIcZ1qVVzbzLZGu6HLtmHhSg+IgZJ3U65MY/2HxQF6LlVwvyrcW/j6L6aDFxzA/UI8a7KMZi1OmsLt3AZXU9sYb+1igV2Y4tY71jSWT6MXQ==");
		return ResponseEntity.ok(applinkResponseDto);
	}
	
	public PaymentRequestDto decryptPayment(String decryptedString) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		mapper.enable(JsonReadFeature.ALLOW_LEADING_ZEROS_FOR_NUMBERS.mappedFeature());


		String timestampHeader = request.getHeader("timestamp");
		String plainString = encryptionAESService.decrypt(decryptedString, timestampHeader);

		logger.info("Decrypted Request String : " + plainString);
		
		InformPaymentDto informPayment = null;
		try {
			informPayment = mapper.readValue(plainString, InformPaymentDto.class);
			
			//Validate Object (BillCode , Amount, Item , etc)
			logger.info("Success Decrypted Request Object : " + informPayment);

			if(!informPayment.getStatus().equals("00") && !informPayment.getStatus().equals("0") ) {
				throw new LinkajaInformPaymentException("02" , "Status is not success" , informPayment.getPartnerTrxID() , informPayment.getLinkAjaRefnum());
			}
			
			if(informPayment.getLinkAjaRefnum() == null) {
				throw new LinkajaInformPaymentException("02" , "LinkAjaRefNum is not found" , informPayment.getPartnerTrxID() , informPayment.getLinkAjaRefnum());
			}
			
			//Construct Payment Object
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			
			PaymentRequestDto payment = new PaymentRequestDto();
			payment.setBillCode(informPayment.getPartnerTrxID());
			payment.setPaidAmount(new BigDecimal(informPayment.getTotalAmount()));
			payment.setAmount(new BigDecimal(informPayment.getTotalAmount()));
			payment.setPaymentRefCode(informPayment.getLinkAjaRefnum());
			payment.setPaymentDesc("LinkAja");
			payment.setPaymentType("LinkAja");
			payment.setTransactionDate(dateFormat.format(date));
			payment.setPaymentStatus(true);
			
			logger.info("Payment Object : " + payment.toString());				
			
			return payment;
			
		} catch (JsonMappingException e) {
			e.printStackTrace();
			logger.error("Error Mapping Encrypted String to Json");
			
			JSONObject jsonObj;
			try {
				jsonObj = new JSONObject(plainString);
						    
			} catch (JSONException ex) {
				logger.error("Error Processing Encrypted String to Json II");			
				e.printStackTrace();
				throw new LinkajaInformPaymentException("02" , "Invalid Transaction" , null , null);  
			}	
			
			throw new LinkajaInformPaymentException("02" , "Invalid Transaction" , jsonObj.get("linkAjaRefnum").toString() , jsonObj.get("partnerTrxID").toString());
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			logger.error("Error Processing Encrypted String to Json");			

			JSONObject jsonObj;
			try {
				jsonObj = new JSONObject(plainString);
						    
			} catch (JSONException ex) {
				logger.error("Error Processing Encrypted String to Json II");			
				e.printStackTrace();
				throw new LinkajaInformPaymentException("02" , "Invalid Transaction" , null , null);  
			}	
			
			throw new LinkajaInformPaymentException("02" , "Invalid Transaction" ,  jsonObj.get("linkAjaRefnum").toString() , jsonObj.get("partnerTrxID").toString());
		}		
		
	}
	
	//@Transactional
	public InformPaymentResponseDto addPayment(PaymentRequestDto newPayment) throws Exception {	
		try {		
			
			//Check if payment status is false
			if(newPayment.isPaymentStatus() == false) {
				throw new LinkajaInformPaymentException("02" , "Payment Status Is False, Data Will Not Recorded" , newPayment.getBillCode() , newPayment.getPaymentRefCode());
			}
			
			//Check If Bill Exist 
			BillDto bill = billService.getBillByCode(newPayment.getBillCode());
			
			//Check If Payment Haven't paid
			Payment existPayment = paymentRepository.findByBillCode(newPayment.getBillCode());
			if(existPayment != null) {
				logger.warn("Bill " + newPayment.getBillCode() + " already paid");
				throw new LinkajaInformPaymentException("02" , "Bill Already Paid" , newPayment.getBillCode() , newPayment.getPaymentRefCode());
			}
			
			//Validate Amount 
			validatePaymentAmount(newPayment);
			
			//Get Account, Bill, Registration , & Customer Primary By RegCode
			RegistrationDto registration = registrationService.getRegistrationByCode(bill.getRegistrationCode());
			AccountCustomerDto account = accountService.getAccountByCode(registration.getAccountCode());
			List<CustomerDto> allCustomer = customerRegistrationService.getAllCustomerByRegCode(bill.getRegistrationCode());
			
			CustomerDto customer = allCustomer.stream()
											.filter( c -> c.getCustomerStatus().equals("0"))
											.findAny()                            
							                .orElse(null);
			
			if(customer == null) {
				logger.error("Primary Customer With Reg Code " + bill.getRegistrationCode() + " Is Not Found");
				throw new LinkajaInformPaymentException("02" , "Invalid Transaction" , newPayment.getBillCode() , newPayment.getPaymentRefCode());
			}
					
			//Save Payment To DB
			PaymentDto paymentResponse = this.addNewPayment(newPayment);		
			logger.info("Payment = " + paymentResponse);
			
			//=============Does we need Update Account to active?????====================
			AccountCustomerDto updateAccount = new AccountCustomerDto();
			updateAccount.setActive(true);
			AccountCustomerDto updatedAccount = accountService.updateAccountStatus(updateAccount, account.getAccountCode());
			logger.info("Set Account : " + account.getAccountCode() + " To Active");
			
			//Save To Create Transaction To Core
			List<CustomerPolicyDto> customerPolicyList = customerPolicyService.saveAllCustomerPolicy(allCustomer, bill.getRegistrationCode());	
			logger.info("Customer Policy = " + customerPolicyList);
			
			//Submit All Customer To Core & Save To Policy  
			List<BaseFreeCovidResponse> responseGrpList = groupInsuranceService.saveToCoreGrp(registration, customerPolicyList , paymentResponse);
				
			//Generate Certificate				
			FreeCovidCertificateResponse freeCovidResponse = null;        
			
			InformPaymentResponseDto informPaymentResponse = new InformPaymentResponseDto();
			informPaymentResponse.setStatus("00");
			informPaymentResponse.setMessage("Success");
			informPaymentResponse.setLinkAjaRefnum(newPayment.getPaymentRefCode());
			informPaymentResponse.setPartnerTrxID(newPayment.getBillCode());
			return informPaymentResponse;	
		}catch(Exception e) {
			logger.error("Failed Add Payment");
			e.printStackTrace();
			if(e instanceof LinkajaInformPaymentException) throw e;
			throw new LinkajaInformPaymentException("05" , "Failed Transaction" , newPayment.getBillCode() , newPayment.getPaymentRefCode());
		}
	}
	
	
	private void validatePaymentAmount(PaymentRequestDto newPayment) {
		//Check Amount & Paid Amount if 0		
		if(newPayment.getAmount().intValue() != newPayment.getPaidAmount().intValue()) {
			logger.warn("Invalid Amount / Paid Amount" + newPayment.getAmount() + "!=" + newPayment.getPaidAmount());
			throw new LinkajaInformPaymentException("02" , "Invalid Amount" , newPayment.getBillCode() , newPayment.getPaymentRefCode());
		}
	}
	
	@Transactional
	public PaymentDto addNewPayment(PaymentRequestDto newPayment) {
		Payment payment = modelMapper.map(newPayment, Payment.class);
		Payment paymentSaved = paymentRepository.save(payment);
		paymentRepository.refresh(paymentSaved);
		PaymentDto paymentResponse = modelMapper.map(paymentSaved, PaymentDto.class);
		return paymentResponse;
	}
	
	
	/*
	 * DELETE THIS IS DUMMY
	 * 
	 */
	public InformPaymentResponseDto dummyFunc(String decryptedString) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		mapper.enable(JsonReadFeature.ALLOW_LEADING_ZEROS_FOR_NUMBERS.mappedFeature());

		String timestampHeader = request.getHeader("timestamp");
		String plainString = encryptionAESService.decrypt(decryptedString, timestampHeader);

		logger.info("Decrypted Request String : " + plainString);
		
		InformPaymentDto informPayment = null;
		
		JSONObject jsonObj;
		try {
			jsonObj = new JSONObject(plainString);
			InformPaymentResponseDto informPaymentResponse = new InformPaymentResponseDto();
			informPaymentResponse.setStatus("00");
			informPaymentResponse.setMessage("Success");
			informPaymentResponse.setLinkAjaRefnum(jsonObj.get("linkAjaRefnum").toString());
			informPaymentResponse.setPartnerTrxID(jsonObj.get("partnerTrxID").toString());
			return informPaymentResponse;			    
		} catch (JSONException e) {
			e.printStackTrace();
			InformPaymentResponseDto informPaymentResponse = new InformPaymentResponseDto();
			informPaymentResponse.setStatus("00");
			informPaymentResponse.setMessage("Success");
			informPaymentResponse.setLinkAjaRefnum("");
			informPaymentResponse.setPartnerTrxID("");
			return informPaymentResponse;			    
		}	
		
	}
	
}
