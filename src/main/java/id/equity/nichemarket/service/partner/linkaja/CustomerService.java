package id.equity.nichemarket.service.partner.linkaja;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.equity.nichemarket.constant.url.GlobalConstantLinkaja;
import id.equity.nichemarket.dto.mgm.account.AccountCustomerDto;
import id.equity.nichemarket.dto.mgm.app.AppDto;
import id.equity.nichemarket.dto.mgm.bill.BillDto;
import id.equity.nichemarket.dto.mgm.custom.CreateCustomerRegistrationDto;
import id.equity.nichemarket.dto.mgm.customer.CustomerDetailPolicyDto;
import id.equity.nichemarket.dto.mgm.customerRegistration.CustomerRegistrationDto;
import id.equity.nichemarket.dto.mgm.customerRegistration.CustomerRegistrationResponseDto;
import id.equity.nichemarket.dto.mgm.registration.RegistrationDto;
import id.equity.nichemarket.dto.mgm.registrationType.RegistrationTypeDto;
import id.equity.nichemarket.dto.partner.linkaja.ApplinkCustomerRegistrationResponseDto;
import id.equity.nichemarket.dto.partner.linkaja.ApplinkItemsRequestDto;
import id.equity.nichemarket.dto.partner.linkaja.ApplinkRefDataRequestDto;
import id.equity.nichemarket.dto.partner.linkaja.ApplinkRequestDto;
import id.equity.nichemarket.dto.partner.linkaja.ApplinkResponseDto;
import id.equity.nichemarket.exception.CustomerValidationException;
import id.equity.nichemarket.exception.ErrorsException;
import id.equity.nichemarket.model.mgm.App;
import id.equity.nichemarket.model.mgm.Customer;
import id.equity.nichemarket.model.mgm.Registration;
import id.equity.nichemarket.model.mgm.RegistrationType;
import id.equity.nichemarket.repository.mgm.CustomerRepository;
import id.equity.nichemarket.repository.mgm.RegistrationRepository;
import id.equity.nichemarket.retrofit.partner.linkaja.IntegrationLinkAjaService;
import id.equity.nichemarket.service.mgm.AccountService;
import id.equity.nichemarket.service.mgm.AppService;
import id.equity.nichemarket.service.mgm.BillService;
import id.equity.nichemarket.service.mgm.CustomerPolicyService;
import id.equity.nichemarket.service.mgm.CustomerRegistrationService;
import id.equity.nichemarket.service.mgm.RegistrationTypeService;
import id.equity.nichemarket.service.rm.UserService;
import id.equity.nichemarket.validation.partner.BaseRegistrationValidation;
import id.equity.nichemarket.validation.partner.RegistrationValidationFactory;


@Service("linkajaCustomerService")
public class CustomerService {
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private RegistrationTypeService registrationTypeService;
	
	@Autowired
	private BillService billService;
	
	@Autowired
	private AppService appService;
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@Autowired
	private UserService userService;
		
	@Autowired
	private CustomerPolicyService customerPolicyService;	
	
	@Autowired
	private CustomerRegistrationService customerRegistrationService;	
	
	@Autowired
	private RegistrationRepository registrationRepository;
	
	@Autowired
	private IntegrationLinkAjaService integrationLinkAjaService;
	
	@Autowired
	private AccountService accountService;

	@Autowired
	private EntityManager em;	
	
    private static final Logger logger = LoggerFactory.getLogger(CustomerService.class);
    
    private static final String baseUrlPartnerAppLink = GlobalConstantLinkaja.BASE_URL_APP_LINK;
    
    
    @Transactional
	public ResponseEntity<ApplinkCustomerRegistrationResponseDto> registerCustomer(CustomerRegistrationDto newCustomer) {
    	Type listType = new TypeToken<List<Customer>>() {}.getType();
		List<Customer> customerList = modelMapper.map(newCustomer.getCustomers(), listType);		
		
		//Get Registration Type Code
		RegistrationType registrationType = modelMapper.map(newCustomer, RegistrationType.class);
		
		//Save Bills & Get billDetail by registration type code	
		RegistrationTypeDto currentRegistrationType = registrationTypeService.getRegistrationTypeByCode(registrationType.getRegistrationTypeCode());
		
		//===========================Validate Begin===========================//
		String errValidationMessage = "";
		
		BaseRegistrationValidation validationFactory = RegistrationValidationFactory.getService("linkaja"); 
		List<String> validationResult = validationFactory.validate(customerList, currentRegistrationType);
		
		if(!validationResult.isEmpty()) {
			logger.warn(validationResult.toString());
			errValidationMessage = "Invalid Customer Data";
			throw new CustomerValidationException(errValidationMessage, validationResult);
		}
		//===========================Validate End===========================//
		
		//Get Customer Parent	
		Customer customerParent = customerList.stream()
									    .filter( c -> c.getCustomerStatus().equals("0"))
									    .map(c -> { 
								    			c.setActive(true);
								    			c.setEmailAddress(c.getEmailAddress().toLowerCase());
								    			return c;
								    	})
									    .findAny()                            
						                .orElse(null);
		
		if(customerParent == null) {
			throw new ErrorsException("Invalid Customer Data");
		}
		
		//Get App Detail
		AppDto currAppDto = appService.getAppByAppCode(newCustomer.getAppCode());		
		App currApp = modelMapper.map(currAppDto ,App.class);
		
		//Save Account 
		AccountCustomerDto account = new AccountCustomerDto();
		account.setAppCode(newCustomer.getAppCode());
		account.setEmailAddress(customerParent.getEmailAddress().toLowerCase());
		account.setUniqueActivationKey(accountService.generateUniqueKey());
		account.setAccountActivated(false);	
		account.setActive(false);
		AccountCustomerDto accountSaved = accountService.saveAccount(account);
		
		//Save Registration with type code
		Registration registration = new Registration();
		registration.setRegistrationTypeCode(registrationType.getRegistrationTypeCode());
		registration.setAppCode(newCustomer.getAppCode());
		registration.setApp(currApp);	
		registration.setAccountCode(accountSaved.getAccountCode());
		Registration registrationSaved = registrationRepository.save(registration);		
		em.refresh(registrationSaved);					
	
		//Save & Get Unique Code Customer Parent
		Customer parentCustomerSaved = customerRepository.save(customerParent);	
		em.refresh(parentCustomerSaved);
		
		//Save Customer Registration Of Customer Parent
		CreateCustomerRegistrationDto parentCustomerRegistration = new CreateCustomerRegistrationDto();
		parentCustomerRegistration.setRegistrationCode(registrationSaved.getRegistrationCode());
		parentCustomerRegistration.setCustomerCode(parentCustomerSaved.getCustomerCode());
		CreateCustomerRegistrationDto customerRegistrationSaved = customerRegistrationService.addCustomerRegistration(parentCustomerRegistration);		
		
		//Save Bill
		BillDto bill = new BillDto();
		bill.setAmount(currentRegistrationType.getBillAmount());	
		bill.setRegistrationCode(registrationSaved.getRegistrationCode());
		BillDto billSaved = billService.saveBill(bill);
		
		//Create Response Object
		CustomerRegistrationResponseDto response = new CustomerRegistrationResponseDto();
		response.setBillAmount(bill.getAmount());
		response.setRegistrationCode(bill.getRegistrationCode());
		response.setBillCode(billSaved.getBillCode());	
		response.setUsedReferralCode(newCustomer.getUsedReferralCode());
		
		
		//==================================Call LinkAja retrofit=========================
		
		ApplinkRequestDto applinkRequest = new ApplinkRequestDto();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		LocalDateTime myDateObj = LocalDateTime.now();
	    DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
	    String formattedDate = myDateObj.format(myFormatObj);
	   
	    //Get Mapping merchant id by registration type code
	    String merchantID = GlobalConstantLinkaja.MERCHANT_ID_MAPPING.get(currentRegistrationType.getRegistrationTypeCode());
	    String itemsName = GlobalConstantLinkaja.ITEMS_NAME_MAPPING.get(currentRegistrationType.getRegistrationTypeCode());
	    byte[] bytes;
		try {
			bytes = registrationSaved.getRegistrationCode().getBytes("UTF-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
			throw new ErrorsException("Error encoding param url");
		}
	    String partnerAppLink = baseUrlPartnerAppLink + "payment/finish?regCode=" + Base64.getEncoder().encodeToString(bytes);
	    if(merchantID == null) {
	    	logger.error("Merchant ID is null");
	    	throw new ErrorsException("Merchant ID is invalid");
	    }

	    List<ApplinkItemsRequestDto> applinkItemsRequest = new ArrayList<ApplinkItemsRequestDto>();
	    applinkItemsRequest.add(new ApplinkItemsRequestDto(
	    													currentRegistrationType.getRegistrationTypeCode()
	    													, itemsName
	    													, bill.getAmount().toBigInteger().toString()
	    													, "1")
	    													);
	    
	    List<ApplinkRefDataRequestDto> applinkRefDataRequest = new ArrayList<ApplinkRefDataRequestDto>();;
	    applinkRefDataRequest.add(new ApplinkRefDataRequestDto("key" , "val"));	    
		applinkRequest.setTrxDate(formattedDate); //timestamp YYYMMDDHHMMSS
		applinkRequest.setPartnerTrxID(billSaved.getBillCode()); //Billcode
		applinkRequest.setMerchantID(merchantID); 
		applinkRequest.setTerminalID("ELI");
		applinkRequest.setTerminalName("PT Equity Life Indonesia");
		applinkRequest.setPartnerApplink(partnerAppLink);
		applinkRequest.setTotalAmount(bill.getAmount().toBigInteger().toString());
		applinkRequest.setItems(applinkItemsRequest);
		applinkRequest.setRefData(applinkRefDataRequest);
		
		ObjectMapper mapper = new ObjectMapper();
		String jsonString;
		
		try {
			jsonString = mapper.writeValueAsString(applinkRequest);
		} catch (JsonProcessingException e) {			
			e.printStackTrace();
			logger.error("Error convert object to string");
			throw new ErrorsException("Error encrypt data");
		}	
		

		String encryted = EncryptionAESService.encrypt(jsonString);
		logger.info("============= Data To Send ==============");
		logger.info("Json      : " + jsonString);
		logger.info("Encrypted : " + encryted);
		logger.info("=======================================");

		
		ApplinkResponseDto applinkResponseDto = integrationLinkAjaService.generateApplink(encryted);
		if(!applinkResponseDto.getStatus().equals("00")) {
            throw new ErrorsException("Error When Generate Applink");
		}		
		
		ApplinkCustomerRegistrationResponseDto appLinkCustomerRegistrationResponse = new ApplinkCustomerRegistrationResponseDto();
		appLinkCustomerRegistrationResponse.setPartnerData(applinkResponseDto);
		appLinkCustomerRegistrationResponse.setBillAmount(bill.getAmount());
		appLinkCustomerRegistrationResponse.setRegistrationCode(bill.getRegistrationCode());
		appLinkCustomerRegistrationResponse.setBillCode(billSaved.getBillCode());	
		appLinkCustomerRegistrationResponse.setUsedReferralCode(newCustomer.getUsedReferralCode());
		appLinkCustomerRegistrationResponse.setProductName(currentRegistrationType.getRegistrationDesc());
		appLinkCustomerRegistrationResponse.setSumInsured(currentRegistrationType.getMaxUP());
		
		return ResponseEntity.ok(appLinkCustomerRegistrationResponse);    
    }


	public ResponseEntity<List<CustomerDetailPolicyDto>> getDetailCustomer(String registrationCode) {
		if(registrationCode == null || registrationCode == "") {
			throw new ErrorsException("registrationCode is not exist", new ArrayList<String>() {{
			    add("registrationCode is empty");	
			}});			
		}				
		
		
		return customerPolicyService.getAllPolicyByRegistrationCode(registrationCode);		
	}
}
