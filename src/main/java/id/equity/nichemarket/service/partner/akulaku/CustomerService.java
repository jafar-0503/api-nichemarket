package id.equity.nichemarket.service.partner.akulaku;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.mgm.CustomerValidationDto;
import id.equity.nichemarket.dto.mgm.account.AccountCustomerDto;
import id.equity.nichemarket.dto.mgm.app.AppDto;
import id.equity.nichemarket.dto.mgm.bill.BillDto;
import id.equity.nichemarket.dto.mgm.custom.CreateCustomerRegistrationDto;
import id.equity.nichemarket.dto.mgm.customer.CreateCustomer;
import id.equity.nichemarket.dto.mgm.customer.CustomerAgeValidationDto;
import id.equity.nichemarket.dto.mgm.customer.CustomerDetailPolicyDto;
import id.equity.nichemarket.dto.mgm.customer.CustomerDto;
import id.equity.nichemarket.dto.mgm.customer.CustomerWhatsappDto;
import id.equity.nichemarket.dto.mgm.customerRegistration.CustomerRegistrationDto;
import id.equity.nichemarket.dto.mgm.customerRegistration.CustomerRegistrationResponseDto;
import id.equity.nichemarket.dto.mgm.dataFromLandingPage.DataFromLandingPage;
import id.equity.nichemarket.dto.mgm.registrationType.RegistrationTypeDto;
import id.equity.nichemarket.exception.CustomerValidationException;
import id.equity.nichemarket.exception.ErrorsException;
import id.equity.nichemarket.model.mgm.App;
import id.equity.nichemarket.model.mgm.Customer;
import id.equity.nichemarket.model.mgm.Registration;
import id.equity.nichemarket.model.mgm.RegistrationType;
import id.equity.nichemarket.repository.mgm.AccountRepository;
import id.equity.nichemarket.repository.mgm.CustomerRepository;
import id.equity.nichemarket.repository.mgm.RegistrationRepository;
import id.equity.nichemarket.service.mgm.AccountService;
import id.equity.nichemarket.service.mgm.AppService;
import id.equity.nichemarket.service.mgm.BillService;
import id.equity.nichemarket.service.mgm.CustomerRegistrationService;
import id.equity.nichemarket.service.mgm.RegistrationService;
import id.equity.nichemarket.service.mgm.RegistrationTypeService;
import id.equity.nichemarket.service.mgm.SiMedisValidatorCoreService;
import id.equity.nichemarket.service.mgm.SiMedisValidatorService;
import id.equity.nichemarket.service.rm.UserService;
import id.equity.nichemarket.validation.partner.BaseRegistrationValidation;
import id.equity.nichemarket.validation.partner.RegistrationValidationFactory;

@Service("akulakuCustomerService")
public class CustomerService {
	@Autowired
	private RegistrationService registrationService;
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private AccountService accountService;

		
	@Autowired
	private CustomerRegistrationService customerRegistrationService;
	
	@Autowired
	private RegistrationRepository registrationRepository;
	
	@Autowired
	private RegistrationTypeService registrationTypeService;
	
	@Autowired
	private BillService billService;
	
	@Autowired
	private AppService appService;
	
	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private EntityManager em;
	
    private static final Logger logger = LoggerFactory.getLogger(CustomerService.class);
    
    @Transactional
	public ResponseEntity<CustomerRegistrationResponseDto> registerCustomer(CustomerRegistrationDto newCustomer) {
		Type listType = new TypeToken<List<Customer>>() {}.getType();
		List<Customer> customerList = modelMapper.map(newCustomer.getCustomers(), listType);		
		
		//Get Registration Type Code
		RegistrationType registrationType = modelMapper.map(newCustomer, RegistrationType.class);
		
		//Save Bills & Get billDetail by registration type code	
		RegistrationTypeDto currentRegistrationType = registrationTypeService.getRegistrationTypeByCode(registrationType.getRegistrationTypeCode());
		
		//===========================Validate Begin===========================//
		String errValidationMessage = "";
		
		BaseRegistrationValidation validationFactory = RegistrationValidationFactory.getService("akulaku"); 
		List<String> validationResult = validationFactory.validate(customerList, currentRegistrationType);
		
		if(!validationResult.isEmpty()) {
			logger.warn(validationResult.toString());
			errValidationMessage = "Invalid Customer Data";
			throw new CustomerValidationException(errValidationMessage, validationResult);
		}
		//===========================Validate End===========================//
				
		//Get Customer Parent	
		Customer customerParent = customerList.stream()
									    .filter( c -> c.getCustomerStatus().equals("0"))
									    .map(c -> { 
								    			c.setActive(true);
								    			c.setEmailAddress("");
								    			return c;
								    	})
									    .findAny()                            
						                .orElse(null);
		
		if(customerParent == null) {
			throw new ErrorsException("Invalid Customer Data");
		}
		
		//Get App Detail
		AppDto currAppDto = appService.getAppByAppCode(newCustomer.getAppCode());		
		App currApp = modelMapper.map(currAppDto ,App.class);
		
		//Save Account 
		AccountCustomerDto account = new AccountCustomerDto();
		account.setAppCode(newCustomer.getAppCode());
		account.setEmailAddress(customerParent.getEmailAddress().toLowerCase());
		account.setUniqueActivationKey(accountService.generateUniqueKey());
		account.setAccountActivated(false);	
		account.setActive(false);
		AccountCustomerDto accountSaved = accountService.saveAccount(account);
		
		//Save Registration with type code
		Registration registration = new Registration();
		registration.setRegistrationTypeCode(registrationType.getRegistrationTypeCode());
		registration.setAppCode(newCustomer.getAppCode());
		registration.setApp(currApp);	
		registration.setAccountCode(accountSaved.getAccountCode());
		Registration registrationSaved = registrationRepository.save(registration);		
		em.refresh(registrationSaved);					
	
		//Save & Get Unique Code Customer Parent
		Customer parentCustomerSaved = customerRepository.save(customerParent);	
		em.refresh(parentCustomerSaved);
		
		//Save Customer Registration Of Customer Parent
		CreateCustomerRegistrationDto parentCustomerRegistration = new CreateCustomerRegistrationDto();
		parentCustomerRegistration.setRegistrationCode(registrationSaved.getRegistrationCode());
		parentCustomerRegistration.setCustomerCode(parentCustomerSaved.getCustomerCode());
		CreateCustomerRegistrationDto customerRegistrationSaved = customerRegistrationService.addCustomerRegistration(parentCustomerRegistration);		
		
		//Save Bill
		BillDto bill = new BillDto();
		bill.setAmount(currentRegistrationType.getBillAmount());	
		bill.setRegistrationCode(registrationSaved.getRegistrationCode());
		BillDto billSaved = billService.saveBill(bill);
		
		//Create Response Object
		CustomerRegistrationResponseDto response = new CustomerRegistrationResponseDto();
		response.setBillAmount(bill.getAmount());
		response.setRegistrationCode(bill.getRegistrationCode());
		response.setBillCode(billSaved.getBillCode());	
		response.setUsedReferralCode(newCustomer.getUsedReferralCode());
		return ResponseEntity.ok(response);

	}

}
