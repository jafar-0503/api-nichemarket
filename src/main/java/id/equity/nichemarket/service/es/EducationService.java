package id.equity.nichemarket.service.es;

import java.lang.reflect.Type;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.es.Education.CreateEducation;
import id.equity.nichemarket.dto.es.Education.EducationDto;
import id.equity.nichemarket.model.es.Education;
import id.equity.nichemarket.repository.es.EducationRepository;

@Service
public class EducationService {
	
	@Autowired
	private EducationRepository educationRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List All Education
	public ResponseEntity<List<EducationDto>> listEducation() {
		List<Education> listEducations = educationRepository.findAll();
		Type targetType = new TypeToken <List<EducationDto>>() {}.getType();
		List<EducationDto> response = modelMapper.map(listEducations, targetType);

		return ResponseEntity.ok(response);
	}
	
	//List Education By Id
	public ResponseEntity<EducationDto> getEducationById(Long id) {
		Education education = educationRepository.findById(id).orElseThrow(()-> new NotFoundException("Education id " + id + " is not exist"));
		EducationDto response = modelMapper.map(education, EducationDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create Education
	public ResponseEntity<EducationDto> addEducation(CreateEducation newEducation) {
		Education education = modelMapper.map(newEducation, Education.class);
		Education educationSaved = educationRepository.save(education);
		EducationDto response = modelMapper.map(educationSaved, EducationDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit Education
	public ResponseEntity<EducationDto> editEducation(CreateEducation updateEducation, Long id) {
		Education education = educationRepository.findById(id).orElseThrow(()-> new NotFoundException("Education id " + id + " is not exist"));
		
		//manual maps
		education.setEducationCode(updateEducation.getEducationCode());
		education.setDescription(updateEducation.getDescription());
		education.setActive(updateEducation.isActive());
		
		educationRepository.save(education);
		EducationDto response = modelMapper.map(education, EducationDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete Education
	public ResponseEntity<EducationDto> deleteEducation(Long id) {
		Education education = educationRepository.findById(id).orElseThrow(()-> new NotFoundException("Education id " + id + " is not exist"));
		
		educationRepository.deleteById(id);
		EducationDto response = modelMapper.map(education, EducationDto.class);

		return ResponseEntity.ok(response);
	}
}