package id.equity.nichemarket.service.es;

import java.lang.reflect.Type;
import java.util.List;

import id.equity.nichemarket.config.response.BaseResponse;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.es.Religion.CreateReligion;
import id.equity.nichemarket.dto.es.Religion.ReligionDto;
import id.equity.nichemarket.model.es.Religion;
import id.equity.nichemarket.repository.es.ReligionRepository;

@Service
public class ReligionService {
	
	@Autowired
	private ReligionRepository religionRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List Religion
	public ResponseEntity<List<ReligionDto>> listReligion() {
		List<Religion> listReligions = religionRepository.findAll();
		Type targetType = new TypeToken <List<ReligionDto>>() {}.getType();
		List<ReligionDto> response = modelMapper.map(listReligions, targetType);

		return ResponseEntity.ok(response);
	}
	
	//List Religion By Id
	public ResponseEntity<ReligionDto> getReligionById(Long id) {
		Religion religion = religionRepository.findById(id).orElseThrow(()-> new NotFoundException("Religion id " + id + " is not exist"));
		ReligionDto response = modelMapper.map(religion, ReligionDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create Religion
	public ResponseEntity<ReligionDto> addReligion(CreateReligion newReligion) {
		Religion religion = modelMapper.map(newReligion, Religion.class);
		Religion religionSaved = religionRepository.save(religion);
		ReligionDto response = modelMapper.map(religionSaved, ReligionDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Put Religion
	public ResponseEntity<ReligionDto> editReligion(CreateReligion updateReligion, Long id) {
		Religion religion = religionRepository.findById(id).orElseThrow(()-> new NotFoundException("Religion id " + id + " is not exist"));
		
		//manual map
		religion.setReligionCode(updateReligion.getReligionCode());
		religion.setDescription(updateReligion.getDescription());
		religion.setActive(updateReligion.isActive());
		
		religionRepository.save(religion);
		ReligionDto response = modelMapper.map(religion, ReligionDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete Religion
	public ResponseEntity<ReligionDto> deleteReligion(Long id) {
		Religion religion = religionRepository.findById(id).orElseThrow(()-> new NotFoundException("Religion id " + id + " is not exist"));
		religionRepository.deleteById(id);
		ReligionDto response = modelMapper.map(religion, ReligionDto.class);

		return ResponseEntity.ok(response);
	}
}