package id.equity.nichemarket.service.es;

import java.lang.reflect.Type;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.es.DocumentChecklist.CreateDocumentChecklist;
import id.equity.nichemarket.dto.es.DocumentChecklist.DocumentChecklistDto;
import id.equity.nichemarket.model.es.DocumentChecklist;
import id.equity.nichemarket.repository.es.DocumentChecklistRepository;

@Service
public class DocumentChecklistService {
	
	@Autowired
	private DocumentChecklistRepository documentChecklistRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List DocumentChecklist
	public ResponseEntity<List<DocumentChecklistDto>> listDocumentChecklist() {
		List<DocumentChecklist> listDocumentChecklists = documentChecklistRepository.findAll();
		Type targetType = new TypeToken <List<DocumentChecklistDto>>() {}.getType();
		List<DocumentChecklistDto> response = modelMapper.map(listDocumentChecklists, targetType);


		return ResponseEntity.ok(response);
	}
	
	//List DocumentChecklist By Id
	public ResponseEntity<DocumentChecklistDto> getDocumentChecklistById(Long id) {
		DocumentChecklist documentChecklist = documentChecklistRepository.findById(id).orElseThrow(()-> new NotFoundException("Document Checklist id " + id + " is not exist"));
		DocumentChecklistDto response = modelMapper.map(documentChecklist, DocumentChecklistDto.class);


		return ResponseEntity.ok(response);
	}
	
	//Create DocumentChecklist
	public ResponseEntity<DocumentChecklistDto> addDocumentChecklist(CreateDocumentChecklist newDocumentChecklist) {
		DocumentChecklist documentChecklist = modelMapper.map(newDocumentChecklist, DocumentChecklist.class);
		DocumentChecklist documentChecklistSaved = documentChecklistRepository.save(documentChecklist);
		DocumentChecklistDto response = modelMapper.map(documentChecklistSaved, DocumentChecklistDto.class);


		return ResponseEntity.ok(response);
	}
	
	//Edit DocumentChecklist
	public ResponseEntity<DocumentChecklistDto> editDocumentChecklist(CreateDocumentChecklist updateDocumentChecklist, Long id) {
		DocumentChecklist documentChecklist = documentChecklistRepository.findById(id).orElseThrow(()-> new NotFoundException("Document Checklist id " + id + " is not exist"));
		
		//manual map
		documentChecklist.setPaymentMethodCode(updateDocumentChecklist.getPaymentMethodCode());
		documentChecklist.setDescription(updateDocumentChecklist.getDescription());
		documentChecklist.setTmpCode(updateDocumentChecklist.getTmpCode());
		documentChecklist.setActive(updateDocumentChecklist.isActive());
		
		documentChecklistRepository.save(documentChecklist);
		DocumentChecklistDto response = modelMapper.map(documentChecklist, DocumentChecklistDto.class);


		return ResponseEntity.ok(response);
	}
	
	//Delete DocumentChecklist
	public ResponseEntity<DocumentChecklistDto> deleteDocumentChecklist(Long id) {
		DocumentChecklist documentChecklist = documentChecklistRepository.findById(id).orElseThrow(()-> new NotFoundException("Document Checklist id " + id + " is not exist"));
		documentChecklistRepository.deleteById(id);
		DocumentChecklistDto response = modelMapper.map(documentChecklist, DocumentChecklistDto.class);

		
		return ResponseEntity.ok(response);
	}	
}