package id.equity.nichemarket.service.es;

import java.lang.reflect.Type;

import java.util.List;

import id.equity.nichemarket.config.response.BaseResponse;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.es.MensCondition.CreateMensCondition;
import id.equity.nichemarket.dto.es.MensCondition.MensConditionDto;
import id.equity.nichemarket.model.es.MensCondition;
import id.equity.nichemarket.repository.es.MensConditionRepository;

@Service
public class MensConditionService {
	
	@Autowired
	private MensConditionRepository mensConditionRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List All MensCondition
	public ResponseEntity<List<MensConditionDto>> listMensCondition() {
		List<MensCondition> listMensConditions = mensConditionRepository.findAll();
		Type targetType = new TypeToken <List<MensConditionDto>>() {}.getType();
		List<MensConditionDto> response = modelMapper.map(listMensConditions, targetType);

		return ResponseEntity.ok(response);
	}
	
	//List MensCondition By Id
	public ResponseEntity<MensConditionDto> getMensConditionById(Long id) {
		MensCondition mensConditionDto = mensConditionRepository.findById(id).orElseThrow(()-> new NotFoundException("Mens Condition id " + id + " is not exist"));
		MensConditionDto response = modelMapper.map(mensConditionDto, MensConditionDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create MensCondition
	public ResponseEntity<MensConditionDto> addMensCondition(CreateMensCondition newMensCondition) {
		MensCondition mensCondition = modelMapper.map(newMensCondition, MensCondition.class);
		MensCondition mensConditionSaved = mensConditionRepository.save(mensCondition);
		MensConditionDto response = modelMapper.map(mensConditionSaved, MensConditionDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit MensCondition
	public ResponseEntity<MensConditionDto> editMensCondition(CreateMensCondition updateMensCondition, Long id) {
		MensCondition mensCondition = mensConditionRepository.findById(id).orElseThrow(()-> new NotFoundException("Mens Condition id " + id + " is not exist"));
		
		//manual map
		mensCondition.setMensConditionCode(updateMensCondition.getMensConditionCode());
		mensCondition.setDescription(updateMensCondition.getDescription());
		mensCondition.setActive(updateMensCondition.isActive());
		mensConditionRepository.save(mensCondition);
		MensConditionDto response = modelMapper.map(mensCondition, MensConditionDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete MensCondition
	public ResponseEntity<MensConditionDto> deleteMensCondition(Long id) {
		MensCondition mensCondition = mensConditionRepository.findById(id).orElseThrow(()-> new NotFoundException("Mens Condition id " + id + " is not exist"));
		
		mensConditionRepository.deleteById(id);
		MensConditionDto response = modelMapper.map(mensCondition, MensConditionDto.class);

		return ResponseEntity.ok(response);
	}
}