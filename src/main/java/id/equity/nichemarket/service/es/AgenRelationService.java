package id.equity.nichemarket.service.es;

import java.lang.reflect.Type;

import java.util.List;

import id.equity.nichemarket.config.response.BaseResponse;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.es.AgenRelation.AgenRelationDto;
import id.equity.nichemarket.dto.es.AgenRelation.CreateAgenRelation;
import id.equity.nichemarket.model.es.AgenRelation;
import id.equity.nichemarket.repository.es.AgenRelationRepository;

@Service
public class AgenRelationService {
	
	@Autowired
	private AgenRelationRepository agenRelationRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List All AgenRelation
	public ResponseEntity<List<AgenRelationDto>> listAgenRelation() {
		List<AgenRelation> listAgenRelations = agenRelationRepository.findAll();
		Type targetType = new TypeToken <List<AgenRelationDto>>() {}.getType();
		List<AgenRelationDto> response = modelMapper.map(listAgenRelations, targetType);

		return ResponseEntity.ok(response);
	}
	
	//List AgenRelation By Id
	public ResponseEntity<AgenRelationDto> getAgenRelationById(Long id) {
		AgenRelation agenRelation = agenRelationRepository.findById(id).orElseThrow(()-> new NotFoundException("Agen Relation id " + id + " is not exist"));
		AgenRelationDto response = modelMapper.map(agenRelation, AgenRelationDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create AgenRelation
	public ResponseEntity<AgenRelationDto> addAgenRelation(CreateAgenRelation newAgenRelation) {
		AgenRelation agenRelation = modelMapper.map(newAgenRelation, AgenRelation.class);
		AgenRelation agenRelationSaved = agenRelationRepository.save(agenRelation);
		AgenRelationDto response = modelMapper.map(agenRelationSaved, AgenRelationDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit AgenRelation
	public ResponseEntity<AgenRelationDto> editAgenRelation(CreateAgenRelation updateAgenRelation, Long id) {
		AgenRelation agenRelation = agenRelationRepository.findById(id).orElseThrow(()-> new NotFoundException("Agen Relation id " + id + " is not exist"));
		
		//manual map
		agenRelation.setAgenRelationCode(updateAgenRelation.getAgenRelationCode());
		agenRelation.setDescription(updateAgenRelation.getDescription());
		agenRelation.setActive(updateAgenRelation.isActive());
		agenRelationRepository.save(agenRelation);
		AgenRelationDto response = modelMapper.map(agenRelation, AgenRelationDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete AgenRelation
	public ResponseEntity<AgenRelationDto> deleteAgenRelation(Long id) {
		AgenRelation agenRelation = agenRelationRepository.findById(id).orElseThrow(()-> new NotFoundException("Agen Relation id " + id + " is not exist"));
		
		agenRelationRepository.deleteById(id);
		AgenRelationDto response = modelMapper.map(agenRelation, AgenRelationDto.class);

		return ResponseEntity.ok(response);
	}
}