package id.equity.nichemarket.service.es;

import java.lang.reflect.Type;

import java.util.List;

import id.equity.nichemarket.config.response.BaseResponse;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.es.OccupationGroup.CreateOccupationGroup;
import id.equity.nichemarket.dto.es.OccupationGroup.OccupationGroupDto;
import id.equity.nichemarket.model.es.OccupationGroup;
import id.equity.nichemarket.repository.es.OccupationGroupRepository;

@Service
public class OccupationGroupService {
	
	@Autowired
	private OccupationGroupRepository occupationGroupRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List All OccupationGroup
	public ResponseEntity<List<OccupationGroupDto>> listOccupationGroup() {
		List<OccupationGroup> listOccupationGroups = occupationGroupRepository.findAll();
		Type targetType = new TypeToken <List<OccupationGroupDto>>() {}.getType();
		List<OccupationGroupDto> response = modelMapper.map(listOccupationGroups, targetType);

		return ResponseEntity.ok(response);
	}
	
	//List OccupationGroup By Id
	public ResponseEntity<OccupationGroupDto> getOccupationGroupById(Long id) {
		OccupationGroup occupationGroup = occupationGroupRepository.findById(id).orElseThrow(()-> new NotFoundException("Occupation Group id " + id + " is not exist"));
		OccupationGroupDto response = modelMapper.map(occupationGroup, OccupationGroupDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create OccupationGroup
	public ResponseEntity<OccupationGroupDto> addOccupationGroup(CreateOccupationGroup newOccupationGroup) {
		OccupationGroup occupationGroup = modelMapper.map(newOccupationGroup, OccupationGroup.class);
		OccupationGroup occupationGroupSaved = occupationGroupRepository.save(occupationGroup);
		OccupationGroupDto response = modelMapper.map(occupationGroupSaved, OccupationGroupDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit OccupationGroup
	public ResponseEntity<OccupationGroupDto> editOccupationGroup(CreateOccupationGroup updateOccupationGroup, Long id) {
		OccupationGroup occupationGroup = occupationGroupRepository.findById(id).orElseThrow(()-> new NotFoundException("Occupation Group id " + id + " is not exist"));
		
		//manual map
		occupationGroup.setOccupationGroupCode(updateOccupationGroup.getOccupationGroupCode());
		occupationGroup.setDescription(updateOccupationGroup.getDescription());
		occupationGroup.setActive(updateOccupationGroup.isActive());
		
		occupationGroupRepository.save(occupationGroup);
		OccupationGroupDto response = modelMapper.map(occupationGroup, OccupationGroupDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete OccupationGroup
	public ResponseEntity<OccupationGroupDto> deleteOccupationGroup(Long id) {
		OccupationGroup occupationGroup = occupationGroupRepository.findById(id).orElseThrow(()-> new NotFoundException("Occupation Group id " + id + " is not exist"));
		
		occupationGroupRepository.deleteById(id);
		OccupationGroupDto response = modelMapper.map(occupationGroup, OccupationGroupDto.class);

		return ResponseEntity.ok(response);
	}
}