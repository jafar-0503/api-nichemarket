package id.equity.nichemarket.service.es;

import java.lang.reflect.Type;

import java.util.List;

import id.equity.nichemarket.config.response.BaseResponse;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.es.CardIssuer.CardIssuerDto;
import id.equity.nichemarket.dto.es.CardIssuer.CreateCardIssuer;
import id.equity.nichemarket.model.es.CardIssuer;
import id.equity.nichemarket.repository.es.CardIssuerRepository;

@Service
public class CardIssuerService {
	
	@Autowired
	private CardIssuerRepository cardIssuerRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List All CardIssuer
	public ResponseEntity<List<CardIssuerDto>>listCardIssuer() {
		List<CardIssuer> listCardIssuers = cardIssuerRepository.findAll();
		Type targetType = new TypeToken <List<CardIssuerDto>>() {}.getType();
		List<CardIssuerDto> response = modelMapper.map(listCardIssuers, targetType);


		return ResponseEntity.ok(response);
	}
	
	//List CardIssuer By Id
	public ResponseEntity<CardIssuerDto> getCardIssuerById(Long id) {
		CardIssuer CardIssuer = cardIssuerRepository.findById(id).orElseThrow(()-> new NotFoundException("Card Issuer id " + id + " is not exist"));
		CardIssuerDto response = modelMapper.map(CardIssuer, CardIssuerDto.class);


		return ResponseEntity.ok(response);
	}
	
	//Create CardIssuer
	public ResponseEntity<CardIssuerDto> addCardIssuer(CreateCardIssuer newCardIssuer) {
		CardIssuer CardIssuer = modelMapper.map(newCardIssuer, CardIssuer.class);
		CardIssuer CardIssuerSaved = cardIssuerRepository.save(CardIssuer);
		CardIssuerDto response = modelMapper.map(CardIssuerSaved, CardIssuerDto.class);


		return ResponseEntity.ok(response);
	}
	
	//Edit CardIssuer
	public ResponseEntity<CardIssuerDto> editCardIssuer(CreateCardIssuer updateCardIssuer, Long id) {
		CardIssuer cardIssuer = cardIssuerRepository.findById(id).orElseThrow(()-> new NotFoundException("Card Issuer id " + id + " is not exist"));
		
		//manual map
		cardIssuer.setCardIssuerCode(updateCardIssuer.getCardIssuerCode());
		cardIssuer.setDescription(updateCardIssuer.getDescription());
		cardIssuer.setActive(updateCardIssuer.isActive());
		cardIssuerRepository.save(cardIssuer);
		CardIssuerDto response = modelMapper.map(cardIssuer, CardIssuerDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete CardIssuer
	public ResponseEntity<CardIssuerDto> deleteCardIssuer(Long id) {
		CardIssuer cardIssuer = cardIssuerRepository.findById(id).orElseThrow(()-> new NotFoundException("Card Issuer id " + id + " is not exist"));
		
		cardIssuerRepository.deleteById(id);
		CardIssuerDto response = modelMapper.map(cardIssuer, CardIssuerDto.class);

		return ResponseEntity.ok(response);
	}
}