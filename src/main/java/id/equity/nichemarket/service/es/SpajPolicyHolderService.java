package id.equity.nichemarket.service.es;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.SpajPolicyHolder.CreateSpajPolicyHolder;
import id.equity.nichemarket.dto.es.SpajPolicyHolder.SpajPolicyHolderDto;
import id.equity.nichemarket.model.es.SpajDocument;
import id.equity.nichemarket.model.es.SpajPolicyHolder;
import id.equity.nichemarket.repository.es.SpajPolicyHolderRepository;
import id.equity.nichemarket.service.eSubmissionStore.ESubmissionBaseData;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.lang.reflect.Type;
import java.util.List;

@Service
public class SpajPolicyHolderService {
    @Autowired
    private SpajPolicyHolderRepository spajPolicyHolderRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private EntityManager em;

    //Get All Data
    public ResponseEntity<List<SpajPolicyHolderDto>> listSpajPolicyHolder() {
        List<SpajPolicyHolder> SpajPolicyHolder= spajPolicyHolderRepository.findAll();
        Type targetType = new TypeToken<List<SpajPolicyHolderDto>>() {}.getType();
        List<SpajPolicyHolderDto> response = modelMapper.map(SpajPolicyHolder, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<SpajPolicyHolderDto> getSpajPolicyHolderById(Long id) {
        SpajPolicyHolder spajPolicyHolder = spajPolicyHolderRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Policy Holder id " + id + " is not exist"));
        SpajPolicyHolderDto response = modelMapper.map(spajPolicyHolder, SpajPolicyHolderDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<SpajPolicyHolderDto> addSpajPolicyHolder(CreateSpajPolicyHolder newSpajPolicyHolder) {
        SpajPolicyHolder spajPolicyHolder = modelMapper.map(newSpajPolicyHolder, SpajPolicyHolder.class);
        SpajPolicyHolder spajPolicyHolderSaved = spajPolicyHolderRepository.save(spajPolicyHolder);
        SpajPolicyHolderDto response = modelMapper.map(spajPolicyHolderSaved, SpajPolicyHolderDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<SpajPolicyHolderDto> editSpajPolicyHolder(CreateSpajPolicyHolder updateSpajPolicyHolder, Long id) {
        SpajPolicyHolder spajPolicyHolder = spajPolicyHolderRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Policy Holder id " + id + " is not exist"));
        spajPolicyHolder.setSpajPolicyHolderCode(updateSpajPolicyHolder.getSpajPolicyHolderCode());
        spajPolicyHolder.setSpajDocumentCode(updateSpajPolicyHolder.getSpajDocumentCode());
        spajPolicyHolder.setSpajNo(updateSpajPolicyHolder.getSpajNo());
        spajPolicyHolder.setFullName(updateSpajPolicyHolder.getFullName());
        spajPolicyHolder.setPlaceOfBirth(updateSpajPolicyHolder.getPlaceOfBirth());
        spajPolicyHolder.setDateOfBirth(updateSpajPolicyHolder.getDateOfBirth());
        spajPolicyHolder.setIdentityType(updateSpajPolicyHolder.getIdentityType());
        spajPolicyHolder.setIdentityNo(updateSpajPolicyHolder.getIdentityNo());
        spajPolicyHolder.setCitizenship(updateSpajPolicyHolder.getCitizenship());
        spajPolicyHolder.setNpwpNo(updateSpajPolicyHolder.getNpwpNo());
        spajPolicyHolder.setGender(updateSpajPolicyHolder.getGender());
        spajPolicyHolder.setMaritalStatus(updateSpajPolicyHolder.getMaritalStatus());
        spajPolicyHolder.setReligion(updateSpajPolicyHolder.getReligion());
        spajPolicyHolder.setReligionOther(updateSpajPolicyHolder.getReligionOther());
        spajPolicyHolder.setHomeAddress1(updateSpajPolicyHolder.getHomeAddress1());
        spajPolicyHolder.setHomeAddress2(updateSpajPolicyHolder.getHomeAddress2());
        spajPolicyHolder.setHomeAddress3(updateSpajPolicyHolder.getHomeAddress3());
        spajPolicyHolder.setHomeCity(updateSpajPolicyHolder.getHomeCity());
        spajPolicyHolder.setHomeZipcode(updateSpajPolicyHolder.getHomeZipcode());
        spajPolicyHolder.setHomeProvince(updateSpajPolicyHolder.getHomeProvince());
        spajPolicyHolder.setHomeCountry(updateSpajPolicyHolder.getHomeCountry());
        spajPolicyHolder.setHomePhone(updateSpajPolicyHolder.getHomePhone());
        spajPolicyHolder.setHomeHandphone(updateSpajPolicyHolder.getHomeHandphone());
        spajPolicyHolder.setEmailAddress(updateSpajPolicyHolder.getEmailAddress());
        spajPolicyHolder.setLastEducation(updateSpajPolicyHolder.getLastEducation());
        spajPolicyHolder.setOffice(updateSpajPolicyHolder.getOffice());
        spajPolicyHolder.setPosition(updateSpajPolicyHolder.getPosition());
        spajPolicyHolder.setIndustry(updateSpajPolicyHolder.getIndustry());
        spajPolicyHolder.setOfficeAddress1(updateSpajPolicyHolder.getOfficeAddress1());
        spajPolicyHolder.setOfficeAddress2(updateSpajPolicyHolder.getOfficeAddress2());
        spajPolicyHolder.setOfficeAddress3(updateSpajPolicyHolder.getOfficeAddress3());
        spajPolicyHolder.setOfficeCity(updateSpajPolicyHolder.getOfficeCity());
        spajPolicyHolder.setOfficeZipcode(updateSpajPolicyHolder.getOfficeZipcode());
        spajPolicyHolder.setOfficeProvince(updateSpajPolicyHolder.getOfficeProvince());
        spajPolicyHolder.setOfficeCountry(updateSpajPolicyHolder.getOfficeCountry());
        spajPolicyHolder.setOfficePhone(updateSpajPolicyHolder.getOfficePhone());
        spajPolicyHolder.setOfficeHandphone(updateSpajPolicyHolder.getOfficeHandphone());
        spajPolicyHolder.setCorrespondenceAddress(updateSpajPolicyHolder.getCorrespondenceAddress());
        spajPolicyHolder.setOtherCorrespondenceAddress1(updateSpajPolicyHolder.getOtherCorrespondenceAddress1());
        spajPolicyHolder.setOtherCorrespondenceAddress2(updateSpajPolicyHolder.getOtherCorrespondenceAddress2());
        spajPolicyHolder.setOtherCorrespondenceAddress3(updateSpajPolicyHolder.getOtherCorrespondenceAddress3());
        spajPolicyHolder.setOtherCorrespondenceCity(updateSpajPolicyHolder.getOtherCorrespondenceCity());
        spajPolicyHolder.setOtherCorrespondenceZipcode(updateSpajPolicyHolder.getOtherCorrespondenceZipcode());
        spajPolicyHolder.setOtherCorrespondenceProvince(updateSpajPolicyHolder.getOtherCorrespondenceProvince());
        spajPolicyHolder.setOtherCorrespondenceCountry(updateSpajPolicyHolder.getOtherCorrespondenceCountry());
        spajPolicyHolder.setOtherCorrespondencePhone(updateSpajPolicyHolder.getOtherCorrespondencePhone());
        spajPolicyHolder.setOtherCorrespondenceHandphone(updateSpajPolicyHolder.getOtherCorrespondenceHandphone());
        spajPolicyHolder.setRelationship(updateSpajPolicyHolder.getRelationship());
        spajPolicyHolder.setOtherRelationship(updateSpajPolicyHolder.getOtherRelationship());
        spajPolicyHolder.setJobLevel(updateSpajPolicyHolder.getJobLevel());
        spajPolicyHolder.setActive(updateSpajPolicyHolder.isActive());
        spajPolicyHolderRepository.save(spajPolicyHolder);
        em.refresh(spajPolicyHolder);
        SpajPolicyHolderDto response = modelMapper.map(spajPolicyHolder, SpajPolicyHolderDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data By Id
    public ResponseEntity<SpajPolicyHolderDto> deleteSpajPolicyHolder(Long id) {
        SpajPolicyHolder spajPolicyHolder = spajPolicyHolderRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Policy Holder id " + id + " is not exist"));
        spajPolicyHolderRepository.deleteById(id);
        SpajPolicyHolderDto response = modelMapper.map(spajPolicyHolder, SpajPolicyHolderDto.class);

        return ResponseEntity.ok(response);
    }

    //isnert data in file json
    @Transactional
    public void savingSpajPolicyHolder(SpajDocument spajDocument, ESubmissionBaseData eSubmissionBaseData, String spajCode){

        SpajPolicyHolder spajPolicyHolder = modelMapper.map(eSubmissionBaseData, SpajPolicyHolder.class);
        spajPolicyHolder.setFullName(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getNama_lengkap());
        spajPolicyHolder.setPlaceOfBirth(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getTempat_lahir());
        spajPolicyHolder.setDateOfBirth(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getTanggal_lahir());
        spajPolicyHolder.setIdentityType(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getJenis_identitas());
        spajPolicyHolder.setIdentityNo(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getNo_identitas());
        spajPolicyHolder.setCitizenship(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getKewarganegaraan());
        spajPolicyHolder.setNpwpNo(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getNo_npwp());
        spajPolicyHolder.setGender(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getJenis_kelamin());
        spajPolicyHolder.setMaritalStatus(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getStatus_kawin());
        spajPolicyHolder.setReligion(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAgama().getAgama());
        spajPolicyHolder.setReligionOther(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAgama().getLainnya());
        spajPolicyHolder.setHomeAddress1(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAlamat_rumah().getAlamat_rumah_1());
        spajPolicyHolder.setHomeAddress2(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAlamat_rumah().getAlamat_rumah_2());
        spajPolicyHolder.setHomeAddress3(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAlamat_rumah().getAlamat_rumah_3());
        spajPolicyHolder.setHomeCity(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAlamat_rumah().getKota());
        spajPolicyHolder.setHomeZipcode(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAlamat_rumah().getKode_pos());
        spajPolicyHolder.setHomeProvince(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAlamat_rumah().getKode_provinsi());
        spajPolicyHolder.setHomeCountry(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAlamat_rumah().getKode_negara());
        spajPolicyHolder.setHomePhone(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAlamat_rumah().getTelp());
        spajPolicyHolder.setHomeHandphone(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAlamat_rumah().getHp());
        spajPolicyHolder.setEmailAddress(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAlamat_email());
        spajPolicyHolder.setLastEducation(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getPendidikan_formal_terakhir());
        spajPolicyHolder.setOffice(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getPekerjaan());
        spajPolicyHolder.setPosition(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getJabatan());
        spajPolicyHolder.setIndustry(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getIndustri());
        spajPolicyHolder.setOfficeAddress1(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAlamat_pekerjaan().getAlamat_pekerjaan_1());
        spajPolicyHolder.setOfficeAddress2(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAlamat_pekerjaan().getAlamat_pekerjaan_2());
        spajPolicyHolder.setOfficeAddress3(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAlamat_pekerjaan().getAlamat_pekerjaan_3());
        spajPolicyHolder.setOfficeCity(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAlamat_pekerjaan().getKota());
        spajPolicyHolder.setOfficeZipcode(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAlamat_pekerjaan().getKode_pos());
        spajPolicyHolder.setOfficeProvince(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAlamat_pekerjaan().getKode_provinsi());
        spajPolicyHolder.setOfficeCountry(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAlamat_pekerjaan().getKode_negara());
        spajPolicyHolder.setOfficePhone(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAlamat_pekerjaan().getTelp());
        spajPolicyHolder.setOfficeHandphone(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAlamat_pekerjaan().getHp());
        spajPolicyHolder.setCorrespondenceAddress(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAlamat_tagihan_korespondensi().getAlamat_tagihan_korespondensi());
        spajPolicyHolder.setOtherCorrespondenceAddress1(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAlamat_tagihan_korespondensi().getLainnya().getAlamat_lainnya_1());
        spajPolicyHolder.setOtherCorrespondenceAddress2(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAlamat_tagihan_korespondensi().getLainnya().getAlamat_lainnya_2());
        spajPolicyHolder.setOtherCorrespondenceAddress3(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAlamat_tagihan_korespondensi().getLainnya().getAlamat_lainnya_3());
        spajPolicyHolder.setOtherCorrespondenceCity(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAlamat_tagihan_korespondensi().getLainnya().getKota());
        spajPolicyHolder.setOtherCorrespondenceZipcode(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAlamat_tagihan_korespondensi().getLainnya().getKode_pos());
        spajPolicyHolder.setOtherCorrespondenceProvince(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAlamat_tagihan_korespondensi().getLainnya().getKode_provinsi());
        spajPolicyHolder.setOtherCorrespondenceCountry(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAlamat_tagihan_korespondensi().getLainnya().getKode_negara());
        spajPolicyHolder.setOtherCorrespondencePhone(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAlamat_tagihan_korespondensi().getLainnya().getTelp());
        spajPolicyHolder.setOtherCorrespondenceHandphone(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getAlamat_tagihan_korespondensi().getLainnya().getHp());
        spajPolicyHolder.setRelationship(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getHubungan().getHubungan());
        spajPolicyHolder.setOtherRelationship(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getHubungan().getLainnya());
        spajPolicyHolder.setJobLevel(eSubmissionBaseData.getData().getData_calon_pemegang_polis().getKelas_pekerjaan());
        spajPolicyHolder.setActive(true);
        spajPolicyHolder.setSpajDocumentCode(spajDocument.getSpajDocumentCode());
        spajPolicyHolder.setSpajNo(spajCode);
        spajPolicyHolderRepository.save(spajPolicyHolder);
    }
}