package id.equity.nichemarket.service.es;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.es.SpajDocument.CreateSpajDocument;
import id.equity.nichemarket.dto.es.SpajDocument.SpajDocumentDto;
import id.equity.nichemarket.model.es.SpajDocument;
import id.equity.nichemarket.repository.es.SpajDocumentRepository;
import id.equity.nichemarket.service.eSubmissionStore.ESubmissionBaseData;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.lang.reflect.Type;
import java.util.List;

@Service
public class SpajDocumentService {
    @Autowired
    private SpajDocumentRepository spajDocumentRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private EntityManager em;

    //Get All Data
    public ResponseEntity<List<SpajDocumentDto>> listSpajDocument() {
        List<SpajDocument> spajDocument= spajDocumentRepository.findAll();
        Type targetType = new TypeToken<List<SpajDocumentDto>>() {}.getType();
        List<SpajDocumentDto> response = modelMapper.map(spajDocument, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<SpajDocumentDto> getSpajDocumentById(Long id) {
        SpajDocument spajDocument = spajDocumentRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Document id " + id + " is not exist"));
        SpajDocumentDto response = modelMapper.map(spajDocument, SpajDocumentDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<SpajDocumentDto> addSpajDocument(CreateSpajDocument newSpajDocument) {
        SpajDocument spajDocument = modelMapper.map(newSpajDocument, SpajDocument.class);
        SpajDocument SpajDocumentSaved = spajDocumentRepository.save(spajDocument);
        SpajDocumentDto response = modelMapper.map(SpajDocumentSaved, SpajDocumentDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<SpajDocumentDto> editSpajDocument(CreateSpajDocument updateSpajDocument, Long id) {
        SpajDocument spajDocument = spajDocumentRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Document id " + id + " is not exist"));

        spajDocument.setSpajDocumentCode(updateSpajDocument.getSpajDocumentCode());
        spajDocument.setUniqueId(updateSpajDocument.getUniqueId());
        spajDocument.setMode(updateSpajDocument.getMode());
        spajDocument.setSource(updateSpajDocument.getSource());
        spajDocument.setDocumentType(updateSpajDocument.getDocumentType());
        spajDocument.setReference(updateSpajDocument.getReference());
        spajDocument.setSpajNo(updateSpajDocument.getSpajNo());
        spajDocument.setMarketingOffice(updateSpajDocument.getMarketingOffice());
        spajDocument.setPolicyNo(updateSpajDocument.getPolicyNo());
        spajDocument.setProposalNo(updateSpajDocument.getProposalNo());
        spajDocument.setMarketingProgram(updateSpajDocument.getMarketingProgram());
        spajDocument.setSoaCode(updateSpajDocument.getSoaCode());
        spajDocument.setUnderwritingNotes(updateSpajDocument.isUnderwritingNotes());

        SpajDocument documentSaved = spajDocumentRepository.save(spajDocument);
        em.refresh(documentSaved);
        SpajDocumentDto response = modelMapper.map(spajDocument, SpajDocumentDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data By Id
    public ResponseEntity<SpajDocumentDto> deleteSpajDocument(Long id) {
        SpajDocument spajDocument = spajDocumentRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Document id " + id + " is not exist"));
        spajDocumentRepository.deleteById(id);
        SpajDocumentDto response = modelMapper.map(spajDocument, SpajDocumentDto.class);

        return ResponseEntity.ok(response);
    }

    //insert data in file json
    @Transactional
    public void savingSpajDocument(SpajDocument spajDocument, ESubmissionBaseData eSubmissionBaseData, String spajCode, String noProposal){
        //Master Spaj Document
        spajDocument.setDocumentType(eSubmissionBaseData.getData().getTipe_dokumen());
        spajDocument.setUniqueId(eSubmissionBaseData.getUnique_id());
        spajDocument.setMode(eSubmissionBaseData.getMode());
        spajDocument.setSource(eSubmissionBaseData.getKode_sumber());
        spajDocument.setMarketingOffice(eSubmissionBaseData.getData().getKantor_pemasaran());
        spajDocument.setPolicyNo(eSubmissionBaseData.getData().getNo_polis());
        spajDocument.setProposalNo(noProposal);
        spajDocument.setSpajNo(spajCode);
        spajDocument.setMarketingProgram(eSubmissionBaseData.getData().getMarketing_program());
        spajDocument.setSpajNo(spajCode);
        spajDocument.setProposalNo(noProposal);
        spajDocumentRepository.save(spajDocument);
        em.refresh(spajDocument);
    }
}