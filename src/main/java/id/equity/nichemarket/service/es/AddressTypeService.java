package id.equity.nichemarket.service.es;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.AddressType.AddressTypeDto;
import id.equity.nichemarket.dto.es.AddressType.CreateAddressType;
import id.equity.nichemarket.model.es.AddressType;
import id.equity.nichemarket.repository.es.AddressTypeRespository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class AddressTypeService {
    @Autowired 
    private AddressTypeRespository addressTypeRespository;
    
    @Autowired
    private ModelMapper modelMapper;

    //Get All Data
    public ResponseEntity<List<AddressTypeDto>> listAddressType() {
        List<AddressType> addressType= addressTypeRespository.findAll();
        Type targetType = new TypeToken<List<AddressTypeDto>>() {}.getType();
        List<AddressTypeDto> response = modelMapper.map(addressType, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<AddressTypeDto> getAddressTypeById(Long id) {
        AddressType addressType = addressTypeRespository.findById(id).orElseThrow(()-> new NotFoundException("Address Type id " + id + " is not exist"));
        AddressTypeDto response = modelMapper.map(addressType, AddressTypeDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<AddressTypeDto> addAddressType(CreateAddressType newAddressType) {
        AddressType addressType = modelMapper.map(newAddressType, AddressType.class);
        AddressType addressTypeSaved = addressTypeRespository.save(addressType);
        AddressTypeDto response = modelMapper.map(addressTypeSaved, AddressTypeDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<AddressTypeDto> editAddressType(CreateAddressType updateAddressType, Long id) {
        AddressType addressType = addressTypeRespository.findById(id).orElseThrow(()-> new NotFoundException("Address Type id " + id + " is not exist"));
        addressType.setAddressTypeCode(updateAddressType.getAddressTypeCode());
        addressType.setDescription(updateAddressType.getDescription());
        addressType.setActive(updateAddressType.isActive());
        addressTypeRespository.save(addressType);
        AddressTypeDto response = modelMapper.map(addressType, AddressTypeDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data By Id
    public ResponseEntity<AddressTypeDto> deleteAddressType(Long id) {
        AddressType addressType = addressTypeRespository.findById(id).orElseThrow(()-> new NotFoundException("Address Type id " + id + " is not exist"));
        addressTypeRespository.deleteById(id);
        AddressTypeDto response = modelMapper.map(addressType, AddressTypeDto.class);

        return ResponseEntity.ok(response);
    }
}