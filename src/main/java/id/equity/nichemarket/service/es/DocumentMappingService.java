package id.equity.nichemarket.service.es;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.es.DocumentMapping.CreateDocumentMapping;
import id.equity.nichemarket.dto.es.DocumentMapping.DocumentMappingDto;
import id.equity.nichemarket.model.es.DocumentMapping;
import id.equity.nichemarket.repository.es.DocumentMappingRespository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class DocumentMappingService {
	
	@Autowired
	private DocumentMappingRespository documentMappingRespository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List DocumentMapping
	public ResponseEntity<List<DocumentMappingDto>> listDocumentMapping() {
		List<DocumentMapping> listDocumentMappings = documentMappingRespository.findAll();
		Type targetType = new TypeToken <List<DocumentMappingDto>>() {}.getType();
		List<DocumentMappingDto> response = modelMapper.map(listDocumentMappings, targetType);

		return ResponseEntity.ok(response);
	}
	
	//List DocumentMapping By Id
	public ResponseEntity<DocumentMappingDto> getDocumentMappingById(Long id) {
		DocumentMapping documentMapping = documentMappingRespository.findById(id).orElseThrow(()-> new NotFoundException("Document Mapping id " + id + " is not exist"));
		DocumentMappingDto response = modelMapper.map(documentMapping, DocumentMappingDto.class);


		return ResponseEntity.ok(response);
	}
	
	//Create DocumentMapping
	public ResponseEntity<DocumentMappingDto> addDocumentMapping(CreateDocumentMapping newDocumentMapping) {
		DocumentMapping documentMapping = modelMapper.map(newDocumentMapping, DocumentMapping.class);
		DocumentMapping documentMappingSaved = documentMappingRespository.save(documentMapping);
		DocumentMappingDto response = modelMapper.map(documentMappingSaved, DocumentMappingDto.class);


		return ResponseEntity.ok(response);
	}
	
	//Edit DocumentMapping
	public ResponseEntity<DocumentMappingDto> editDocumentMapping(CreateDocumentMapping updateDocumentMapping, Long id) {
		DocumentMapping documentMapping = documentMappingRespository.findById(id).orElseThrow(()-> new NotFoundException("Document Mapping id " + id + " is not exist"));
		
		//manual map
		documentMapping.setDocumentMappingCode(updateDocumentMapping.getDocumentMappingCode());
		documentMapping.setMappingCode(updateDocumentMapping.getMappingCode());
		documentMapping.setDescription(updateDocumentMapping.getDescription());
		documentMapping.setActive(updateDocumentMapping.isActive());
		
		documentMappingRespository.save(documentMapping);
		DocumentMappingDto response = modelMapper.map(documentMapping, DocumentMappingDto.class);


		return ResponseEntity.ok(response);
	}
	
	//Delete DocumentMapping
	public ResponseEntity<DocumentMappingDto> deleteDocumentMapping(Long id) {
		DocumentMapping documentMapping = documentMappingRespository.findById(id).orElseThrow(()-> new NotFoundException("Document Mapping id " + id + " is not exist"));
		documentMappingRespository.deleteById(id);
		DocumentMappingDto response = modelMapper.map(documentMapping, DocumentMappingDto.class);


		return ResponseEntity.ok(response);
	}	
}