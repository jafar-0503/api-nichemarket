package id.equity.nichemarket.service.es;

import java.lang.reflect.Type;
import java.util.List;

import id.equity.nichemarket.config.response.BaseResponse;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.es.Country.CountryDto;
import id.equity.nichemarket.dto.es.Country.CreateCountry;
import id.equity.nichemarket.model.es.Country;
import id.equity.nichemarket.repository.es.CountryRepository;

@Service
public class CountryService {
	
	@Autowired
	private CountryRepository countryRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List All Country
	public ResponseEntity<List<CountryDto>> listCountry() {
		List<Country> listCountrys = countryRepository.findAll();
		Type targetType = new TypeToken <List<CountryDto>>() {}.getType();
		List<CountryDto> response = modelMapper.map(listCountrys, targetType);


		return ResponseEntity.ok(response);
	}
	
	//List Country By Id
	public ResponseEntity<CountryDto> getCountryById(Long id) {
		Country country = countryRepository.findById(id).orElseThrow(()-> new NotFoundException("Country id " + id + " is not exist"));
		CountryDto response = modelMapper.map(country, CountryDto.class);


		return ResponseEntity.ok(response);
	}
	
	//Create Country
	public ResponseEntity<CountryDto> addCountry(CreateCountry newCountry) {
		Country country = modelMapper.map(newCountry, Country.class);
		Country countrySaved = countryRepository.save(country);
		CountryDto response = modelMapper.map(countrySaved, CountryDto.class);


		return ResponseEntity.ok(response);
	}
	
	//Edit Country
	public ResponseEntity<CountryDto> editCountry(CreateCountry updateCountry, Long id) {
		Country country = countryRepository.findById(id).orElseThrow(()-> new NotFoundException("Country id " + id + " is not exist"));
		
		//manual map
		country.setCountryCode(updateCountry.getCountryCode());
		country.setDescription(updateCountry.getDescription());
		country.setActive(updateCountry.isActive());
		countryRepository.save(country);
		CountryDto response = modelMapper.map(country, CountryDto.class);


		return ResponseEntity.ok(response);
	}
	
	//Delete Country
	public ResponseEntity<CountryDto> deleteCountry(Long id) {
		Country country = countryRepository.findById(id).orElseThrow(()-> new NotFoundException("Country id " + id + " is not exist"));
		
		countryRepository.deleteById(id);
		CountryDto response = modelMapper.map(country, CountryDto.class);


		return ResponseEntity.ok(response);
	}
}