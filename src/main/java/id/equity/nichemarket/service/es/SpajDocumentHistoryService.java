package id.equity.nichemarket.service.es;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.es.SpajDocumentHistory.CreateSpajDocumentHistory;
import id.equity.nichemarket.dto.es.SpajDocumentHistory.SpajDocumentHistoryDto;
import id.equity.nichemarket.model.es.SpajDocument;
import id.equity.nichemarket.model.es.SpajDocumentHistory;
import id.equity.nichemarket.repository.es.SpajDocumentHistoryRepository;
import id.equity.nichemarket.service.eSubmissionStore.ESubmissionBaseData;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.lang.reflect.Type;
import java.util.List;

@Service
public class SpajDocumentHistoryService {
    
    @Autowired
    private SpajDocumentHistoryRepository spajDocumentHistoryRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private EntityManager em;

    //Get All Data
    public ResponseEntity<List<SpajDocumentHistoryDto>> listSpajDocumentHistory() {
        List<SpajDocumentHistory> spajDocumentHistory = spajDocumentHistoryRepository.findAll();
        Type targetType = new TypeToken<List<SpajDocumentHistoryDto>>() {}.getType();
        List<SpajDocumentHistoryDto> response = modelMapper.map(spajDocumentHistory, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<SpajDocumentHistoryDto> getSpajDocumentHistoryById(Long id) {
        SpajDocumentHistory spajDocumentHistory = spajDocumentHistoryRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Document History id " + id + " is not exist"));
        SpajDocumentHistoryDto response = modelMapper.map(spajDocumentHistory, SpajDocumentHistoryDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<SpajDocumentHistoryDto> addSpajDocumentHistory(CreateSpajDocumentHistory newSpajDocumentHistory) {
        SpajDocumentHistory spajDocumentHistory = modelMapper.map(newSpajDocumentHistory, SpajDocumentHistory.class);
        SpajDocumentHistory SpajDocumentSaved = spajDocumentHistoryRepository.save(spajDocumentHistory);
        SpajDocumentHistoryDto response = modelMapper.map(SpajDocumentSaved, SpajDocumentHistoryDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<SpajDocumentHistoryDto> editSpajDocumentHistory(CreateSpajDocumentHistory updateSpajDocumentHistory, Long id) {
        SpajDocumentHistory spajDocumentHistory = spajDocumentHistoryRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Document History id " + id + " is not exist"));

        spajDocumentHistory.setSpajDocumentHistoryCode(updateSpajDocumentHistory.getSpajDocumentHistoryCode());
        spajDocumentHistory.setSpajDocumentCode(updateSpajDocumentHistory.getSpajDocumentCode());
        spajDocumentHistory.setStatus(updateSpajDocumentHistory.getStatus());
        spajDocumentHistory.setDescription(updateSpajDocumentHistory.getDescription());
        spajDocumentHistory.setActive(updateSpajDocumentHistory.isActive());

        SpajDocumentHistory documentSaved = spajDocumentHistoryRepository.save(spajDocumentHistory);
        em.refresh(documentSaved);
        SpajDocumentHistoryDto response = modelMapper.map(spajDocumentHistory, SpajDocumentHistoryDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data By Id
    public ResponseEntity<SpajDocumentHistoryDto> deleteSpajDocumentHistory(Long id) {
        SpajDocumentHistory spajDocumentHistory = spajDocumentHistoryRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Document History id " + id + " is not exist"));
        spajDocumentHistoryRepository.deleteById(id);
        SpajDocumentHistoryDto response = modelMapper.map(spajDocumentHistory, SpajDocumentHistoryDto.class);

        return ResponseEntity.ok(response);
    }

    //insert data in file json
    @Transactional
    public void savingSpajDocumentHistory(SpajDocument spajDocument, ESubmissionBaseData eSubmissionBaseData){

        //master spaj Document History
        SpajDocumentHistory spajDocumentHistory = modelMapper.map(eSubmissionBaseData, SpajDocumentHistory.class);
        spajDocumentHistory.setActive(true);
        spajDocumentHistory.setSpajDocumentCode(spajDocument.getSpajDocumentCode());
        spajDocumentHistoryRepository.save(spajDocumentHistory);
    }
}