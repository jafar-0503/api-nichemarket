package id.equity.nichemarket.service.rm;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.rm.UserMenu.CreateUserMenu;
import id.equity.nichemarket.dto.rm.UserMenu.UserMenuDto;
import id.equity.nichemarket.model.rm.UserMenu;
import id.equity.nichemarket.repository.rm.UserMenuRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class UserMenuService {

	@Autowired
	private UserMenuRepository userMenuRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List Channel
	public ResponseEntity<List<UserMenuDto>> listUserMenu(){
		List<UserMenu> lsUserMenu= userMenuRepository.findAll();
		Type targetType = new TypeToken<List<UserMenuDto>>() {}.getType();
		List<UserMenuDto> response = modelMapper.map(lsUserMenu, targetType);

		return ResponseEntity.ok(response);
	}

	//Get Channel By Id
	public ResponseEntity<UserMenuDto> getUserMenuById(Long id) {
		UserMenu userMenu = userMenuRepository.findById(id).orElseThrow(()-> new NotFoundException("User Menu id " + id + "is not exist"));
		UserMenuDto response = modelMapper.map(userMenu, UserMenuDto.class);

		return ResponseEntity.ok(response);
	}

	//Post Channel
	public ResponseEntity<UserMenuDto> addUserMenu(CreateUserMenu newUserMenu) {
		UserMenu userMenu = modelMapper.map(newUserMenu, UserMenu.class);
		UserMenu userChannelSaved = userMenuRepository.save(userMenu);
		UserMenuDto response = modelMapper.map(userChannelSaved, UserMenuDto.class);

		return ResponseEntity.ok(response);
	}

	//Put Channel
	public ResponseEntity<UserMenuDto> editUserMenu(CreateUserMenu updateUserMenu, Long id) {
		UserMenu userMenu = userMenuRepository.findById(id).orElseThrow(()-> new NotFoundException("User Menu id " + id + "is not exist"));
		userMenu.setUserMenuCode(updateUserMenu.getUserMenuCode());
		userMenu.setUsername(updateUserMenu.getUsername());
		userMenu.setMenuCode(updateUserMenu.getMenuCode());
		userMenu.setActive(updateUserMenu.isActive());
		userMenuRepository.save(userMenu);
		UserMenuDto response = modelMapper.map(userMenu, UserMenuDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete Channel	
	public ResponseEntity<UserMenuDto> deleteUserMenu(Long id) {
		UserMenu findChannel = userMenuRepository.findById(id).orElseThrow(()-> new NotFoundException("User Menu id " + id + " is not exist"));
		userMenuRepository.deleteById(id);
		UserMenuDto response = modelMapper.map(findChannel, UserMenuDto.class);

		return ResponseEntity.ok(response);
	}
}