package id.equity.nichemarket.service.rm;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.rm.RoleMenu.CreateRoleMenu;
import id.equity.nichemarket.dto.rm.RoleMenu.RoleMenuDto;
import id.equity.nichemarket.model.rm.RoleMenu;
import id.equity.nichemarket.repository.rm.RoleMenuRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class RoleMenuService {

	@Autowired
	private RoleMenuRepository roleMenuRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List RoleMenu
	public ResponseEntity<List<RoleMenuDto>> listRoleMenu(){
		List<RoleMenu> lsRoleMenu= roleMenuRepository.findAll();
		Type targetType = new TypeToken<List<RoleMenuDto>>() {}.getType();
		List<RoleMenuDto> response = modelMapper.map(lsRoleMenu, targetType);

		return ResponseEntity.ok(response);
	}
	
	//Get RoleMenu By Id
	public ResponseEntity<RoleMenuDto> getRoleMenuById(Long id) {
		RoleMenu roleMenu = roleMenuRepository.findById(id).orElseThrow(()-> new NotFoundException("Role Menu id " + id + "is not exist"));
		RoleMenuDto response = modelMapper.map(roleMenu, RoleMenuDto.class);

		return ResponseEntity.ok(response);
	}

	//Post RoleMenu
	public ResponseEntity<RoleMenuDto> addRoleMenu(CreateRoleMenu newRoleMenu) {
		RoleMenu roleMenu = modelMapper.map(newRoleMenu, RoleMenu.class);
		RoleMenu roleMenuSaved = roleMenuRepository.save(roleMenu);
		RoleMenuDto response = modelMapper.map(roleMenuSaved, RoleMenuDto.class);

		return ResponseEntity.ok(response);
	}

	//Put RoleMenu
	public ResponseEntity<RoleMenuDto> editRoleMenu(CreateRoleMenu updateRoleMenu, Long id) {
		RoleMenu roleMenu = roleMenuRepository.findById(id).orElseThrow(()-> new NotFoundException("Role Menu id " + id + "is not exist"));
		roleMenu.setRoleMenuCode(updateRoleMenu.getRoleMenuCode());
		roleMenu.setRoleCode(updateRoleMenu.getRoleCode());
		roleMenu.setMenuCode(updateRoleMenu.getMenuCode());
		roleMenu.setActive(updateRoleMenu.isActive());
		roleMenuRepository.save(roleMenu);
		RoleMenuDto response = modelMapper.map(roleMenu, RoleMenuDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete RoleMenu	
	public ResponseEntity<RoleMenuDto> deleteRoleMenu(Long id) {
		RoleMenu findRoleMenu = roleMenuRepository.findById(id).orElseThrow(()-> new NotFoundException("Role Menu id " + id + " is not exist"));
		roleMenuRepository.deleteById(id);
		RoleMenuDto response = modelMapper.map(findRoleMenu, RoleMenuDto.class);

		return ResponseEntity.ok(response);
	}
}