package id.equity.nichemarket.service.rm;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.rm.Channel.ChannelDto;
import id.equity.nichemarket.dto.rm.UserChannel.CreateUserChannel;
import id.equity.nichemarket.dto.rm.UserChannel.UserChannelDto;
import id.equity.nichemarket.model.rm.UserChannel;
import id.equity.nichemarket.repository.rm.UserChannelRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class UserChannelService {

	@Autowired
	private UserChannelRepository userChannelRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List Channel
	public ResponseEntity<List<UserChannelDto>> listUserChannel(){
		List<UserChannel> lsUserChannel= userChannelRepository.findAll();
		Type targetType = new TypeToken<List<ChannelDto>>() {}.getType();
		List<UserChannelDto> response = modelMapper.map(lsUserChannel, targetType);

		return ResponseEntity.ok(response);
	}

	//Get Channel By Id
	public ResponseEntity<UserChannelDto> getUserChannelById(Long id) {
		UserChannel userChannel = userChannelRepository.findById(id).orElseThrow(()-> new NotFoundException("User Channel id " + id + "is not exist"));
		UserChannelDto response = modelMapper.map(userChannel, UserChannelDto.class);

		return ResponseEntity.ok(response);
	}

	//Post Channel
	public ResponseEntity<UserChannelDto> addUserChannel(CreateUserChannel newUserChannel) {
		UserChannel userChannel = modelMapper.map(newUserChannel, UserChannel.class);
		UserChannel channelSaved = userChannelRepository.save(userChannel);
		UserChannelDto response = modelMapper.map(channelSaved, UserChannelDto.class);

		return ResponseEntity.ok(response);
	}

	//Put Channel
	public ResponseEntity<UserChannelDto> editUserChannel(CreateUserChannel updateUserChannel, Long id) {
		UserChannel userChannel = userChannelRepository.findById(id).orElseThrow(()-> new NotFoundException("User Channel id " + id + "is not exist"));
		userChannel.setUserChannelCode(updateUserChannel.getUserChannelCode());
		userChannel.setUsername(updateUserChannel.getUsername());
		userChannel.setChannelCode(updateUserChannel.getChannelCode());
		userChannel.setActive(updateUserChannel.isActive());
		userChannelRepository.save(userChannel);
		UserChannelDto response = modelMapper.map(userChannel, UserChannelDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete Channel	
	public ResponseEntity<UserChannelDto> deleteUserChannel(Long id) {
		UserChannel findChannel = userChannelRepository.findById(id).orElseThrow(()-> new NotFoundException("User Channel id " + id + " is not exist"));
		userChannelRepository.deleteById(id);
		UserChannelDto response = modelMapper.map(findChannel, UserChannelDto.class);

		return ResponseEntity.ok(response);
	}
}