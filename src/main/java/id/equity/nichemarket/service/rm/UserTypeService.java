package id.equity.nichemarket.service.rm;

import id.equity.nichemarket.model.rm.UserType;
import id.equity.nichemarket.repository.rm.UserTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserTypeService {
    @Autowired
    UserTypeRepository userTypeRepository;

    public boolean isUserTypeExist(String code) {
        UserType userType = userTypeRepository.findByCode(code);

        if (userType != null) return true;

        return false;
    }
}
