package id.equity.nichemarket.service.rm;

import id.equity.nichemarket.dto.rm.user.CreateUserDto;
import id.equity.nichemarket.model.rm.User;
import id.equity.nichemarket.repository.rm.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ModelMapper modelMapper;

    public User save(CreateUserDto createUserDto) {
        User user = modelMapper.map(createUserDto, User.class);

        if (createUserDto.getPassword() != null) {
            user.setPassword(passwordEncoder.encode(createUserDto.getPassword()));
        }

        return userRepository.save(user);
    }
    
    public boolean isUsernameExist(String email, String userTypeCode) {
    	User user = userRepository.findByUsernameAndTypeCode(email, userTypeCode);
    	if(user != null) {
    		return true;
    	}
    	return false;
    }

    public User save(User user, CreateUserDto createUserDto) {
        modelMapper.map(createUserDto, user);

        return userRepository.save(user);
    }
}