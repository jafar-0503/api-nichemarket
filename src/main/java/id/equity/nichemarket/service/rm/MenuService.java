package id.equity.nichemarket.service.rm;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.rm.Menu.CreateMenu;
import id.equity.nichemarket.dto.rm.Menu.MenuDto;
import id.equity.nichemarket.model.rm.Menu;
import id.equity.nichemarket.repository.rm.MenuRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class MenuService {

	@Autowired
	private MenuRepository menuRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List Menu
	public ResponseEntity<List<MenuDto>> listMenu(){
		List<Menu> lsMenu= menuRepository.findAll();
		Type targetType = new TypeToken<List<MenuDto>>() {}.getType();
		List<MenuDto> response = modelMapper.map(lsMenu, targetType);

		return ResponseEntity.ok(response);
	}

	//Get Menu By Id
	public ResponseEntity<MenuDto> getMenuById(Long id) {
		Menu menu = menuRepository.findById(id).orElseThrow(()-> new NotFoundException("Menu id " + id + "is not exist"));
		MenuDto response = modelMapper.map(menu, MenuDto.class);

		return ResponseEntity.ok(response);
	}

	//Post Menu
	public ResponseEntity<MenuDto> addMenu(CreateMenu newMenu) {
		Menu menu = modelMapper.map(newMenu, Menu.class);
		Menu menuSaved = menuRepository.save(menu);
		MenuDto response = modelMapper.map(menuSaved, MenuDto.class);

		return ResponseEntity.ok(response);
	}

	//Put Menu
	public ResponseEntity<MenuDto> editMenu(CreateMenu updateMenu, Long id) {
		Menu menu = menuRepository.findById(id).orElseThrow(()-> new NotFoundException("Menu id " + id + "is not exist"));
		menu.setMenuCode(updateMenu.getMenuCode());
		menu.setDescription(updateMenu.getDescription());
		menu.setUri(updateMenu.getUri());
		menu.setParentId(updateMenu.getParentId());
		menu.setActive(updateMenu.isActive());
		menuRepository.save(menu);
		MenuDto response = modelMapper.map(menu, MenuDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete Menu	
	public ResponseEntity<MenuDto> deleteMenu(Long id) {
		Menu findMenu = menuRepository.findById(id).orElseThrow(()-> new NotFoundException("Menu id " + id + " is not exist"));
		menuRepository.deleteById(id);
		MenuDto response = modelMapper.map(findMenu, MenuDto.class);

		return ResponseEntity.ok(response);
	}
}