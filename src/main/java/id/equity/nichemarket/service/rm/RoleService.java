package id.equity.nichemarket.service.rm;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.rm.Role.CreateRole;
import id.equity.nichemarket.dto.rm.Role.RoleDto;
import id.equity.nichemarket.model.rm.Role;
import id.equity.nichemarket.repository.rm.RoleRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class RoleService {

	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List Role
	public ResponseEntity<List<RoleDto>> listRole(){
		List<Role> lsRole= roleRepository.findAll();
		Type targetType = new TypeToken<List<RoleDto>>() {}.getType();
		List<RoleDto> response = modelMapper.map(lsRole, targetType);

		return ResponseEntity.ok(response);
	}
	
	//Get Role By Id
	public ResponseEntity<RoleDto> getRoleById(Long id) {
		Role role = roleRepository.findById(id).orElseThrow(()-> new NotFoundException("Role id " + id + "is not exist"));
		RoleDto response = modelMapper.map(role, RoleDto.class);

		return ResponseEntity.ok(response);
	}

	//Post Role
	public ResponseEntity<RoleDto> addRole(CreateRole newRole) {
		Role role = modelMapper.map(newRole, Role.class);
		Role roleSaved = roleRepository.save(role);
		RoleDto response = modelMapper.map(roleSaved, RoleDto.class);

		return ResponseEntity.ok(response);
	}

	//Put Role
	public ResponseEntity<RoleDto> editRole(CreateRole updateRole, Long id) {
		Role role = roleRepository.findById(id).orElseThrow(()-> new NotFoundException("Role id " + id + "is not exist"));
		role.setRoleCode(updateRole.getRoleCode());
		role.setDescription(updateRole.getDescription());
		role.setActive(updateRole.isActive());
		roleRepository.save(role);
		RoleDto response = modelMapper.map(role, RoleDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete Role	
	public ResponseEntity<RoleDto> deleteRole(Long id) {
		Role findRole = roleRepository.findById(id).orElseThrow(()-> new NotFoundException("Role id " + id + " is not exist"));
		roleRepository.deleteById(id);
		RoleDto response = modelMapper.map(findRole, RoleDto.class);

		return ResponseEntity.ok(response);
	}
}