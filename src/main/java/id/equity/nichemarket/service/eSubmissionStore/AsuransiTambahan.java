package id.equity.nichemarket.service.eSubmissionStore;

import lombok.Data;

@Data
public class AsuransiTambahan {
    private String asuransi_tambahan;
    private String uang_pertanggungan;
}
