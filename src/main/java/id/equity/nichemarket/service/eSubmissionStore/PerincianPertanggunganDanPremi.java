package id.equity.nichemarket.service.eSubmissionStore;

import lombok.Data;

@Data
public class PerincianPertanggunganDanPremi {
    private String mata_uang;
    private String periode_pembayaran_premi;
    private String premi;
    private String total_premi;
    private String asuransi_dasar;
    private String uang_pertanggungan;
    private String mpp;
    private String mk;
    private AsuransiTambahan[] asuransi_tambahan;
}
