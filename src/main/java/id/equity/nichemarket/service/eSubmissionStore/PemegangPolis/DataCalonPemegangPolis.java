package id.equity.nichemarket.service.eSubmissionStore.PemegangPolis;

import id.equity.nichemarket.service.eSubmissionStore.Hubungan;
import lombok.Data;

import java.sql.Date;

@Data
public class DataCalonPemegangPolis {
    private String nama_lengkap;
    private Date tanggal_lahir;
    private String tempat_lahir;
    private String jenis_identitas;
    private String no_identitas;
    private String kewarganegaraan;
    private String no_npwp;
    private String jenis_kelamin;
    private String status_kawin;
    private Agama agama;
    private AlamatRumah alamat_rumah;
    private String alamat_email;
    private String pendidikan_formal_terakhir;
    private String pekerjaan;
    private String jabatan;
    private String industri;
    private AlamatPekerjaan alamat_pekerjaan;
    private AlamatTagihanKorespondensi alamat_tagihan_korespondensi;
    private Hubungan hubungan;
    private String kelas_pekerjaan;
}
