package id.equity.nichemarket.service.eSubmissionStore;

import lombok.Data;

@Data
public class Hubungan {
    private String hubungan;
    private String lainnya;
}
