package id.equity.nichemarket.service.eSubmissionStore;

import lombok.Data;

@Data
public class TujuanMembeliAsuransi {
    private String tujuan_membeli_asuransi;
    private String lainnya;
}
