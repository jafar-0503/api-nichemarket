package id.equity.nichemarket.service.eSubmissionStore;

import lombok.Data;

@Data
public class SuratPernyataan {
    private String tempat_pengajuan;
    private String tanggal_pengajuan;
}
