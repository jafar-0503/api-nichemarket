package id.equity.nichemarket.service.eSubmissionStore.CalonTertanggung;

import lombok.Data;

import java.sql.Date;

@Data
public class DataCalonTertanggung {
    private String nama_lengkap;
    private Date tanggal_lahir;
    private String tempat_lahir;
    private String jenis_identitas;
    private String no_identitas;
    private String kewarganegaraan;
    private String no_npwp;
    private String jenis_kelamin;
    private String status_kawin;
    private Agama agama;
    private AlamatRumah alamat_rumah;
    private String alamat_email;
    private String pendidikan_formal_terakhir;
    private String pekerjaan;
    private String jabatan;
    private String industri;
    private AlamatPekerjaan alamat_pekerjaan;
    private AlamatKorespondensi alamat_korespondensi;
    private String kelas_pekerjaan;
}
