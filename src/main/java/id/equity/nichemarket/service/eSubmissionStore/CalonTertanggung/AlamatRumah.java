package id.equity.nichemarket.service.eSubmissionStore.CalonTertanggung;

import lombok.Data;

@Data
public class AlamatRumah {
    private String alamat_rumah_1;
    private String alamat_rumah_2;
    private String alamat_rumah_3;
    private String kota;
    private String kode_pos;
    private String kode_provinsi;
    private String kode_negara;
    private String telp;
    private String hp;
}
