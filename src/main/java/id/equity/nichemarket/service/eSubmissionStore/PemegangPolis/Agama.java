package id.equity.nichemarket.service.eSubmissionStore.PemegangPolis;

import lombok.Data;

@Data
public class Agama {
    private String agama;
    private String lainnya;
}
