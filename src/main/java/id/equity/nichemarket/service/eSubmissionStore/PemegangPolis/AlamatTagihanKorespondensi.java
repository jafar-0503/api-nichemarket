package id.equity.nichemarket.service.eSubmissionStore.PemegangPolis;

import lombok.Data;

@Data
public class AlamatTagihanKorespondensi {
    private String alamat_tagihan_korespondensi;
    private AlamatTagihanKorespondensiLainnya lainnya;
}
