package id.equity.nichemarket.service.eSubmissionStore;

import lombok.Data;

@Data
public class SumberDana {
    private String sumber_dana;
    private String lainnya;
}
