package id.equity.nichemarket.constant.url;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Data;

@Data
public class GlobalConstantLinkaja {
	
	/**	
	 * Credential For Development
	 * 
	 * @author ferry
	 */	 
//	public static final String LINK_AJA_URL = "https://partner-dev.linkaja.com/";
//	public static final String USERNAME_LINKAJA = "equity";
//	public static final String PASSWORD_LINKAJA = "l1nk4j4#3q4!ty";	
//	public static final String SECRET_KEY = "89YJJkKo6yS10$3bQlkabkdMTY37QtgT";	
//	public static String HEADER_TIMESTAMP = "1563246277000000";
//	public static final String BASE_URL_APP_LINK = "https://eli-sit.myequity.id/linkaja/";
	
	
	/**	
	 * Credential For Production
	 * 
	 * @author ferry
	 */	
	public static final String LINK_AJA_URL = "https://partner.linkaja.com/";
	public static final String USERNAME_LINKAJA = "equity";
	public static final String PASSWORD_LINKAJA = "l1nK4j4#3Q@!tY";	
	public static final String SECRET_KEY = "4v9y$HjVbkdM9YJJkKTYEpnPGSgB?Qe3";	
	public static String HEADER_TIMESTAMP = "1563246277000000";
	public static final String BASE_URL_APP_LINK = "https://eli-pro.myequity.id/linkaja/";
	
	/**	
	 * HARDCODE MAPPING REGISTRATION CODE
	 * <ul>
	 * 	<li>"R2010001", "quity_app1"</li>
	 * 	<li>"R2010002", "quity_app3"</li>
	 * </ul>
	 * @author ferry
	 */	 
    public static Map<String, String> MERCHANT_ID_MAPPING; 
    
    public static Map<String, String> ITEMS_NAME_MAPPING; 
  
    // Instantiating the static map 
    static
    { 
    	MERCHANT_ID_MAPPING = new HashMap<>(); 
    	MERCHANT_ID_MAPPING.put("R2010001", "quity_app1"); 
    	MERCHANT_ID_MAPPING.put("R2010002", "quity_app3"); 
    } 
    
    // Instantiating the static map 
    static
    { 
    	ITEMS_NAME_MAPPING = new HashMap<>(); 
    	ITEMS_NAME_MAPPING.put("R2010001", "Masa Asuransi 1 Bulan"); 
    	ITEMS_NAME_MAPPING.put("R2010002", "Masa Asuransi 3 Bulan"); 
    } 
   
}
