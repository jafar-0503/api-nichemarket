package id.equity.nichemarket.aspect;

import id.equity.nichemarket.config.response.BaseResponse;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class BaseResponseEsubAspect<T> {

    @Around("execution(* id.equity.nichemarket.controller.es.*.*(..))")
    public Object resultBaseResponse(ProceedingJoinPoint proceedingJoinPoint){

        Object result = new Object();

        try{
            result = proceedingJoinPoint.proceed();
        } catch (Throwable e) {
            e.printStackTrace();
            return ResponseEntity.ok().body(new BaseResponse<>(false, null, e.getMessage()));
        }

        ResponseEntity responseEntity = (ResponseEntity) result;
        String getName = proceedingJoinPoint.getSignature().getName();
        String messageAction = null;

        if (getName.startsWith("list")){
            getName = getName.substring(4);
            messageAction = "retrieved";
        }
        else if (getName.startsWith("get")) {
            getName = getName.substring(3, getName.length()-4);
            messageAction = "retrieved";
        }
        else if (getName.startsWith("delete")) {
            getName = getName.substring(6);
            messageAction = "deleted";
        }
        else if (getName.startsWith("add")){
            getName = getName.substring(3);
            messageAction = ("added");
        }
        else if (getName.startsWith("edit")){
            getName = getName.substring(4);
            messageAction = ("changed");
        }
        else if (getName.startsWith("patch")){
            getName = getName.substring(5);
            messageAction = ("changed");
        }

        if (result.toString().startsWith("<409")){
            return ResponseEntity.ok().body(new BaseResponse<>(false, null,
                    "Data " + getName + " already exist"));
        }

        return ResponseEntity.ok(new BaseResponse<>(
                true,
                responseEntity.getBody(),
                "Data " + getName + " has been " + messageAction + " successfully")
        );
    }
}