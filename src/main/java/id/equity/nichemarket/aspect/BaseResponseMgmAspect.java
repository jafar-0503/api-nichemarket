package id.equity.nichemarket.aspect;

import java.util.ArrayList;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.config.response.ErrorBaseResponse;
import id.equity.nichemarket.exception.APICoreException;
import id.equity.nichemarket.exception.CustomerRegistrationException;
import id.equity.nichemarket.exception.CustomerValidationException;
import id.equity.nichemarket.exception.ErrorsException;

@Aspect
@Component
public class BaseResponseMgmAspect<T> {
    private static final Logger logger = LoggerFactory.getLogger(BaseResponseMgmAspect.class);

    @Around("execution(* id.equity.nichemarket.controller.mgm.*.*(..)) "
    		+ "|| execution(* id.equity.nichemarket.controller.partner.akulaku.*.*(..)) || execution(* id.equity.nichemarket.controller.partner.linkaja.CustomerController.*(..))")
    public Object resultBaseResponse(ProceedingJoinPoint proceedingJoinPoint) throws Throwable{

        Object result = new Object();

        try{
            result = proceedingJoinPoint.proceed();
        } catch (Throwable e) {

        	e.printStackTrace();
            
            HttpStatus httpStatus = HttpStatus.ACCEPTED;
            if(e instanceof NotFoundException) {
            	httpStatus = HttpStatus.NOT_FOUND;
            }else if(e instanceof NullPointerException || e instanceof APICoreException) {
            	httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            }else if(e instanceof ErrorsException) {
            	httpStatus = HttpStatus.BAD_REQUEST;
            }else if(e instanceof CustomerValidationException || e instanceof CustomerRegistrationException ) {
            	throw e;
            }else if(e instanceof Exception) {
            	httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;            	
            }
            ErrorBaseResponse errorResponse = new ErrorBaseResponse(false, null, e.getMessage(), new ArrayList<>());
            logger.info("================= Response Begin =====================================");
            logger.info("RESPONSE        : {}", errorResponse);     
            logger.info("=================  Response End  =============================");
			logger.error("Error :" ,e);

            return ResponseEntity.status(httpStatus).body(errorResponse);
        }

        ResponseEntity responseEntity = (ResponseEntity) result;
        String getName = proceedingJoinPoint.getSignature().getName();
        String messageAction = "";

        if (getName.startsWith("list")){
            getName = getName.substring(4);
            messageAction = "retrieve ";
        }
        else if (getName.contains("ById")) {
            getName = getName.substring(3, getName.length()-4);
            messageAction = "retrieve ";
        }
        else if (getName.contains("generate")) {
        	getName = "";
//            getName = getName.substring(8, getName.length()-4);
            messageAction = "generate ";
        }
        else if (getName.contains("ByKey")) {
            getName = getName.substring(3, getName.length()-5);
            messageAction = "retrieve ";
        }
        else if (getName.startsWith("delete")) {
            getName = getName.substring(6);
            messageAction = "delete ";
        }
        else if (getName.startsWith("add")){
            getName = getName.substring(3);
            messageAction = ("add ");
        }
        else if (getName.startsWith("edit")){
            getName = getName.substring(4);
            messageAction = ("change ");
        }
        else if (getName.startsWith("patch")){
            getName = getName.substring(5);
            messageAction = ("change ");
        }
        else if (getName.startsWith("send")){
            getName = getName.substring(4).toLowerCase();
            messageAction = ("send ");
        }

        if (result.toString().startsWith("<409")){
            return ResponseEntity.ok().body(new BaseResponse<>(false, null,
                    getName + " already exist"));
        } else if (responseEntity.getBody() == null){
            return ResponseEntity.ok().body(new BaseResponse<>(false, null,
                    "failed " + messageAction + getName));
        }
        

        return ResponseEntity.ok(new BaseResponse<>(
                true,
                responseEntity.getBody(),
                "Success " + messageAction + getName)
        );
    }
}
