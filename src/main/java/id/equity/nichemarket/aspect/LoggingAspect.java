package id.equity.nichemarket.aspect;
import java.util.Arrays;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * Aspect for logging execution of service and repository Spring components.
 * @author Ramesh Fadatare
 *
 */
@Aspect
@Component
public class LoggingAspect {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * Pointcut that matches all repositories, services and Web REST endpoints.
     */
    @Pointcut("within(@org.springframework.stereotype.Repository *)" +
        " || within(@org.springframework.stereotype.Service *)" +
        " || within(@org.springframework.web.bind.annotation.RestController *)")
    public void springBeanPointcut() {
        // Method is empty as this is just a Pointcut, the implementations are in the advices.
    }

    /**
     * Pointcut that matches all Spring beans in the application's main packages.
     */
    @Pointcut("within(id.equity.nichemarket.controller.mgm..*) || within(id.equity.nichemarket.controller.partner..*)")
    public void applicationPackagePointcut() {
        // Method is empty as this is just a Pointcut, the implementations are in the advices.
    }

    /**
     * Advice that logs methods throwing exceptions.
     *
     * @param joinPoint join point for advice
     * @param e exception
     */
    @AfterThrowing(pointcut = "applicationPackagePointcut() && springBeanPointcut()", throwing = "e")
    public void logAfterThrowing(JoinPoint joinPoint, Throwable e) {
        log.error("Exception in {}.{}() with cause = {}", joinPoint.getSignature().getDeclaringTypeName(),
            joinPoint.getSignature().getName(), e.getCause() != null ? e.getCause() : "NULL");
    }

    /**
     * Advice that logs when a method is entered and exited.
     *
     * @param joinPoint join point for advice
     * @return result
     * @throws Throwable throws IllegalArgumentException
     */
//    @Around("applicationPackagePointcut() && springBeanPointcut()")
//    public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
//        if (log.isInfoEnabled()) {
////            log.info("Enter: {}.{}() with argument[s] = {}", joinPoint.getSignature().getDeclaringTypeName(),
////                joinPoint.getSignature().getName(), Arrays.toString(joinPoint.getArgs()));
//            //This is the information to record the http request.
//        	 ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
//             HttpServletRequest request = attributes.getRequest();
//
//            log.info("========================Request Begin======================================");
//     
//            //IP
//            log.info("IP           : {}", request.getRemoteAddr());
//            //URL
//            log.info("URL          : {}", request.getRequestURL());
//            //METHOD
//            log.info("METHOD       : {}", request.getMethod());
//            //CLASS_METHOD
//            log.info("CLASS_METHOD : {}", joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
//            //ARGS
//            log.info("ARGS         : {}", joinPoint.getArgs());
//          //ARGS
////            log.info("JSON         : {}", request.getReader().lines().collect(Collectors.joining()));
//     
//            log.info("==========================Request End====================================");
//
//        }
//        try {
//            Object result = joinPoint.proceed();
//            if (log.isInfoEnabled()) {
////                log.info("Exit: {}.{}() with result = {}", joinPoint.getSignature().getDeclaringTypeName(),
////                    joinPoint.getSignature().getName(), result);
//            	log.info("============================response begin==========================================");
//                log.info("RESPONSE  : {}", result);
//                log.info("===========================response end=================================================");
//            }
//            return result;
//        } catch (IllegalArgumentException e) {
//            log.error("Illegal argument: {} in {}.{}()", Arrays.toString(joinPoint.getArgs()),
//                joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName());
//            throw e;
//        }
//    }
    
    @Before("execution(* id.equity.nichemarket.controller.mgm.*.*(..)) || execution(* id.equity.nichemarket.controller.partner.*.*.*(..))")
    public void logBefore(JoinPoint joinPoint) {
 
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
 
                 //This is the information to record the http request.
      log.info("============================= Request Begin ========================================");

      //IP
      log.info("IP           : {}", request.getRemoteAddr());
      //URL
      log.info("URL          : {}", request.getRequestURL());
      //METHOD
      log.info("METHOD       : {}", request.getMethod());
      //CLASS_METHOD
      log.info("CLASS_METHOD : {}", joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
      //ARGS
      log.info("ARGS         : {}", joinPoint.getArgs());
      //ARGS
      //log.info("JSON         : {}", request.getReader().lines().collect(Collectors.joining()));

      log.info("============================ Request End ======================================");
 
 
    }
    
    @AfterReturning(value = "execution(* id.equity.nichemarket.controller.mgm.*.*(..)) || execution(* id.equity.nichemarket.controller.partner.*.*.*(..))", returning = "object")
    public void logAfterReturn(Object object) {
 
                 //This is the information returned by the record http request.
	    log.info("========================= Response Begin =====================================");
        
        //RESPONSE
        log.info("RESPONSE = {}", object.toString());
 
        log.info("=========================  Response End  ============================="); 
    }
}