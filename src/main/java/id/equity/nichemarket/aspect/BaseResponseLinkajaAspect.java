package id.equity.nichemarket.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.LinkajaBaseResponse;
import id.equity.nichemarket.exception.APICoreException;
import id.equity.nichemarket.exception.ErrorsException;
import id.equity.nichemarket.exception.LinkajaInformPaymentException;

@Aspect
@Component
public class BaseResponseLinkajaAspect<T> {
    private static final Logger logger = LoggerFactory.getLogger(BaseResponseMgmAspect.class);

    @Around("execution(* id.equity.nichemarket.controller.partner.linkaja.PaymentController.*(..))")
    public Object resultBaseResponse(ProceedingJoinPoint proceedingJoinPoint) throws Throwable{

        Object result = new Object();

        try{
            result = proceedingJoinPoint.proceed();
        } catch (Throwable e) {

        	e.printStackTrace();
            
            HttpStatus httpStatus = HttpStatus.OK;
            LinkajaInformPaymentException ex = new LinkajaInformPaymentException();
            if(e instanceof NotFoundException) {
            	httpStatus = HttpStatus.NOT_FOUND;
            	ex.setStatus("05");
            	ex.setMessage("Failed Transaction");
            }else if(e instanceof NullPointerException || e instanceof APICoreException) {
            	httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            	ex.setStatus("05");
            	ex.setMessage("Failed Transaction");
            }else if(e instanceof ErrorsException) {
            	httpStatus = HttpStatus.BAD_REQUEST;
            	ex.setStatus("05");
            	ex.setMessage("Failed Transaction");
            }else if(e instanceof LinkajaInformPaymentException) {
            	ex.setStatus(((LinkajaInformPaymentException) e).getStatus());
            	ex.setMessage(((LinkajaInformPaymentException) e).getMessage());
            	ex.setPartnerTrxID(((LinkajaInformPaymentException) e).getPartnerTrxID());
            	ex.setLinkAjaRefNum(((LinkajaInformPaymentException) e).getLinkAjaRefNum());            	
            }else if(e instanceof Exception) {
            	httpStatus = HttpStatus.INTERNAL_SERVER_ERROR; 
            	ex.setStatus("05");
            	ex.setMessage("Failed Transaction");
            }
            
            LinkajaBaseResponse errorResponse = new LinkajaBaseResponse(ex.getStatus(), ex.getMessage(), ex.getPartnerTrxID(), ex.getLinkAjaRefNum());
            logger.info("================= Response Begin =====================================");
            logger.info("RESPONSE        : {}", errorResponse);     
            logger.info("=================  Response End  =============================");
			logger.error("Error :" ,e);

            return ResponseEntity.status(httpStatus).body(errorResponse);
        }

        ResponseEntity responseEntity = (ResponseEntity) result;

        if (responseEntity.getBody() == null){
            return ResponseEntity.ok().body(new LinkajaBaseResponse("05", "Failed", "", ""));
        }       
      
        return ResponseEntity.ok(responseEntity.getBody());
    }
}
