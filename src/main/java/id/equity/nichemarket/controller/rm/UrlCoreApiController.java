package id.equity.nichemarket.controller.rm;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.retrofit.mgm.EmailNotificationService;
import id.equity.nichemarket.retrofit.mgm.freeCovid.FreeCovidService;

@RestController
@RequestMapping("api/v1/url")
public class UrlCoreApiController {
	
	 @Autowired
	 private FreeCovidService freeCovidService;
	 
	 @Autowired
	 private EmailNotificationService emailNotificationService;	
	 
	 @GetMapping
     public ResponseEntity<Object> listUrlApiCore() {
		 System.out.println("========="+TimeZone.getDefault());
		//method 1
	        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	        System.out.println(timestamp);

	        //method 2 - via Date
	        Date date = new Date();
	        System.out.println(new Timestamp(date.getTime()));

	 	String coreApiUrlGrp = freeCovidService.getCoreApiUrl();
	 	String coreApiUrlNotification = emailNotificationService.getCoreApiUrl();
	    Map<String, String> urlApiCore = new HashMap<String, String>();
	    urlApiCore.put("grp", coreApiUrlGrp);
	    urlApiCore.put("notification", coreApiUrlNotification);
        return ResponseEntity.ok(urlApiCore);
     }
}
