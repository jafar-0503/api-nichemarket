package id.equity.nichemarket.controller.rm;

import id.equity.nichemarket.config.error.ResourceNotFoundException;
import id.equity.nichemarket.dto.rm.user.CreateUserDto;
import id.equity.nichemarket.dto.rm.user.UserDto;
import id.equity.nichemarket.model.rm.User;
import id.equity.nichemarket.repository.rm.UserRepository;
import id.equity.nichemarket.service.rm.UserService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.lang.reflect.Type;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("api/v1/users")
public class UserController {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private UserService userService;

    @GetMapping
    public ResponseEntity<List<UserDto>> listUser() {
        Iterable<User> users = userRepository.findAll();
        Type targetListType = new TypeToken<List<UserDto>>() {}.getType();
        List<UserDto> userDtos = modelMapper.map(users, targetListType);

        return ResponseEntity.ok(userDtos);
    }

    @PostMapping
    public ResponseEntity<UserDto> addUser(
//            @Valid
            @RequestBody
                    CreateUserDto createUserDto
    ) {
        try {
            User existUsername = userRepository.findByUsername(createUserDto.getUsername());
            User existEmail = null;
            try{
                existEmail = userRepository.findByEmail(createUserDto.getEmail());
            } catch (NullPointerException e){
            	e.printStackTrace();
            }
            if (existUsername.getUsername().equals(createUserDto.getUsername())) {
                return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
            } else if (existEmail.getEmail().equals(createUserDto.getEmail())) {
                return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        User user = userService.save(createUserDto);
        UserDto userDto = modelMapper.map(user, UserDto.class);

        return ResponseEntity.status(HttpStatus.CREATED).body(userDto);
    }

    @GetMapping("{id}")
    public ResponseEntity<UserDto> getUserById(@PathVariable Long id) throws ResourceNotFoundException {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        UserDto userDto = modelMapper.map(user, UserDto.class);

        return ResponseEntity.ok(userDto);
    }

    @PatchMapping("{id}")
    public ResponseEntity<UserDto> patchUser(
            @PathVariable
            Long id,
            @Valid
            @RequestBody
            CreateUserDto createUserDto
    ) throws ResourceNotFoundException {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        User updatedUser = userService.save(user, createUserDto);
        UserDto updatedUserDto = modelMapper.map(updatedUser, UserDto.class);

        return ResponseEntity.ok(updatedUserDto);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<UserDto> deleteUser(@PathVariable Long id) throws ResourceNotFoundException {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        UserDto userDto = modelMapper.map(user, UserDto.class);

        userRepository.delete(user);

        return ResponseEntity.ok(userDto);
    }
}