package id.equity.nichemarket.controller.rm;

import id.equity.nichemarket.dto.rm.RoleMenu.CreateRoleMenu;
import id.equity.nichemarket.dto.rm.RoleMenu.RoleMenuDto;
import id.equity.nichemarket.service.rm.RoleMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/role-menus")
public class RoleMenuController {

	@Autowired
	private RoleMenuService roleMenuService;
	
	//GET ALL DATA
	@GetMapping
	public ResponseEntity<List<RoleMenuDto>> listRoleMenu(){
		return roleMenuService.listRoleMenu();
	}

	//GET DATA BY ID
	@GetMapping("{id}")
	public ResponseEntity<RoleMenuDto> getRoleMenuById(@PathVariable Long id) {
		return roleMenuService.getRoleMenuById(id);
	}
	
	//POST DATA
	@PostMapping
	public ResponseEntity<RoleMenuDto> addRoleMenu(@RequestBody CreateRoleMenu newRoleMenu) {
		return roleMenuService.addRoleMenu(newRoleMenu);
	}
	
	//PUT DATA
	@PutMapping("{id}")
	public ResponseEntity<RoleMenuDto> editRoleMenu(@RequestBody CreateRoleMenu updateRoleMenu, @PathVariable Long id) {
		return roleMenuService.editRoleMenu(updateRoleMenu, id);
	}
	
	//DETELE DATA
	@DeleteMapping("{id}")
	public ResponseEntity<RoleMenuDto> deleteRoleMenu(@PathVariable Long id) {
		return roleMenuService.deleteRoleMenu(id);
	}	
}