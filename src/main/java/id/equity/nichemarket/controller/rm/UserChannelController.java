package id.equity.nichemarket.controller.rm;

import id.equity.nichemarket.dto.rm.UserChannel.CreateUserChannel;
import id.equity.nichemarket.dto.rm.UserChannel.UserChannelDto;
import id.equity.nichemarket.service.rm.UserChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/user-channels")
public class UserChannelController {

	@Autowired
	private UserChannelService userChannelService;
	
	//GET ALL DATA
	@GetMapping
	public ResponseEntity<List<UserChannelDto>> listUserChannel(){
		return userChannelService.listUserChannel();
	}

	//GET DATA BY ID
	@GetMapping("{id}")
	public ResponseEntity<UserChannelDto> getUserChannelById(@PathVariable Long id) {
		return userChannelService.getUserChannelById(id);
	}
	
	//POST DATA
	@PostMapping
	public ResponseEntity<UserChannelDto> addUserChannel(@RequestBody CreateUserChannel newUserChannel) {
		return userChannelService.addUserChannel(newUserChannel);
	}
	
	//PUT DATA
	@PutMapping("{id}")
	public ResponseEntity<UserChannelDto> editUserChannel(@RequestBody CreateUserChannel updateUserChannel, @PathVariable Long id) {
		return userChannelService.editUserChannel(updateUserChannel, id);
	}
	
	//DETELE DATA
	@DeleteMapping("{id}")
	public ResponseEntity<UserChannelDto> deleteUserChannel(@PathVariable Long id) {
		return userChannelService.deleteUserChannel(id);
	}	
}