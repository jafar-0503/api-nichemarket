package id.equity.nichemarket.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.config.response.NewBaseResponse;
import id.equity.nichemarket.dto.es.Proposal.CreateProposal;
import id.equity.nichemarket.dto.es.Proposal.ProposalDto;
import id.equity.nichemarket.dto.es.Spaj.CreateSpaj;
import id.equity.nichemarket.dto.es.Spaj.SpajDto;
import id.equity.nichemarket.dto.fileUpload.UploadFileResponseDto;
import id.equity.nichemarket.model.es.SpajDocument;
import id.equity.nichemarket.model.es.SpajDocumentFile;
import id.equity.nichemarket.repository.es.SpajDocumentFileRepository;
import id.equity.nichemarket.repository.es.SpajDocumentRepository;
import id.equity.nichemarket.service.FileService;
import id.equity.nichemarket.service.eSubmissionStore.ESubmissionBaseData;
import id.equity.nichemarket.service.eSubmissionStore.ESubmissionDataService;
import id.equity.nichemarket.service.eSubmissionStore.documentData.BaseDocumentData;
import id.equity.nichemarket.service.eSubmissionStore.documentData.DocumentDataService;
import id.equity.nichemarket.service.es.ProposalService;
import id.equity.nichemarket.service.es.SpajService;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@RestController
@RequestMapping("api/v1/uploads")
public class FileController {
    private static final Logger logger = LoggerFactory.getLogger(FileController.class);

    @Autowired
    private ESubmissionDataService eSubmissionDataService;
    @Autowired
    private DocumentDataService documentDataService;
    @Autowired
    private SpajService spajService;
    @Autowired
    private ProposalService proposalService;
    @Autowired
    private FileService fileService;
    @Autowired
    private SpajDocumentFileRepository spajDocumentFileRepository;
    @Autowired
    private SpajDocumentRepository spajDocumentRepository;

    @Value("${file.upload-dir:}")
    private String fileUploadDir;

    int successCode = HttpStatus.OK.value();
    int failedCode = HttpStatus.BAD_REQUEST.value();

    private File updateJsonFile(File f, ESubmissionBaseData eSubmissionBaseData, String noSpaj, String noProposal)
            throws IOException {
        eSubmissionBaseData.getData().setNo_spaj(noSpaj);
        eSubmissionBaseData.getData().setNo_proposal(noProposal);

        JSONObject updateNasabahData = new JSONObject(eSubmissionBaseData);
        updateNasabahData.remove("unique_id");
        updateNasabahData.remove("mode");
        updateNasabahData.remove("kode_sumber");

//        Gson gson = new GsonBuilder().setPrettyPrinting().create();
//        String saveJson = gson.toJson(updateNasabahData);

        FileUtils.writeStringToFile(f, String.valueOf(updateNasabahData));
        return f;
    }

    private File multipartToFile(MultipartFile multipart, String fileName) throws IllegalStateException, IOException {
        File convFile = new File(System.getProperty("java.io.tmpdir") + "/" + fileName);
        multipart.transferTo(convFile);

        return convFile;
    }

    //Function for store file and response per file
    private synchronized BaseResponse<UploadFileResponseDto> uploadEachFile(MultipartFile file) {
        UploadFileResponseDto uploadFileResponseDto = new UploadFileResponseDto(
                file.getOriginalFilename(),
                file.getContentType(),
                file.getSize()
        );
        try {
            ObjectMapper mapping = new ObjectMapper();
            File f = multipartToFile(file, file.getOriginalFilename());

            if(file.getContentType().equals("application/json")) {
                //Parsing json file
                BaseDocumentData document = mapping.readValue(f, BaseDocumentData.class);
                documentDataService.save(document);
            }

            return new BaseResponse<>(true, uploadFileResponseDto, "Files uploaded successfully");
        } catch (IOException err) {
            err.printStackTrace();
            return new BaseResponse<>(true, uploadFileResponseDto,"Failed to upload!");
        }
    }

    //for single file
    @PostMapping("document")
    public BaseResponse<UploadFileResponseDto> uploadMultipleFile(
            @RequestParam(name = "file") MultipartFile file,
            @RequestParam(name = "no_spaj") String noSpaj,
            @RequestParam(name = "dms_code") String dmsCode,
            @RequestParam(name = "order") Integer order,
            @RequestParam(name = "total") Integer total
    ) throws Exception {
        if (file.isEmpty()){
                return new BaseResponse<>(true, new UploadFileResponseDto(
                        null,null,null),
                        "File is empty. Please check your last sended file");
            }
        String pdfMimeType = "application/pdf", jpgMimeType = "image/jpeg", pngMimeType = "image/png";
        if (!file.getContentType().equals(pdfMimeType)
                && !file.getContentType().equals(jpgMimeType)
                && (!file.getContentType().equals(pngMimeType))) {
            return new BaseResponse<>(true, new UploadFileResponseDto(
                    file.getOriginalFilename(),
                    file.getContentType(),
                    file.getSize()
            ), "Failed. Only pdf jpg, and png are allowed");
        }

        String documentName = "";
        SpajDocumentFile existData = spajDocumentFileRepository.findByNoSpajDmsCode(noSpaj, dmsCode);
        SpajDocument spajDocument = spajDocumentRepository.findBySpajNo(noSpaj);
        if (spajDocument == null){
            throw new Exception("Spaj Document is not exist. Please input the correct SPAJ Number");
        }

        try {
            byte[] bytes = file.getBytes();

            if (file.getContentType().equals(pdfMimeType)) {
                documentName = noSpaj + "_" + dmsCode + ".pdf";
            } else if (file.getContentType().equals(jpgMimeType)) {
                documentName = noSpaj + "_" + dmsCode + ".jpg";
            } else {
                documentName = noSpaj + "_" + dmsCode + ".png";
            }

            File checkDir = new File(this.fileUploadDir + noSpaj);
            if (!checkDir.exists()) {
                checkDir.mkdir();
            }
            String insPath = checkDir + "/" + documentName;
            Files.write(Paths.get(insPath), bytes).toString();
        } catch (IOException err) {
            err.printStackTrace();
        }

        if (existData != null) {
            existData.setDocumentName(documentName);
            spajDocumentFileRepository.save(existData);
        } else {
            SpajDocumentFile spajDocumentFile = new SpajDocumentFile();
            spajDocumentFile.setFile(documentName);
            spajDocumentFile.setDmsCode(dmsCode);
            spajDocumentFile.setDocumentName(documentName);
            spajDocumentFile.setSpajDocumentCode(spajDocument.getSpajDocumentCode());
            spajDocumentFile.setSpajNo(noSpaj);
            spajDocumentFile.setActive(true);
            spajDocumentFileRepository.save(spajDocumentFile);
        }

        Integer totalSpajFile = spajDocumentFileRepository.findCountByNoSpaj(noSpaj);
        if (totalSpajFile == total) {
            //to save data to local directory
            fileService.uploadToSFTP(noSpaj);
        }
        UploadFileResponseDto uploadFileResponseDto = new UploadFileResponseDto(
                documentName,
                file.getContentType(),
                file.getSize()
        );
        return new BaseResponse<>(true, uploadFileResponseDto, "Success Created");
    }

    //Get spaj_no and proposal_no
    @PostMapping()
    public BaseResponse<NewBaseResponse> uploadJsonData(@RequestParam(name = "files") MultipartFile files){
        try {
        if (files.isEmpty()){
                return new BaseResponse<NewBaseResponse>(false, new NewBaseResponse(400, null, null), "File is empty" );
        }
        if(!files.getContentType().equals("application/json")) {
            return new BaseResponse<NewBaseResponse>(false, new NewBaseResponse(400, null, null), "Failed! Only json file are allowed" );
        }
            //Trigger no_spaj
            CreateSpaj spaj = new CreateSpaj( true);
            ResponseEntity <SpajDto> spajSaved = spajService.addSpaj(spaj);
            String noSpaj = spajSaved.getBody().getSpajCode();

            //Trigger no_proposal
            CreateProposal proposal = new CreateProposal(true);
            ResponseEntity <ProposalDto> proposalSaved = proposalService.addProposal(proposal);
            String noProposal = proposalSaved.getBody().getProposalNo();

            ObjectMapper mapping = new ObjectMapper();
            File f = multipartToFile(files, files.getOriginalFilename());
            ESubmissionBaseData eSubmissionBaseData = mapping.readValue(f, ESubmissionBaseData.class);
            eSubmissionDataService.save(eSubmissionBaseData, noSpaj, noProposal);
            //Upadate Json File
            updateJsonFile(f, eSubmissionBaseData, noSpaj, noProposal);

            FileInputStream input = new FileInputStream(f);
            MultipartFile newFile = new MockMultipartFile(files.getOriginalFilename(),
                    f.getName(), "application/json", IOUtils.toByteArray(input));

            byte[] bytes = newFile.getBytes();
            String updatedName = "NASABAH_" + noSpaj + ".json";
            File checkDir = new File(this.fileUploadDir + noSpaj);
            if(!checkDir.exists()){
                checkDir.mkdir();
            }
            String insPath = checkDir + "/" + updatedName;
            Files.write(Paths.get(insPath), bytes).toString();

            return new BaseResponse<NewBaseResponse>(true, new NewBaseResponse(200, noSpaj, noProposal), "Data successfully saved" );
        } catch (IOException err){
            err.printStackTrace();
            String errorMessage = err.getMessage();
            String errorMessage1 = errorMessage.substring(errorMessage.indexOf("Unrecognized"));
            errorMessage1 = errorMessage1.substring(0,errorMessage1.indexOf("("));
            String errorMessage2 = errorMessage.substring(errorMessage.indexOf("at [Source: (File);"));
            errorMessage2 = errorMessage2.substring(0,errorMessage2.indexOf("]"));
            if (errorMessage1.startsWith("Unrecognized")){
                errorMessage = errorMessage1 + " " + errorMessage2;
            }
            return new BaseResponse<NewBaseResponse>(false, new NewBaseResponse(400, null, null), "Failed! " + errorMessage );
        }
    }
}