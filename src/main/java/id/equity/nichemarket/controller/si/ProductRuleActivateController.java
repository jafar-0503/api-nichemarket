package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.ProductRuleActivate.CreateProductRuleActivate;
import id.equity.nichemarket.dto.si.ProductRuleActivate.ProductRuleActivateDto;
import id.equity.nichemarket.service.si.ProductRuleActivateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/product-rule-activaties")
public class ProductRuleActivateController {
	
	@Autowired
	private ProductRuleActivateService productRuleActivateService;
	
	//get All ProductRuleActivate
	@GetMapping
	public ResponseEntity<List<ProductRuleActivateDto>> listProductRuleActivate(){
		return productRuleActivateService.listProductRuleActivate();
	}
	
	//Get ProductRuleActivate By Id
	@GetMapping("{id}")
	public ResponseEntity<ProductRuleActivateDto> getProductRuleActivateById(@PathVariable Long id) {
		return productRuleActivateService.getProductRuleActivateById(id);
	}
	
	//Post ProductRuleActivate
	@PostMapping
	public ResponseEntity<ProductRuleActivateDto> addProductRuleActivate(@RequestBody CreateProductRuleActivate id) {
		return productRuleActivateService.addProductRuleActivate(id);
	}
	
	//Edit ProductRuleActivate
	@PutMapping("{id}")
	public ResponseEntity<ProductRuleActivateDto> editProductRuleActivate(@RequestBody CreateProductRuleActivate updateProductRuleActivate, @PathVariable Long id) {
		return productRuleActivateService.editProductRuleActivate(updateProductRuleActivate, id);
	}
	
	//Delete ProductRuleActivate
	@DeleteMapping("{id}")
	public ResponseEntity<ProductRuleActivateDto> deleteProductRuleActivate(@PathVariable Long id) {
		return productRuleActivateService.deleteProductRuleActivate(id);
	}
}