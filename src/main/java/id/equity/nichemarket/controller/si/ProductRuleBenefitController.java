package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.dto.si.ProductRuleBenefit.CreateProductRuleBenefit;
import id.equity.nichemarket.dto.si.ProductRuleBenefit.ProductRuleBenefitDto;
import id.equity.nichemarket.service.si.ProductRuleBenefitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/product-rule-benefits")
public class ProductRuleBenefitController {
	
	@Autowired
	private ProductRuleBenefitService productRuleBenefitService;
	
	//get All ProductRuleBenefit
	@GetMapping
	public ResponseEntity<List<ProductRuleBenefitDto>> listProductRuleBenefit(){
		return productRuleBenefitService.listProductRuleBenefit();
	}
	
	//Get ProductRuleBenefit By Id
	@GetMapping("{id}")
	public ResponseEntity<ProductRuleBenefitDto> getProductRuleBenefitById(@PathVariable Long id) {
		return productRuleBenefitService.getProductRuleBenefitById(id);
	}
	
	//Post ProductRuleBenefit
	@PostMapping
	public ResponseEntity<ProductRuleBenefitDto> addProductRuleBenefit(@RequestBody CreateProductRuleBenefit id) {
		return productRuleBenefitService.addProductRuleBenefit(id);
	}
	
	//Edit ProductRuleBenefit
	@PutMapping("{id}")
	public ResponseEntity<ProductRuleBenefitDto> editProductRuleBenefit(@RequestBody CreateProductRuleBenefit updateProductRuleBenefit, @PathVariable Long id) {
		return productRuleBenefitService.editProductRuleBenefit(updateProductRuleBenefit, id);
	}
	
	//Delete ProductRuleBenefit
	@DeleteMapping("{id}")
	public ResponseEntity<ProductRuleBenefitDto> deleteProductRuleBenefit(@PathVariable Long id) {
		return productRuleBenefitService.deleteProductRuleBenefit(id);
	}
}