package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.dto.si.ProductRuleBenefitItemRate.CreateProductRuleBenefitItemRate;
import id.equity.nichemarket.dto.si.ProductRuleBenefitItemRate.ProductRuleBenefitItemRateDto;
import id.equity.nichemarket.service.si.ProductRuleBenefitItemRateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/product-rule-benefit-item-rates")
public class ProductRuleBenefitItemRateController {

	@Autowired
	private ProductRuleBenefitItemRateService productRuleBenefitItemRateService;

	//get All ProductRuleBenefitItemRate
	@GetMapping
	public ResponseEntity<List<ProductRuleBenefitItemRateDto>> listProductRuleBenefitItemRate(){
		return productRuleBenefitItemRateService.listProductRuleBenefitItemRate();
	}

	//Get ProductRuleBenefit By Id
	@GetMapping("{id}")
	public ResponseEntity<ProductRuleBenefitItemRateDto> getProductRuleBenefitItemRateById(@PathVariable Long id) {
		return productRuleBenefitItemRateService.getProductRuleBenefitItemRateById(id);
	}

	//Post ProductRuleBenefit
	@PostMapping
	public ResponseEntity<ProductRuleBenefitItemRateDto> addProductRuleBenefitItemRate(@RequestBody CreateProductRuleBenefitItemRate id) {
		return productRuleBenefitItemRateService.addProductRuleBenefitItemRate(id);
	}

	//Edit ProductRuleBenefit
	@PutMapping("{id}")
	public ResponseEntity<ProductRuleBenefitItemRateDto> editProductRuleBenefitItemRate(@RequestBody CreateProductRuleBenefitItemRate updateProductRuleBenefitItemRate, @PathVariable Long id) {
		return productRuleBenefitItemRateService.editProductRuleBenefitItemRate(updateProductRuleBenefitItemRate, id);
	}

	//Delete ProductRuleBenefit
	@DeleteMapping("{id}")
	public ResponseEntity<ProductRuleBenefitItemRateDto> deleteProductRuleBenefitItemRate(@PathVariable Long id) {
		return productRuleBenefitItemRateService.deleteProductRuleBenefitItemRate(id);
	}
}