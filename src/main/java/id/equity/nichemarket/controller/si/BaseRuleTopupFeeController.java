package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.BaseRuleTopupFee.BaseRuleTopupFeeDto;
import id.equity.nichemarket.dto.si.BaseRuleTopupFee.CreateBaseRuleTopupFee;
import id.equity.nichemarket.service.si.BaseRuleTopupFeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/base-rule-topup-fees")
public class BaseRuleTopupFeeController {
    @Autowired
    private BaseRuleTopupFeeService baseRuleTopupFeeService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<BaseRuleTopupFeeDto>> listBaseRuleTopupFee(){
        return baseRuleTopupFeeService.listBaseRuleTopupFee();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<BaseRuleTopupFeeDto> getBaseRuleTopupFeeById(@PathVariable Long id) {
        return baseRuleTopupFeeService.getBaseRuleTopupFeeById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<BaseRuleTopupFeeDto> addBaseRuleTopupFee(@RequestBody CreateBaseRuleTopupFee newBaseRuleTopupFee) {
        return baseRuleTopupFeeService.addBaseRuleTopupFee(newBaseRuleTopupFee);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<BaseRuleTopupFeeDto> editBaseRuleTopupFee(@RequestBody CreateBaseRuleTopupFee updateBaseRuleTopupFee, @PathVariable Long id) {
        return baseRuleTopupFeeService.editBaseRuleTopupFee(updateBaseRuleTopupFee, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<BaseRuleTopupFeeDto> deleteBaseRuleTopupFee(@PathVariable Long id) {
        return baseRuleTopupFeeService.deleteBaseRuleTopupFee(id);
    }
}