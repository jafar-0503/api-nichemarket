package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.BaseRuleContractPeriod.BaseRuleContractPeriodDto;
import id.equity.nichemarket.dto.si.BaseRuleContractPeriod.CreateBaseRuleContractPeriod;
import id.equity.nichemarket.service.si.BaseRuleContractPeriodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/base-rule-contract-periods")
public class BaseRuleContractPeriodController {
    @Autowired
    private BaseRuleContractPeriodService baseRuleContractPeriodService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<BaseRuleContractPeriodDto>> listBaseRuleContractPeriod(){
        return baseRuleContractPeriodService.listBaseRuleContractPeriod();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<BaseRuleContractPeriodDto> getBaseRuleContractPeriodById(@PathVariable Long id) {
        return baseRuleContractPeriodService.getBaseRuleContractPeriodyId(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<BaseRuleContractPeriodDto> addBaseRuleContractPeriod(@RequestBody CreateBaseRuleContractPeriod newBaseRuleContractPeriod) {
        return baseRuleContractPeriodService.addBaseRuleContractPeriod(newBaseRuleContractPeriod);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<BaseRuleContractPeriodDto> editBaseRuleContractPeriod(@RequestBody CreateBaseRuleContractPeriod updateBaseRuleContractPeriod, @PathVariable Long id) {
        return baseRuleContractPeriodService.editBaseRuleContractPeriod(updateBaseRuleContractPeriod, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<BaseRuleContractPeriodDto> deleteBaseRuleContractPeriod(@PathVariable Long id) {
        return baseRuleContractPeriodService.deleteBaseRuleContractPeriod(id);
    }
}