package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.dto.si.BaseRulePremiumPeriod.BaseRulePremiumPeriodDto;
import id.equity.nichemarket.dto.si.BaseRulePremiumPeriod.CreateBaseRulePremiumPeriod;
import id.equity.nichemarket.service.si.BaseRulePremiumPeriodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/base-rule-premium-periods")
public class BaseRulePremiumPeriodController {
    @Autowired
    private BaseRulePremiumPeriodService baseRulePremiumPeriodService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<BaseRulePremiumPeriodDto>> listBaseRulePremiumPeriod(){
        return baseRulePremiumPeriodService.listBaseRulePremiumPeriod();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<BaseRulePremiumPeriodDto> getBaseRulePremiumPeriodById(@PathVariable Long id) {
        return baseRulePremiumPeriodService.getBaseRulePremiumPeriodById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<BaseRulePremiumPeriodDto> addBaseRulePremiumPeriod(@RequestBody CreateBaseRulePremiumPeriod newBaseRulePremiumPeriod) {
        return baseRulePremiumPeriodService.addBaseRulePremiumPeriod(newBaseRulePremiumPeriod);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<BaseRulePremiumPeriodDto> editBaseRulePremiumPeriod(@RequestBody CreateBaseRulePremiumPeriod updateBaseRulePremiumPeriod, @PathVariable Long id) {
        return baseRulePremiumPeriodService.editBaseRulePremiumPeriod(updateBaseRulePremiumPeriod, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<BaseRulePremiumPeriodDto> deleteBaseRulePremiumPeriod(@PathVariable Long id) {
        return baseRulePremiumPeriodService.deleteBaseRulePremiumPeriod(id);
    }
}