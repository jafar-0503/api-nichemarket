package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.dto.si.BaseRuleDebtAccount.BaseRuleDebtAccountDto;
import id.equity.nichemarket.dto.si.BaseRuleDebtAccount.CreateBaseRuleDebtAccount;
import id.equity.nichemarket.service.si.BaseRuleDebtAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/base-rule-debt-accounts")
public class BaseRuleDebtAccountController {

    @Autowired
    private BaseRuleDebtAccountService baseRuleDebtAccountService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<BaseRuleDebtAccountDto>> listBaseRuleDebtAccount(){
        return baseRuleDebtAccountService.listBaseRuleDebtAccount();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<BaseRuleDebtAccountDto> getBaseRuleDebtAccountById(@PathVariable Long id) {
        return baseRuleDebtAccountService.getBaseRuleDebtAccountById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<BaseRuleDebtAccountDto> addBaseRuleDebtAccount(@RequestBody CreateBaseRuleDebtAccount newBaseRuleDebtAccount) {
        return baseRuleDebtAccountService.addBaseRuleDebtAccount(newBaseRuleDebtAccount);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<BaseRuleDebtAccountDto> editBaseRuleDebtAccount(@RequestBody CreateBaseRuleDebtAccount updateBaseRuleDebtAccount, @PathVariable Long id) {
        return baseRuleDebtAccountService.editBaseRuleDebtAccount(updateBaseRuleDebtAccount, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<BaseRuleDebtAccountDto> deleteBaseRuleDebtAccount(@PathVariable Long id) {
        return baseRuleDebtAccountService.deleteBaseRuleDebtAccount(id);
    }
}