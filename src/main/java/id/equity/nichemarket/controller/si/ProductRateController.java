package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.dto.si.ProductRate.CreateProductRate;
import id.equity.nichemarket.dto.si.ProductRate.ProductRateDto;
import id.equity.nichemarket.service.si.ProductRateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/product-rates")
public class ProductRateController {

	@Autowired
	private ProductRateService productRateService;
	
	//Get All Data
	@GetMapping
	public ResponseEntity<List<ProductRateDto>> listProductRate(){
		return productRateService.listProductRate();
	}
	
	//Get Data By Id
	@GetMapping("{id}")
	public ResponseEntity<ProductRateDto> getProductRateById(@PathVariable Long id) {
		return productRateService.getProductRateById(id);
	}
	
	//Post Data
	@PostMapping
	public ResponseEntity<ProductRateDto> addProductRate(@RequestBody CreateProductRate newProductRate) {
		return productRateService.addProductRate(newProductRate);
	}
	
	//Put Data By Id
	@PutMapping("{id}")
	public ResponseEntity<ProductRateDto> editProductRate(@RequestBody CreateProductRate updateProductRate, @PathVariable Long id) {
		return productRateService.editProductRate(updateProductRate, id);
	}
	
	//Delete By Id
	@DeleteMapping("{id}")
	public ResponseEntity<ProductRateDto> deleteProductRate(@PathVariable Long id) {
		return productRateService.deleteProductRate(id);
	}
}