package id.equity.nichemarket.controller.si;

import java.util.List;

import id.equity.nichemarket.config.response.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.si.BaseRuleRedeem.BaseRuleRedeemDto;
import id.equity.nichemarket.dto.si.BaseRuleRedeem.CreateBaseRuleRedeem;

import id.equity.nichemarket.service.si.BaseRuleRedeemService;

@RestController
@RequestMapping("api/v1/base-rule-redeems")
public class BaseRuleRedeemController {
	
	@Autowired
	private BaseRuleRedeemService basRuleRedeemService;
	
	//Get All Redeem
	@GetMapping
	public ResponseEntity<List<BaseRuleRedeemDto>>listBaseRuleRedeem(){
		return basRuleRedeemService.listBaseRuleRedeem();
	}
	
	//Get Redeem By Id
	@GetMapping("{id}")
	public ResponseEntity<BaseRuleRedeemDto> getBaseRuleRedeemById(@PathVariable Long id) {
		return basRuleRedeemService.getBaseRuleRedeemById(id);
	}
	
	//Post Redeem
	@PostMapping
	public ResponseEntity<BaseRuleRedeemDto> addBaseRuleRedeem(@RequestBody CreateBaseRuleRedeem newRedeem) {
		return basRuleRedeemService.addBaseRuleRedeem(newRedeem);
	}
	
	//Edit Redeem
	@PutMapping("{id}")
	public ResponseEntity<BaseRuleRedeemDto> editBaseRuleRedeem(@RequestBody CreateBaseRuleRedeem updateRedeem, @PathVariable Long id) {
		return basRuleRedeemService.editBaseRuleRedeem(updateRedeem, id);
	}
	
	//Delete Invest
	@DeleteMapping("{id}")
	public ResponseEntity<BaseRuleRedeemDto> deleteBaseRuleRedeem(@PathVariable Long id) {
		return basRuleRedeemService.deleteBaseRuleRedeem(id);
	}
}