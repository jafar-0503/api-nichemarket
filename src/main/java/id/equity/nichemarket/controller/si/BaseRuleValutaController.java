package id.equity.nichemarket.controller.si;

import java.util.List;

import id.equity.nichemarket.config.response.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.si.BaseRuleValuta.BaseRuleValutaDto;
import id.equity.nichemarket.dto.si.BaseRuleValuta.CreateBaseRuleValuta;
import id.equity.nichemarket.service.si.BaseRuleValutaService;

@RestController
@RequestMapping("api/v1/base-rule-valutas")
public class BaseRuleValutaController {
	
	@Autowired
	private BaseRuleValutaService baseRuleValutaService;
	
	//get All BaseRuleValuta
	@GetMapping
	public ResponseEntity<List<BaseRuleValutaDto>> listBaseRuleValuta(){
		return baseRuleValutaService.listBaseRuleValuta();
	}
	
	//Get BaseRuleValuta By Id
	@GetMapping("{id}")
	public ResponseEntity<BaseRuleValutaDto> getBaseRuleValutaById(@PathVariable Long id) {
		return baseRuleValutaService.getBaseRuleValutaById(id);
	}
	
	//Post BaseRuleValuta
	@PostMapping
	public ResponseEntity<BaseRuleValutaDto> addBaseRuleValuta(@RequestBody CreateBaseRuleValuta newBaseRuleValuta) {
		return baseRuleValutaService.addBaseRuleValuta(newBaseRuleValuta);
	}
	
	//Edit BaseRuleValuta
	@PutMapping("{id}")
	public ResponseEntity<BaseRuleValutaDto> editBaseRuleValuta(@RequestBody CreateBaseRuleValuta updateBaseRuleValuta, @PathVariable Long id) {
		return baseRuleValutaService.editBaseRuleValuta(updateBaseRuleValuta, id);
	}
	
	//Delete BaseRuleValuta
	@DeleteMapping("{id}")
	public ResponseEntity<BaseRuleValutaDto> deleteBaseRuleValutaa(@PathVariable Long id) {
		return baseRuleValutaService.deleteBaseRuleValuta(id);
	}
}