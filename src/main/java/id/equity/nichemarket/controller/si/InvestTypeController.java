package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.dto.si.InvestType.CreateInvestType;
import id.equity.nichemarket.dto.si.InvestType.InvestTypeDto;
import id.equity.nichemarket.service.si.InvestTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/invest-types")
public class InvestTypeController {
	
	@Autowired
	private InvestTypeService investService;
	
	@GetMapping
	public ResponseEntity<List<InvestTypeDto>> listInvestType(){
		return investService.listInvestType();
	}
	
	@GetMapping("{id}")
	public ResponseEntity<InvestTypeDto> getInvestTypeById(@PathVariable Long id) {
		return investService.getInvestTypeById(id);
	}
	
	@PostMapping
	public ResponseEntity<InvestTypeDto> addInvestType(@RequestBody CreateInvestType newInvestType) {
		return investService.addInvestType(newInvestType);
	}
	
	@PutMapping("{id}")
	public ResponseEntity<InvestTypeDto> editInvestType(@RequestBody CreateInvestType updateInvestType, @PathVariable Long id) {
		return investService.editInvestType(updateInvestType, id);
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity<InvestTypeDto> deleteInvestType(@PathVariable Long id) {
		return investService.deleteInvestType(id);
	}
}