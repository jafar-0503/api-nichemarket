package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.dto.si.ProductRuleClass.CreateProductRuleClass;
import id.equity.nichemarket.dto.si.ProductRuleClass.ProductRuleClassDto;
import id.equity.nichemarket.service.si.ProductRuleClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/product-rule-class")
public class ProductRuleClassController {
	
	@Autowired
	private ProductRuleClassService productRuleClassService;
	
	//get All ProductRuleClass
	@GetMapping
	public ResponseEntity<List<ProductRuleClassDto>> listProductRuleClass(){
		return productRuleClassService.listProductRuleClass();
	}
	
	//Get ProductRuleClass By Id
	@GetMapping("{id}")
	public ResponseEntity<ProductRuleClassDto> getProductRuleClassById(@PathVariable Long id) {
		return productRuleClassService.getProductRuleClassById(id);
	}
	
	//Post ProductRuleClass
	@PostMapping
	public ResponseEntity<ProductRuleClassDto> addProductRuleClass(@RequestBody CreateProductRuleClass id) {
		return productRuleClassService.addProductRuleClass(id);
	}
	
	//Edit ProductRuleClass
	@PutMapping("{id}")
	public ResponseEntity<ProductRuleClassDto> editProductRuleClass(@RequestBody CreateProductRuleClass updateProductRuleClass, @PathVariable Long id) {
		return productRuleClassService.editProductRuleClass(updateProductRuleClass, id);
	}
	
	//Delete ProductRuleClass
	@DeleteMapping("{id}")
	public ResponseEntity<ProductRuleClassDto> deleteProductRuleClass(@PathVariable Long id) {
		return productRuleClassService.deleteProductRuleClass(id);
	}
}