package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.As.CreateAs;
import id.equity.nichemarket.dto.si.As.AsDto;
import id.equity.nichemarket.repository.si.AsRepository;
import id.equity.nichemarket.service.si.AsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/as")
public class AsController {
	
	@Autowired
	private AsService asService;

	@GetMapping
	public ResponseEntity<List<AsDto>> listAs(){
		return asService.listAs();
	}
	
	@GetMapping("{id}")
	public ResponseEntity<AsDto> getAsById(@PathVariable Long id) {
		return asService.getAsById(id);
	}
	
	@PostMapping
	public ResponseEntity<AsDto> addAs(@RequestBody CreateAs newAs) {
		return asService.addAs(newAs);
	}
	
	@PutMapping("{id}")
	public ResponseEntity<AsDto> editAs(@RequestBody CreateAs updateAs, @PathVariable Long id) {
		return asService.editAs(updateAs, id);
	}

	@DeleteMapping("{id}")
	public ResponseEntity<AsDto> deleteAs(@PathVariable Long id) {
		return asService.deleteAs(id);
	}
}