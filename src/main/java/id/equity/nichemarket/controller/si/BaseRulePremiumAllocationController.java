package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.BaseRulePremiumAllocation.CreateBaseRulePremiumAllocation;
import id.equity.nichemarket.dto.si.BaseRulePremiumAllocation.BaseRulePremiumAllocationDto;
import id.equity.nichemarket.service.si.BaseRulePremiumAllocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/base-rule-premium-allocations")
public class BaseRulePremiumAllocationController {

	@Autowired
	private BaseRulePremiumAllocationService bRPAlocationService;
	
	//GET ALL DATA
	@GetMapping
	public ResponseEntity<List<BaseRulePremiumAllocationDto>> listBaseRulePremiumAllocation(){
		return bRPAlocationService.listBaseRulePremiumAllocation();
	}
	
	//GET DATA BY ID
	@GetMapping("{id}")
	public ResponseEntity<BaseRulePremiumAllocationDto> getBaseRulePremiumAllocationById(@PathVariable Long id) {
		return bRPAlocationService.getBaseRulePremiumAllocationById(id);
	}
	
	//POST DATA
	@PostMapping
	public ResponseEntity<BaseRulePremiumAllocationDto> addBaseRulePremiumAllocation(@RequestBody CreateBaseRulePremiumAllocation newBaseRulePremiumAllocation) {
		return bRPAlocationService.addBaseRulePremiumAllocation(newBaseRulePremiumAllocation);
	}
	
	//PUT DATA
	@PutMapping("{id}")
	public ResponseEntity<BaseRulePremiumAllocationDto> editBaseRulePremiumAllocation(@RequestBody CreateBaseRulePremiumAllocation updateBaseRulePremiumAllocation, @PathVariable Long id) {
		return bRPAlocationService.editBaseRulePremiumAllocation(updateBaseRulePremiumAllocation, id);
	}
	
	//DETELE DATA
	@DeleteMapping("{id}")
	public ResponseEntity<BaseRulePremiumAllocationDto> deleteBaseRulePremiumAllocation(@PathVariable Long id) {
		return bRPAlocationService.deleteBaseRulePremiumAllocation(id);
	}
}