package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.dto.si.ProductRuleRisk.CreateProductRuleRisk;
import id.equity.nichemarket.dto.si.ProductRuleRisk.ProductRuleRiskDto;
import id.equity.nichemarket.service.si.ProductRuleRiskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/product-rule-risks")
public class ProductRuleRiskController {
	
	@Autowired
	private ProductRuleRiskService productRuleRiskService;
	
	//get All ProductRuleRisk
	@GetMapping
	public ResponseEntity<List<ProductRuleRiskDto>> listProductRuleRisk(){
		return productRuleRiskService.listProductRuleRisk();
	}
	
	//Get ProductRuleRisk By Id
	@GetMapping("{id}")
	public ResponseEntity<ProductRuleRiskDto> getProductRuleRiskById(@PathVariable Long id) {
		return productRuleRiskService.getProductRuleRiskById(id);
	}
	
	//Post ProductRuleRisk
	@PostMapping
	public ResponseEntity<ProductRuleRiskDto> addProductRuleRisk(@RequestBody CreateProductRuleRisk id) {
		return productRuleRiskService.addProductRuleRisk(id);
	}
	
	//Edit ProductRuleRisk
	@PutMapping("{id}")
	public ResponseEntity<ProductRuleRiskDto> editProductRuleRisk(@RequestBody CreateProductRuleRisk updateProductRuleRisk, @PathVariable Long id) {
		return productRuleRiskService.editProductRuleRisk(updateProductRuleRisk, id);
	}
	
	//Delete ProductRuleRisk
	@DeleteMapping("{id}")
	public ResponseEntity<ProductRuleRiskDto> deleteProductRuleRisk(@PathVariable Long id) {
		return productRuleRiskService.deleteProductRuleRisk(id);
	}
}