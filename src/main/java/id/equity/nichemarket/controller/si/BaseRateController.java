package id.equity.nichemarket.controller.si;

import java.util.List;

import id.equity.nichemarket.config.response.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.si.BaseRate.BaseRateDto;
import id.equity.nichemarket.dto.si.BaseRate.CreateBaseRate;
import id.equity.nichemarket.service.si.BaseRateService;

@RestController
@RequestMapping("api/v1/base-rates")
public class BaseRateController {
	
	@Autowired
	private BaseRateService baseRateService;
	
	//get All BaseRate
	@GetMapping
	public ResponseEntity<List<BaseRateDto>> listBaseRate(){
		return baseRateService.listBaseRate();
	}
	
	//Get BaseRate By Id
	@GetMapping("{id}")
	public ResponseEntity<BaseRateDto> getBaseRateById(@PathVariable Long id) {
		return baseRateService.getBaseRateById(id);
	}
	
	//Post BaseRate
	@PostMapping
	public ResponseEntity<BaseRateDto> addBaseRate(@RequestBody CreateBaseRate newBaseRate) {
		return baseRateService.addBaseRate(newBaseRate);
	}

	//Put BaseRate
	@PutMapping("{id}")
	public ResponseEntity<BaseRateDto> editBaserate(@RequestBody CreateBaseRate updateBaserate, @PathVariable Long id) {
		return baseRateService.editBaserate(updateBaserate, id);
	}
	
	//Delete BaseRate
	@DeleteMapping("{id}")
	public ResponseEntity<BaseRateDto> deleteBaserate(@PathVariable Long id) {
		return baseRateService.deleteBaserate(id);
	}
}