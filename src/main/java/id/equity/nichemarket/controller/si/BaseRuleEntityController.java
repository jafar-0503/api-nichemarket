package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.dto.si.BaseRuleEntity.BaseRuleEntityDto;
import id.equity.nichemarket.dto.si.BaseRuleEntity.CreateBaseRuleEntity;
import id.equity.nichemarket.service.si.BaseRuleEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/base-rule-entities")
public class BaseRuleEntityController {

    @Autowired
    private BaseRuleEntityService baseRuleEntityService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<BaseRuleEntityDto>> listBaseRuleEntity(){
        return baseRuleEntityService.listBaseRuleEntity();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<BaseRuleEntityDto> getBaseRuleEntityById(@PathVariable Long id) {
        return baseRuleEntityService.getBaseRuleEntityById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<BaseRuleEntityDto> addBaseRuleEntity(@RequestBody CreateBaseRuleEntity newBaseRuleEntity) {
        return baseRuleEntityService.addBaseRuleEntity(newBaseRuleEntity);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<BaseRuleEntityDto> editBaseRuleEntity(@RequestBody CreateBaseRuleEntity updateBaseRuleEntity, @PathVariable Long id) {
        return baseRuleEntityService.editBaseRuleEntity(updateBaseRuleEntity, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<BaseRuleEntityDto> deleteBaseRuleEntity(@PathVariable Long id) {
        return baseRuleEntityService.deleteBaseRuleEntity(id);
    }
}