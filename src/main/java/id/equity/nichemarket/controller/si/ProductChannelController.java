package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.ProductChannel.CreateProductChannel;
import id.equity.nichemarket.dto.si.ProductChannel.ProductChannelDto;
import id.equity.nichemarket.service.si.ProductChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/product-channels")
public class ProductChannelController {

    @Autowired
    private ProductChannelService productChannelservice;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<ProductChannelDto>> listProductChannel(){
        return productChannelservice.listProductChannel();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<ProductChannelDto> getProductChanneById(@PathVariable Long id) {
        return productChannelservice.getProductChanneById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<ProductChannelDto> addProductChannel(@RequestBody CreateProductChannel newProdChannel) {
        return productChannelservice.addProductChannel(newProdChannel);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<ProductChannelDto> editProductChannel(@RequestBody CreateProductChannel updateProdChannel, @PathVariable Long id) {
        return productChannelservice.editProductChannel(updateProdChannel, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<ProductChannelDto> deleteProductChannel(@PathVariable Long id) {
        return productChannelservice.deleteProductChannel(id);
    }
}