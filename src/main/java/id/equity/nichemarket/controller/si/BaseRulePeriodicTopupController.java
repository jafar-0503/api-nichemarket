package id.equity.nichemarket.controller.si;

import java.util.List;

import id.equity.nichemarket.dto.si.BaseRulePeriodicTopup.BaseRulePeriodicTopupDto;
import id.equity.nichemarket.dto.si.BaseRulePeriodicTopup.CreateBaseRulePeriodicTopup;
import id.equity.nichemarket.service.si.BaseRulePeriodicTopupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/base-rule-periodic-topup")
public class BaseRulePeriodicTopupController {
	
	@Autowired
	private BaseRulePeriodicTopupService baseRulePeriodicTopupService;
	
	//get All BaseRulePeriodicTopup
	@GetMapping
	public ResponseEntity<List<BaseRulePeriodicTopupDto>> listBaseRulePeriodicTopup(){
		return baseRulePeriodicTopupService.listBaseRulePeriodicTopup();
	}
	
	//Get BaseRulePeriodicTopup By Id
	@GetMapping("{id}")
	public ResponseEntity<BaseRulePeriodicTopupDto> getBaseRulePeriodicTopupById(@PathVariable Long id) {
		return baseRulePeriodicTopupService.getBaseRulePeriodicTopupById(id);
	}
	
	//Post BaseRulePeriodicTopup
	@PostMapping
	public ResponseEntity<BaseRulePeriodicTopupDto> addBaseRulePeriodicTopup(@RequestBody CreateBaseRulePeriodicTopup newBaseRulePeriodicTopup) {
		return baseRulePeriodicTopupService.addBaseRulePeriodicTopup(newBaseRulePeriodicTopup);
	}
	
	//Edit BaseRulePeriodicTopup
	@PutMapping("{id}")
	public ResponseEntity<BaseRulePeriodicTopupDto> editBaseRulePeriodicTopup(@RequestBody CreateBaseRulePeriodicTopup updateBaseRulePeriodicTopup, @PathVariable Long id) {
		return baseRulePeriodicTopupService.editBaseRulePeriodicTopup(updateBaseRulePeriodicTopup, id);
	}
	
	//Delete BaseRulePeriodicTopup
	@DeleteMapping("{id}")
	public ResponseEntity<BaseRulePeriodicTopupDto> deleteBaseRulePeriodicTopup(@PathVariable Long id) {
		return baseRulePeriodicTopupService.deleteBaseRulePeriodicTopup(id);
	}
}	