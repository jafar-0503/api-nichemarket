package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.dto.si.ProductRuleRequired.CreateProductRuleRequired;
import id.equity.nichemarket.dto.si.ProductRuleRequired.ProductRuleRequiredDto;
import id.equity.nichemarket.service.si.ProductRuleRequiredService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/product-rule-requireds")
public class ProductRuleRequiredController {
	
	@Autowired
	private ProductRuleRequiredService productRuleRequiredService;
	
	//get All ProductRuleRequired
	@GetMapping
	public ResponseEntity<List<ProductRuleRequiredDto>> listProductRuleRequired(){
		return productRuleRequiredService.listProductRuleRequired();
	}
	
	//Get ProductRuleRequired By Id
	@GetMapping("{id}")
	public ResponseEntity<ProductRuleRequiredDto> getProductRuleRequiredById(@PathVariable Long id) {
		return productRuleRequiredService.getProductRuleRequiredById(id);
	}
	
	//Post ProductRuleRequired
	@PostMapping
	public ResponseEntity<ProductRuleRequiredDto> addProductRuleRequired(@RequestBody CreateProductRuleRequired id) {
		return productRuleRequiredService.addProductRuleRequired(id);
	}
	
	//Edit ProductRuleRequired
	@PutMapping("{id}")
	public ResponseEntity<ProductRuleRequiredDto> editProductRuleRequired(@RequestBody CreateProductRuleRequired updateProductRuleRequired, @PathVariable Long id) {
		return productRuleRequiredService.editProductRuleRequired(updateProductRuleRequired, id);
	}
	
	//Delete ProductRuleRequired
	@DeleteMapping("{id}")
	public ResponseEntity<ProductRuleRequiredDto> deleteProductRuleRequired(@PathVariable Long id) {
		return productRuleRequiredService.deleteProductRuleRequired(id);
	}
}