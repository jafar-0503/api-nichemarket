package id.equity.nichemarket.controller.si;

import java.util.List;

import id.equity.nichemarket.config.response.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.si.ProductRuleGrouping.CreateProductRuleGrouping;
import id.equity.nichemarket.dto.si.ProductRuleGrouping.ProductRuleGroupingDto;
import id.equity.nichemarket.service.si.ProductRuleGroupingService;

@RestController
@RequestMapping("api/v1/product-rule-groupings")
public class ProductRuleGroupingController {
	
	@Autowired
	private ProductRuleGroupingService productRuleGroupingService;
	
	//get All productRuleGrouping
	@GetMapping
	public ResponseEntity<List<ProductRuleGroupingDto>> listProductRuleGrouping(){
		return productRuleGroupingService.listProductRuleGrouping();
	}
	
	//Get productRuleGrouping By Id
	@GetMapping("{id}")
	public ResponseEntity<ProductRuleGroupingDto> getProductRuleGroupingById(@PathVariable Long id) {
		return productRuleGroupingService.getProductRuleGroupingById(id);
	}
	
	//Post productRuleGrouping
	@PostMapping
	public ResponseEntity<ProductRuleGroupingDto> addProductRuleGrouping(@RequestBody CreateProductRuleGrouping id) {
		return productRuleGroupingService.addProductRuleGrouping(id);
	}
	
	//Edit productRuleGrouping
	@PutMapping("{id}")
	public ResponseEntity<ProductRuleGroupingDto> editProductRuleGrouping(@RequestBody CreateProductRuleGrouping updateProductRuleGrouping, @PathVariable Long id) {
		return productRuleGroupingService.editProductRuleGrouping(updateProductRuleGrouping, id);
	}
	
	//Delete productRuleGrouping
	@DeleteMapping("{id}")
	public ResponseEntity<ProductRuleGroupingDto> deleteProductRuleGrouping(@PathVariable Long id) {
		return productRuleGroupingService.deleteProductRuleGrouping(id);
	}	
}	