package id.equity.nichemarket.controller.partner.linkaja;

import javax.validation.groups.Default;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.equity.nichemarket.constant.url.GlobalConstantLinkaja;
import id.equity.nichemarket.dto.mgm.customerRegistration.CustomerRegistrationDto;
import id.equity.nichemarket.dto.mgm.registration.RegistrationDto;
import id.equity.nichemarket.dto.partner.linkaja.ApplinkRequestDto;
import id.equity.nichemarket.service.partner.linkaja.CustomerService;
import id.equity.nichemarket.service.partner.linkaja.EncryptionAESService;
import id.equity.nichemarket.validation.groups.LinkAjaRegistration;

@RestController("linkajaCustomerController")
@RequestMapping("api/v1/linkaja/customers")
public class CustomerController {
	
	@Autowired
	@Qualifier("linkajaCustomerService")
	private CustomerService customerService;
	
	
	@Autowired
	private EncryptionAESService encryptionAESService;
	

	@PostMapping("/register")
	public ResponseEntity<?> registerCustomer(@Validated({LinkAjaRegistration.class, Default.class}) @RequestBody CustomerRegistrationDto newCustomer){
		return customerService.registerCustomer(newCustomer);
	}
	
	
	@PostMapping("/detail")
	public ResponseEntity<?> detailCustomer(@RequestBody RegistrationDto registration ){
		return customerService.getDetailCustomer(registration.getRegistrationCode());
	}
	
	@GetMapping("/env")
	public ResponseEntity<String> getEnvVariable() {
		StringBuilder sb = new StringBuilder();
		sb.append("BASE_URL_APP_LINK: " + GlobalConstantLinkaja.BASE_URL_APP_LINK);
		sb.append(System.lineSeparator());
		sb.append("USERNAME_LINKAJA: " + GlobalConstantLinkaja.USERNAME_LINKAJA);
		sb.append(System.lineSeparator());
		sb.append("PASSWORD_LINKAJA: " + GlobalConstantLinkaja.PASSWORD_LINKAJA);
		sb.append(System.lineSeparator());
		sb.append("SECRET_KEY: " + GlobalConstantLinkaja.SECRET_KEY);
		sb.append(System.lineSeparator());
		sb.append("HEADER_TIMESTAMP: " + GlobalConstantLinkaja.HEADER_TIMESTAMP);
		sb.append(System.lineSeparator());
		sb.append("LINK_AJA_URL: " + GlobalConstantLinkaja.LINK_AJA_URL);
	    return ResponseEntity.ok(sb.toString());
	}

	
	@PostMapping("/encrypt")
	public ResponseEntity<String> encrypt(@RequestBody ApplinkRequestDto applinkRequestDto) throws JsonProcessingException{
		ObjectMapper mapper = new ObjectMapper();
		String myRequest = mapper.writeValueAsString(applinkRequestDto);
		System.out.println(myRequest);
		String rs = EncryptionAESService.encrypt(myRequest);
		System.out.println(rs);
		return ResponseEntity.ok(rs);
	}
}
