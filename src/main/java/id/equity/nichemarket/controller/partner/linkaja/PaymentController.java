package id.equity.nichemarket.controller.partner.linkaja;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.mgm.payment.v2.PaymentRequestDto;
import id.equity.nichemarket.dto.mgm.payment.v2.PaymentSuccessResponseDto;
import id.equity.nichemarket.dto.partner.linkaja.InformPaymentResponseDto;
import id.equity.nichemarket.exception.ErrorsException;
import id.equity.nichemarket.service.partner.linkaja.LinkajaAuthorizationService;
import id.equity.nichemarket.service.partner.linkaja.PaymentService;

@RestController("linkajaPaymentController")
@RequestMapping("api/v1/linkaja/payments")
public class PaymentController {

	@Autowired
	@Qualifier("linkajaPaymentService")
	PaymentService paymentService;
	
	@Autowired
	@Qualifier("linkajaAuthorizationService")
	LinkajaAuthorizationService linkajaAuthorizationService;
	
	@Autowired
	private HttpServletRequest request;	
	

	//POST DATA
	@PostMapping
	public ResponseEntity<?> addPayment(@RequestBody String decryptedString) throws Exception {
		if(request.getHeader("Authorization") == null) {
			throw new ErrorsException("Error Authorization is null");
		}
		
		Boolean isValidCredentials = linkajaAuthorizationService.isValidCredentials(request.getHeader("Authorization"));
		if(isValidCredentials) {			
			PaymentRequestDto payment = paymentService.decryptPayment(decryptedString);
			return ResponseEntity.ok(paymentService.addPayment(payment));			
		}else {
			InformPaymentResponseDto informPaymentResponse = new InformPaymentResponseDto();
			informPaymentResponse.setStatus("03");
			informPaymentResponse.setMessage("Unauthorized");
			informPaymentResponse.setLinkAjaRefnum("");
			informPaymentResponse.setPartnerTrxID("");
			return ResponseEntity.ok(informPaymentResponse);
		}	
	}
	
	//POST DATA
	@PostMapping("_dummy")
	public ResponseEntity<?> dummyAddPayment(@RequestBody String decryptedString) throws Exception {
		if(request.getHeader("Authorization") == null) {
			throw new ErrorsException("Error Authorization is null");
		}
		
		Boolean isValidCredentials = linkajaAuthorizationService.isValidCredentials(request.getHeader("Authorization"));
		if(isValidCredentials) {			
			return ResponseEntity.ok(paymentService.dummyFunc(decryptedString));
		}else {
			InformPaymentResponseDto informPaymentResponse = new InformPaymentResponseDto();
			informPaymentResponse.setStatus("03");
			informPaymentResponse.setMessage("Unauthorized");
			informPaymentResponse.setLinkAjaRefnum("");
			informPaymentResponse.setPartnerTrxID("");
			return ResponseEntity.ok(informPaymentResponse);
		}	
	}
}
