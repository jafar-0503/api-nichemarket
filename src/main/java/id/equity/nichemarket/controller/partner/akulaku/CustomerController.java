package id.equity.nichemarket.controller.partner.akulaku;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.mgm.customerRegistration.CustomerRegistrationDto;
import id.equity.nichemarket.retrofit.mgm.freeCovid.FreeCovidService;
import id.equity.nichemarket.service.mgm.AccountService;
import id.equity.nichemarket.service.mgm.GroupInsuranceService;
import id.equity.nichemarket.service.mgm.ProductPlanPolicyService;
import id.equity.nichemarket.service.mgm.ProductPolicyService;
import id.equity.nichemarket.service.mgm.RegistrationTypeService;
import id.equity.nichemarket.service.mgm.SiMedisValidatorService;
import id.equity.nichemarket.service.partner.akulaku.CustomerService;

@RestController("akulakuCustomerController")
@RequestMapping("api/v1/akulaku/customers")
public class CustomerController {
	
	@Autowired
    private FreeCovidService freeCovidService;
	
	@Autowired
	@Qualifier("akulakuCustomerService")
	private CustomerService customerService;
	
	@Autowired
	private AccountService accountService;
	

	@Autowired
	private RegistrationTypeService registrationTypeService;	

	@Autowired
	private ProductPolicyService productPolicyService;
	
	@Autowired
	private GroupInsuranceService groupInsuranceService;
	
	@Autowired
	private ProductPlanPolicyService productPlanPolicyService;
	
	@Autowired
	private SiMedisValidatorService siMedisValidatorService;
	
    private static final Logger logger = LoggerFactory.getLogger(CustomerController.class);

	@PostMapping("/register")
	public ResponseEntity<?> registerCustomer(@Valid @RequestBody CustomerRegistrationDto newCustomer){
		return customerService.registerCustomer(newCustomer);
	}
}
