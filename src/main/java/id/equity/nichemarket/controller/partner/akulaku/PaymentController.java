package id.equity.nichemarket.controller.partner.akulaku;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.mgm.payment.PaymentDto;
import id.equity.nichemarket.dto.mgm.payment.PaymentRequestDto;
import id.equity.nichemarket.dto.mgm.payment.PaymentSuccessResponseDto;
import id.equity.nichemarket.service.partner.akulaku.PaymentService;

@RestController("akulakuPaymentController")
@RequestMapping("api/v1/akulaku/payments")
public class PaymentController {
	
	@Autowired
	@Qualifier("akulakuPaymentService")
	private PaymentService paymentService;
	
	//POST DATA
	@PostMapping
	public ResponseEntity<PaymentSuccessResponseDto> addPayment(@Valid @RequestBody PaymentRequestDto newPayment) throws Exception {
		return paymentService.addPayment(newPayment);
	}
	
}
