package id.equity.nichemarket.controller.es;

import id.equity.nichemarket.dto.es.Spaj.CreateSpaj;
import id.equity.nichemarket.dto.es.Spaj.SpajDto;
import id.equity.nichemarket.service.es.SpajService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/spaj")
public class SpajController {
	
	@Autowired
	private SpajService spajService;

	//Get Spaj All
	@GetMapping
	public ResponseEntity<Page<SpajDto>> listSpaj(
			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "sortBy", defaultValue = "id") String sortBy)
	{
		return spajService.listSpaj(pageNo, pageSize, sortBy);
	}
	
	//Get Spaj By ID
	@GetMapping("{id}")
	public ResponseEntity<SpajDto> getSpajById(@PathVariable Long id) {
		return spajService.getSpajById(id);
	}
	
	//Post BaseRate
	@PostMapping
	public ResponseEntity<SpajDto> addSpaj(@RequestBody CreateSpaj newSpaj) {
		return spajService.addSpaj(newSpaj);
//		return response;
	}

	//Put Spaj
	@PutMapping("{id}")
	public ResponseEntity<SpajDto> editSpaj(@RequestBody CreateSpaj updateSpaj, @PathVariable Long id) {
		return spajService.editSpaj(updateSpaj, id);

	}
	
	//Delete Invest
	@DeleteMapping("{id}")
	public ResponseEntity<SpajDto> deleteSpaj(@PathVariable Long id) {
		return spajService.deleteSpaj(id);
	}
}