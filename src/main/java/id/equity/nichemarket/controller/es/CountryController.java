package id.equity.nichemarket.controller.es;

import java.util.List;


import id.equity.nichemarket.config.response.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.es.Country.CountryDto;
import id.equity.nichemarket.dto.es.Country.CreateCountry;
import id.equity.nichemarket.service.es.CountryService;

@RestController
@RequestMapping("api/v1/countries")
public class CountryController {
	
	@Autowired
	private CountryService countryService;
	
	//get All Country
	@GetMapping
	public ResponseEntity<List<CountryDto>> listCountry(){
		return countryService.listCountry();
	}
	
	//Get Country By Id
	@GetMapping("{id}")
	public ResponseEntity<CountryDto> getCountryById(@PathVariable Long id) {
		return countryService.getCountryById(id);
	}
	
	//Post Country
	@PostMapping
	public ResponseEntity<CountryDto> addCountry(@RequestBody CreateCountry newCountry) {
		return countryService.addCountry(newCountry);
	}
	
	//Put Country
	@PutMapping("{id}")
	public ResponseEntity<CountryDto> editCountry(@RequestBody CreateCountry updateCountry, @PathVariable Long id) {
		return countryService.editCountry(updateCountry, id);
	}
	
	//Delete Country
	@DeleteMapping("{id}")
	public ResponseEntity<CountryDto> deleteCountry(@PathVariable Long id) {
		return countryService.deleteCountry(id);
	}
}