package id.equity.nichemarket.controller.es;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.ResidenceStatus.CreateResidenceStatus;
import id.equity.nichemarket.dto.es.ResidenceStatus.ResidenceStatusDto;
import id.equity.nichemarket.service.es.ResidenceStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/residence-status")
public class ResidenceStatusController {
    @Autowired
    private ResidenceStatusService residenceStatusService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<ResidenceStatusDto>> listResidenceStatus(){
        return residenceStatusService.listResidenceStatus();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<ResidenceStatusDto> getResidenceStatusById(@PathVariable Long id) {
        return residenceStatusService.getResidenceStatusById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<ResidenceStatusDto> addResidenceStatus (@RequestBody CreateResidenceStatus newResidenceStatus) {
        return residenceStatusService.addResidenceStatus(newResidenceStatus);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<ResidenceStatusDto> editResidenceStatus(@RequestBody CreateResidenceStatus updateResidenceStatus, @PathVariable Long id) {
        return residenceStatusService.editResidenceStatus(updateResidenceStatus, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<ResidenceStatusDto> deleteResidenceStatus(@PathVariable Long id) {
        return residenceStatusService.deleteResidenceStatus(id);
    }
}