package id.equity.nichemarket.controller.es;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.es.InsuredRelation.CreateInsuredRelation;
import id.equity.nichemarket.dto.es.InsuredRelation.InsuredRelationDto;
import id.equity.nichemarket.service.es.InsuredRelationService;

@RestController
@RequestMapping("api/v1/insured-relations")
public class InsuredRelationController {
	
	@Autowired
	private InsuredRelationService insuredRelationService;
	
	//get All Insured Relation
	@GetMapping
	public ResponseEntity<List<InsuredRelationDto>> listInsuredRelation(){
		return insuredRelationService.listInsuredRelation();
	}
	
	//Get InsuredRelation By Id
	@GetMapping("{id}")
	public ResponseEntity<InsuredRelationDto> getInsuredRelationById(@PathVariable Long id) {
		return insuredRelationService.getInsuredRelationById(id);
	}
	
	//Post InsuredRelation
	@PostMapping
	public ResponseEntity<InsuredRelationDto> addInsuredRelation(@RequestBody CreateInsuredRelation newInsuredRelation) {
		return insuredRelationService.addInsuredRelation(newInsuredRelation);
	}
	
	//Put InsuredRelation
	@PutMapping("{id}")
	public ResponseEntity<InsuredRelationDto> editInsuredRelation(@RequestBody CreateInsuredRelation updateInsuredRelation, @PathVariable Long id) {
		return insuredRelationService.editInsuredRelation(updateInsuredRelation, id);
	}
	
	//Delete InsuredRelation
	@DeleteMapping("{id}")
	public ResponseEntity<InsuredRelationDto> deleteInsuredRelation(@PathVariable Long id) {
		return insuredRelationService.deleteInsuredRelation(id);
	}
}