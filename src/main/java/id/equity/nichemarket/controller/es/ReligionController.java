package id.equity.nichemarket.controller.es;

import java.util.List;

import id.equity.nichemarket.config.response.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.es.Religion.CreateReligion;
import id.equity.nichemarket.dto.es.Religion.ReligionDto;
import id.equity.nichemarket.service.es.ReligionService;

@RestController
@RequestMapping("api/v1/religions")
public class ReligionController {
	
	@Autowired
	private ReligionService religionService;
	
	//get All Religion
	@GetMapping
	public ResponseEntity<List<ReligionDto>> listReligion(){
		return religionService.listReligion();
	}
	
	//Get Religion By Id
	@GetMapping("{id}")
	public ResponseEntity<ReligionDto> getReligionById(@PathVariable Long id) {
		return religionService.getReligionById(id);
	}
	
	//Post religion
	@PostMapping
	public ResponseEntity<ReligionDto> addReligion(@RequestBody CreateReligion newReligion) {
		return religionService.addReligion(newReligion);
	}
	
	//Put religion
	@PutMapping("{id}")
	public ResponseEntity<ReligionDto> editReligion(@RequestBody CreateReligion updateReligion, @PathVariable Long id) {
		return religionService.editReligion(updateReligion, id);
	}
	
	//Delete religion
	@DeleteMapping("{id}")
	public ResponseEntity<ReligionDto> deleteReligion(@PathVariable Long id) {
		return religionService.deleteReligion(id);
	}
}