package id.equity.nichemarket.controller.es;

import java.util.List;


import id.equity.nichemarket.config.response.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.es.PaymentMethod.CreatePaymentMethod;
import id.equity.nichemarket.dto.es.PaymentMethod.PaymentMethodDto;
import id.equity.nichemarket.service.es.PaymentMethodService;

@RestController
@RequestMapping("api/v1/payment-methods")
public class PaymentMethodController {
	
	@Autowired
	private PaymentMethodService paymentMethodService;
	
	//get All PaymentMethod
	@GetMapping
	public ResponseEntity<List<PaymentMethodDto>> listPaymentMethod(){
		return paymentMethodService.listPaymentMethod();
	}
	
	//Get PaymentMethod By Id
	@GetMapping("{id}")
	public ResponseEntity<PaymentMethodDto> getpaymentMethodById(@PathVariable Long id) {
		return paymentMethodService.getpaymentMethodById(id);
	}
	
	//Post BaseRate
	@PostMapping
	public ResponseEntity<PaymentMethodDto> addPaymentMethod(@RequestBody CreatePaymentMethod newPaymentMethod) {
		return paymentMethodService.addPaymentMethod(newPaymentMethod);
	}
	
	//Put PaymentMethod
	@PutMapping("{id}")
	public ResponseEntity<PaymentMethodDto> editPaymentMethod(@RequestBody CreatePaymentMethod updatePaymentMethod, @PathVariable Long id) {
		return paymentMethodService.editPaymentMethod(updatePaymentMethod, id);
	}
	
	//Delete PaymentMethod
	@DeleteMapping("{id}")
	public ResponseEntity<PaymentMethodDto> deletePaymentMethod(@PathVariable Long id) {
		return paymentMethodService.deletePaymentMethod(id);
	}
}