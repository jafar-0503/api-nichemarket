package id.equity.nichemarket.controller.es;

import java.util.List;

import id.equity.nichemarket.config.response.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.es.Education.CreateEducation;
import id.equity.nichemarket.dto.es.Education.EducationDto;
import id.equity.nichemarket.service.es.EducationService;

@RestController
@RequestMapping("api/v1/educations")
public class EducationController {
	
	@Autowired
	private EducationService educationService;
	
	//get All Education
	@GetMapping
	public ResponseEntity<List<EducationDto>> listEducation(){
		return educationService.listEducation();
	}
	
	//Get Education By Id
	@GetMapping("{id}")
	public ResponseEntity<EducationDto> getEducationById(@PathVariable Long id) {
		return educationService.getEducationById(id);
	}
	
	//Post education
	@PostMapping
	public ResponseEntity<EducationDto> addEducation(@RequestBody CreateEducation newEducation) {
		return educationService.addEducation(newEducation);
	}
	
	//Put Education
	@PutMapping("{id}")
	public ResponseEntity<EducationDto> editEducation(@RequestBody CreateEducation updateEducation, @PathVariable Long id) {
		return educationService.editEducation(updateEducation, id);
	}
	
	//Delete Education
	@DeleteMapping("{id}")
	public ResponseEntity<EducationDto> deleteEducation(@PathVariable Long id) {
		return educationService.deleteEducation(id);
	}
}