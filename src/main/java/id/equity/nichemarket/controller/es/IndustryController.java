package id.equity.nichemarket.controller.es;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.es.Industry.CreateIndustry;
import id.equity.nichemarket.dto.es.Industry.IndustryDto;
import id.equity.nichemarket.service.es.IndustryService;

@RestController
@RequestMapping("api/v1/industries")
public class IndustryController {
	
	@Autowired
	private IndustryService industryService;
	
	//get All Industry
	@GetMapping
	public ResponseEntity<List<IndustryDto>> listIndustry(){
		return industryService.listIndustry();
	}
	
	//Get Industry By Id
	@GetMapping("{id}")
	public ResponseEntity<IndustryDto> getIndustryById(@PathVariable Long id) {
		return industryService.getIndustryById(id);
	}
	
	//Post Industry
	@PostMapping
	public ResponseEntity<IndustryDto> addIndustry(@RequestBody CreateIndustry newIndustry) {
		return industryService.addIndustry(newIndustry);
	}
	
	//Put Industry
	@PutMapping("{id}")
	public ResponseEntity<IndustryDto> editIndustry(@RequestBody CreateIndustry updateIndustry, @PathVariable Long id) {
		return industryService.editIndustry(updateIndustry, id);
	}
	
	//Delete Industry
	@DeleteMapping("{id}")
	public ResponseEntity<IndustryDto> deleteIndustry(@PathVariable Long id) {
		return industryService.deleteIndustry(id);
	}
}