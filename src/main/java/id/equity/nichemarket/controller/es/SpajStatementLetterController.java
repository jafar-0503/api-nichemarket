package id.equity.nichemarket.controller.es;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.SpajStatementLetter.CreateSpajStatementLetter;
import id.equity.nichemarket.dto.es.SpajStatementLetter.SpajStatementLetterDto;
import id.equity.nichemarket.service.es.SpajStatementLetterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/spaj-statement-letters")
public class SpajStatementLetterController {
    @Autowired
    private SpajStatementLetterService spajStatementLetterService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<SpajStatementLetterDto>> listSpajStatementLetter(){
        return spajStatementLetterService.listSpajStatementLetter();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<SpajStatementLetterDto> getSpajStatementLetterById(@PathVariable Long id) {
        return spajStatementLetterService.getSpajStatementLetterById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<SpajStatementLetterDto> addSpajStatementLetter (@RequestBody CreateSpajStatementLetter newSpajStatementLetter) {
        return spajStatementLetterService.addSpajStatementLetter(newSpajStatementLetter);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<SpajStatementLetterDto> editSpajStatementLetter(@RequestBody CreateSpajStatementLetter updateSpajStatementLetter, @PathVariable Long id) {
        return spajStatementLetterService.editSpajStatementLetter(updateSpajStatementLetter, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<SpajStatementLetterDto> deleteSpajStatementLetter(@PathVariable Long id) {
        return spajStatementLetterService.deleteSpajStatementLetter(id);
    }
}