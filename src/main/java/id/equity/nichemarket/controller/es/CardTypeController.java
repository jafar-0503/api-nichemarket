package id.equity.nichemarket.controller.es;

import java.util.List;


import id.equity.nichemarket.config.response.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.es.CardType.CardTypeDto;
import id.equity.nichemarket.dto.es.CardType.CreateCardType;
import id.equity.nichemarket.service.es.CardTypeService;
@RestController
@RequestMapping("api/v1/card-types")
public class CardTypeController {
	
	@Autowired
	private CardTypeService cardTypeService;
	
	//get All CardType
	@GetMapping
	public ResponseEntity<List<CardTypeDto>> listCardType(){
		return cardTypeService.listCardType();
	}
	
	//Get CardType By Id
	@GetMapping("{id}")
	public ResponseEntity<CardTypeDto> gettCardTypeById(@PathVariable Long id) {
		return cardTypeService.getCardTypeById(id);
	}
	
	//Post CardType
	@PostMapping
	public ResponseEntity<CardTypeDto> addCardType(@RequestBody CreateCardType newCardType) {
		return cardTypeService.addCardType(newCardType);
	}
	
	//Put CardType
	@PutMapping("{id}")
	public ResponseEntity<CardTypeDto> editCardType(@RequestBody CreateCardType updateCardType, @PathVariable Long id) {
		return cardTypeService.editCardType(updateCardType, id);
	}
	
	//Delete CardType
	@DeleteMapping("{id}")
	public ResponseEntity<CardTypeDto> deleteCardType(@PathVariable Long id) {
		return cardTypeService.deleteCardType(id);
	}
}