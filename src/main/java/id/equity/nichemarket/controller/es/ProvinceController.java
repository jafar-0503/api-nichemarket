package id.equity.nichemarket.controller.es;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.Province.CreateProvince;
import id.equity.nichemarket.dto.es.Province.ProvinceDto;
import id.equity.nichemarket.service.es.ProvinceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/provincies")
public class ProvinceController {
    @Autowired
    private ProvinceService provinceService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<ProvinceDto>> listProvince(){
        return provinceService.listProvince();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<ProvinceDto> getProvinceById(@PathVariable Long id) {
        return provinceService.getProvinceById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<ProvinceDto> addProvince (@RequestBody CreateProvince newProvince) {
        return provinceService.addProvince(newProvince);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<ProvinceDto> editProvince(@RequestBody CreateProvince updateProvince, @PathVariable Long id) {
        return provinceService.editProvince(updateProvince, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<ProvinceDto> deleteProvince(@PathVariable Long id) {
        return provinceService.deleteProvince(id);
    }
}