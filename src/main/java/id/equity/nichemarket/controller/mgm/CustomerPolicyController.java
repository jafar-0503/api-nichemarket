package id.equity.nichemarket.controller.mgm;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.mgm.customer.CustomerDetailPolicyDto;
import id.equity.nichemarket.dto.mgm.customerRegistration.CustomerCertificateReqDto;
import id.equity.nichemarket.dto.mgm.registration.RegistrationDto;
import id.equity.nichemarket.retrofit.mgm.freeCovid.FreeCovidCertificateResponse;
import id.equity.nichemarket.service.mgm.CustomerPolicyService;

@RestController
@RequestMapping("api/v1/customer-policies")
public class CustomerPolicyController {
	@Autowired
	private CustomerPolicyService customerPolicyService;
	
	@PostMapping("/certificate")
	public ResponseEntity<FreeCovidCertificateResponse> generateCertificate(@Valid @RequestBody CustomerCertificateReqDto customerRegistrationReq){
		return customerPolicyService.generateCertificateByCustomerCode(customerRegistrationReq);	
	}
	
	@PostMapping("/detail-policy")
	public ResponseEntity<List<CustomerDetailPolicyDto>> getAllPolicyByRegistrationCode(@RequestBody RegistrationDto registration){
		return customerPolicyService.getAllPolicyByRegistrationCode(registration.getRegistrationCode());
	}
	
}
