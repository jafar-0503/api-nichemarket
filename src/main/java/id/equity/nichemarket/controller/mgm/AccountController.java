package id.equity.nichemarket.controller.mgm;

import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.config.ftp.FtpConfig;
import id.equity.nichemarket.config.response.ErrorBaseResponse;
import id.equity.nichemarket.config.response.NewErrorBaseResponse;
import id.equity.nichemarket.dto.mgm.account.AccountDetailPolicyResponseDto;
import id.equity.nichemarket.dto.mgm.account.AccountDto;
import id.equity.nichemarket.dto.mgm.account.AccountPointResponseDto;
import id.equity.nichemarket.dto.mgm.account.AccountProfileResponseDto;
import id.equity.nichemarket.dto.mgm.account.RedeemationPointRequestDto;
import id.equity.nichemarket.dto.mgm.account.RedeemationPointResponseDto;
import id.equity.nichemarket.dto.mgm.account.ValidationRedeemResponseDto;
import id.equity.nichemarket.dto.mgm.gift.GiftDto;
import id.equity.nichemarket.service.mgm.AccountService;
import id.equity.nichemarket.service.mgm.GiftService;
import id.equity.nichemarket.service.mgm.RedeemService;
import id.equity.nichemarket.service.mgm.ReportService;

@RestController
@RequestMapping("api/v1/account")
public class AccountController {
	
	
    @Value("${file.upload-dir:}")
	private String tmpDirectory;
    
    @Value("${ftp.remote.directory}")
    private String ftpRemoteDir;
	
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private GiftService giftService;
	
	@Autowired
	private ReportService reportService;
		
    @Autowired
    private FtpConfig.MyGateway myGateway;

	@Autowired
	private RedeemService redeemService;
	
	@PostMapping("/policy")
	public ResponseEntity<List<AccountDetailPolicyResponseDto>> getAccountPolicy(@RequestBody AccountDto accountDto){
		return accountService.getListAccountPolicy(accountDto.getEmailAddress());
	}
	
	@PostMapping("/profile")
	public ResponseEntity<AccountProfileResponseDto> getAccountProfile(@RequestBody AccountDto accountDto){
		return accountService.getAccountProfile(accountDto.getEmailAddress());
	}
		
	
	@PostMapping("/point")
	public ResponseEntity<AccountPointResponseDto> getAccountPoint(@RequestBody AccountDto accountDto){
		return accountService.getAccountPoint(accountDto.getEmailAddress());
	}
	
	@GetMapping("/gift")
	public ResponseEntity<List<GiftDto>> getAllGift(){
		return giftService.listGift();
	}
	
	
	@PostMapping("/redeem")
	public  ResponseEntity<RedeemationPointResponseDto> redeemPoint(@RequestBody RedeemationPointRequestDto redeemationPoint){
		return redeemService.redeem(redeemationPoint);
	} 
	
	@PostMapping("/validateRedeem")
	public  ResponseEntity<ValidationRedeemResponseDto> validateRedeem(@RequestBody RedeemationPointRequestDto redeemationPoint){
		return redeemService.validateRedeem(redeemationPoint);
	} 
	
	@GetMapping("/gen")
	public String generateRandom() {
		 AtomicLong instance = new AtomicLong(System.currentTimeMillis());		    
	     System.out.println(instance.incrementAndGet());	     
	     return "";
	}
	
	//New Error Feature
	@GetMapping("/errors")
	public ResponseEntity<?> generateErrors() {
		Map<String, Object> fielderror = new HashMap<>();
        
        fielderror.put("name", "name is null");
        fielderror.put("test", "test is null");
        
        NewErrorBaseResponse errorResponse = new NewErrorBaseResponse(false, null, "", fielderror);

        
        return ResponseEntity.status(400).body(errorResponse);
		
	}
	
    //Function for create daily report to stakeholder
	@GetMapping("/report")
	public ResponseEntity<?> generateReport() {
		
		String fileLocation = tmpDirectory + "/DATA_PRODUCTION.xlsx";
		
		//Generate excel file
		boolean isCreated = reportService.createDailyProductionReport(fileLocation);
		
		if(isCreated) {
			//Send to FTP
			try {
				myGateway.sendToFtp(new File(fileLocation),
		                   ftpRemoteDir
                           + "DailyReport"
		                   + "/");
			} catch (Exception e) {
				System.out.println("error upload ftp");
				e.printStackTrace();
			}
			 
			
			//Send Email via API Notification			
			
		}
		return ResponseEntity.ok("");		
	}
	
	
	
	
}
