package id.equity.nichemarket.controller.mgm;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.mgm.relationship.RelationshipDto;
import id.equity.nichemarket.service.mgm.RelationshipService;

@RestController
@RequestMapping("api/v1/relationships")
public class RelationshipController {

	@Autowired
	private RelationshipService relationshipService;
	
	//GET ALL DATA
	@GetMapping
	public ResponseEntity<List<RelationshipDto>> listRelationship(){
		return relationshipService.listRelationship();
	}

	//GET DATA BY ID
	@GetMapping("id")
	public ResponseEntity<RelationshipDto> getRelationshipById(@PathVariable Long id) {
		return relationshipService.getRelationshipById(id);
	}

	
	//POST DATA
	@PostMapping
	public ResponseEntity<RelationshipDto> addRelationship(@RequestBody RelationshipDto newRelationship) {
		return relationshipService.addRelationship(newRelationship);
	}
	

	
	//PUT DATA
	@PutMapping("{id}")
	public ResponseEntity<RelationshipDto> editRelationship(@RequestBody RelationshipDto updateRelationship, @PathVariable Long id) {
		return relationshipService.editRelationship(updateRelationship, id);
	}
	
	//DETELE DATA
	@DeleteMapping("{id}")
	public ResponseEntity<RelationshipDto> deleteRelationship(@PathVariable Long id) {
		return relationshipService.deleteRelationship(id);
	}	
}