package id.equity.nichemarket.controller.mgm;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.mgm.payment.PaymentDto;
import id.equity.nichemarket.dto.mgm.payment.PaymentRequestDto;
import id.equity.nichemarket.dto.mgm.payment.PaymentSuccessResponseDto;
import id.equity.nichemarket.service.mgm.PaymentService;

@RestController
@RequestMapping("api/v1/payments")
public class PaymentController {

	@Autowired
	private PaymentService paymentService;
	
	//GET ALL DATA
	@GetMapping
	public ResponseEntity<List<PaymentDto>> listPayment(){
		return paymentService.listPayment();
	}

	//GET DATA BY ID
	@GetMapping("id")
	public ResponseEntity<PaymentDto> getPaymentById(@PathVariable Long id) {
		return paymentService.getPaymentById(id);
	}

	
	//POST DATA
	@PostMapping
	public ResponseEntity<PaymentSuccessResponseDto> addPayment(@Valid @RequestBody PaymentRequestDto newPayment) throws Exception {
		return paymentService.addPayment(newPayment);
	}
	
	
	//PUT DATA
	@PutMapping("{id}")
	public ResponseEntity<PaymentDto> editPayment(@RequestBody PaymentDto updatePayment, @PathVariable Long id) {
		return paymentService.editPayment(updatePayment, id);
	}
	
	//DETELE DATA
	@DeleteMapping("{id}")
	public ResponseEntity<PaymentDto> deletePayment(@PathVariable Long id) {
		return paymentService.deletePayment(id);
	}	
}