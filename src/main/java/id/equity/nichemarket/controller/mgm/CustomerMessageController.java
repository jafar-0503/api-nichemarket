package id.equity.nichemarket.controller.mgm;

import id.equity.nichemarket.dto.mgm.customerMessage.CreateCustomerMessage;
import id.equity.nichemarket.dto.mgm.customerMessage.CustomerMessageDto;
import id.equity.nichemarket.service.mgm.CustomerMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;
import java.util.HashMap;

@RestController
@RequestMapping("api/v1/customer-messages")
public class CustomerMessageController {
    @Autowired
    private CustomerMessageService customerMessageService;

    @PostMapping()
    @Transactional
    public ResponseEntity addCustomerMessage(@RequestBody CreateCustomerMessage createCustomerMessage){
        CustomerMessageDto customerMessageDto = customerMessageService.addCustomerMessage(createCustomerMessage);
        HashMap<String, String> data = new HashMap<>();
        data.put("email_address", customerMessageDto.getEmailAddress());
        data.put("variable_name_1", customerMessageDto.getCustomerName());

        return ResponseEntity.ok(data);
    }

}
