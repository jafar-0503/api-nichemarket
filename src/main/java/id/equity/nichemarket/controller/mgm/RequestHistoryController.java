package id.equity.nichemarket.controller.mgm;

import id.equity.nichemarket.dto.mgm.customer.CustomerDto;
import id.equity.nichemarket.dto.mgm.customerReference.CustomerReferenceDto;
import id.equity.nichemarket.dto.mgm.requestHistory.CreateRequestHistory;
import id.equity.nichemarket.dto.mgm.requestHistory.RequestHistoryDto;
import id.equity.nichemarket.exception.ErrorsException;
import id.equity.nichemarket.retrofit.mgm.EmailNotificationResponse;
import id.equity.nichemarket.retrofit.mgm.freeCovid.FreeCovidResponse;
import id.equity.nichemarket.service.mgm.CustomerReferenceService;
import id.equity.nichemarket.service.mgm.CustomerService;
import id.equity.nichemarket.service.mgm.RequestHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("api/v1/request-histories")
public class RequestHistoryController {
    @Autowired
    private RequestHistoryService requestHistoryService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private CustomerReferenceService customerReferenceService;

    @PostMapping("generate-otp")
    public ResponseEntity requestOtp(@RequestParam(name = "email", defaultValue = "") String email,
                                     @RequestParam(name = "phone", defaultValue = "") String phoneNo,
                                     @RequestParam(name = "key", defaultValue = "") String key) {
        CustomerReferenceDto checkUniqueKey = customerReferenceService.checkByUniqueKey(key);
        if (checkUniqueKey == null){
            throw new ErrorsException("Unique Key is not exist");
        }
        RequestHistoryDto otpData = requestHistoryService.requestOtp(checkUniqueKey.getCustomerReferenceCode());
        CustomerDto checkCustomer = customerService.getCustomerByEmailAndCodeOrPhoneAndCode(checkUniqueKey.getCustomerCode(), email, phoneNo);

        EmailNotificationResponse sendNotif = requestHistoryService.sendOtp(checkCustomer, otpData);
        String responseCode = sendNotif != null ? sendNotif.getError_code() : "";
        if (!responseCode.equals("6801")){
            return ResponseEntity.ok(null);
        }
        HashMap<String, String> data = new HashMap<>();
        data.put("email", checkCustomer.getEmailAddress());
        data.put("phone", checkCustomer.getPhoneNo());
        data.put("otp", "Generated");

        return ResponseEntity.ok(data);
    }

    @PostMapping("verify-otp")
    public ResponseEntity verifyOtp(@RequestParam(name = "otp") String otp){
        RequestHistoryDto otpData = requestHistoryService.checkOtp(otp);
        CustomerDto customerDto = customerReferenceService.getCustomer(otpData.getCustomerReferenceCode());
        FreeCovidResponse freeCovidResponse =  requestHistoryService.registerCertificate(customerDto, otpData.getCustomerReferenceCode());

        if (otpData == null){
            return ResponseEntity.ok(null);
        }
        return ResponseEntity.ok(otpData);
    }

    @GetMapping
    public ResponseEntity<List<RequestHistoryDto>> listRequestHistory(){
        return requestHistoryService.listRequestHistory();
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<RequestHistoryDto> addRequestHistory(@RequestBody CreateRequestHistory createRequestHistory) {
        return requestHistoryService.addRequestHistory(createRequestHistory);
    }

}
