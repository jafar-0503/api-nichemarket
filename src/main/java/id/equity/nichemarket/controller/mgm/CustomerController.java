package id.equity.nichemarket.controller.mgm;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.groups.Default;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.mgm.CustomerValidationDto;
import id.equity.nichemarket.dto.mgm.account.AccountActivationDto;
import id.equity.nichemarket.dto.mgm.account.AccountActivationResponseDto;
import id.equity.nichemarket.dto.mgm.account.AccountValidationResponseDto;
import id.equity.nichemarket.dto.mgm.activationKey.ActivationKeyDto;
import id.equity.nichemarket.dto.mgm.customer.CreateCustomer;
import id.equity.nichemarket.dto.mgm.customer.CustomerAgeValidationDto;
import id.equity.nichemarket.dto.mgm.customer.CustomerCoreDataDto;
import id.equity.nichemarket.dto.mgm.customer.CustomerDto;
import id.equity.nichemarket.dto.mgm.customer.CustomerEmailValidationDto;
import id.equity.nichemarket.dto.mgm.customerRegistration.CustomerRegistrationDto;
import id.equity.nichemarket.dto.mgm.customerRegistration.CustomerRegistrationResponseDto;
import id.equity.nichemarket.exception.CustomerValidationException;
import id.equity.nichemarket.exception.ErrorsException;
import id.equity.nichemarket.retrofit.mgm.freeCovid.BaseFreeCovidResponse;
import id.equity.nichemarket.retrofit.mgm.freeCovid.FreeCovidService;
import id.equity.nichemarket.service.mgm.AccountService;
import id.equity.nichemarket.service.mgm.CustomerService;
import id.equity.nichemarket.service.mgm.GroupInsuranceService;
import id.equity.nichemarket.service.mgm.ProductPlanPolicyService;
import id.equity.nichemarket.service.mgm.ProductPolicyService;
import id.equity.nichemarket.service.mgm.RegistrationTypeService;
import id.equity.nichemarket.service.mgm.SiMedisValidatorService;
import id.equity.nichemarket.validation.groups.WhatsappSimedisRegistration;

@RestController
@RequestMapping("api/v1/customers")
public class CustomerController {

	@Autowired
    private FreeCovidService freeCovidService;
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private AccountService accountService;
	

	@Autowired
	private RegistrationTypeService registrationTypeService;	

	@Autowired
	private ProductPolicyService productPolicyService;
	
	@Autowired
	private GroupInsuranceService groupInsuranceService;
	
	@Autowired
	private ProductPlanPolicyService productPlanPolicyService;
	
	@Autowired
	private SiMedisValidatorService siMedisValidatorService;
	
    private static final Logger logger = LoggerFactory.getLogger(CustomerController.class);

	
	//GET ALL DATA
	@GetMapping
	public ResponseEntity<List<CustomerDto>> listCustomer(){
		return customerService.listCustomer();
	}

	//GET DATA BY ID
	@GetMapping("id")
	public ResponseEntity<CustomerDto> getCustomerById(@PathVariable Long id) {
		return customerService.getCustomerById(id);
	}

//	//GET DATA BY UNIQUE KEY
//	@GetMapping("{key}")
//	public ResponseEntity<CustomerDto> getCustomerByKey(@PathVariable String key){
//		return customerService.getCustomerByKey(key);
//	}

	//GET DATA BY PARENT CUSTOMER CODE
//	@GetMapping("{refCustomerCode}")
//	public ResponseEntity<List<CustomerDto>> getCustomerByRefCustomerCode(@PathVariable String refCustomerCode){
//		return customerService.getCustomerByRefCustomerCode(refCustomerCode);
//	}
	
	//GET DATA BY PARENT CUSTOMER CODE
	@GetMapping("{customerCode}")
	public ResponseEntity<CustomerDto> getCustomerByCustomerCode(@PathVariable String customerCode){
		return customerService.getCustomerByCustomerCode(customerCode);
	}
	
	//POST DATA
	@PostMapping
	public ResponseEntity<CustomerDto> addCustomer(@RequestBody CreateCustomer newCustomer) {
		return customerService.addCustomer(newCustomer);
	}
	
	@PostMapping("/register")
	public ResponseEntity<CustomerRegistrationResponseDto> registerCustomer(@Validated({WhatsappSimedisRegistration.class}) @RequestBody CustomerRegistrationDto newCustomer){
		return customerService.registerCustomer(newCustomer);
	}
	
	@PostMapping("/activate")
	public ResponseEntity<AccountActivationResponseDto> activateAccount(@RequestBody AccountActivationDto newAccount){
		return accountService.activateAccount(newAccount);
	}
	
	@PostMapping("/validateKey")
	public ResponseEntity<AccountValidationResponseDto> validateActivationKey(@RequestBody ActivationKeyDto activationKey){
		return accountService.validateActivationKey(activationKey.getUniqueActivationKey());
	}
	
	//PUT DATA
	@PutMapping("{id}")
	public ResponseEntity<CustomerDto> editCustomer(@RequestBody CreateCustomer updateCustomer, @PathVariable Long id) {
		return customerService.editCustomer(updateCustomer, id);
	}
	
	//DETELE DATA
	@DeleteMapping("{id}")
	public ResponseEntity<CustomerDto> deleteInvest(@PathVariable Long id) {
		return customerService.deleteCustomer(id);
	}	
	
	
	@PostMapping("/validate/age")
	public ResponseEntity<?> validateAge(@RequestBody CustomerAgeValidationDto customerAgeValidation){
		boolean isValid = siMedisValidatorService.isValidAge(customerAgeValidation);
		if(!isValid) {
			throw new ErrorsException("Age Is Not Valid");
		}
		return ResponseEntity.ok("");
	}
	
	@PostMapping("/validate")
	public ResponseEntity<?> validate(@Valid @RequestBody CustomerValidationDto customerValidation){
		return siMedisValidatorService.validateKtpNoAndDob(customerValidation);				
	}
	
	
	@PostMapping("/validate/email")
	public ResponseEntity<?> validateEmail(@Valid @RequestBody CustomerEmailValidationDto customerEmailValidation){
		boolean isNotDuplicateEmailAddress = siMedisValidatorService.isNotDuplicateEmailAddress(customerEmailValidation.getEmailAddress());				
		ArrayList<String> errors = new ArrayList<>();
		if(!isNotDuplicateEmailAddress) {
			errors.add("Email Address Already Registered");
			throw new CustomerValidationException("Invalid Data" , errors);
		}
		return ResponseEntity.ok("");
	}
	
	@PostMapping("/resync")
	public ResponseEntity<List<BaseFreeCovidResponse>> resync(@RequestBody List<CustomerCoreDataDto> customerCoreDataList){
		List<BaseFreeCovidResponse> response = groupInsuranceService.resyncToCore(customerCoreDataList);
		return ResponseEntity.ok(response);
	} 
	
}