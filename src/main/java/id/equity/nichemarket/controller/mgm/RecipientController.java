package id.equity.nichemarket.controller.mgm;

import id.equity.nichemarket.dto.mgm.recipient.CreateRecipient;
import id.equity.nichemarket.dto.mgm.recipient.RecipientDto;
import id.equity.nichemarket.service.mgm.RecipientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/recipients")
public class RecipientController {

	@Autowired
	private RecipientService recipientService;
	
	//GET ALL DATA
	@GetMapping
	public ResponseEntity<List<RecipientDto>> listRecipient(){
		return recipientService.listRecipient();
	}

	//GET DATA BY ID
	@GetMapping("{id}")
	public ResponseEntity<RecipientDto> getRecipientById(@PathVariable Long id) {
		return recipientService.getRecipientById(id);
	}
	
	//POST DATA
	@PostMapping
	public ResponseEntity<RecipientDto> addRecipient(@RequestBody CreateRecipient newRecipient) {
		return recipientService.addRecipient(newRecipient);
	}
	
	//PUT DATA
	@PutMapping("{id}")
	public ResponseEntity<RecipientDto> editRecipient(@RequestBody CreateRecipient updateRecipient, @PathVariable Long id) {
		return recipientService.editRecipient(updateRecipient, id);
	}
	
	//DETELE DATA
	@DeleteMapping("{id}")
	public ResponseEntity<RecipientDto> deleteInvest(@PathVariable Long id) {
		return recipientService.deleteRecipient(id);
	}	
}