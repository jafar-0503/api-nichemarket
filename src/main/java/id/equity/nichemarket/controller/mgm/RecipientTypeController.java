package id.equity.nichemarket.controller.mgm;

import id.equity.nichemarket.dto.mgm.recipientType.CreateRecipientType;
import id.equity.nichemarket.dto.mgm.recipientType.RecipientTypeDto;
import id.equity.nichemarket.service.mgm.RecipientTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("api/v1/recipient-types")
public class RecipientTypeController {

	@Autowired
	private RecipientTypeService recipientTypeService;
	
	//GET ALL DATA
	@GetMapping
	public ResponseEntity<List<RecipientTypeDto>> listRecipientType(){
		return recipientTypeService.listRecipientType();
	}

	//GET DATA BY ID
	@GetMapping("{id}")
	public ResponseEntity<RecipientTypeDto> getRecipientTypeById(@PathVariable Long id) {
		return recipientTypeService.getRecipientTypeById(id);
	}
	
	//POST DATA
	@PostMapping
	public ResponseEntity<RecipientTypeDto> addRecipientType(@RequestBody CreateRecipientType newRecipientType) {
		return recipientTypeService.addRecipientType(newRecipientType);
	}
	
	//PUT DATA
	@PutMapping("{id}")
	public ResponseEntity<RecipientTypeDto> editRecipientType(@RequestBody CreateRecipientType updateRecipientType, @PathVariable Long id) {
		return recipientTypeService.editRecipientType(updateRecipientType, id);
	}
	
	//DETELE DATA
	@DeleteMapping("{id}")
	public ResponseEntity<RecipientTypeDto> deleteInvest(@PathVariable Long id) {
		return recipientTypeService.deleteRecipientType(id);
	}	
}