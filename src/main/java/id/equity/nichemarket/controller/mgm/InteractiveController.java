package id.equity.nichemarket.controller.mgm;

import id.equity.nichemarket.dto.mgm.customerPolicy.CustomerPolicyDto;
import id.equity.nichemarket.dto.mgm.interactive.CreateInteractiveNeedAndPriority;
import id.equity.nichemarket.exception.ErrorsException;
import id.equity.nichemarket.retrofit.mgm.freeCovid.FreeCovidCertificateResponse;
import id.equity.nichemarket.service.mgm.CustomerPolicyService;
import id.equity.nichemarket.service.mgm.InteractiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("api/v1/interactive")
public class InteractiveController {
    @Autowired
    private InteractiveService interactiveService;
    @Autowired
    private CustomerPolicyService customerPolicyService;

    @PostMapping()
    public ResponseEntity sendEmail(@RequestBody CreateInteractiveNeedAndPriority createInteractiveNeedAndPriority){
        interactiveService.sendEmailNotification(
                createInteractiveNeedAndPriority, createInteractiveNeedAndPriority.getCustomerPriority());
        interactiveService.savingData(createInteractiveNeedAndPriority);

        HashMap<String, String> data = new HashMap<>();
        data.put("email", createInteractiveNeedAndPriority.getEmailAddress());
        data.put("name", createInteractiveNeedAndPriority.getCustomerName());
        data.put("phone", createInteractiveNeedAndPriority.getPhoneNo());

        return ResponseEntity.ok(data);
    }

    @PostMapping("covid-certificate")
    public ResponseEntity getCertificate(@RequestParam(name = "referenceCode") String referencCode){
    	//Get  Customer Policy Info
        CustomerPolicyDto customerPolicyDto = customerPolicyService.checkCustomerPolicy(referencCode);
        //Generate Certificate Based On Customer Policy Info
        FreeCovidCertificateResponse freeCovidResponse = customerPolicyService.getCovidCertificate(customerPolicyDto);

        if (freeCovidResponse == null){
            throw new ErrorsException("Failed to get e-certificate");
        }

        return ResponseEntity.ok().body(freeCovidResponse.getData());

    }
}
