package id.equity.nichemarket.controller.mgm;

import id.equity.nichemarket.dto.mgm.logMessage.CreateLogMessage;
import id.equity.nichemarket.dto.mgm.logMessage.LogMessageDto;
import id.equity.nichemarket.service.mgm.LogMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("api/v1/log-messages")
public class LogMessageController {

	@Autowired
	private LogMessageService logMessageService;
	
	//GET ALL DATA
	@GetMapping
	public ResponseEntity<List<LogMessageDto>> listLogMessage(){
		return logMessageService.listLogMessage();
	}

	//GET DATA BY ID
	@GetMapping("{id}")
	public ResponseEntity<LogMessageDto> getLogMessageById(@PathVariable Long id) {
		return logMessageService.getLogMessageById(id);
	}
	
	//POST DATA
	@PostMapping
	public ResponseEntity<LogMessageDto> addLogMessage(@RequestBody CreateLogMessage newLogMessage) {
		return logMessageService.addLogMessage(newLogMessage);
	}
	
	//PUT DATA
	@PutMapping("{id}")
	public ResponseEntity<LogMessageDto> editLogMessage(@RequestBody CreateLogMessage updateLogMessage, @PathVariable Long id) {
		return logMessageService.editLogMessage(updateLogMessage, id);
	}
	
	//DETELE DATA
	@DeleteMapping("{id}")
	public ResponseEntity<LogMessageDto> deleteInvest(@PathVariable Long id) {
		return logMessageService.deleteLogMessage(id);
	}	
}