package id.equity.nichemarket.controller.mgm;

import id.equity.nichemarket.dto.mgm.customerReference.CustomerReferenceDto;
import id.equity.nichemarket.service.mgm.CustomerReferenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/v1/customer-references")
public class CustomerReferenceController {

    @Autowired
    private CustomerReferenceService customerReferenceService;

    @GetMapping()
    public ResponseEntity<List<CustomerReferenceDto>> listCustomerReference(){
        return customerReferenceService.listCustomerReference();
    }
}
