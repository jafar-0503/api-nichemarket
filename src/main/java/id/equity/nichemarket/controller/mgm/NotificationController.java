package id.equity.nichemarket.controller.mgm;

import id.equity.nichemarket.dto.mgm.dataFromLandingPage.DataFromLandingPage;
import id.equity.nichemarket.dto.mgm.dataFromLandingPage.DataFromLandingPage2;
import id.equity.nichemarket.retrofit.mgm.EmailNotificationResponse;
import id.equity.nichemarket.service.mgm.LandingPageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@RequestMapping("api/v1/mgm")
public class NotificationController {
    @Autowired
    private LandingPageService landingPageService;
    @PostMapping("email-notifications")
    public ResponseEntity createEmailJson(@RequestBody DataFromLandingPage dataFromLandingPage) throws Exception {
        //Saving data to table
        String customerCode = landingPageService.savingDataFromLandingPage(dataFromLandingPage);
        if (customerCode == null){
            throw new Exception("Failed to generate customer code");
        }

        //Code to Send Email to Customer
        //Need to update value to send to API email notifications core
        EmailNotificationResponse sendingEmailNotif = landingPageService.sendEmail(dataFromLandingPage, customerCode);
        String responseCode = sendingEmailNotif != null ? sendingEmailNotif.getError_code() : "";

        HashMap<String, String> data = new HashMap<>();
        data.put("email_address", dataFromLandingPage.getEmailAddress());
        data.put("variable_name_1", dataFromLandingPage.getCustomerName());

        if (!responseCode.equals("2001")){
            return ResponseEntity.ok().body(null);
        }

        return ResponseEntity.ok().body(data);
    }

    //API to sending email with format exclude E24
    @PostMapping("notifications")
    public ResponseEntity sendNotif (@RequestBody DataFromLandingPage2 data){
        EmailNotificationResponse sendNotif = landingPageService.sendNotif(data, null);
        String responseCode = sendNotif != null ? sendNotif.getError_code() : "";

        String message = "";
        if (data.getMessage_type().equals("E")){
            message = "email";
        } else if (data.getMessage_type().equals("S")){
            message = "sms";
        }

        HashMap<String, String> dataMgm = new HashMap<>();
        dataMgm.put("email_address", data.getEmail_address());
        dataMgm.put("variable_name_1", data.getVariable_name_1());

        if (!responseCode.equals("2001") && !responseCode.equals("6801")){
            return ResponseEntity.ok().body(null);
        }

        return ResponseEntity.ok().body(dataMgm);
    }
}
