package id.equity.nichemarket.controller.mgm;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.mgm.gender.GenderDto;
import id.equity.nichemarket.service.mgm.GenderMgmService;

@RestController
@RequestMapping("api/v1/gender")
public class GenderMgmController {

	@Autowired
	private GenderMgmService genderService;
	
	//GET ALL DATA
	@GetMapping
	public ResponseEntity<List<GenderDto>> listGender(){
		return genderService.listGender();
	}

	//GET DATA BY ID
	@GetMapping("id")
	public ResponseEntity<GenderDto> getGenderById(@PathVariable Long id) {
		return genderService.getGenderById(id);
	}

	
	//POST DATA
	@PostMapping
	public ResponseEntity<GenderDto> addGender(@RequestBody GenderDto newGender) {
		return genderService.addGender(newGender);
	}
	

	
	//PUT DATA
	@PutMapping("{id}")
	public ResponseEntity<GenderDto> editGender(@RequestBody GenderDto updateGender, @PathVariable Long id) {
		return genderService.editGender(updateGender, id);
	}
	
	//DETELE DATA
	@DeleteMapping("{id}")
	public ResponseEntity<GenderDto> deleteGender(@PathVariable Long id) {
		return genderService.deleteGender(id);
	}	
}
