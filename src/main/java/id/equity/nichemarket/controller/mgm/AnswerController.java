package id.equity.nichemarket.controller.mgm;

import id.equity.nichemarket.dto.mgm.answer.CreateAnswer;
import id.equity.nichemarket.dto.mgm.answer.AnswerDto;
import id.equity.nichemarket.service.mgm.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/answers")
public class AnswerController {

	@Autowired
	private AnswerService answerService;
	
	//GET ALL DATA
	@GetMapping
	public ResponseEntity<List<AnswerDto>> listAnswer(){
		return answerService.listAnswer();
	}

	//GET DATA BY ID
	@GetMapping("{id}")
	public ResponseEntity<AnswerDto> getAnswerById(@PathVariable Long id) {
		return answerService.getAnswerById(id);
	}
	
	//POST DATA
	@PostMapping
	public ResponseEntity<AnswerDto> addAnswer(@RequestBody CreateAnswer newAnswer) {
		return answerService.addAnswer(newAnswer);
	}
	
	//PUT DATA
	@PutMapping("{id}")
	public ResponseEntity<AnswerDto> editAnswer(@RequestBody CreateAnswer updateAnswer, @PathVariable Long id) {
		return answerService.editAnswer(updateAnswer, id);
	}
	
	//DETELE DATA
	@DeleteMapping("{id}")
	public ResponseEntity<AnswerDto> deleteInvest(@PathVariable Long id) {
		return answerService.deleteAnswer(id);
	}	
}