package id.equity.nichemarket.controller.mgm;

import id.equity.nichemarket.dto.mgm.tAnswer.CreateTAnswer;
import id.equity.nichemarket.dto.mgm.tAnswer.TAnswerDto;
import id.equity.nichemarket.service.mgm.TAnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/t-answers")
public class TAnswerController {

	@Autowired
	private TAnswerService tAnswerService;
	
	//GET ALL DATA
	@GetMapping
	public ResponseEntity<List<TAnswerDto>> listTAnswer(){
		return tAnswerService.listTAnswer();
	}

	//GET DATA BY ID
	@GetMapping("{id}")
	public ResponseEntity<TAnswerDto> getTAnswerById(@PathVariable Long id) {
		return tAnswerService.getTAnswerById(id);
	}
	
	//POST DATA
	@PostMapping
	public ResponseEntity<TAnswerDto> addTAnswer(@RequestBody CreateTAnswer newTAnswer) {
		return tAnswerService.addTAnswer(newTAnswer);
	}
	
	//PUT DATA
	@PutMapping("{id}")
	public ResponseEntity<TAnswerDto> editTAnswer(@RequestBody CreateTAnswer updateTAnswer, @PathVariable Long id) {
		return tAnswerService.editTAnswer(updateTAnswer, id);
	}
	
	//DETELE DATA
	@DeleteMapping("{id}")
	public ResponseEntity<TAnswerDto> deleteInvest(@PathVariable Long id) {
		return tAnswerService.deleteTAnswer(id);
	}	
}