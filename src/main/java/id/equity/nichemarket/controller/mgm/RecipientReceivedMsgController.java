package id.equity.nichemarket.controller.mgm;

import id.equity.nichemarket.dto.mgm.recipientReceivedMsg.CreateRecipientReceivedMsg;
import id.equity.nichemarket.model.mgm.RecipientReceivedMsg;
import id.equity.nichemarket.service.mgm.RecipientReceivedMsgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/recipient-received-msg")
public class RecipientReceivedMsgController {
    @Autowired
    private RecipientReceivedMsgService recipientReceivedMsgService;

    @PostMapping()
    public ResponseEntity addRecipientReceived(@RequestBody CreateRecipientReceivedMsg createRecipientReceivedMsg){
        return recipientReceivedMsgService.addRecipientReceived(createRecipientReceivedMsg);
    }
}
