package id.equity.nichemarket.exception;


import java.util.List;

import lombok.Data;

@Data
public class APICoreException extends RuntimeException{
	private List<String> errors;
    public APICoreException(String message) {
        super(message);
    }
    
    public APICoreException(String message, List<String> errors) {
        super(message);
        this.errors = errors;
    }

    public APICoreException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public APICoreException(String message, Throwable cause, List<String> errors) {
        super(message, cause);
        this.errors = errors;
    }
}
