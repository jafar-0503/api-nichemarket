package id.equity.nichemarket.exception;

import java.util.List;

import lombok.Data;

@Data
public class CustomerRegistrationException extends RuntimeException {
	private List<String> errors;
    public CustomerRegistrationException(String message) {
        super(message);
    }
    
    public CustomerRegistrationException(String message, List<String> errors) {
        super(message);
        this.errors = errors;
    }

    public CustomerRegistrationException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public CustomerRegistrationException(String message, Throwable cause, List<String> errors) {
        super(message, cause);
        this.errors = errors;
    }
}
