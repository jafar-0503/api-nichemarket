package id.equity.nichemarket.exception;

import java.util.List;

import lombok.Data;

@Data
public class CustomerValidationException extends RuntimeException {
	private List<String> errors;
	
	 public CustomerValidationException(String message) {
        super(message);        
     }
	
	 public CustomerValidationException(String message, List<String> errors) {
	    super(message);        
	    this.errors = errors;
	}

    public CustomerValidationException(String message, Throwable cause) {
        super(message, cause);
    }
}



