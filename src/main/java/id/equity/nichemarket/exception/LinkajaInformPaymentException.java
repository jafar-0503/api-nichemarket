package id.equity.nichemarket.exception;

import lombok.Data;

@Data
public class LinkajaInformPaymentException extends RuntimeException{
	private String status;
    private String message;
    private String partnerTrxID;
    private String linkAjaRefNum;
    
    public LinkajaInformPaymentException(String status, String message, String partnerTrxID, String linkAjaRefNum) {
    	super();
        this.status = status;
        this.message = message;
        this.partnerTrxID = partnerTrxID;
        this.linkAjaRefNum = linkAjaRefNum;
    }
    
    public LinkajaInformPaymentException() {
    	
    }
}
