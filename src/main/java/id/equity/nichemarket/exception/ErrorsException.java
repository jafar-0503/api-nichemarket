package id.equity.nichemarket.exception;

import java.util.List;

import lombok.Data;

@Data
public class ErrorsException extends RuntimeException{
	private List<String> errors;
    public ErrorsException(String message) {
        super(message);
    }
    
    public ErrorsException(String message, List<String> errors) {
        super(message);
        this.errors = errors;
    }

    public ErrorsException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public ErrorsException(String message, Throwable cause, List<String> errors) {
        super(message, cause);
        this.errors = errors;
    }
}
