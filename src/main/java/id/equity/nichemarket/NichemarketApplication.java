package id.equity.nichemarket;

import javax.sql.DataSource;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;

import id.equity.nichemarket.property.FileStorageProperties;
import id.equity.nichemarket.repository.mgm.CustomRepositoryImpl;

@SpringBootApplication
@EnableConfigurationProperties({FileStorageProperties.class})
@IntegrationComponentScan
@EnableIntegration
@EnableJpaRepositories(repositoryBaseClass = CustomRepositoryImpl.class)

public class NichemarketApplication implements CommandLineRunner{

    @Autowired
    DataSource dataSource;


	public static void main(String[] args) {
		SpringApplication.run(NichemarketApplication.class, args);
	}
	
	 @Override
    public void run(String... args) throws Exception {
    //    Flyway.configure().baselineOnMigrate(true).dataSource(dataSource).load().migrate();
    }


}