package id.equity.nichemarket.model.rm;

import id.equity.nichemarket.audit.Auditable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name="m_role_menu")
public class RoleMenu extends Auditable<String>{
	@Column(unique = true, length = 10, nullable = false, name = "role_menu_code")
	private String roleMenuCode;

	@Column(length = 10, name = "role_code")
	private String roleCode;

	@Column(length = 10, name = "menu_code")
	private String menuCode;

	@Column(name = "is_active")
	private boolean isActive;
}
