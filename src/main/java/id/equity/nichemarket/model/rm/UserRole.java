package id.equity.nichemarket.model.rm;

import id.equity.nichemarket.audit.Auditable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name="m_user_role")
public class UserRole extends Auditable<String>{
	@Column(unique = true, length = 30, nullable = false, name = "user_role_code")
	private String userRoleCode;

	@Column(length = 30, name = "username")
	private String username;

	@Column(length = 10, name = "role_code")
	private String roleCode;

	@Column(name = "is_active")
	private boolean isActive;
}
