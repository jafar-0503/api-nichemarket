package id.equity.nichemarket.model.mgm;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_interactive")
public class Interactive extends Auditable<String> {
    @Column(nullable = false, length = 12, name = "interactive_code")
    private String interactiveCode;
    @Column(nullable = false, length = 100, name = "customer_name")
    private String customerName;
    @Column(nullable = false, length = 1, name = "gender_code")
    private String genderCode;
    @Column(nullable = false, length = 100, name = "date_of_birth")
    private String dateOfBirth;
    @Column(nullable = false, length = 100, name = "occupation")
    private String occupation;
    @Column(nullable = false, length = 50, name = "phone_no")
    private String phoneNo;
    @Column(nullable = false, length = 100, name = "email_address")
    private String emailAddress;
    @Column(nullable = false, length = 100, name = "income")
    private String income;
    @Column(nullable = false, length = 3, name = "appointment_time_code")
    private String appointmentTimeCode;
    @Column(nullable = false, name = "is_active")
    private boolean isActive;
}
