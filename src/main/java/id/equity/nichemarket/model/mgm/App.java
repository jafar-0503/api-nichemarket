package id.equity.nichemarket.model.mgm;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Entity
@Table(name = "m_app")
@Data	
public class App extends Auditable<String>{
	
	@Column(unique = true, length = 15, nullable = false, name = "app_code")
	private String appCode;		
	
	@OneToMany(mappedBy="app")
	private List<Registration> registrations;
	
	@Column(length = 50, nullable = false, name = "app_description")
	private String appDescription;	

	@Column(length = 50, nullable = true, name = "app_action_name")
	private String appActionName;
	
	@Column(nullable = false, name = "is_active")
	private boolean isActive = true;
}

