package id.equity.nichemarket.model.mgm;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_answer")
public class TAnswer extends Auditable<String> {
		
	@Column(unique = true, length = 10, nullable = false, name = "answer_code")
	private String answerCode;
	
	@Column(nullable = false, length = 15, name = "customer_code")
	private String customerCode;

	@Column(nullable = false, length = 3, name = "appointment_time_code")
	private String appointmentTimeCode;

	@Column(nullable = false, length = 60, name = "question_1")
	private String question1;

	@Column(nullable = false, length = 60, name = "question_2")
	private String question2;

	@Column(nullable = false, length = 60, name = "question_3")
	private String question3;

	@Column(nullable = false, length = 60, name = "question_4")
	private String question4;

	@Column(nullable = false, length = 60, name = "question_5")
	private String question5;

	@Column(nullable = false, length = 60, name = "question_6")
	private String question6;

	@Column(nullable = false, length = 60, name = "question_7")
	private String question7;

	@Column(nullable = false, name = "is_processed")
	private boolean isProcessed;

	@Column(nullable = true, name = "extract_date")
	private Date extractDate;
	
	@Column(nullable = false, name = "is_active")
	private boolean isActive;
}