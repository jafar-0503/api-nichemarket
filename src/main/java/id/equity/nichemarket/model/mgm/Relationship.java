package id.equity.nichemarket.model.mgm;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Entity
@Table(name = "m_relationship")
@Data	
public class Relationship extends Auditable<String>{
	@Column(unique = true,length = 1, nullable = false, name = "relationship_code")
    private String relationshipCode;

    @Column(length = 100,name = "relationship_desc")
    private String relationshipDesc;

    @Column(unique = false,length = 1, nullable = true, name = "relationship_core_code")
    private String relationshipCoreCode;
    
    @Column(nullable = false, name = "is_active")
    private boolean isActive;
}
