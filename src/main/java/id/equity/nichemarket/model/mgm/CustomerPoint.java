package id.equity.nichemarket.model.mgm;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_customer_point")
public class CustomerPoint extends Auditable<String> {
	
	@Column(unique = true, length = 15, nullable = false, name = "customerPointCode")
	private String customerPointCode;

	
	@Column(unique = false, length = 15, nullable = true, name = "account_code")
	private String accountCode;

	@Column(length=10, name = "point_in")
	private Integer pointIn = 0;

	@Column(length=10, name = "point_out")
	private Integer pointOut = 0;
	
	@Column(length=10, name = "current_point")
	private Integer currentPoint = 0;
	
	@Column(unique = false, length = 50, nullable = true, name = "description")
	private String description;
	
	@Column(nullable = false, name = "is_active")
	private boolean isActive = true;
}
