package id.equity.nichemarket.model.mgm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_customer_registration")
public class CustomerRegistration extends Auditable<String> {
	
	@Column(unique = false, length = 15, nullable = false, name = "customer_code")
	private String customerCode;
	
	@Column(unique = false, length = 15, nullable = false, name = "registration_code")
	private String registrationCode;
	
	@Column(nullable = false, name = "is_active")
	private boolean isActive = true;

}
