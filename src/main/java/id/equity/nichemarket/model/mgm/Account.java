package id.equity.nichemarket.model.mgm;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_account")
public class Account extends Auditable<String> {		

	@Column(unique = true, length = 15, nullable = false, name = "account_code")
	private String accountCode;
	
	@Column(unique = false, length = 15, nullable = false, name = "app_code")
	private String appCode;
	
	@Column(unique = false, nullable = false,length = 100, name = "email_address")
	private String emailAddress;
	
	@Column(unique = true, nullable = false, length = 30, name = "unique_activation_key")
	private String uniqueActivationKey;
	
	@Column(nullable = false, name = "is_account_activated")
	private boolean isAccountActivated = false;	
	
	@Column(nullable = false, name = "is_active")
	private boolean isActive = true;
}