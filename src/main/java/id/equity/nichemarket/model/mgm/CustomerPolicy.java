package id.equity.nichemarket.model.mgm;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_customer_policy")
public class CustomerPolicy extends Auditable<String> {
    @Column(nullable = false, unique = true, length = 12, name = "customer_policy_code")
    private String customerPolicyCode;
	@Column(unique = false, length = 15, nullable = true, name = "registration_code")
	private String registrationCode;
    @Column(nullable = false, unique = true, length = 50, name = "customer_policy_core_code")
    private String customerPolicyCoreCode;
    @Column(nullable = false, length = 12, name = "customer_reference_code")
    private String customerReferenceCode;
    @Column(nullable = false, length = 12, name = "customer_code")
    private String customerCode;
    @Column(length = 12, name = "policy_no")
    private String policyNo;
    @Column(length = 15, name = "member_no")
    private String memberNo;
    @Column(length = 100, name = "insurance_type")
    private String insuranceType;
    @Column(length = 100, name = "sum_insured_ajb")
    private String sumInsuredAjb;
    @Column(length = 60, name = "sum_insured_inpatient")
    private String sumInsuredInpatient;
    @Column(length = 60, name = "insurance_period")
    private String insurancePeriod;
    @Column(length = 20, name = "ref_no")
    private String refNo;
    @Column(length = 3, name = "file_type_code")
    private String fileTypeCode;    
    @Column(nullable = false, name = "is_active")
    private boolean isActive;
    @Column(nullable = false, name = "is_synced_to_core")
    private boolean isSyncedToCore;
}
