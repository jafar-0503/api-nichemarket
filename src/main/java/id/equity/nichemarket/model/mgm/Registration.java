package id.equity.nichemarket.model.mgm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Entity
@Data
@Table(name = "t_registration")
public class Registration extends Auditable<String>{
	
	@Column(unique = true, length = 15, nullable = false, name = "registration_code")
	private String registrationCode;
	
	@Column(unique = false, length = 15, nullable = false, name = "registration_type_code")
	private String registrationTypeCode;
	
	@ManyToOne()
	@JoinColumn(name="app_id")
	private App app;
	
	@Column(unique = false, length = 15, nullable = false, name = "app_code")
	private String appCode;
	
	@Column(unique = false, length = 15, nullable = true, name = "account_code")
	private String accountCode;
	
	
	@Column(nullable = false, name = "is_active")
	private boolean isActive = true;	
	
}
