package id.equity.nichemarket.model.mgm;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Entity
@Data
@Table(name = "m_registration_type")
public class RegistrationType extends Auditable<String>{
	
	@Column(unique = true, length = 15, nullable = false, name = "registration_type_code")
	private String registrationTypeCode;		
	
	@Column(unique = false, length = 50, nullable = false, name = "registration_type_desc")
	private String registrationDesc;

	//T_product_plan
	@Column(unique = false, length = 15, nullable = true, name = "product_policy_code")
	private String productPolicyCode;	
	
	@Column(unique = false, length = 15, nullable = true, name = "gift_code")
	private String giftCode;	
	
	@Column(name = "max_UP", precision=12, scale=2)
	private BigDecimal maxUP;
	
	@Column(name = "billAmount", precision=12, scale=2)
	private BigDecimal billAmount;	
	
	@Column(name = "point", length=10)
	private Integer point;
	
	@Column(name = "min_age", length=10)
	private Integer minAge;
	
	@Column(name = "max_age", length=10)
	private Integer maxAge;
	
	@Column(name = "min_customer", length=10)
	private Integer minCustomer;
	
	@Column(name = "max_customer", length=10)
	private Integer maxCustomer;
	
	@Column(name = "premium", precision=12, scale=2, columnDefinition="numeric(12,2) default 0.00")
	private BigDecimal premium;	

	@Column(nullable = false, name = "is_active")
	private boolean isActive = true;
}
