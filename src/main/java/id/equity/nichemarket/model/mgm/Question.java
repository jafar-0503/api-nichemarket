package id.equity.nichemarket.model.mgm;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Data
@NoArgsConstructor
@Entity
@Table(name = "m_question")
public class Question extends Auditable<String> {
		
	@Column(unique = true, length = 10, nullable = false, name = "question_code")
	private String questionCode;
	
	@Column(nullable = false, length = 255, name = "description")
	private String description;

	@Column(nullable = false, name = "sequence")
	private Integer sequence;
	
	@Column(nullable = false, name = "is_active")
	private boolean isActive;
}