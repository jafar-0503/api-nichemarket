package id.equity.nichemarket.model.mgm;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Entity
@Table(name = "t_bill")
@Data	
public class Bill extends Auditable<String>{
	
	@Column(unique = true, length = 15, nullable = false, name = "bill_code")
	private String billCode;		
	
	@Column(unique = true, length = 15, nullable = false, name = "registration_code")
	private String registrationCode;
	
	@Column(name = "amount", precision=8, scale=2)
	private BigDecimal amount;

	@Column(nullable = false, name = "is_active")
	private boolean isActive = true;
}
