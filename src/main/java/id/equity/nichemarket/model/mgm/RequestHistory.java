package id.equity.nichemarket.model.mgm;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_request_history")
public class RequestHistory extends Auditable<String> {
    @Column(nullable = false, length = 12, name = "request_history_code")
    private String requestHistoryCode;
    @Column(nullable = false, length = 12, name = "customer_reference_code")
    private String customerReferenceCode;
    @Column(nullable = false, length = 4, name = "code")
    private String code;
    @Column(nullable = false, name = "is_active")
    private boolean isActive;
}
