package id.equity.nichemarket.model.mgm;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_customer_message")
public class CustomerMessage extends Auditable<String> {
    @Column(unique = true, length = 12, nullable = false, name = "customer_message_code")
    private String customerMessageCode;

    @Column(nullable = false, length = 100, name = "customer_name")
    private String customerName;

    @Column(nullable = false,length = 50, name = "email_address")
    private String emailAddress;

    @Column(nullable = false,length = 100, name = "phone_no")
    private String phoneNo;

    @Column(nullable = false,length = 100, name = "title")
    private String title;

    @Column(nullable = false, name = "message", columnDefinition = "TEXT")
    private String message;

    @Column(nullable = false, name = "is_active")
    private boolean isActive;
}
