package id.equity.nichemarket.model.mgm;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_recipient_received_msg",
        uniqueConstraints = @UniqueConstraint(columnNames={"recipient_code", "type_code"}))
public class RecipientReceivedMsg extends Auditable<String> {
    @Column(unique = true, length = 12, nullable = false, name = "recipient_received_msg_code")
    private String recipientReceivedMsgCode;

    @Column(nullable = false, length = 10, name = "recipient_code")
    private String recipientCode;

    @Column(nullable = false, length = 1, name = "message_type")
    private String messageType;

    @Column(nullable = false, length = 3, name = "type_code")
    private String typeCode;

    @Column(nullable = false, name = "is_active")
    private boolean isActive;
}
