package id.equity.nichemarket.model.mgm;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Entity
@Table(name = "m_gift")
@Data
public class Gift extends Auditable<String>{
	@Column(unique = true, length = 15, nullable = false, name = "gift_code")
	private String giftCode;
	
	@Column(unique = false, length = 15, nullable = false, name = "ref_gift_code")
	private String refGiftCode;

	@Column(length=10, name = "point_used")
	private Integer pointUsed = 0;

	@Column(length=10, name = "point_gained")
	private Integer pointGained = 0;
	
	@Column(name = "amount", precision=8, scale=2)
	private BigDecimal amount;
	
	@Column(unique = false, length = 10, nullable = false, name = "giftType")
	private String giftType;
	
	@Column(unique = false, length = 50, nullable = false, name = "gift_redeem_desc")
	private String giftRedeemDesc;
	
	@Column(unique = false, length = 50, nullable = false, name = "gift_bonus_desc")
	private String giftBonusDesc;
	
	@Column(nullable = false, name = "is_active")
	private boolean isActive = true;
}
