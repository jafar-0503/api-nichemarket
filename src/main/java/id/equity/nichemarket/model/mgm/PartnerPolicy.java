//package id.equity.nichemarket.model.mgm;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.Table;
//
//import id.equity.nichemarket.audit.Auditable;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
//@NoArgsConstructor
//@Entity
//@Table(name = "m_partner_policy")
//@Data	
//public class PartnerPolicy extends Auditable<String>{
//
//	
//	@Column(unique = true, length = 15, nullable = false, name = "partner_policy_code")
//	private String partnerPolicyCode;		
//	
//	@Column(unique = true, length = 15, nullable = false, name = "partner_policy_desc")
//	private String partnerPolicyDesc;
//	
//	@Column(nullable = false, name = "is_active")
//	private boolean isActive = true;
//}
