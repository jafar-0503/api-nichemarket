package id.equity.nichemarket.model.mgm;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_log_message")
public class LogMessage extends Auditable<String> {
		
	@Column(unique = true, length = 10, nullable = false, name = "log_message_code")
	private String logMessageCode;
	
	@Column(nullable = false, length = 1, name = "message_type")
	private String messageType;

	@Column(nullable = false, length = 3, name = "type_code")
	private String typeCode;

	@Column(nullable = false, length = 50, name = "variable_no_1")
	private String variableNo1;

	@Column(nullable = false, length = 100, name = "variable_name_1")
	private String variableName1;

	@Column(length = 100, name = "variable_date_1")
	private String variableDate1;

	@Column(length = 100, name = "variable_sum_1")
	private String variableSum1;

	@Column(length = 100, name = "variable_date_2")
	private String variableDate2;

	@Column(length = 100, name = "variable_sum_2")
	private String variableSum2;

	@Column(nullable = false, length = 100, name = "send_date")
	private String sendDate;

	@Column(length = 100, name = "phone_no")
	private String phoneNo;

	@Column(length = 150, name = "email_address")
	private String emailAddress;

	@Column(length = 10, name = "valuta_id")
	private String valutaId;

	@Column(length = 20, name = "va_bca")
	private String vaBca;

	@Column(length = 20, name = "va_permata")
	private String vaPermata;

	@Column(length = 100, name = "nav")
	private String nav;

	@Column(length = 2, name = "gender_code")
	private String genderCode;

	@Column(name = "notes", columnDefinition = "TEXT")
	private String notes;

	@Column(length = 20, name = "variable_no_2")
	private String variableNo2;

	@Column(length = 100, name = "variable_sum_3")
	private String variableSum3;

	@Column(length = 100, name = "variable_name_2")
	private String variableName2;

	@Column(length = 50, name = "variable_no_3")
	private String variableNo3;

	@Column(name = "sent_status")
	private boolean sentStatus;

	@Column(nullable = false, length = 150, name = "last_response_description")
	private String lastResponseDescription;

	@Column(nullable = false, name = "is_active")
	private boolean isActive;
}