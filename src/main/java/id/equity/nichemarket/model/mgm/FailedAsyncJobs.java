package id.equity.nichemarket.model.mgm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;
@NoArgsConstructor
@Entity
@Table(name = "t_failed_async_jobs")
@Data
public class FailedAsyncJobs extends Auditable<String>{
	
	@Column(unique = false, length = 1000, name = "method")
	private String method;
	
	@Column(unique = false, length = 1000, name = "payload")
	private String payload;
	
	@Column(unique = false, length = 5000, name = "exception")
	private String exception;
	
	@Column(unique = false, length = 1000, name = "cause_exception")
	private String causeException;
	
	@Column(unique = false, length = 1000, name = "response")
	private String response;
	
	
	@Column(nullable = false, name = "is_active")
	private boolean isActive = true;
}
