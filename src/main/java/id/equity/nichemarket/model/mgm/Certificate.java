package id.equity.nichemarket.model.mgm;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Entity
@Table(name = "m_certificate")
@Data	
public class Certificate extends Auditable<String>{
	
	
	@Column(unique = true, length = 15, nullable = false, name = "certificate_code")
	private String certificateCode;		
	
	@Column(unique = true, length = 15, nullable = false, name = "certificate_desc")
	private String certificateDesc;
	
	@Column(length = 15, nullable = false, name = "certificate_template_code")
	private String certificateTemplateCode;	

	@Column(nullable = false, name = "is_active")
	private boolean isActive = true;
}
