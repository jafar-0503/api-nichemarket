package id.equity.nichemarket.model.mgm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Entity
@Table(name = "m_product_plan")
@Data
public class ProductPlan  extends Auditable<String>{
	@Column(unique = true, length = 15, nullable = false, name = "product_policy_code")
	private String productPlanCode;	
		
	@Column(unique = false, length = 15,name = "partner_code_core")
	private String productPlanCodeCore;
	
	@Column(unique = false, length = 50, nullable = false, name = "product_policy_desc")
	private String productPlanDesc;	
	
	@Column(nullable = false, name = "is_active")
	private boolean isActive = true;
}
