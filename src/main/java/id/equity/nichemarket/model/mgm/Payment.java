package id.equity.nichemarket.model.mgm;

import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Entity
@Table(name = "t_payment")
@Data	
public class Payment extends Auditable<String>{
	
	@Column(unique = true, length = 15, nullable = false, name = "payment_code")
	private String paymentCode;		
	
	@Column(length = 50, name = "payment_desc")
	private String paymentDesc;
	
	@Column(length = 50, name = "payment_type")
	private String paymentType;
	
	@Column(unique = false, length = 60, nullable = false, name = "payment_ref_code")
	private String paymentRefCode;		
	
	@Column(length = 15, nullable = false, name = "bill_code")
	private String billCode;
	
	@Column(name = "amount", precision=15, scale=2)
	private BigDecimal amount;		
	
	@Column(name = "paid_amount", precision=15, scale=2)
	private BigDecimal paidAmount;		

	@Column(name = "transaction_date")
	private Date transactionDate;	

	@Column(nullable = false, name = "payment_status")
	private boolean paymentStatus;	

	@Column(nullable = false, name = "is_active")
	private boolean isActive= true;
}
