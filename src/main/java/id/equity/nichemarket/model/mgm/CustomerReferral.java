package id.equity.nichemarket.model.mgm;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Entity
@Table(name = "t_customer_referral")
@Data	
public class CustomerReferral extends Auditable<String>{
	
	@Column(unique = true, length = 15, nullable = false, name = "referral_code")
	private String referralCode;	
	
	@Column(unique = false, length = 15, nullable = false, name = "used_referral_code")
	private String usedReferralCode;
	

	@Column(unique = false, length = 15, nullable = true, name = "account_code")
	private String accountCode;
	
	@Column(nullable = false, name = "is_active")
	private boolean isActive = true;
}
