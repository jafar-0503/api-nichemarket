package id.equity.nichemarket.model.mgm;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Entity
@Data
@Table(name = "m_rule_insured_age")
public class RuleInsuredAge extends Auditable<String>{
	@Column(unique = true, length = 15, nullable = false, name = "rule_insured_age_code")
	private String ruleInsuredAgeCode;		
	
	@Column(length = 15, nullable = false, name = "registration_type_code")
	private String registrationTypeCode;	
	
	@Column(length = 15, nullable = false, name = "relationship_code")
	private String relationshipCode;	
		
	@Column(name = "min_age", length=10)
	private Integer minAge;
	
	@Column(name = "max_age", length=10)
	private Integer maxAge;

	@Column(nullable = false, name = "is_active")
	private boolean isActive = true;
}
