package id.equity.nichemarket.model.mgm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Entity
@Table(name = "m_product_plan_policy")
@Data
public class ProductPlanPolicy extends Auditable<String>{
	@Column(unique = false, length = 15, nullable = false, name = "registration_type_code")
	private String registrationTypeCode;	
	
	@Column(unique = false, length = 15, nullable = false, name = "product_policy_code")
	private String productPolicyCode;	
	
	@Column(unique = false, length = 15, nullable = false, name = "plan_code_core")
	private String planCodeCore;
	
	@Column(unique = false, length = 15, nullable = true, name = "class_core_code")
	private String classCoreCode;
	
	@Column(name = "type")
	private Integer type;	
	
	@Column(nullable = false, name = "is_active")
	private boolean isActive = true;

}
