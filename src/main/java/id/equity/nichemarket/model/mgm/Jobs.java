package id.equity.nichemarket.model.mgm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;
@NoArgsConstructor
@Entity
@Table(name = "t_jobs")
@Data
public class Jobs extends Auditable<String>{	
	
	@Column(unique = true, length = 200, name = "transaction_id")
	private String transactionId;
	
	@Column(unique = false, length = 50, name = "type")
	private String type;
	
	@Column(unique = false, length = 1000, name = "method")
	private String method;
	
	@Column(unique = false, length = 1000, name = "payload")
	private String payload;
	
	@Column(unique = false, length = 1000, name = "response")
	private String response;	
	
	@Column(unique = false, length = 500, name = "transaction_code_core")
	private String transactionCodeCore;	
	
	@Column(name = "count_retry")
	private Integer countRetry;
	
	@Column(name = "sequence")
	private Integer sequence;
	
	@Column(nullable = false, name = "is_active")
	private boolean isActive = true;
}
