package id.equity.nichemarket.model.mgm;

import com.fasterxml.jackson.annotation.JsonBackReference;
import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_customer_reference")
public class CustomerReference extends Auditable<String> {
    @Column(nullable = false, length = 12, name = "customer_reference_code")
    private String customerReferenceCode;
    @Column(unique = true, nullable = false, length = 15, name = "unique_key")
    private String uniqueKey;
    @Column(nullable = false, length = 15, name = "customer_code")
    private String customerCode;
    @Column(nullable = false, name = "is_active")
    private boolean isActive;

//    @ManyToOne
//    @JoinColumn(name = "customer_code", referencedColumnName = "customer_code")
//    private Customer customer;

//    @OneToMany(mappedBy = "t_request_history")
//    private Set<RequestHistory> requestHistories;
//    @ManyToOne
////    @PrimaryKeyJoinColumn(name = "customer_data", referencedColumnName = "customer_code")
//    @JoinColumn(name="request_history_code", referencedColumnName = "request_history_code")
//    private RequestHistory requestHistories;

}
