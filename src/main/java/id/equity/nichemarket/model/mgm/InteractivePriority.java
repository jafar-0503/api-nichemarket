package id.equity.nichemarket.model.mgm;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_interactive_priority")
public class InteractivePriority extends Auditable <String>{
    @Column(nullable = false, length = 12, name = "interactive_priority_code")
    private String interactivePriorityCode;
    @Column(nullable = false, length = 12, name = "interactive_code")
    private String interactiveCode;
    @Column(nullable = false, length = 250, name = "description")
    private String description;
    @Column(nullable = false, name = "is_active")
    private boolean isActive;
}
