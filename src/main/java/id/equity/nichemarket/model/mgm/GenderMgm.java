package id.equity.nichemarket.model.mgm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Entity
@Data
@Table(name="m_gender_mgm")
public class GenderMgm extends Auditable<String>{
	@Column(unique = true, length = 1, nullable = false, name = "gender_code")
	private String genderCode;

	@Column(length = 100, nullable = false, name = "gender_desc")
	private String genderDesc;

	@Column(nullable = false, name = "is_active")
	private boolean isActive;
}

