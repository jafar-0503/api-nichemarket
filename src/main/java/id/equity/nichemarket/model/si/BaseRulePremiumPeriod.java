package id.equity.nichemarket.model.si;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_base_rule_premium_period")
public class BaseRulePremiumPeriod extends Auditable<String> {
    @Column(unique = true, length = 10, nullable = false, name = "base_rule_premium_period_code")
    private String baseRulePremiumPeriodCode;

    @Column(length = 10, nullable = false, name = "product_code")
    private String productCode;

    @Column(length = 10, nullable = false, name = "payment_period_code")
    private String paymentPeriodCode;

    @Column(nullable = false, name = "min")
    private Integer min;

    @Column(nullable = false, name = "max")
    private Integer max;

    @Column(nullable = false, name = "step")
    private Integer step;

    @Column(nullable = false, name = "max_age")
    private Integer maxAge;

    @Column(nullable = false, name = "is_active")
    private boolean isActive;
}
