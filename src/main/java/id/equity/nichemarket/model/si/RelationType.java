package id.equity.nichemarket.model.si;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_relation_type")
public class RelationType extends Auditable<String> {

    @Column(unique = true,length = 1, nullable = false, name = "relation_type_code")
    private String relationTypeCode;

    @Column(length = 100,name = "description")
    private String description;

    @Column(nullable = false, name = "is_active")
    private boolean isActive;
}
