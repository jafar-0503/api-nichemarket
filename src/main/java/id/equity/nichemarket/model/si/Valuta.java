package id.equity.nichemarket.model.si;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import id.equity.nichemarket.audit.Auditable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "m_valuta")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@EntityListeners(AuditingEntityListener.class)
public class Valuta extends Auditable<String> {
	
	@Column(unique = true, length = 6, nullable = false, name = "valuta_code")
	private String valutaCode;
	
	@Column(nullable = false, length = 30, name = "description")
	private String description;
	
	@Column(nullable = false, length = 10, name = "symbol")
	private String symbol;
	
	@Column(nullable = false, name = "is_active")
	private boolean isActive;
}