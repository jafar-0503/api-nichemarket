package id.equity.nichemarket.model.si;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_product_rule_class")
public class ProductRuleClass extends Auditable<String> {
	
	@Column(unique = true, length = 10, nullable = false, name = "product_rule_class_code")
	private String productRuleClassCode;
	
	@Column(length = 10, name = "product_code")
	private String productCode;
	
	@Column(length = 10, name = "payment_period_code")
	private String paymentPeriodCode;
	
	@Column(nullable = false, name = "max_sum_insured_base")
	private Double maxSumInsuredBase;

	@Column(nullable = false, name = "max_premium_periodic")
	private Double maxPremiumPeriodic;

	@Column(nullable = false, name = "max_tu_periodic")
	private Double maxTuPeriodic;

	@Column(nullable = false, name = "\"class\"" )
	private Double classes;

	@Column(name = "is_active")
	private boolean isActive;
}