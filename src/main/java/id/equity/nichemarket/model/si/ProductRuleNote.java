package id.equity.nichemarket.model.si;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_product_rule_notes")
public class ProductRuleNote extends Auditable<String> {
	
	@Column(unique = true, length = 10, nullable = false, name = "product_rule_note_code")
	private String productRuleNoteCode;
	
	@Column(length = 10, name = "product_code")
	private String productCode;
	
	@Column(length = 6, name = "valuta_code")
	private String valutaCode;
	
	@Column(length = 3, name = "page_code")
	private String pageCode;

	@Column(length = 2, name = "fill_ph_pb_pf")
	private String fillPhPbPf;

	@Column(length = 1, name = "fill_h_d")
	private String fillHD;

	@Column(nullable = false, name = "\"order\"" )
	private Integer order;

	@Column(length = 250, name = "description")
	private String description;

	@Column(length = 250, name = "other_description")
	private String otherDescription;

	@Column(name = "is_active")
	private boolean isActive;
}