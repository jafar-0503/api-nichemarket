package id.equity.nichemarket.model.si;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import id.equity.nichemarket.audit.Auditable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "t_base_rule_periodic_topup")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@EntityListeners(AuditingEntityListener.class)
public class BaseRulePeriodicTopup extends Auditable<String> {
	
	@Column(unique = true, length = 10, nullable = false)
	private String baseRulePeriodicTopupCode;
	
	@Column(length = 10, nullable = false, name = "product_code")
	private String productCode;
	
	@Column(length = 10, nullable = false, name = "valuta_code")
	private String valutaCode;
	
	@Column(length = 10, nullable = false, name = "payment_period_code")
	private String paymentPeriodCode;
	
	@Column(length = 2, nullable = false, name = "min")
	private Integer min;
	
	@Column(length = 10, nullable = false, name = "max")
	private Integer max;
	
	@Column(length = 10, nullable = false, name = "step")
	private Integer step;
	
	@Column(length = 2, nullable = false, name = "is_active")
	private boolean isActive;
}
