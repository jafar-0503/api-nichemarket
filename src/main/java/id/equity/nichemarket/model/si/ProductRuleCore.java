package id.equity.nichemarket.model.si;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_product_rule_core")
public class ProductRuleCore extends Auditable<String> {
	
	@Column(unique = true, length = 10, nullable = false, name = "product_rule_core_code")
	private String productRuleCoreCode;
	
	@Column(length = 10, name = "product_code")
	private String productCode;
	
	@Column(length = 6, name = "valuta_code")
	private String valutaCode;

	@Column(length = 10, name = "payment_period_code")
	private String paymentPeriodCode;
	
	@Column(nullable = false, name = "sum_insured_class")
	private Integer sumInsuredClass;

	@Column(length = 10, name = "risk")
	private String risk;

	@Column(length = 2, name = "as_code")
	private String asCode;

	@Column(length = 20, name = "mapp_code")
	private String mappCode;
	
	@Column(name = "is_active")
	private boolean isActive;
}