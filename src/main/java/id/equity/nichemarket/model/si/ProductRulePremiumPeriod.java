package id.equity.nichemarket.model.si;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_product_rule_premium_period")
public class ProductRulePremiumPeriod extends Auditable<String> {
	
	@Column(unique = true, length = 10, nullable = false, name = "product_rule_premium_period_code")
	private String productRulePremiumPeriodCode;
	
	@Column(length = 10, name = "product_code")
	private String productCode;
	
	@Column(length = 30, name = "min_premium_period")
	private String minPremiumPeriod;

	@Column(length = 30, name = "max_premium_period")
	private String maxPremiumPeriod;
	
	@Column(nullable = false, name = "step_premium_period")
	private Integer stepPremiumPeriod;

	@Column(nullable = false, name = "max_age_premium_period")
	private Integer maxAgePremiumPeriod;
	
	@Column(name = "is_active")
	private boolean isActive;
}