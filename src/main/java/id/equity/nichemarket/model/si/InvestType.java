package id.equity.nichemarket.model.si;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import id.equity.nichemarket.audit.Auditable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "m_invest_type")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@EntityListeners(AuditingEntityListener.class)
public class InvestType extends Auditable<String> {
	
	@Column(unique = true, length = 10, nullable = false, name = "invest_type_code")
	private String investTypeCode;

	@Column(nullable = false, length=100, name = "description")
	private String description;
	
	@Column(nullable = false, name = "is_active")
	private boolean isActive;
}