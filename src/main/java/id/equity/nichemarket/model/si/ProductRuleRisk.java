package id.equity.nichemarket.model.si;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_product_rule_risk")
public class ProductRuleRisk extends Auditable<String> {
	
	@Column(unique = true, length = 10, nullable = false, name = "product_rule_risk_code")
	private String productRuleRiskCode;
	
	@Column(length = 10, name = "product_code")
	private String productCode;
	
	@Column(length = 12, name = "risk_name")
	private String riskName;
	
	@Column(name = "is_active")
	private boolean isActive;
}