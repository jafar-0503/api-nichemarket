package id.equity.nichemarket.model.si;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_base_rule_invest_type")
public class BaseRuleInvestType extends Auditable<String> {

    @Column(length = 10, nullable = false, name = "base_rule_invest_type_code")
    private String baseRuleInvestTypeCode;

    @Column(nullable = false, length = 10, name = "product_code")
    private String productCode;

    @Column(nullable = false, length = 6, name = "valuta_code")
    private String valutaCode;

    @Column(nullable = false, length = 10, name = "invest_type_code")
    private String investTypeCode;

    @Column(nullable = false, name = "low_interest")
    private Double lowInterest;

    @Column(nullable = false, name = "medium_interest")
    private Double mediumInterest;

    @Column(nullable = false, name = "high_interest")
    private Double highInterest;

    @Column(nullable = false, name = "is_active")
    private boolean isActive;
}
