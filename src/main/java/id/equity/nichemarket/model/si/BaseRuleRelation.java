package id.equity.nichemarket.model.si;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_base_rule_relation")
public class BaseRuleRelation extends Auditable<String> {

    @Column(unique = true, length = 10, nullable = false, name = "base_rule_relation_code")
    private String baseRuleRelationCode;

    @Column(length = 10, nullable = false, name = "product_code")
    private String productCode;

    @Column(length = 10, nullable = false, name = "as_code")
    private String asCode;

    @Column(length = 10, nullable = false, name = "relation_type_code")
    private String relationTypeCode;

    @Column(nullable = false, name = "is_active")
    private boolean isActive;
}
