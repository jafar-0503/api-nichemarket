package id.equity.nichemarket.model.si;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_product_rule_required")
public class ProductRuleRequired extends Auditable<String> {
	
	@Column(unique = true, length = 10, nullable = false, name = "product_rule_required_code")
	private String productRuleRequiredCode;
	
	@Column(length = 10, name = "product_code")
	private String productCode;
	
	@Column(length = 10, name = "as_code")
	private String asCode;

	@Column(length = 10, name = "relation_type_code")
	private String relationTypeCode;
	
	@Column(name = "is_active")
	private boolean isActive;
}