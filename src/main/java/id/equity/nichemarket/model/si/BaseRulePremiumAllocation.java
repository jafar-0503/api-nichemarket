package id.equity.nichemarket.model.si;

import lombok.Data;
import lombok.NoArgsConstructor;
import id.equity.nichemarket.audit.Auditable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_base_rule_premium_allocation")
public class BaseRulePremiumAllocation extends Auditable<String>{

	@Column(unique = true, length = 6, nullable = false, name = "base_rule_premium_allocation_code")
	private String baseRulePremiumAllocationCode;

	@Column(length = 10, nullable = false, name = "product_code")
	private String productCode;

	@Column(length = 6, nullable = false, name = "valuta_code")
	private String valutaCode;

	@Column(length = 3, nullable = false, name = "payment_period_code")
	private String paymentPeriodCode;

	@Column(nullable = false, name = "premium_percent")
	private Double premiumPercent;

	@Column(nullable = false, name = "topup_percent")
	private Double topupPercent;

	@Column(nullable = false, name = "is_active")
	private boolean isActive;
	
}
