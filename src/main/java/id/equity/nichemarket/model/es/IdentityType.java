package id.equity.nichemarket.model.es;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import id.equity.nichemarket.audit.Auditable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "m_identity_type")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class IdentityType extends Auditable<String>{
	
	@Column(unique = true, length = 6, nullable = false, name = "identity_type_code")
    private String identityTypeCode;
	
	@Column(length = 100, nullable = false,name = "description")
    private String description;
	
	@Column(nullable = false, name = "is_active")
    private boolean isActive;
}