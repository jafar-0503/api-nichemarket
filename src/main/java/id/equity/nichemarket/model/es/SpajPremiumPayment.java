package id.equity.nichemarket.model.es;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_spaj_premium_payment")
public class SpajPremiumPayment extends Auditable<String> {
	
    @Column(unique = true, length = 12, name = "spaj_premium_payment_code")
    private String spajPremiumPaymentCode;
    
    @Column(length = 12, name = "spaj_document_code")
    private String spajDocumentCode;
    
    @Column(length = 12, name = "spaj_no")
    private String spajNo;
    
    @Column(length = 5, nullable = false, name = "premium_payment_method")
    private String premiumPaymentMethod;
    
    @Column(length = 20, nullable = false, name = "bank_name")
    private String bankName;

    @Column(length = 5, name = "other_bank")
    private String otherBank;
    
    @Column(nullable = false, name = "is_active")
    private boolean isActive;
}