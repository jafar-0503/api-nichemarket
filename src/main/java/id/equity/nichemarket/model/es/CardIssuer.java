package id.equity.nichemarket.model.es;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import id.equity.nichemarket.audit.Auditable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "m_card_issuer")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class CardIssuer extends Auditable<String>{
	
	@Column(unique = true, length = 12, nullable = false, name = "card_issuer_code")
    private String cardIssuerCode;
	
	@Column(length = 30, nullable = false, name = "description")
    private String description;
	
	@Column(nullable = false, name = "is_active")
    private boolean isActive;
}