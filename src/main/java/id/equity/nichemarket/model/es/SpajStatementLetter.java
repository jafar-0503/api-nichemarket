package id.equity.nichemarket.model.es;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_spaj_statement_letter")
public class SpajStatementLetter extends Auditable<String> {

    @Column(unique = true, length = 12, name = "spaj_statement_letter_code")
    private String spajStatementLetterCode;

    @Column(length = 12, name = "spaj_document_code")
    private String spajDocumentCode;

    @Column(length = 12, name = "spaj_no")
    private String spajNo;

    @Column(length = 30, name = "submission_place")
    private String submissionPlace;

    @Column(length = 20, name = "submission_date")
    private String submissionDate;

    @Column(nullable = false, name = "is_active")
    private boolean isActive;
}
