package id.equity.nichemarket.model.es;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_spaj_document_history")
public class SpajDocumentHistory extends Auditable<String> {

    @Column(unique = true, length = 12, name = "spaj_document_history_code")
    private String spajDocumentHistoryCode;

    @Column(length = 12, name = "spaj_document_code")
    private String spajDocumentCode;

    @Column(length = 20, name = "status")
    private String status;

    @Column(length = 60, name = "description")
    private String description;

    @Column(name = "is_active")
    private boolean isActive;
}