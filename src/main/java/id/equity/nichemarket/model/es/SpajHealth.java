package id.equity.nichemarket.model.es;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_spaj_health")
public class SpajHealth extends Auditable<String> {

    @Column(unique = true, length = 12, name = "spaj_health_code")
    private String spajHealthCode;

    @Column(length = 12, name = "spaj_document_code")
    private String spajDocumentCode;

    @Column(length = 12, name = "spaj_no")
    private String spajNo;

    @Column(length = 15, name = "weight_policy_holder")
    private String weightPolicyHolder;

    @Column(length = 15, name = "weight_insured")
    private String weightInsured;

    @Column(length = 60, name = "height_policy_holder")
    private String heightPolicyHolder;

    @Column(length = 60, name = "height_insured")
    private String heightInsured;

    @Column(length = 30, name = "blood_type_policy_holder")
    private String bloodTypePolicyHolder;

    @Column(length = 30, name = "blood_type_insured")
    private String bloodTypeInsured;

    @Column(length = 10, name = "policy_holder_question_1")
    private String policyHolderQuestion1;

    @Column(length = 10, name = "insured_question_1")
    private String insuredQuestion1;

    @Column(length = 10, name = "policy_holder_question_2a")
    private String policyHolderQuestion2A;

    @Column(length = 10, name = "insured_question_2a")
    private String insuredQuestion2A;

    @Column(length = 10, name = "policy_holder_question_2b")
    private String policyHolderQuestion2B;

    @Column(length = 10, name = "insured_question_2b")
    private String insuredQuestion2B;

    @Column(length = 10, name = "policy_holder_question_2c")
    private String policyHolderQuestion2C;

    @Column(length = 10, name = "insured_question_2c")
    private String insuredQuestion2C;

    @Column(length = 10, name = "policy_holder_question_2d")
    private String policyHolderQuestion2D;

    @Column(length = 10, name = "insured_question_2d")
    private String insuredQuestion2D;

    @Column(length = 10, name = "policy_holder_question_2e")
    private String policyHolderQuestion2E;

    @Column(length = 10, name = "insured_question_2e")
    private String insuredQuestion2E;

    @Column(length = 10, name = "policy_holder_question_2f")
    private String policyHolderQuestion2F;

    @Column(length = 10, name = "insured_question_2f")
    private String insuredQuestion2F;

    @Column(length = 10, name = "policy_holder_question_2g")
    private String policyHolderQuestion2G;

    @Column(length = 10, name = "insured_question_2g")
    private String insuredQuestion2G;

    @Column(length = 10, name = "policy_holder_question_2h")
    private String policyHolderQuestion2H;

    @Column(length = 10, name = "insured_question_2h")
    private String insuredQuestion2H;

    @Column(length = 10, name = "policy_holder_question_2i")
    private String policyHolderQuestion2I;

    @Column(length = 10, name = "insured_question_2i")
    private String insuredQuestion2I;

    @Column(length = 10, name = "policy_holder_question_3")
    private String policyHolderQuestion3;

    @Column(length = 10, name = "policy_holder_total_question_3")
    private String policyHolderTotalQuestion3;

    @Column(length = 10, name = "insured_question_3")
    private String insuredQuestion3;

    @Column(length = 10, name = "policy_holder_question_4")
    private String policyHolderQuestion4;

    @Column(length = 10, name = "insured_question_4")
    private String insuredQuestion4;

    @Column(length = 10, name = "policy_holder_question_5")
    private String policyHolderQuestion5;

    @Column(length = 10, name = "insured_question_5")
    private String insuredQuestion5;

    @Column(length = 10, name = "policy_holder_question_6")
    private String policyHolderQuestion6;

    @Column(length = 10, name = "insured_question_6")
    private String insuredQuestion6;

    @Column(length = 10, name = "policy_holder_question_6a")
    private String policyHolderQuestion6A;

    @Column(length = 10, name = "insured_question_6a")
    private String insuredQuestion6A;

    @Column(length = 10, name = "policy_holder_question_6b")
    private String policyHolderQuestion6B;

    @Column(length = 10, name = "insured_question_6b")
    private String insuredQuestion6B;

    @Column(length = 10, name = "policy_holder_question_6c")
    private String policyHolderQuestion6C;

    @Column(length = 10, name = "insured_question_6c")
    private String insuredQuestion6C;

    @Column(length = 10, name = "policy_holder_question_6d")
    private String policyHolderQuestion6D;

    @Column(length = 10, name = "insured_question_6d")
    private String insuredQuestion6D;

    @Column(length = 10, name = "policy_holder_question_7")
    private String policyHolderQuestion7;

    @Column(length = 10, name = "insured_question_7")
    private String insuredQuestion7;

    @Column(length = 10, name = "policy_holder_month_question_14a")
    private String policyHolderMonthQuestion14A;

    @Column(length = 10, name = "insured_month_question_14a")
    private String insuredMonthQuestion14A;

    @Column(length = 10, name = "notes_question")
    private String notesQuestion;

    @Column(name = "is_active")
    private boolean isActive;
}
