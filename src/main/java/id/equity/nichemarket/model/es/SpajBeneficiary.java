package id.equity.nichemarket.model.es;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_spaj_beneficiary")
public class SpajBeneficiary extends Auditable<String> {

    @Column(unique = true, length = 12, nullable = false, name = "spaj_beneficiary_code")
    private String spajBeneficiaryCode;

    @Column(length = 12, name = "spaj_document_code")
    private String spajDocumentCode;

    @Column(length = 12, nullable = false, name = "spaj_no")
    private String spajNo;

    @Column(length = 60, name = "beneficiary_1")
    private String beneficiary1;

    @Column(length = 2, name = "relationship_1")
    private String relationship1;

    @Column(length = 30, name = "date_of_birth_1")
    private String dateOfBirth1;

    @Column(length = 2, name = "gender_1")
    private String gender1;

    @Column(length = 20, name = "percentage_1")
    private String percentage1;

    @Column(nullable = false, name = "is_active")
    private boolean isActive;
}
