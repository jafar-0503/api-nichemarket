package id.equity.nichemarket.model.es;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_spaj_document_file",
        uniqueConstraints = @UniqueConstraint(columnNames={"spaj_no", "dms_code"}))
public class SpajDocumentFile extends Auditable<String>{

    @Column(unique = true, length = 12, name = "spaj_document_file_code")
    private String spajDocumentFileCode;

    @Column(length = 12, nullable = false, name = "spaj_document_code")
    private String spajDocumentCode;

    @Column(length = 12, nullable = false, name = "spaj_no")
    private String spajNo;

    @Column(length = 12, nullable = false, name = "dms_code")
    private String dmsCode;

    @Column(nullable = false, name = "file")
    private String file;

    @Column(length = 60, name = "document_name")
    private String documentName;

    @Column(nullable = false, name = "is_active")
    private boolean isActive;
}
