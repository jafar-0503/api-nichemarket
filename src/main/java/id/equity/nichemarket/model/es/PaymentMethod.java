package id.equity.nichemarket.model.es;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import id.equity.nichemarket.audit.Auditable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "m_payment_method")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PaymentMethod extends Auditable<String>{
	
	@Column(unique = true, length = 5, nullable = false, name = "payment_method_code")
    private String paymentMethodCode;
	
	@Column(length = 100, name = "description")
    private String description;
	
	@Column(length = 5, name = "is_active")
    private boolean isActive;
}