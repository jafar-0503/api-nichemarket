package id.equity.nichemarket.model.es;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Date;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_spaj_policy_holder")
public class SpajPolicyHolder extends Auditable<String> {

    @Column(unique = true, length = 12, name = "spaj_policy_holder_code")
    private String spajPolicyHolderCode;

    @Column(length = 12, name = "spaj_document_code")
    private String spajDocumentCode;

    @Column(length = 12, name = "spaj_no")
    private String spajNo;

    @Column(length = 60, name = "full_name")
    private String fullName;

    @Column(length = 30, name = "place_of_birth")
    private String placeOfBirth;

    @Column(name = "date_of_birth")
    private Date dateOfBirth;

    @Column(length = 6, nullable = false, name = "identity_type")
    private String identityType;

    @Column(length = 30, name = "identity_no")
    private String identityNo;

    @Column(length = 3, name = "citizenship")
    private String citizenship;

    @Column(length = 30, name = "npwp_no")
    private String npwpNo;

    @Column(length = 1, name = "gender")
    private String gender;

    @Column(length = 2, name = "marital_status")
    private String maritalStatus;

    @Column(length = 1, name = "religion")
    private String religion;

    @Column(length = 15, name = "religion_other")
    private String religionOther;

    @Column(length = 225, name = "home_address_1")
    private String homeAddress1;

    @Column(length = 225, name = "home_address_2")
    private String homeAddress2;

    @Column(length = 225, name = "home_address_3")
    private String homeAddress3;

    @Column(length = 30, name = "home_city")
    private String homeCity;

    @Column(length = 10, name = "home_zipcode")
    private String homeZipcode;

    @Column(length = 4, name = "home_province")
    private String homeProvince;

    @Column(length = 4, name = "home_country")
    private String homeCountry;

    @Column(length = 30, name = "home_phone")
    private String homePhone;

    @Column(length = 30, name = "home_handphone")
    private String homeHandphone;

    @Column(length = 30, name = "email_address")
    private String emailAddress;

    @Column(length = 30, name = "last_education")
    private String lastEducation;

    @Column(length = 30, name = "office")
    private String office;

    @Column(length = 30, name = "position")
    private String position;

    @Column(length = 6, name = "industry")
    private String industry;

    @Column(length = 225, name = "office_address_1")
    private String officeAddress1;

    @Column(length = 225, name = "office_address_2")
    private String officeAddress2;

    @Column(length = 225, name = "office_address_3")
    private String officeAddress3;

    @Column(length = 30, name = "office_city")
    private String officeCity;

    @Column(length = 10, name = "office_zipcode")
    private String officeZipcode;

    @Column(length = 4, name = "office_province")
    private String officeProvince;

    @Column(length = 8, name = "office_country")
    private String officeCountry;

    @Column(length = 30, name = "office_phone")
    private String officePhone;

    @Column(length = 30, name = "office_handphone")
    private String officeHandphone;

    @Column(length = 225, name = "correspondence_address")
    private String correspondenceAddress;

    @Column(length = 225, name = "other_correspondence_address_1")
    private String otherCorrespondenceAddress1;

    @Column(length = 225, name = "other_correspondence_address_2")
    private String otherCorrespondenceAddress2;

    @Column(length = 225, name = "other_correspondence_address_3")
    private String otherCorrespondenceAddress3;

    @Column(length = 30, name = "other_correspondence_city")
    private String otherCorrespondenceCity;

    @Column(length = 10, name = "other_correspondence_zipcode")
    private String otherCorrespondenceZipcode;

    @Column(length = 4, name = "other_correspondence_province")
    private String otherCorrespondenceProvince;

    @Column(length = 8, name = "other_correspondence_country")
    private String otherCorrespondenceCountry;

    @Column(length = 30, name = "other_correspondence_phone")
    private String otherCorrespondencePhone;

    @Column(length = 30, name = "other_correspondence_handphone")
    private String otherCorrespondenceHandphone;

    @Column(length = 2, nullable = false, name = "relationship")
    private String relationship;

    @Column(length = 30, name = "other_relationship")
    private String otherRelationship;

    @Column(length = 5, name = "job_level")
    private String jobLevel;

    @Column(nullable = false, name = "is_active")
    private boolean isActive;
}
