package id.equity.nichemarket.validation.partner;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import id.equity.nichemarket.dto.mgm.CustomerValidationDto;
import id.equity.nichemarket.dto.mgm.customer.CustomerAgeValidationDto;
import id.equity.nichemarket.dto.mgm.registrationType.RegistrationTypeDto;
import id.equity.nichemarket.dto.mgm.ruleInsuredAge.RuleInsuredAgeDto;
import id.equity.nichemarket.retrofit.mgm.freeCovid.FreeCovidService;
import id.equity.nichemarket.service.mgm.AccountService;
import id.equity.nichemarket.service.mgm.ProductPlanPolicyService;
import id.equity.nichemarket.service.mgm.ProductPolicyService;
import id.equity.nichemarket.service.mgm.RegistrationService;
import id.equity.nichemarket.service.mgm.RegistrationTypeService;
import id.equity.nichemarket.service.mgm.RuleInsuredAgeService;
import id.equity.nichemarket.service.mgm.SiMedisValidatorCoreService;
import id.equity.nichemarket.service.mgm.UtilitiesService;

public abstract class BaseRegistrationValidation<T, V> {
	

    private static final Logger logger = LoggerFactory.getLogger(BaseRegistrationValidation.class);

	
	@Autowired
	private RegistrationService registrationService;
	
	@Autowired
	private SiMedisValidatorCoreService siMedisValidatorCoreService;
	
	@Autowired
	private RuleInsuredAgeService ruleInsuredAgeService;
	
	
	@Autowired
	private RegistrationTypeService registrationTypeService;
	
	@Autowired
	private ProductPolicyService productPolicyService;
	
	@Autowired
	private AccountService accountService;

	@Autowired
	private ProductPlanPolicyService productPlanPolicyService;

	@Autowired
	private FreeCovidService freeCovidService;


	@Autowired
	private UtilitiesService utilitiesService;
	
	
	public boolean isValidAge(CustomerAgeValidationDto customerAgeValidation) {
		Integer customerAge = 0;	
		
		//Get Rule Min Max Age From Reg Type
//		RegistrationTypeDto registrationType = registrationTypeService.getRegistrationTypeByCode(customerAgeValidation.getRegistrationTypeCode());
		RuleInsuredAgeDto ruleInsuredAge = ruleInsuredAgeService.getRuleInsuredAgeByRegTypeCodeAndCustomerStatus(customerAgeValidation.getRegistrationTypeCode(),customerAgeValidation.getCustomerStatus());
		
		//Calculate Age
		customerAge = utilitiesService.calculateAge(customerAgeValidation.getDateOfBirth(), "");
		
		if(customerAge >= ruleInsuredAge.getMinAge() && customerAge <= ruleInsuredAge.getMaxAge()) {
			logger.info("Current Age " + customerAge + " Is Valid");
			return true;
		}else {
			logger.warn("Current Age " + customerAge + " Is Not Valid");
			return false;
		}
		
	}
	
	public boolean isNotDuplicateKtp(String ktpNO, String productPolicyCode) {
		Integer totalActiveKtpNo = registrationService.countActiveKtpNo(ktpNO, productPolicyCode);
		if(totalActiveKtpNo > 0) {
			logger.warn("Duplicate Ktp No " + ktpNO + " found: " + totalActiveKtpNo);
			return false;
		}else {
			logger.info("Non Duplicate Ktp No " + ktpNO );
			return true;
		}		
	}
	
	
	public boolean isNotDuplicateEmailAddress(String emailAddress) {
		Integer totalActiveEmailAddress = accountService.countActiveEmailAddress(emailAddress.toLowerCase());
		if(totalActiveEmailAddress > 0) {
			logger.warn("Duplicate Email Address " + emailAddress + " found: " + totalActiveEmailAddress);
			return false;
		}else {
			logger.info("Non Duplicate Email Address " + emailAddress);
			return true;
		}
	}
	
	public boolean isValidTotalCustomer(int totalCustomer, RegistrationTypeDto currentRegistrationType) {
		// TODO Auto-generated method stub
		if(totalCustomer >= currentRegistrationType.getMinCustomer() && totalCustomer <= currentRegistrationType.getMaxCustomer()) {
			return true;
		}else {
    		logger.warn("Total Registered Customer Is Invalid");
			return false;
		}		
	}
	
	public abstract boolean isValidKtpCore(CustomerValidationDto customerValidation); 
	
	public abstract List<String> validate(List<T> customers, V registrationType);

	public abstract String getType();

}
