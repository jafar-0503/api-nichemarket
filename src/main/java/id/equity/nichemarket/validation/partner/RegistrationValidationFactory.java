package id.equity.nichemarket.validation.partner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import id.equity.nichemarket.dto.mgm.registrationType.RegistrationTypeDto;
import id.equity.nichemarket.model.mgm.Customer;
import id.equity.nichemarket.service.partner.akulaku.AkulakuValidatorService;

@Component
public class RegistrationValidationFactory {	

	private static final Map<String, BaseRegistrationValidation> myServiceCache = new HashMap<>();

	@Autowired
    private RegistrationValidationFactory(List<BaseRegistrationValidation> services) {
        for(BaseRegistrationValidation service : services) {
        	myServiceCache.put(service.getType(), service);
        }
    }
	
	public static BaseRegistrationValidation getService(String type) {
		BaseRegistrationValidation service = myServiceCache.get(type);
        if(service == null) throw new RuntimeException("Unknown service type: " + type);
        return service;
    }
	
//	public static BaseRegistrationValidation<Customer, RegistrationTypeDto> getInstance(String registrationValidationProduct) {
//		BaseRegistrationValidation<Customer, RegistrationTypeDto> baseRegistrationValidation = null;
//
//		switch (registrationValidationProduct) {
//		case "akulaku":
//			baseRegistrationValidation = new AkulakuValidatorService();
//			break;
//
//		default:
//			break;		
//		}
//		
//		return baseRegistrationValidation;
//	}
}
