package id.equity.nichemarket.validation.constraints;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NumberValidator implements ConstraintValidator<Number, String> {
    protected long Number;
   
    
    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        if(value == null) {
        	return false;
        }
        
    	if(value.matches("[0-9]+")) {
    		return true;
    	}else {
    		return false;
    	}
    }
}