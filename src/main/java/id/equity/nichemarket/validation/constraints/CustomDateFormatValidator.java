package id.equity.nichemarket.validation.constraints;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.expression.ParseException;

public class CustomDateFormatValidator implements ConstraintValidator<CustomDateFormat, String> {
    protected String pattern;
   
    @Override
    public void initialize(CustomDateFormat customDateFormat) {
        this.pattern = customDateFormat.pattern();       
    }
    
    @Override
    public boolean isValid(String customDate, ConstraintValidatorContext constraintValidatorContext) {
	  try{
	  	DateTimeFormatter formatter = DateTimeFormatter.ofPattern(this.pattern);		
		LocalDate date1 = LocalDate.parse(customDate, formatter);
		return true;
	  }catch(Exception e) {
		  return false;
	  }
    }
}