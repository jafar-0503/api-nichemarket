package id.equity.nichemarket.validation.constraints;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class KtpValidator implements ConstraintValidator<Ktp, String> {
    protected long ktp;
   
    
    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        if(value == null) {
        	return false;
        }
    	if(value.matches("[0-9]+") && value.length() == 16) {
    		return true;
    	}else {
    		return false;
    	}
    }
}