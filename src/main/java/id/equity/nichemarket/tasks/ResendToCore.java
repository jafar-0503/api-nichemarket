package id.equity.nichemarket.tasks;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import id.equity.nichemarket.dto.mgm.customerPolicy.CustomerPolicyDto;
import id.equity.nichemarket.model.mgm.Jobs;
import id.equity.nichemarket.retrofit.mgm.freeCovid.BaseFreeCovidResponse;
import id.equity.nichemarket.retrofit.mgm.freeCovid.FreeCovidService;
import id.equity.nichemarket.service.mgm.CustomerPolicyService;
import id.equity.nichemarket.service.mgm.JobsService;

@Configuration
@EnableAsync
@EnableScheduling
public class ResendToCore {
	
	@Autowired
	private CustomerPolicyService customerPolicyService;
	

	@Autowired
	private JobsService jobsService;
	
	@Autowired
	private FreeCovidService freeCovidService;
    
	private static final Logger logger = LoggerFactory.getLogger(ResendToCore.class);

    @Async
    @Scheduled(cron = "0 0 */2 * * ?")
    public void run() {
		logger.info("============= Executing schedule send to core for failed transaction ============");
		   
		//Get transaction code from t_customer_policy sync core = false and count retry <= 3
		List<CustomerPolicyDto> failedCustomerPolicy = customerPolicyService.getFailedSyncCustomerPolicy();
		List<String> failedTransactionCodeList = new ArrayList<>();
		
		for (CustomerPolicyDto customerPolicy : failedCustomerPolicy) {
			logger.info("customer policy : " + customerPolicy);
			failedTransactionCodeList.add(customerPolicy.getCustomerPolicyCoreCode());
		}	
		
		logger.info("Found : " + failedTransactionCodeList.size() + " Failed Transaction");
	    List<BaseFreeCovidResponse> freeCovidResponse = new ArrayList<>();
	    if(!failedTransactionCodeList.isEmpty()) {
	    	//Check from t_jobs if retry > 3
			List<Jobs> failedJobs = jobsService.getFailedJobsWithRetryLessThan(5 , failedTransactionCodeList);
			
			//Loop and send to core
			for (Jobs job : failedJobs) {
				logger.info("Failed Jobs : " + job.getTransactionCodeCore());
				
				JsonObject payload = new Gson().fromJson(job.getPayload(), JsonObject.class);
				
				//Send to Core
				logger.info("Sending Async Customer Data :" + payload);			
				BaseFreeCovidResponse coreResponse = freeCovidService.sendToCoreGrp(payload);
		        logger.info("Result Async Sending To Core : " + coreResponse);	
		        
		        if (coreResponse == null){
		        	logger.error("Failed to send customer to core with transaction id: " + job.getTransactionCodeCore());
		        	BaseFreeCovidResponse failedPrimaryResponse = new BaseFreeCovidResponse();
		        	failedPrimaryResponse.setError_code("2");
		        	failedPrimaryResponse.setData(null);
		        	failedPrimaryResponse.setError_description("Failed to send customer to core with transaction id: " + job.getTransactionCodeCore());
		        	freeCovidResponse.add(coreResponse);
		        }else if(coreResponse.getError_code().equals("1") || coreResponse.getError_code().equals("1")) {
		        	logger.info("Success to send customer to core : " + coreResponse.getError_code() + " , " + coreResponse.getError_description());	        	
		        	freeCovidResponse.add(coreResponse);
		        	//Update To Customer Policy If Succeed To Core
			        CustomerPolicyDto customerPolicyDto = customerPolicyService.updateFromFreeCovidResponse(coreResponse.getData());
		        }else {
		        	freeCovidResponse.add(coreResponse);
		        }      
			
				//Update Jobs By Transaction Code
			    Jobs jobs = jobsService.updateJobs(job.getTransactionCodeCore());   	
				
			}		
	    }
		logger.info("============= End Executing scheduler ============");

    }
}
