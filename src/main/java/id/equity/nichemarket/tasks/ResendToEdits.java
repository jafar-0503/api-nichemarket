package id.equity.nichemarket.tasks;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.equity.nichemarket.model.es.SpajDocument;
import id.equity.nichemarket.model.es.SpajDocumentHistory;
import id.equity.nichemarket.repository.es.SpajDocumentHistoryRepository;
import id.equity.nichemarket.repository.es.SpajDocumentRepository;
import id.equity.nichemarket.service.FileService;
import id.equity.nichemarket.service.eSubmissionStore.ESubmissionDokumenData;
import id.equity.nichemarket.retrofit.eSubmission.EditsResponse;
import id.equity.nichemarket.retrofit.eSubmission.EditsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Configuration
@EnableAsync
@EnableScheduling
public class ResendToEdits {
    @Autowired
    private SpajDocumentHistoryRepository spajDocumentHistoryRepository;
    @Autowired
    private SpajDocumentRepository spajDocumentRepository;
    @Autowired
    private EditsService editsService;
    @Autowired
    private FileService fileService;

    @Value("${file.upload-dir:}")
    private String fileUploadDir;

//    @Async
//    @Scheduled(cron = "2 * * * * ?")
//    public void run() {
//        List<SpajDocumentHistory> failedList = spajDocumentHistoryRepository.findByStatus("FAILED", "FAILEDDOC");
//        for (SpajDocumentHistory postEdits : failedList) {
////            sendEachToEdits(postEdits);
//            SpajDocument spajDocument = spajDocumentRepository.findBySpajDocumentCode(postEdits.getSpajDocumentCode());
//            if (spajDocument != null){
//                String noSpaj = spajDocument.getSpajNo();
//                String spajLocalDirectory = fileUploadDir + noSpaj + "/";
//                File dataNasabah = new File(spajLocalDirectory + "NASABAH_" + noSpaj + ".json");
//                ObjectMapper mapping = new ObjectMapper();
//                try {
//                    ESubmissionDokumenData jsonNasabah = mapping.readValue(dataNasabah, ESubmissionDokumenData.class);
//                    EditsResponse responseEdits = editsService.resendToEdits(jsonNasabah);
//                    //provide code to update data to database
//                    fileService.updateEditsResponse(postEdits, spajDocument, responseEdits);
//                } catch (IOException err) {
//                    err.printStackTrace();
//                    System.out.println("========== Directory " + noSpaj + " or file is not exist or have been deleted ==========");
//                } catch (Exception err){
//                    err.printStackTrace();
//                }
//            }
//        }
//    }
}
