package id.equity.nichemarket.tasks.mgm;

import id.equity.nichemarket.config.ftp.FtpConfig;
import id.equity.nichemarket.model.mgm.Customer;
import id.equity.nichemarket.model.mgm.Recipient;
import id.equity.nichemarket.model.mgm.TAnswer;
import id.equity.nichemarket.repository.mgm.CustomerRepository;
import id.equity.nichemarket.repository.mgm.RecipientRepository;
import id.equity.nichemarket.repository.mgm.TAnswerRepository;
import id.equity.nichemarket.retrofit.mgm.EmailNotificationResponse;
import id.equity.nichemarket.service.mgm.LandingPageService;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

@Configuration
@EnableAsync
@EnableScheduling
public class GenerateCSV {

    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private TAnswerRepository tAnswerRepository;
    @Autowired
    private RecipientRepository recipientRepository;
    @Autowired
    private LandingPageService landingPageService;
    @Autowired
    private FtpConfig.MyGateway myGateway;
    @Value("${ftp.remote.directory}")
    private String ftpRemoteDir;

    private static final Logger logger = LoggerFactory.getLogger(GenerateCSV.class);

//    @Async
//    @Scheduled(cron = "0 */2 * * * ?")
//    public void run(){
//        List<TAnswer> dataToSendEmail = tAnswerRepository.findByIsProcessed(false);
//        List<Recipient> recipientData = recipientRepository.findAll();
//        DateTimeFormatter dateFormat3 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//        DateTimeFormatter dtfHours = DateTimeFormatter.ofPattern("HHmm");
//        LocalDateTime localDateNow = LocalDateTime.now();
//        String timeMinute = localDateNow.format(dtfHours);
//
//        if (dataToSendEmail.size() != 0 ) {
//            if (recipientData.size() != 0){
//                for ( Recipient recipients : recipientData) {
//                    String fileLocation = System.getProperty("java.io.tmpdir") + "/" + recipients.getRecipientCode()
//                            + "_"
//                            + localDateNow.format(dateFormat3)
//                            + "_"
//                            + timeMinute
//                            + ".xls";
//                    Boolean createXls = createXls(dataToSendEmail, fileLocation, recipients);
//                    if (createXls != null) {
//                        logger.info("====== Creating xls done ======");
//                    } else {
//                        logger.info("====== Creating xls failed ======");
//                    }
//
//                    myGateway.sendToFtp(new File(fileLocation),
//                            ftpRemoteDir
//                                    +recipients.getRecipientCode()
//                            + "/"
//                            + recipients.getEmailTypeCode()
//                            + "/");
//                    logger.info("====== Send to FTP Done ======");
//
//                    EmailNotificationResponse request = landingPageService.sendNotification(recipients, timeMinute);
//                    if (request.getError_code().equals("2001")) {
//                        for (TAnswer postAnswers : dataToSendEmail) {
//                            Date dateNow = new Date();
//                            postAnswers.setProcessed(true);
//                            postAnswers.setExtractDate(dateNow);
//                            tAnswerRepository.save(postAnswers);
//                        }
//                        boolean sentStatus = true;
//                        String lastSendingRespose = "sent";
//                        landingPageService.savingRecipientLogMessage(recipients, timeMinute, sentStatus, lastSendingRespose);
//                        logger.info("====== response_code 2001: Success sent to recipient ======");
//                    } else {
//                        boolean sentStatus = false;
//                        String lastSendingRespose = "not sent";
//                        landingPageService.savingRecipientLogMessage(recipients, timeMinute, sentStatus, lastSendingRespose);
//                        logger.info("====== Failed sent Email to recipient ======");
//                    }
//                }
//            }
//        }
//    }

    private boolean createXls(List<TAnswer> dataToSendEmail, String fileLocation, Recipient recipients){
        int i = 1;
        try (Workbook workbook = new XSSFWorkbook()) {
            SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd MMM yyyy HH.mm");
            Sheet sheet = workbook.createSheet(dateFormat2.format(new Date()));
            Row row = sheet.createRow(0);
            CellStyle cellStyle = workbook.createCellStyle();

            //create header row
            Cell cell = row.createCell(0);
            cell.setCellValue("Customer Code");
            cell.setCellStyle(cellStyle);

            cell = row.createCell(1);
            cell.setCellValue("Customer Name");
            cell.setCellStyle(cellStyle);

            cell = row.createCell(2);
            cell.setCellValue("Gender Code");
            cell.setCellStyle(cellStyle);

            cell = row.createCell(3);
            cell.setCellValue("Date Of Birth");
            cell.setCellStyle(cellStyle);

            cell = row.createCell(4);
            cell.setCellValue("Occupation");
            cell.setCellStyle(cellStyle);

            cell = row.createCell(5);
            cell.setCellValue("Phone Number");
            cell.setCellStyle(cellStyle);

            cell = row.createCell(6);
            cell.setCellValue("Email Address");
            cell.setCellStyle(cellStyle);

            cell = row.createCell(7);
            cell.setCellValue("Appointment Time Code");
            cell.setCellStyle(cellStyle);

            cell = row.createCell(8);
            cell.setCellValue("question1");
            cell.setCellStyle(cellStyle);

            cell = row.createCell(9);
            cell.setCellValue("question2");
            cell.setCellStyle(cellStyle);

            cell = row.createCell(10);
            cell.setCellValue("question3");
            cell.setCellStyle(cellStyle);

            cell = row.createCell(11);
            cell.setCellValue("question4");
            cell.setCellStyle(cellStyle);

            cell = row.createCell(12);
            cell.setCellValue("question5");
            cell.setCellStyle(cellStyle);

            cell = row.createCell(13);
            cell.setCellValue("question6");
            cell.setCellStyle(cellStyle);

            cell = row.createCell(14);
            cell.setCellValue("question7");
            cell.setCellStyle(cellStyle);

            //looping new row per data
            for (TAnswer postAnswers : dataToSendEmail) {

                Customer customer = customerRepository.findByCustomerCode(postAnswers.getCustomerCode());

                row = sheet.createRow(i);
                row.createCell(0).setCellValue(customer.getCustomerCode());
                row.createCell(1).setCellValue(customer.getCustomerName());
                row.createCell(2).setCellValue(customer.getGenderCode());
                row.createCell(3).setCellValue(customer.getDateOfBirth());
                row.createCell(4).setCellValue(customer.getOccupation());
                row.createCell(5).setCellValue(customer.getPhoneNo());
                row.createCell(6).setCellValue(customer.getEmailAddress());
                row.createCell(7).setCellValue(postAnswers.getAppointmentTimeCode());
                row.createCell(8).setCellValue(postAnswers.getQuestion1());
                row.createCell(9).setCellValue(postAnswers.getQuestion2());
                row.createCell(10).setCellValue(postAnswers.getQuestion3());
                row.createCell(11).setCellValue(postAnswers.getQuestion4());
                row.createCell(12).setCellValue(postAnswers.getQuestion5());
                row.createCell(13).setCellValue(postAnswers.getQuestion6());
                row.createCell(14).setCellValue(postAnswers.getQuestion7());

                //change is processed status in tAnswer
//                Date dateNow = new Date();
//                postAnswers.setIsProcessed(true);
//                postAnswers.setExtractDate(dateNow);
//                tAnswerRepository.save(postAnswers);

                i++;

            }
            //making size of column
            sheet.autoSizeColumn(0);
            sheet.autoSizeColumn(1);
            sheet.autoSizeColumn(2);
            sheet.autoSizeColumn(3);
            sheet.autoSizeColumn(4);
            sheet.autoSizeColumn(5);
            sheet.autoSizeColumn(6);
            sheet.autoSizeColumn(7);
            sheet.autoSizeColumn(8);
            sheet.autoSizeColumn(9);
            sheet.autoSizeColumn(10);
            sheet.autoSizeColumn(11);
            sheet.autoSizeColumn(12);
            sheet.autoSizeColumn(13);
            sheet.autoSizeColumn(14);
            FileOutputStream outputStream = new FileOutputStream(fileLocation);
            workbook.write(outputStream);
            workbook.close();

        } catch (IOException e){
            e.printStackTrace();
            logger.info("Failed to generate xlsx file");
            return false;
        }
        return true;
    }
}

