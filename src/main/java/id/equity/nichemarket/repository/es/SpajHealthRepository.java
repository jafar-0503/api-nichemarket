package id.equity.nichemarket.repository.es;

import id.equity.nichemarket.model.es.SpajHealth;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpajHealthRepository extends JpaRepository<SpajHealth, Long> {
}
