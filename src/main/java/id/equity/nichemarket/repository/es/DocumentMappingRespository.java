package id.equity.nichemarket.repository.es;

import id.equity.nichemarket.model.es.AddressType;
import id.equity.nichemarket.model.es.DocumentMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentMappingRespository extends JpaRepository<DocumentMapping, Long> {
}
