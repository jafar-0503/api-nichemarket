package id.equity.nichemarket.repository.es;

import id.equity.nichemarket.model.es.SourceFund;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SourceFundRepository extends JpaRepository<SourceFund, Long> {
}
