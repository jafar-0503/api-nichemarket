package id.equity.nichemarket.repository.es;

import id.equity.nichemarket.model.es.SpajInsured;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpajInsuredRepository extends JpaRepository<SpajInsured, Long> {
}
