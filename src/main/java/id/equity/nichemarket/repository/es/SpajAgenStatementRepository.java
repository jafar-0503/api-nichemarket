package id.equity.nichemarket.repository.es;

import id.equity.nichemarket.model.es.SpajAgenStatement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpajAgenStatementRepository extends JpaRepository<SpajAgenStatement, Long> {
}
