package id.equity.nichemarket.repository.es;

import id.equity.nichemarket.model.es.SpajStatementLetter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpajStatementLetterRepository extends JpaRepository<SpajStatementLetter, Long> {
}
