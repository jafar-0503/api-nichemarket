package id.equity.nichemarket.repository.es;

import id.equity.nichemarket.model.es.SpajDocumentFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SpajDocumentFileRepository extends JpaRepository<SpajDocumentFile, Long> {

    @Query("select s from SpajDocumentFile s where s.spajNo = :spajNo and s.dmsCode = :dms_code")
    SpajDocumentFile findByNoSpajDmsCode(String spajNo, String dms_code);

    @Query("select COUNT (s.dmsCode) from SpajDocumentFile s where s.spajNo = ?1")
    Integer findCountByNoSpaj(String noSpaj);

    @Query("select s from SpajDocumentFile s where s.spajNo = ?1")
    List<SpajDocumentFile> findByNoSpaj(String noSpaj);
}
