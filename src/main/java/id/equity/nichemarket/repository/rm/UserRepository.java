package id.equity.nichemarket.repository.rm;

import id.equity.nichemarket.model.rm.User;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long> {
    User findByUsername(String username);
    User findByEmail(String email);
    
    @Query("select cp from User cp where cp.username = :username AND cp.userTypeCode = :userTypeCode")
	User findByUsernameAndTypeCode(String username, String userTypeCode);
}
