package id.equity.nichemarket.repository.rm;

import id.equity.nichemarket.model.rm.RoleMenu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleMenuRepository extends JpaRepository<RoleMenu, Long> {

}
