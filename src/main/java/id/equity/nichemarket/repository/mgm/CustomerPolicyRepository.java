package id.equity.nichemarket.repository.mgm;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.equity.nichemarket.dto.mgm.customer.CustomerDetailPolicyDto;
import id.equity.nichemarket.model.mgm.CustomerPolicy;

@Repository
public interface CustomerPolicyRepository extends JpaRepository<CustomerPolicy, Long> {
    @Query("select cp from CustomerPolicy cp where cp.customerReferenceCode = :customerReferenceCode")
    CustomerPolicy findByCustomerReference(String customerReferenceCode);

    @Query("select cp from CustomerPolicy cp where cp.customerCode = :customerCode")
	CustomerPolicy findByCustomerCode(String customerCode);
    
    @Query("select cp from CustomerPolicy cp where cp.customerPolicyCoreCode = :customerPolicyCoreCode")
	Optional<CustomerPolicy> findByCustomerPolicyCoreCode(String customerPolicyCoreCode);

    @Query("SELECT "
    		+ "cp.customerPolicyCode AS customerPolicyCode, " + 
    		"cp.customerPolicyCoreCode AS customerPolicyCoreCode, " + 
    		"cp.customerCode AS customerCode, " + 
    		"cp.policyNo AS policyNo, " + 
    		"cp.memberNo AS memberNo, " + 
    		"cp.insuranceType AS insuranceType, " + 
    		"cp.sumInsuredAjb AS sumInsuredAjb, " + 
    		"cp.sumInsuredInpatient AS sumInsuredInpatient, " + 
    		"cp.insurancePeriod AS insurancePeriod, " + 
    		"cp.refNo AS refNo, " + 
    		"c.customerName AS customerName, " + 
    		"c.customerCode AS customerCode, " + 
    		"c.refCustomerCode AS refCustomerCode, " + 
    		"c.ktpNO AS ktpNO, " + 
    		"c.genderCode AS genderCode, " + 
    		"c.dateOfBirth AS dateOfBirth, " + 
    		"c.phoneNo AS phoneNo, " + 
    		"c.emailAddress AS emailAddress, " + 
    		"c.customerStatus AS customerStatus, " +
    		"rt.billAmount AS billAmount "
    		+ "FROM CustomerPolicy cp "
    		+ "JOIN Customer c "
    		+ "ON cp.customerCode = c.customerCode "
    		+ "JOIN CustomerRegistration cr "
    		+ "ON cp.customerCode = cr.customerCode "
    		+ "JOIN Registration r "
    		+ "ON cr.registrationCode = r.registrationCode "
    		+ "JOIN RegistrationType rt "
    		+ "ON r.registrationTypeCode = rt.registrationTypeCode "
    		+ "WHERE c.isActive = true AND cp.isActive = true AND cp.customerCode IN :customerCodeList")
	List<CustomerDetailPolicyDto> findByMultipleCustomerCode(ArrayList<String> customerCodeList);

    @Query("SELECT COUNT(*) "
    		+ "FROM CustomerRegistration cr "
    		+ "JOIN CustomerPolicy cp "
    		+ "ON cr.customerCode = cp.customerCode "
    		+ "WHERE cr.registrationCode = :registrationCode")
	int countMemberInsuredByRegCode(String registrationCode);

    @Query("select cp from CustomerPolicy cp where cp.customerCode = :customerCode AND cp.registrationCode = :registrationCode")
	CustomerPolicy findByCustomerCodeAndRegistrationCode(String customerCode, String registrationCode);

    @Query("select cp from CustomerPolicy cp where cp.isSyncedToCore = :status")
	List<CustomerPolicy> findByStatusSyncCore(Boolean status);
}
