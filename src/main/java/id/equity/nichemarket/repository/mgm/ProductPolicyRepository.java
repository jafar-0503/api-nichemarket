package id.equity.nichemarket.repository.mgm;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.equity.nichemarket.model.mgm.ProductPolicy;



@Repository
public interface ProductPolicyRepository extends JpaRepository<ProductPolicy, Long> {
	@Query("SELECT c FROM ProductPolicy c WHERE c.productPolicyCode = :productPolicyCode")
	ProductPolicy findByProductPolicyCode(String productPolicyCode);

}
