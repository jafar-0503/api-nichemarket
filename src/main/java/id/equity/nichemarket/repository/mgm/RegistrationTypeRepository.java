package id.equity.nichemarket.repository.mgm;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import id.equity.nichemarket.model.mgm.RegistrationType;

public interface RegistrationTypeRepository extends JpaRepository<RegistrationType, Long> {
    @Query("select c from RegistrationType c where c.registrationTypeCode = ?1")
    Optional<RegistrationType> findByRegistrationTypeCode(String registrationTypeCode);

}
