package id.equity.nichemarket.repository.mgm;

import id.equity.nichemarket.model.mgm.InteractiveNeeds;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InteractiveNeedsRepository extends JpaRepository<InteractiveNeeds, Long> {
}
