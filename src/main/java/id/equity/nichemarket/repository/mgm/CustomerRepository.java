package id.equity.nichemarket.repository.mgm;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.equity.nichemarket.dto.mgm.customer.CustomerDetailPolicyDto;
import id.equity.nichemarket.model.mgm.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    @Query("select c from Customer c where c.customerCode = ?1")
    Customer findByCustomerCode(String customerCode);
    
    @Query("select c from Customer c inner join CustomerReference cr on c.customerCode = cr.customerCode " +
            "where cr.uniqueKey = ?1")
    Customer findByUniqueKey(String uniqueKey);
    @Query ("select c from Customer c where c.customerCode = :customerCode and c.phoneNo = :phoneNo " +
            "OR c.customerCode = :customerCode AND c.emailAddress = :email")
    Customer findByCustomerCodeAndPhoneNo(String customerCode, String phoneNo, String email);
    
    @Query ("select c from Customer c where c.customerCode = :customerCode and c.phoneNo = :phoneNo " +
            "OR c.customerCode = :customerCode AND c.emailAddress = :email")
    Customer findByEmailAndCodeOrPhoneAndCode(String customerCode, String phoneNo, String email);
    

    
    @Query("select c from Customer c where c.refCustomerCode = :refCustomerCode OR c.customerCode = :refCustomerCode")
	List<Customer> findByRefCustomerCode(String refCustomerCode);

    @Query("select c from Customer c where c.emailAddress = :emailAddress")
	Optional<Customer> findByEmailAddress(String emailAddress);

    @Query("SELECT "
    		+ "c.customerName AS customerName, "
    		+ "c.customerCode AS customerCode, " 
    		+ "c.refCustomerCode AS refCustomerCode, " 
    		+ "c.ktpNO AS ktpNO, " 
    		+ "c.genderCode AS genderCode," 
    		+ "c.dateOfBirth AS dateOfBirth,"  
    		+ "c.phoneNo AS phoneNo," 
    		+ "c.emailAddress AS emailAddress,"  
    		+ "c.customerStatus AS customerStatus,"  
    		+ "d.customerPolicyCoreCode AS customerPolicyCoreCode "
    		+ "FROM Customer c "
    		+ "JOIN CustomerPolicy d "
    		+ "ON c.customerCode = d.customerCode "
    		+ "WHERE c.customerCode IN (:customerCodeList)")     
	List<CustomerDetailPolicyDto> findAllByCustomerCodeIn(List<String> customerCodeList);

    @Query("SELECT c,cp FROM CustomerPolicy c JOIN Customer cp ON c.customerCode = cp.customerCode WHERE cp.customerCode IN (:customerCodeList) ")
    Object[] findAllByCustomerPolicyCoreCode(List<String> customerCodeList);    

}