package id.equity.nichemarket.repository.mgm;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.equity.nichemarket.model.mgm.ProductPlanPolicy;

@Repository
public interface ProductPlanPolicyRepository extends JpaRepository<ProductPlanPolicy, Long> {

    @Query("select c from ProductPlanPolicy c where c.registrationTypeCode = :registrationTypeCode AND c.type = :type")
	ProductPlanPolicy findByRegTypeCodeAndType(String registrationTypeCode, Integer type);


}
