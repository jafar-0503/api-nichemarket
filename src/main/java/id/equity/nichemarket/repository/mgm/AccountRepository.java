package id.equity.nichemarket.repository.mgm;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.equity.nichemarket.dto.mgm.projection.ICountActiveEmailAddressDto;
import id.equity.nichemarket.dto.mgm.projection.ISummaryProductionDataDto;
import id.equity.nichemarket.model.mgm.Account;
import id.equity.nichemarket.model.mgm.Registration;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
    @Query("select c from Account c where c.accountCode = ?1")
    Account findByAccountCode(String accountCode);

    @Query("select c from Account c where c.uniqueActivationKey = :uniqueActivationKey")
    Account findByKey(String uniqueActivationKey);

    @Query("select c from Account c where c.emailAddress = :emailAddress AND c.isActive = true")
	Account findByEmailAddressAndActive(String emailAddress);

    @Query("SELECT COUNT(a.emailAddress) AS count "
    		+ "FROM Account a "    		
    		+ "WHERE a.isActive = true AND a.emailAddress = :emailAddress")
	ICountActiveEmailAddressDto countEmailAddressPaidPayment(String emailAddress);
    
    @Query(nativeQuery = true, value="SELECT " + 
    		"pay.payment_ref_code AS transactionNo " + 
    		", pay.transaction_date AS transactionDate " + 
    		", bill.amount AS premi" + 
    		", reg_type.registration_type_desc AS planType " + 
    		", cust.customer_name AS customerName " + 
    		", acc.account_code AS accountId " + 
    		", cust_ref.referral_code AS referralCode " +
    		", cust_ref.used_referral_code AS usedReferralCode " +
    		", cust_pol.policy_no AS policyNo " + 
    		", cust_pol.member_no AS memberNo " + 
    		", cust_point.current_point AS latestPoint " + 
    		"FROM t_payment AS pay " + 
    		"JOIN t_bill AS bill ON pay.bill_code = bill.bill_code " + 
    		"JOIN t_registration c ON bill.registration_code = c.registration_code " + 
    		"JOIN m_registration_type reg_type ON c.registration_type_code = reg_type.registration_type_code " + 
    		"JOIN t_customer_registration d ON d.registration_code = c.registration_code " + 
    		"JOIN t_customer cust ON cust.customer_code = d.customer_code " + 
    		"JOIN m_account acc ON acc.email_address = cust.email_address " +
    		"JOIN t_customer_referral cust_ref ON acc.account_code = cust_ref.account_code " + 
    		"JOIN t_customer_policy cust_pol ON cust.customer_code = cust_pol.customer_code " + 
    		"JOIN t_customer_point cust_point ON acc.account_code = cust_point.account_code " + 
    		"WHERE bill.bill_code = 'BI2009040009' " + 
    		"ORDER BY cust_point.id DESC LIMIT 1")
    List<ISummaryProductionDataDto> getProductionData(); 

}