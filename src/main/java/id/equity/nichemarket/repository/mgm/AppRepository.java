package id.equity.nichemarket.repository.mgm;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.equity.nichemarket.model.mgm.App;

@Repository
public interface AppRepository extends JpaRepository<App, Long> {
    @Query("select c from App c where c.appCode = ?1")
    App findByAppCode(String appCode);

}