package id.equity.nichemarket.repository.mgm;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.equity.nichemarket.model.mgm.Payment;

@Repository
public interface PaymentRepository extends CustomRepository<Payment, Long> {
    @Query("select c from Payment c where c.paymentCode = ?1")
    Payment findByPaymentCode(String paymentCode);

    @Query("select c from Payment c where c.billCode = :billCode")
	Payment getPaymentByBillCode(String billCode);
    
    @Query("select c from Payment c where c.billCode = :billCode")
	Payment findByBillCode(String billCode);

}