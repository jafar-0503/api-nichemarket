package id.equity.nichemarket.repository.mgm;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.equity.nichemarket.model.mgm.Bill;

@Repository
public interface BillRepository extends JpaRepository<Bill, Long> {
    @Query("select c from Bill c where c.billCode = ?1")
    Bill findByBillCode(String billCode);

}