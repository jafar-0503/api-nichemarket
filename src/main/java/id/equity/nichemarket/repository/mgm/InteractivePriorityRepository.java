package id.equity.nichemarket.repository.mgm;

import id.equity.nichemarket.model.mgm.Interactive;
import id.equity.nichemarket.model.mgm.InteractivePriority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface InteractivePriorityRepository extends JpaRepository<InteractivePriority, Long> {
    @Query("select i from Interactive i where i.emailAddress = :emailAddress")
    Interactive findByEmail(String emailAddress);
    @Query("select i from Interactive i where i.phoneNo = :phoneNo")
    Interactive findByPhone(String phoneNo);
}
