package id.equity.nichemarket.repository.mgm;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.equity.nichemarket.model.mgm.CustomerReference;
import id.equity.nichemarket.model.mgm.CustomerReferral;
@Repository
public interface CustomerReferralRepository extends CustomRepository<CustomerReferral, Long> {
    @Query("select cr from CustomerReferral cr where cr.referralCode = :referralCode")
    CustomerReferral findByReferralCode(String referralCode);

    @Query("SELECT COUNT(*) as total FROM CustomerReferral cr WHERE cr.usedReferralCode = :referralCode")
	Integer sumReferencedByReferralCode(String referralCode);
    
    @Query("select cr from CustomerReferral cr where cr.accountCode = :accountCode")
	CustomerReferral findByAccountCode(String accountCode);
}

