package id.equity.nichemarket.repository.mgm;

import id.equity.nichemarket.model.mgm.RequestHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestHistoryRepository extends JpaRepository<RequestHistory, Long> {
    @Query("select rh from RequestHistory rh where rh.code = ?1")
    RequestHistory findByOtp(String code);
    @Query("select rh from RequestHistory rh where rh.customerReferenceCode = :customerReferenceCode")
    RequestHistory findByCustomerReference(String customerReferenceCode);
}
