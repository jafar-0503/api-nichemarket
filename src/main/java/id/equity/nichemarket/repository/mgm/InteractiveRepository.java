package id.equity.nichemarket.repository.mgm;

import id.equity.nichemarket.model.mgm.Interactive;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InteractiveRepository extends JpaRepository<Interactive, Long> {
}
