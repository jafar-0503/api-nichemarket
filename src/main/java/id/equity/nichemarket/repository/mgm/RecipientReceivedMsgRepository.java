package id.equity.nichemarket.repository.mgm;

import id.equity.nichemarket.model.mgm.RecipientReceivedMsg;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecipientReceivedMsgRepository extends JpaRepository<RecipientReceivedMsg, Long> {
    List<RecipientReceivedMsg> findByTypeCode(String messageType);
}
