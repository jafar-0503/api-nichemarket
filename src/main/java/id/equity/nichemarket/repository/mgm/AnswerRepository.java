package id.equity.nichemarket.repository.mgm;

import id.equity.nichemarket.model.mgm.Answer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnswerRepository extends JpaRepository<Answer, Long> {
}