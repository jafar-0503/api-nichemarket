package id.equity.nichemarket.repository.mgm;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import id.equity.nichemarket.model.mgm.CustomerPolicy;
import id.equity.nichemarket.model.mgm.Jobs;

public interface JobsRepository extends JpaRepository<Jobs, Long>{

    @Query("select cp from Jobs cp where cp.transactionCodeCore = :transactionCode")
	Optional<Jobs> findByTransactionCoreCode(String transactionCode);

    @Query("select cp from Jobs cp where cp.transactionCodeCore IN :transactionCode AND (cp.countRetry < :limit OR cp.countRetry IS NULL)")
	List<Jobs> findByRetryLessThan(Integer limit, List<String> transactionCode);


}
