package id.equity.nichemarket.repository.mgm;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.equity.nichemarket.model.mgm.Relationship;

@Repository
public interface RelationshipRepository extends JpaRepository<Relationship, Long> {
    @Query("select c from Relationship c where c.relationshipCode = ?1")
    Relationship findByRelationshipCode(String relationshipCode);

}