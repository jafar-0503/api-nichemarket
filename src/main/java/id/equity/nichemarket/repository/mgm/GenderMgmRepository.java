package id.equity.nichemarket.repository.mgm;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.equity.nichemarket.model.mgm.GenderMgm;

@Repository
public interface GenderMgmRepository extends JpaRepository<GenderMgm, Long> {
    @Query("select c from Gender c where c.genderCode = ?1")
    GenderMgm findByGenderCode(String genderCode);

}