package id.equity.nichemarket.repository.mgm;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import id.equity.nichemarket.dto.mgm.projection.ICountActiveEmailAddressDto;
import id.equity.nichemarket.dto.mgm.projection.ICountActiveKtpCustomerDto;
import id.equity.nichemarket.model.mgm.Registration;

public interface RegistrationRepository extends JpaRepository<Registration, Long> {

  
//    @Query("select c from Registration c where c.regisrationCode = ?1")
//    Registration findByRegistrationCode(String registrationsCode);

    @Query("select c from Registration c where c.registrationCode = :registrationCode")
	Registration findByRegistrationCode(String registrationCode);

    @Query("SELECT COUNT(a.ktpNO) AS count "
    		+ "FROM Customer a "
    		+ "JOIN CustomerRegistration cr ON a.customerCode = cr.customerCode "
    		+ "JOIN Registration r ON cr.registrationCode = r.registrationCode "
    		+ "JOIN RegistrationType rt ON r.registrationTypeCode = rt.registrationTypeCode "
    		+ "JOIN Bill b ON cr.registrationCode = b.registrationCode "
    		+ "JOIN Payment c ON b.billCode = c.billCode "
    		+ "WHERE c.paymentStatus = true AND a.ktpNO = :ktpNO AND rt.productPolicyCode = :productPolicyCode")
    ICountActiveKtpCustomerDto countKtpNoPaidPayment(String ktpNO, String productPolicyCode);

    @Query("SELECT COUNT(a.emailAddress) AS count "
    		+ "FROM Customer a "
    		+ "JOIN CustomerRegistration cr ON a.customerCode = cr.customerCode "
    		+ "JOIN Bill b ON cr.registrationCode = b.registrationCode "
    		+ "JOIN Payment c ON b.billCode = c.billCode "
    		+ "WHERE c.paymentStatus = true AND a.customerStatus = '0' AND a.emailAddress = :emailAddress")
	ICountActiveEmailAddressDto countEmailAddressPaidPayment(String emailAddress);

    @Query("select c from Registration c where c.accountCode = :accountCode ORDER BY c.createdDate DESC")
	List<Registration> findByAccountCode(String accountCode);


}
