package id.equity.nichemarket.repository.mgm;

import id.equity.nichemarket.model.mgm.LogMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogMessageRepository extends JpaRepository<LogMessage, Long> {
}