package id.equity.nichemarket.repository.mgm;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.equity.nichemarket.model.mgm.FailedAsyncJobs;

@Repository
public interface FailedAsyncJobsRepository extends JpaRepository<FailedAsyncJobs, Long> {
    

}
