package id.equity.nichemarket.repository.mgm;

import id.equity.nichemarket.model.mgm.CustomerReference;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerReferenceRepository extends JpaRepository<CustomerReference, Long> {

    @Query("select cr from CustomerReference cr where cr.uniqueKey = ?1")
    CustomerReference findByKey(String uniqueKey);
    @Query("select cr from CustomerReference cr where cr.customerReferenceCode = ?1")
    CustomerReference findByCustomerReferenceCode(String customerReferenceCode);

}
