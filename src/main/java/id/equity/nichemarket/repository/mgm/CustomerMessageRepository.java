package id.equity.nichemarket.repository.mgm;

import id.equity.nichemarket.model.mgm.CustomerMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerMessageRepository extends JpaRepository<CustomerMessage, Long> {
}
