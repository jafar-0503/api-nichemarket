package id.equity.nichemarket.repository.mgm;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.equity.nichemarket.model.mgm.Customer;
import id.equity.nichemarket.model.mgm.CustomerRegistration;
import id.equity.nichemarket.model.mgm.Registration;

@Repository
public interface CustomerRegistrationRepository extends JpaRepository<CustomerRegistration, Long> {

	@Query("SELECT r "
			+ "FROM CustomerRegistration cr "
			+ "JOIN Registration r "
			+ "ON cr.registrationCode = r.registrationCode "
			+ "WHERE r.isActive = true AND cr.customerCode = :customerCode")
	Optional<Registration> findActiveCustomerRegistrationByCustomerCode(String customerCode);

	@Query("SELECT c "
			+ "FROM CustomerRegistration cr "
			+ "JOIN Customer c "
			+ "ON cr.customerCode = c.customerCode "
			+ "WHERE cr.registrationCode = :registrationCode")
	List<Customer> findAllCustomerByRegistrationCode(String registrationCode);

	@Query("SELECT c "
			+ "FROM CustomerRegistration cr "
			+ "JOIN Customer c "
			+ "ON cr.customerCode = c.customerCode "
			+ "WHERE c.customerStatus = '0' AND cr.registrationCode = :registrationCode")
	Optional<Customer> findPrimaryCustomerByRegistrationCode(String registrationCode);
   
}