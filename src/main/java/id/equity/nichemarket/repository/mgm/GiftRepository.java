package id.equity.nichemarket.repository.mgm;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.equity.nichemarket.model.mgm.Gift;

@Repository
public interface GiftRepository extends JpaRepository<Gift, Long> {
    @Query("select c from Gift c where c.giftCode = ?1")
    Gift findByGiftCode(String giftCode);

}