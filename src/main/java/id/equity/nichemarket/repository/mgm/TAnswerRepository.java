package id.equity.nichemarket.repository.mgm;

import id.equity.nichemarket.model.mgm.TAnswer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TAnswerRepository extends JpaRepository<TAnswer, Long> {
    @Query("select t from TAnswer t where t.isProcessed = ?1")
    List<TAnswer> findByIsProcessed(boolean value);
}