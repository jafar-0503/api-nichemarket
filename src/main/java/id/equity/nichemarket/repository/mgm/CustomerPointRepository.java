package id.equity.nichemarket.repository.mgm;

import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.equity.nichemarket.model.mgm.CustomerPoint;

@Repository
public interface CustomerPointRepository extends CustomRepository<CustomerPoint, Long> {
    @Query("select cp from CustomerPoint cp where cp.customerPointCode = :customerPointCode")
    Optional<CustomerPoint> findByCustomerPointCode(String customerPointCode);
   

	Optional<CustomerPoint> findTop1ByAccountCodeOrderByIdDesc(String accountCode);


}
