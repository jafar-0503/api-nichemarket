package id.equity.nichemarket.repository.mgm;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.equity.nichemarket.model.mgm.RuleInsuredAge;

@Repository
public interface RuleInsuredAgeRepository extends JpaRepository<RuleInsuredAge, Long> {
    @Query("select c from RuleInsuredAge c where c.ruleInsuredAgeCode = ?1")
    RuleInsuredAge findByRuleInsuredAgeCode(String ruleInsuredAgeCode);

    @Query("select c from RuleInsuredAge c where c.relationshipCode =: relationshipCode")
    RuleInsuredAge findByRelationshipCode(String relationshipCode);

    @Query("select c from RuleInsuredAge c where c.relationshipCode = :customerStatus AND c.registrationTypeCode = :registrationTypeCode")
	RuleInsuredAge findByRegistrationTypeCodeAndCustomerStatus(String registrationTypeCode, String customerStatus);

}