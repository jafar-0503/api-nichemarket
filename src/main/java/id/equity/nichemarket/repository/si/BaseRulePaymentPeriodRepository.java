package id.equity.nichemarket.repository.si;

import id.equity.nichemarket.model.si.BaseRulePaymentPeriod;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BaseRulePaymentPeriodRepository extends JpaRepository<BaseRulePaymentPeriod, Long> {
}
