package id.equity.nichemarket.repository.si;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import id.equity.nichemarket.model.si.Valuta;

@Repository
public interface ValutaRepository extends JpaRepository<Valuta, Long>{

}
