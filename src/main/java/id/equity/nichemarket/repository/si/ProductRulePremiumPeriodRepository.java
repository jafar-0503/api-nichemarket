package id.equity.nichemarket.repository.si;

import id.equity.nichemarket.model.si.ProductRulePremiumPeriod;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRulePremiumPeriodRepository extends JpaRepository<ProductRulePremiumPeriod, Long>{

}
