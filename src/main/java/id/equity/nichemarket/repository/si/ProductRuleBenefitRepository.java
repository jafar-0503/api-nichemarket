package id.equity.nichemarket.repository.si;

import id.equity.nichemarket.model.si.ProductRuleBenefit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRuleBenefitRepository extends JpaRepository<ProductRuleBenefit, Long>{

}
