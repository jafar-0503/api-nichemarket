package id.equity.nichemarket.repository.si;

import id.equity.nichemarket.model.si.ProductRuleCore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRuleCoreRepository extends JpaRepository<ProductRuleCore, Long>{

}
