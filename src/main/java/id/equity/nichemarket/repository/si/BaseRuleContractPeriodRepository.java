package id.equity.nichemarket.repository.si;

import id.equity.nichemarket.model.si.BaseRuleContractPeriod;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BaseRuleContractPeriodRepository extends JpaRepository<BaseRuleContractPeriod, Long> {
}
