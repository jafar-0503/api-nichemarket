package id.equity.nichemarket.repository.si;

import id.equity.nichemarket.model.si.ProductRuleImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRuleImageRepository extends JpaRepository<ProductRuleImage, Long>{

}
