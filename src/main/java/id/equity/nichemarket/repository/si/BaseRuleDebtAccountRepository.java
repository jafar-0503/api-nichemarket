package id.equity.nichemarket.repository.si;

import id.equity.nichemarket.model.si.BaseRuleDebtAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BaseRuleDebtAccountRepository extends JpaRepository<BaseRuleDebtAccount, Long> {
}
