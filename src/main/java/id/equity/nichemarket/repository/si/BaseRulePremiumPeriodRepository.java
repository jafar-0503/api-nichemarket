package id.equity.nichemarket.repository.si;

import id.equity.nichemarket.model.si.BaseRulePremiumPeriod;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BaseRulePremiumPeriodRepository extends JpaRepository<BaseRulePremiumPeriod, Long> {
}
