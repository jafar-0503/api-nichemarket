package id.equity.nichemarket.dto.rm.user;

import lombok.Builder;
import lombok.Data;

@Data
public class CreateUserDto extends UserDto {
    private String password;
}
