package id.equity.nichemarket.dto.rm.RoleMenu;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RoleMenuDto {
	
	private Long id;
	private String roleMenuCode;
	private String roleCode;
	private String menuCode;
	private boolean isActive;
}
