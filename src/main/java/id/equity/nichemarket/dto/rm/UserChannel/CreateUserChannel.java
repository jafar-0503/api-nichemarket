package id.equity.nichemarket.dto.rm.UserChannel;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateUserChannel extends UserChannelDto {
}
