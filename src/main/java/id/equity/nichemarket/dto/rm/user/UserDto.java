package id.equity.nichemarket.dto.rm.user;

import javax.persistence.MappedSuperclass;

import id.equity.nichemarket.validator.usertype.ExistUserTypeValidator;
import lombok.Builder;
import lombok.Data;


@Data
@MappedSuperclass
public class UserDto {
    private Long id;
    private String firstName;
    private String lastName;
    private String username;
    private String email;
    @ExistUserTypeValidator
    private String userTypeCode;
}
