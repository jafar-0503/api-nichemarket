package id.equity.nichemarket.dto.rm.userType;

import lombok.Data;

import javax.persistence.MappedSuperclass;

@Data
@MappedSuperclass
public class UserTypeDto {
    private Long id;
    private String code;
    private String description;
    private boolean isActive;
}
