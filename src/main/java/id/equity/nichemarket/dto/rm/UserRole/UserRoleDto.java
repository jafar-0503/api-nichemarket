package id.equity.nichemarket.dto.rm.UserRole;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@Data
@NoArgsConstructor
public class UserRoleDto {
	
	private Long id;
	private String userRoleCode;
	private String username;
	private String roleCode;
	private boolean isActive;
}
