package id.equity.nichemarket.dto.rm.Channel;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateChannel extends ChannelDto {
}
