package id.equity.nichemarket.dto.rm.Role;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateRole extends RoleDto {
}
