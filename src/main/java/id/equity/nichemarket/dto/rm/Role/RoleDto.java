package id.equity.nichemarket.dto.rm.Role;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RoleDto {
	
	private Long id;
	private String roleCode;
	private String description;
	private boolean isActive;
}
