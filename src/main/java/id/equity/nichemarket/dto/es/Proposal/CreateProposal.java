package id.equity.nichemarket.dto.es.Proposal;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateProposal extends ProposalDto{

    public CreateProposal(boolean isActive) {
        this.setActive(isActive);
    }
}
