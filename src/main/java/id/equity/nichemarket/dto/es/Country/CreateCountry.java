package id.equity.nichemarket.dto.es.Country;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateCountry extends CountryDto{ 
	
}
