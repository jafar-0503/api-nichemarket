package id.equity.nichemarket.dto.es.DocumentChecklist;

import lombok.Data;

@Data
public class DocumentChecklistDto {
    
	private Long id;
	private String paymentMethodCode;
    private String description;
    private String tmpCode;
    private boolean isActive;
}