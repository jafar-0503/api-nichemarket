package id.equity.nichemarket.dto.es.CardIssuer;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CardIssuerDto {
	
	private long id;
	private String cardIssuerCode;
	private String description;
	private boolean isActive;
}
