package id.equity.nichemarket.dto.es.CardIssuer;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateCardIssuer extends CardIssuerDto{ 
	
}
