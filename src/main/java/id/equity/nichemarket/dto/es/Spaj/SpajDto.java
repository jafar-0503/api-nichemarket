package id.equity.nichemarket.dto.es.Spaj;

import lombok.Data;

@Data
public class SpajDto {
    
	private Long id;
	private String spajCode;
	private boolean isActive;
}