package id.equity.nichemarket.dto.es.RecurringType;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateRecurringType extends RecurringTypeDto{ 
	
}
