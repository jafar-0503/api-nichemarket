package id.equity.nichemarket.dto.es.CardType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CardTypeDto {
	
	private long id;
	private String cardTypeCode;
	private String description;
	private boolean isActive;
}
