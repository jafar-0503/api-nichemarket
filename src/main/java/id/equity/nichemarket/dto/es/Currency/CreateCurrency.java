package id.equity.nichemarket.dto.es.Currency;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateCurrency extends CurrencyDto {
	
}
