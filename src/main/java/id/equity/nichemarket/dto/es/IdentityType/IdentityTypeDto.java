package id.equity.nichemarket.dto.es.IdentityType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IdentityTypeDto {
	
	private long id;
	private String identityTypeCode;
	private String description;
	private boolean isActive;
}
