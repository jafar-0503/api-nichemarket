package id.equity.nichemarket.dto.es.AgenRelation;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AgenRelationDto {
	
	private long id;
	private String agenRelationCode;
	private String description;
	private boolean isActive;
}
