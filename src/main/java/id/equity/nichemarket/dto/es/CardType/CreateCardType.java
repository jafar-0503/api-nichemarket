package id.equity.nichemarket.dto.es.CardType;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateCardType extends CardTypeDto{ 
	
}
