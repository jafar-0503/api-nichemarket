package id.equity.nichemarket.dto.es.SpajSourceType;

import lombok.Data;

@Data
public class SpajSourceTypeDto {
    private Long id;
    private String spajSourceTypeCode;
    private String spajSourceTypeName;
    private boolean isActive;
}
