package id.equity.nichemarket.dto.es.Industry;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IndustryDto {
	
	private long id;
	private String industryCode;
	private String description;
	private boolean isActive;
}
