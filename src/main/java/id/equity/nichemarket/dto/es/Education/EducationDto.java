package id.equity.nichemarket.dto.es.Education;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EducationDto {
	
	private long id;
	private String educationCode;
	private String description;
	private boolean isActive;
}
