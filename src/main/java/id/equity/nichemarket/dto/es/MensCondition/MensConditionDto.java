package id.equity.nichemarket.dto.es.MensCondition;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MensConditionDto {
	
	private long id;
	private String mensConditionCode;
	private String description;
	private boolean isActive;
}
