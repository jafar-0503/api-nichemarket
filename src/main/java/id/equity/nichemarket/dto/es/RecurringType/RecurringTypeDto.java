package id.equity.nichemarket.dto.es.RecurringType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecurringTypeDto {
	
	private long id;
	private String recurringTypeCode;
	private String description;
	private boolean isActive;
}
