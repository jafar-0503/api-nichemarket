package id.equity.nichemarket.dto.es.Currency;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CurrencyDto {
	
	private long id;
	private String currencyCode;
	private String description;
	private boolean isActive;
}
