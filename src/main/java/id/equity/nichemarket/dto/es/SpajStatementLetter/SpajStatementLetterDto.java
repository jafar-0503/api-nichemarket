package id.equity.nichemarket.dto.es.SpajStatementLetter;

import lombok.Data;

@Data
public class SpajStatementLetterDto {
    private Long id;
    private String spaj_statement_letter_code;
    private String spaj_document_code;
    private String spaj_no;
    private String submission_place;
    private String submission_date;
    private boolean is_active;
}
