package id.equity.nichemarket.dto.si.BaseRuleRedeem;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseRuleRedeemDto {

	private long id;
	private String baseRuleRedeemCode;
	private String productCode;
	private Integer yearTh;
	private Integer ageTh;
	private Double percent;
	private boolean isActive;
}