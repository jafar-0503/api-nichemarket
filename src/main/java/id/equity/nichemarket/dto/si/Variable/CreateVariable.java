package id.equity.nichemarket.dto.si.Variable;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateVariable extends VariableDto{
}
