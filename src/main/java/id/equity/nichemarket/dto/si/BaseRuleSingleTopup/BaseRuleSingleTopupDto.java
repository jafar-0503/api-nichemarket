package id.equity.nichemarket.dto.si.BaseRuleSingleTopup;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseRuleSingleTopupDto {
	
	private Long id;
	private String baseRuleSingleTopupCode;
	private String productCode;
	private String valutaCode;
	private String paymentPeriodCode;
	private Integer min;
	private Integer max;
	private Double step;
	private boolean isActive;
}