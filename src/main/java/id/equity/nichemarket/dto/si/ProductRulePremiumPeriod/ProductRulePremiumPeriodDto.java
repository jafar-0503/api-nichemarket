package id.equity.nichemarket.dto.si.ProductRulePremiumPeriod;

import lombok.Data;

import javax.persistence.Column;

@Data
public class ProductRulePremiumPeriodDto {
	
	private long id;
	private String productRulePremiumPeriodCode;
	private String productCode;
	private String minPremiumPeriod;
	private String maxPremiumPeriod;
	private Integer stepPremiumPeriod;
	private Integer maxAgePremiumPeriod;
	private boolean isActive;
}