package id.equity.nichemarket.dto.si.PaymentPeriod;

import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class PaymentPeriodDto{
	private Long id;
	private String paymentPeriodCode;
	private String description;
	private Integer totYear;
	private Integer totMonth;
	private boolean isActive;
}
