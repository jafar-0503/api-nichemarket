package id.equity.nichemarket.dto.si.BaseRuleTopupFee;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BaseRuleTopupFeeDto{
    private Long id;
    private String baseRuleTopupFeeCode;
    private String productCode;
    private String valutaCode;
    private String paymentPeriodCode;
    private Double rate;
    private boolean isActive;
}
