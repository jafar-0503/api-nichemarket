package id.equity.nichemarket.dto.si.ServiceUnit;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateServiceUnit extends ServiceUnitDto{
}
