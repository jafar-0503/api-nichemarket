package id.equity.nichemarket.dto.si.Valuta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValutaDto{
	
	private Long id;
	private String valutaCode;
	private String description;
	private String symbol;
	private boolean isActive;
}
