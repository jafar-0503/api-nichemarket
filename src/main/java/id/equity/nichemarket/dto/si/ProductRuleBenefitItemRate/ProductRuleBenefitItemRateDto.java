package id.equity.nichemarket.dto.si.ProductRuleBenefitItemRate;

import lombok.Data;

import javax.persistence.Column;

@Data
public class ProductRuleBenefitItemRateDto {
	
	private long id;
	private String productRuleBenefitItemRateCode;
	private String productCode;
	private Integer order;
	private String classes;
	private String asCharge;
	private String useMaxSumInsured;
	private Double rate;
	private String classValue;
	private boolean isActive;
}