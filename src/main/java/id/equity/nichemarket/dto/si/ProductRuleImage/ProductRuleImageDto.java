package id.equity.nichemarket.dto.si.ProductRuleImage;

import lombok.Data;

@Data
public class ProductRuleImageDto {
	
	private long id;
	private String productRuleImageCode;
	private String productCode;
	private String title;
	private String imageCode;
	private boolean isActive;
}