package id.equity.nichemarket.dto.si.Product;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateProduct extends ProductDto{

}
