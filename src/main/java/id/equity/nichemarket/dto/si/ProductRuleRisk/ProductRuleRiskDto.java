package id.equity.nichemarket.dto.si.ProductRuleRisk;

import lombok.Data;

@Data
public class ProductRuleRiskDto {
	
	private long id;
	private String productRuleRiskCode;
	private String productCode;
	private String riskName;
	private boolean isActive;
}