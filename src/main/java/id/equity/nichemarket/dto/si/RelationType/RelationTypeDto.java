package id.equity.nichemarket.dto.si.RelationType;

import lombok.Data;

@Data
public class RelationTypeDto {
    private Long id;
    private String relationTypeCode;
    private String description;
    private boolean isActive;
}