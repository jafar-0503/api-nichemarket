package id.equity.nichemarket.dto.si.ProductRuleCore;

import lombok.Data;

@Data
public class ProductRuleCoreDto {
	
	private long id;
	private String productRuleCoreCode;
	private String productCode;
	private String valutaCode;
	private String paymentPeriodCode;
	private Integer sumInsuredClass;
	private String risk;
	private String asCode;
	private String mappCode;
	private boolean isActive;
}