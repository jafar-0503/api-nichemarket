package id.equity.nichemarket.dto.si.BaseRulePeriodicTopup;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateBaseRulePeriodicTopup extends BaseRulePeriodicTopupDto{

}
