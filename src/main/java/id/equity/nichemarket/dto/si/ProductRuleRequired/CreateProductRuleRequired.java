package id.equity.nichemarket.dto.si.ProductRuleRequired;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateProductRuleRequired extends ProductRuleRequiredDto {

}
