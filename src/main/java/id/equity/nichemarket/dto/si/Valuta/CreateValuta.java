package id.equity.nichemarket.dto.si.Valuta;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateValuta extends ValutaDto{
}
