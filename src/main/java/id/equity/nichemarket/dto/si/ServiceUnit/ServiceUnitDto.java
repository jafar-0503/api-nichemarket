package id.equity.nichemarket.dto.si.ServiceUnit;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ServiceUnitDto{
	private Long id;
	private String serviceUnitCode;
	private String description;
	private boolean isActive;
}
