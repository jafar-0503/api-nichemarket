package id.equity.nichemarket.dto.si.ProductRuleBenefit;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateProductRuleBenefit extends ProductRuleBenefitDto {

}
