package id.equity.nichemarket.dto.si.BaseRuleDebtAccount;

import lombok.Data;

@Data
public class BaseRuleDebtAccountDto {
    private Long id;
    private String baseRuleDebtAccountCode;
    private String productCode;
    private Integer fromYearTh;
    private Double fromYearPercen;
    private Integer toYearTh;
    private Double toYearPercen;
    private boolean isActive;
}
