package id.equity.nichemarket.dto.si.ProductRuleContractPeriod;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateProductRuleContractPeriod extends ProductRuleContractPeriodDto {

}
