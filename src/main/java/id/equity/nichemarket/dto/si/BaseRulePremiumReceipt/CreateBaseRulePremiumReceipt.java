package id.equity.nichemarket.dto.si.BaseRulePremiumReceipt;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateBaseRulePremiumReceipt extends BaseRulePremiumReceiptDto{

}
