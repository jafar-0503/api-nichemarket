package id.equity.nichemarket.dto.si.BaseRulePremiumAllocation;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateBaseRulePremiumAllocation extends BaseRulePremiumAllocationDto {

}
