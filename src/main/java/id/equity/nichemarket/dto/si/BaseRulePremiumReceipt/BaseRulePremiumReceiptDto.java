package id.equity.nichemarket.dto.si.BaseRulePremiumReceipt;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseRulePremiumReceiptDto {
	
	private long id;
	private String baseRulePremiumReceiptCode;
	private String productCode;
	private String valutaCode;
	private String paymentPeriodCode;
	private Double rate;
	private boolean isActive;
}
