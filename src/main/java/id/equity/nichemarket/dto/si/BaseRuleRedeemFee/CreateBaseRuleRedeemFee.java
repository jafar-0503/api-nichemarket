package id.equity.nichemarket.dto.si.BaseRuleRedeemFee;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateBaseRuleRedeemFee extends BaseRuleRedeemFeeDto{
}
