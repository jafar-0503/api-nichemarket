package id.equity.nichemarket.dto.si.ProductRuleRequired;

import lombok.Data;

import javax.persistence.Column;

@Data
public class ProductRuleRequiredDto {
	
	private long id;
	private String productRuleRequiredCode;
	private String productCode;
	private String asCode;
	private String relationTypeCode;
	private boolean isActive;
}