package id.equity.nichemarket.dto.si.BaseRulePremiumPeriodic;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateBaseRulePremiumPeriodic extends BaseRulePremiumPeriodicDto{

}
