package id.equity.nichemarket.dto.si.Variable;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class VariableDto {
	private Long id;
	private String variableCode;
	private String description;
	private String dataType;
	private String blockName;
	private boolean isActive;
}
