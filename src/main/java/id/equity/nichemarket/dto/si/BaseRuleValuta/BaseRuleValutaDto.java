package id.equity.nichemarket.dto.si.BaseRuleValuta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseRuleValutaDto {
	
	private long id;
	private String baseRuleSingleTopupCode;
	private String productCode;
	private String valutaCode;
	private boolean isActive;
}