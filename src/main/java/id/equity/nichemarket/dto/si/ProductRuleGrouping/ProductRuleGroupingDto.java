package id.equity.nichemarket.dto.si.ProductRuleGrouping;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductRuleGroupingDto {
	
	private long id;
	private String productRuleGroupingCode;
	private String productCode;
	private String valutaCode;
	private String productCodeRef;
	private Integer min;
	private Integer max;
	private Integer mandatory;
	private String groupingCode;
	private boolean isActive;
}