package id.equity.nichemarket.dto.si.BaseRuleInvestType;

import lombok.Data;

@Data
public class BaseRuleInvestTypeDto {
    private Long id;
    private String baseRuleInvestTypeCode;
    private String productCode;
    private String valutaCode;
    private String investTypeCode;
    private Double lowInterest;
    private Double mediumInterest;
    private Double highInterest;
    private boolean isActive;
}
