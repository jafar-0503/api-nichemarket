package id.equity.nichemarket.dto.si.Gender;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class GenderDto {
	
	private Long id;
	private String genderCode;
	private String description;
	private boolean isActive;
}
