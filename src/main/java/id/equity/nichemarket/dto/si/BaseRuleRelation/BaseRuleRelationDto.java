package id.equity.nichemarket.dto.si.BaseRuleRelation;

import lombok.Data;

@Data
public class BaseRuleRelationDto {
    private Long id;
    private String baseRuleRelationCode;
    private String productCode;
    private String asCode;
    private String relationTypeCode;
    private boolean isActive;
}
