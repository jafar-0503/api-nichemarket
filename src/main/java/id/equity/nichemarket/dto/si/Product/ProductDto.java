package id.equity.nichemarket.dto.si.Product;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductDto {
	
	private long id;
	private String productCode;
	private String parentProductCode;
	private String productCodeMapping;
	private String description;
	private Integer order;
	private boolean isBasic;
	private boolean isClass;
	private boolean isRisk;
	private boolean isContractPeriod;
	private boolean isPremiumPeriod;
	private boolean isSumInsured;
	private boolean isPremium;
	private boolean isActivate;
	private boolean isRequired;
	private boolean isPayor;
	private boolean isActive;
}
