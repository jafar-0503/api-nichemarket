package id.equity.nichemarket.dto.si.BaseRuleAdmin;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseRuleAdminDto {

	private long id;
	private String baseRuleAdminFeeCode;
	private String productCode;
	private String valutaCode;
	private String paymentPeriodCode;
	private int rate;
	private boolean isActive;
}