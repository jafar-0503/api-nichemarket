package id.equity.nichemarket.dto.si.Pages;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreatePages extends PagesDto {

}