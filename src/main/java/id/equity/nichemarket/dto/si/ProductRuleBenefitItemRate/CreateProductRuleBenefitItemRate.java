package id.equity.nichemarket.dto.si.ProductRuleBenefitItemRate;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateProductRuleBenefitItemRate extends ProductRuleBenefitItemRateDto {

}
