package id.equity.nichemarket.dto.mgm.activationKey;

import lombok.Data;

@Data
public class ActivationKeyDto {
	private String uniqueActivationKey;
}
