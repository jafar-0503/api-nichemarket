package id.equity.nichemarket.dto.mgm.interactivePriority;

import lombok.Data;

import javax.persistence.Column;

@Data
public class InteractivePriorityDto {
    private Long id;
    private String interactivePriorityCode;
    private String interactiveCode;
    private String description;
    private boolean isActive;
}
