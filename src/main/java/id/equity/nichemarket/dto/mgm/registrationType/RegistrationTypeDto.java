package id.equity.nichemarket.dto.mgm.registrationType;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class RegistrationTypeDto {
	private String registrationTypeCode;		

	private String registrationDesc;
	
	private String giftCode;

	private BigDecimal billAmount;

	private Integer point;
	
	private String productPolicyCode;
	
	private BigDecimal maxUP;
	
	private Integer minAge;
	
	private Integer maxAge;
	
	private Integer minCustomer;
	
	private Integer maxCustomer;

	private boolean isActive;

}
