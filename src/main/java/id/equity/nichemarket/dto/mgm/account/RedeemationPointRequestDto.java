package id.equity.nichemarket.dto.mgm.account;

import lombok.Data;

@Data
public class RedeemationPointRequestDto {
	private String emailAddress;
	private String giftCode;
}
