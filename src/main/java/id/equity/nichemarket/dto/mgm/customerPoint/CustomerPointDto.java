package id.equity.nichemarket.dto.mgm.customerPoint;

import lombok.Data;

@Data
public class CustomerPointDto {

	private String customerPointCode;	
	private String customerCode;
	private String accountCode;
	private Integer pointIn;
	private Integer pointOut;	
	private Integer currentPoint;	
	private String description;	
	private boolean isActive;
}
