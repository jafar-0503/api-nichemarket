package id.equity.nichemarket.dto.mgm.projection;

public interface ICountActiveKtpCustomerDto {
	Integer getCount();
	String getKtpNO();
}
