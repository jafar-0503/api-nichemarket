package id.equity.nichemarket.dto.mgm.account;

import lombok.Data;

@Data
public class AccountActivationDto {
	private String accountCode;
	private String username;
	private String password;
}
