package id.equity.nichemarket.dto.mgm.recipientType;

import lombok.Data;

@Data
public class CreateRecipientType extends RecipientTypeDto {
}