package id.equity.nichemarket.dto.mgm.productPlanPolicy;

import lombok.Data;

@Data
public class ProductPlanPolicyDto {
	private String registrationTypeCode;	
	
	private String productPolicyCode;	
	
	private String planCodeCore;
	
	private String classCoreCode;
	
	private Integer type;	
	
	private boolean isActive = true;
}
