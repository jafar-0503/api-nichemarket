package id.equity.nichemarket.dto.mgm.app;

import lombok.Data;

@Data
public class AppDto {
	private String appCode;
	private String appDesc;
	private String appActionName;
	private boolean isActive;
}
