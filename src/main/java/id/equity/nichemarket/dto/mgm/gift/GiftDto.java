package id.equity.nichemarket.dto.mgm.gift;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class GiftDto {
	private String giftCode;
	
	private String refGiftCode;

	private Integer pointUsed = 0;

	private Integer pointGained = 0;
	
	private BigDecimal amount = new BigDecimal(0);
	
	private String giftType;
	
	private String giftRedeemDesc;
	
	private String giftBonusDesc;
	
	private boolean isActive = true;
}
