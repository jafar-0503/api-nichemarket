package id.equity.nichemarket.dto.mgm.bill;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class BillDto {
	private String billCode;		
	private String registrationCode;
	private BigDecimal amount;
	private boolean isActive;
}
