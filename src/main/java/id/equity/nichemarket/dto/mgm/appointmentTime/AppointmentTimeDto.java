package id.equity.nichemarket.dto.mgm.appointmentTime;

import lombok.Data;

@Data
public class AppointmentTimeDto {
    private Long id;
    private String appointmentTimeCode;
    private String description;
    private boolean isActive;
}
