package id.equity.nichemarket.dto.mgm.customerPolicy;

import lombok.Data;

@Data
public class CustomerPolicyDto {
    private String customerPolicyCode;
    private String registrationCode;
    private String customerPolicyCoreCode;
    private String customerReferenceCode;
    private String customerCode;
    private String policyNo;
    private String memberNo;
    private String insuranceType;
    private String sumInsuredAjb;
    private String sumInsuredInpatient;
    private String insurancePeriod;
    private String refNo;
    private String fileTypeCode;
    private boolean isActive;
    private boolean isSyncedToCore;
}

