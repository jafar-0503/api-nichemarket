package id.equity.nichemarket.dto.mgm.interactiveNeeds;

import lombok.Data;

@Data
public class InteractiveNeedsDto {
    private Long id;
    private String interactiveNeedsCode;
    private String interactiveCode;
    private String description;
    private boolean isActive;
}
