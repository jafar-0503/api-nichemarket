package id.equity.nichemarket.dto.mgm.customer;

import lombok.Data;

@Data
public class CustomerAgeValidationDto {
	private String registrationTypeCode;
	private String dateOfBirth;
	private String customerStatus;
}
