package id.equity.nichemarket.dto.mgm.gender;

import lombok.Data;

@Data
public class GenderDto {
	private String genderCode;
	private String genderDesc;
	private boolean isActive;
}
