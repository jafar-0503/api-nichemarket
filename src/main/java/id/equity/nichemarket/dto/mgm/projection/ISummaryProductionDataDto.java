package id.equity.nichemarket.dto.mgm.projection;

public interface ISummaryProductionDataDto {
	String getTransactionDate();
	String getTransactionNo();
	String getCustomerName();
	String getAccountId();
	String getReferralCode();
	String getUsedReferralCode();
	String getPlanType();
	String getLatestPoint();
	String getPremi();
	String getPolicyNo();
	String getMemberNo();

}
