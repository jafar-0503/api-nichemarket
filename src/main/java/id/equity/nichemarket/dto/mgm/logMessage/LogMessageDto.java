package id.equity.nichemarket.dto.mgm.logMessage;

import lombok.Data;

@Data
public class LogMessageDto {
    private Long id;
    private String logMessageCode;
    private String messageType;
    private String typeCode;
    private String variableNo1;
    private String variableName1;
    private String variableDate1;
    private String variableSum1;
    private String variableDate2;
    private String variableSum2;
    private String sendDate;
    private String phoneNo;
    private String emailAddress;
    private String valutaId;
    private String vaBca;
    private String vaPermata;
    private String nav;
    private String genderCode;
    private String notes;
    private String variableNo2;
    private String variableSum3;
    private String variableName2;
    private String variableNo3;
    private boolean sentStatus;
    private String lastResponseDescription;
    private boolean isActive;
}
