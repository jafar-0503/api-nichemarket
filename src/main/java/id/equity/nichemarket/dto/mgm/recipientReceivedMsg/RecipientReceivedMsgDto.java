package id.equity.nichemarket.dto.mgm.recipientReceivedMsg;

import lombok.Data;

@Data
public class RecipientReceivedMsgDto {
    private Long id;
    private String recipientReceivedMsgCode;
    private String recipientCode;
    private String messageType;
    private String typeCode;
    private boolean isActive;
}
