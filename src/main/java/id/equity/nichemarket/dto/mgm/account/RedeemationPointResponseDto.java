package id.equity.nichemarket.dto.mgm.account;

import lombok.Data;

@Data
public class RedeemationPointResponseDto {
	private String emailAddress;
	private String giftCode;
	private Integer usedPoint = 0;
	private Integer currentPoint = 0;
}
