package id.equity.nichemarket.dto.mgm.customer;

import id.equity.nichemarket.model.mgm.Customer;

public interface CustomerDetailPolicyDto {
	String getCustomerPolicyCode();
	String getPolicyNo();
	String getMemberNo();
	String getInsuranceType();
	String getSumInsuredAjb();
	String getSumInsuredInpatient();
	String getInsurancePeriod();
	String getRefNo();
	
    String getCustomerPolicyCoreCode();  
    String getCustomerName();
    String getCustomerCode(); 
    String getRefCustomerCode();
    String getKtpNO();
    String getGenderCode();
    String getDateOfBirth();
    String getPhoneNo();
    String getEmailAddress();
    String getCustomerStatus();
    
    String getBillAmount();
}
