package id.equity.nichemarket.dto.mgm.account;

import lombok.Data;

@Data
public class AccountCustomerDto {

	private String accountCode;
	
	private String appCode;
	
	private String emailAddress;
	
	private String uniqueActivationKey;
	
	private boolean isAccountActivated;	
	
	private boolean isActive;
}
