package id.equity.nichemarket.dto.mgm.account;

import id.equity.nichemarket.dto.mgm.customerPolicy.CustomerPolicyDto;
import id.equity.nichemarket.dto.mgm.product.ProductPolicyDto;
import id.equity.nichemarket.dto.mgm.registration.RegistrationDto;
import id.equity.nichemarket.dto.mgm.registrationType.RegistrationTypeDto;
import lombok.Data;


@Data
public class AccountDetailPolicyResponseDto {
	private String registrationCode;
	private RegistrationTypeDto registrationType;
	private ProductPolicyDto productPolicy;
	private CustomerPolicyDto customerPolicy;
	private Integer totalMemberInsured;
	
}
