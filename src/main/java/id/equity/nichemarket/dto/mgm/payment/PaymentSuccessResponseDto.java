package id.equity.nichemarket.dto.mgm.payment;

import java.util.List;

import id.equity.nichemarket.dto.mgm.customer.CustomerDto;
import id.equity.nichemarket.retrofit.mgm.EmailNotificationResponse;
import id.equity.nichemarket.retrofit.mgm.freeCovid.BaseFreeCovidResponse;
import id.equity.nichemarket.retrofit.mgm.freeCovid.FreeCovidCertificateResponse;
import lombok.Data;

/**
 * 
 * @author ferry
 * Version 1
 * Used for [simedis, linkaja]
 */
@Data
public class PaymentSuccessResponseDto {
	private String registrationCode;
	private String referralCode;
	private PaymentResponseDto payment;
	private List<BaseFreeCovidResponse> members;
	private FreeCovidCertificateResponse certificate;
	private EmailNotificationResponse emailStatus;
}
