package id.equity.nichemarket.dto.mgm.recipient;

import lombok.Data;

@Data
public class RecipientDto {
    private Long id;
    private String recipientCode;
    private String recipientName;
    private String emailAddress;
    private String recipientTypeCode;
    private String partnerName;
    private String emailTypeCode;
    private boolean isActive;
}