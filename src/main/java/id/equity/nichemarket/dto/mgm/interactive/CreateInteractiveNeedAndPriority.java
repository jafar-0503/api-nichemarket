package id.equity.nichemarket.dto.mgm.interactive;

import lombok.Data;

@Data
public class CreateInteractiveNeedAndPriority extends InteractiveDto{
    private String[] customerNeeds;
    private String[] customerPriority;
}
