package id.equity.nichemarket.dto.mgm.question;

import lombok.Data;

@Data
public class QuestionDto {
    private Long id;
    private String questionCode;
    private String description;
    private Integer sequence;
    private boolean isActive;
}
