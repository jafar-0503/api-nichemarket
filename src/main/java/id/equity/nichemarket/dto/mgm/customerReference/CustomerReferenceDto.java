package id.equity.nichemarket.dto.mgm.customerReference;

import id.equity.nichemarket.model.mgm.Customer;
import lombok.Data;

@Data
public class CustomerReferenceDto {
    private Long id;
    private String customerReferenceCode;
    private String uniqueKey;
    private String customerCode;
    private boolean isActive;
}
