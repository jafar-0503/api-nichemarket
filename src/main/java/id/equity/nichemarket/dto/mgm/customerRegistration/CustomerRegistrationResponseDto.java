package id.equity.nichemarket.dto.mgm.customerRegistration;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class CustomerRegistrationResponseDto{
	public String registrationCode;
	public String billCode;
	public String usedReferralCode;
	public BigDecimal billAmount;
}
