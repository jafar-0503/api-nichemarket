package id.equity.nichemarket.dto.mgm.tAnswer;

import lombok.Data;

import java.util.Date;

@Data
public class TAnswerDto {
    private Long id;
    private String answerCode;
    private String customerCode;
    private String appointmentTimeCode;
    private String question1;
    private String question2;
    private String question3;
    private String question4;
    private String question5;
    private String question6;
    private String question7;
    private boolean isProcessed;
    private Date extractDate;
    private boolean isActive;
}