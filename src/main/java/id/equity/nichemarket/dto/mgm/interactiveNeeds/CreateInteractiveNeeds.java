package id.equity.nichemarket.dto.mgm.interactiveNeeds;

import id.equity.nichemarket.dto.mgm.interactive.InteractiveDto;
import lombok.Data;

@Data
public class CreateInteractiveNeeds extends InteractiveDto {
}
