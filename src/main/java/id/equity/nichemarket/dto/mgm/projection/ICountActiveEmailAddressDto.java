package id.equity.nichemarket.dto.mgm.projection;

public interface ICountActiveEmailAddressDto {
	Integer getCount();
	String getEmailAddress();
}
