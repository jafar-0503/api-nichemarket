package id.equity.nichemarket.dto.mgm.customerRegistration;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class CustomerCertificateReqDto {
	@NotNull(message = "Customer Code Is Null")
	@NotEmpty(message = "Customer Code Is Empty")
	private String customerCode;
	
	@NotNull(message = "Registration Code Is Null")
	@NotEmpty(message = "Registration Code Is Empty")
	private String registrationCode;
}
