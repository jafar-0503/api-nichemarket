package id.equity.nichemarket.dto.mgm.account;

import lombok.Data;

@Data
public class AccountActivationResponseDto {
	private String accountCode;
	private String email;	
	private String username;
}
