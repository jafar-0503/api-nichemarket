package id.equity.nichemarket.dto.mgm.ruleInsuredAge;

import lombok.Data;

@Data
public class RuleInsuredAgeDto {
	private String ruleInsuredAgeCode;		
	
	private String registrationTypeCode;	
		
	private String relationshipCode;	
			
	private Integer minAge;
		
	private Integer maxAge;

	private boolean isActive = true;
}
