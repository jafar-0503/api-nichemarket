package id.equity.nichemarket.dto.mgm.customer;

import lombok.Data;

@Data
public class CustomerCoreDataDto {
	private String customerName;
	private String ktpNO;
	private String dateOfBirth;
	private String emailAddress;
	private String gender;
	private String phoneNo;
	private String customerStatus;
	private String customerCode;
	private String customerRefCode;
	private String transactionCode;
	private String partnerCode;
	private String planCode;
	private String startDate;
	private String classType;
	private String emailCode;
}
