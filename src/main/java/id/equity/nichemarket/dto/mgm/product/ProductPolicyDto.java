package id.equity.nichemarket.dto.mgm.product;

import lombok.Data;

@Data
public class ProductPolicyDto {
	private String productPolicyCode;	
	
	private String policyNo;	
	
	private String productPolicyDesc;	
	
	private String partnerCodeCore;
	
	private String emailCode;
	
	private boolean isActive = true;
}
