package id.equity.nichemarket.dto.mgm.payment;

import java.math.BigDecimal;

import lombok.Data;

/**
 * [Version 1]
 * <p>Used for json response [simedis, akulaku]<p>
 * @author ferry
 * 
 */
@Data
public class PaymentResponseDto {
	private String paymentCode;	
	
	private String paymentDesc;	

	private String paymentRefCode;	

	private String billCode;
	
	private String usedReferralCode;	

	private BigDecimal amount;	

	private BigDecimal paidAmount;
	
	private boolean paymentStatus;
	
	private String transactionDate;

	private boolean isActive = true;
}
