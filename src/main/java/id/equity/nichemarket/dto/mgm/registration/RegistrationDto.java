package id.equity.nichemarket.dto.mgm.registration;

import lombok.Data;

@Data
public class RegistrationDto {
	private String registrationCode;
	private String registrationTypeCode;
	private String uniqueActivationKey;
	private String appCode;
	private String accountCode;
	private boolean isAccountActivated;	
}
