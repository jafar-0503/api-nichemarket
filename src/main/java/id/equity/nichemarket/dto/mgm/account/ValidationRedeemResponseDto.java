package id.equity.nichemarket.dto.mgm.account;

import lombok.Data;

@Data
public class ValidationRedeemResponseDto {
	private boolean valid;
}
