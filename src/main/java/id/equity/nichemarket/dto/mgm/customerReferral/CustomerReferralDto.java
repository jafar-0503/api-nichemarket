package id.equity.nichemarket.dto.mgm.customerReferral;

import lombok.Data;

@Data
public class CustomerReferralDto {
	private String referralCode;
	private String accountCode;
	private boolean isActive = true;

}
