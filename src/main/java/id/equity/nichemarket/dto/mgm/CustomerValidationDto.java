package id.equity.nichemarket.dto.mgm;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import id.equity.nichemarket.validation.constraints.Ktp;
import lombok.Data;

@Data
public class CustomerValidationDto {
	@Ktp
	@NotEmpty(message="Ktp Is Empty")
	@NotNull(message="Ktp Is Null")
	private String ktpNo;
	
	@NotEmpty(message="Date Of Birth Is Empty")
	@NotNull(message="Date Of Birth Is Null")
	private String dateOfBirth;
	
	@NotEmpty(message="Registration Type Code is Empty")
	@NotNull(message="Registration Type Code is Null")
	private String registrationTypeCode;
	
	@NotEmpty(message="Customer Status is Empty")
	@NotNull(message="Customer Status is Null")
	private String customerStatus;
}
