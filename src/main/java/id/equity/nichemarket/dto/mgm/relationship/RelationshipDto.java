package id.equity.nichemarket.dto.mgm.relationship;

import lombok.Data;

@Data
public class RelationshipDto {	
    private String relationshipCode;
    private String relationshipDesc;
    private boolean isActive = true;
}
