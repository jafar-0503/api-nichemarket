package id.equity.nichemarket.dto.mgm.customerMessage;

import lombok.Data;

@Data
public class CustomerMessageDto {
    private Long id;
    private String customerMessageCode;
    private String customerName;
    private String emailAddress;
    private String phoneNo;
    private String title;
    private String message;
    private boolean isActive;
}
