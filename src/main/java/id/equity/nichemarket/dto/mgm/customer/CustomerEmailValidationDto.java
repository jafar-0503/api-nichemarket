package id.equity.nichemarket.dto.mgm.customer;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class CustomerEmailValidationDto {
	
	@Email
	@NotNull(message = "Email Address is Null")
	@NotEmpty(message = "Email Address is Empty")
	private String emailAddress;
}
