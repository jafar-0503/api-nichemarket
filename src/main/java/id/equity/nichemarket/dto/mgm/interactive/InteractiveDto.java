package id.equity.nichemarket.dto.mgm.interactive;

import lombok.Data;

@Data
public class InteractiveDto {
    private Long id;
    private String interactiveCode;
    private String customerName;
    private String genderCode;
    private String dateOfBirth;
    private String occupation;
    private String phoneNo;
    private String emailAddress;
    private String income;
    private String appointmentTimeCode;
    private boolean isActive;
}
