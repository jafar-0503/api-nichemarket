package id.equity.nichemarket.dto.mgm.custom;

import lombok.Data;

@Data
public class CreateCustomerRegistrationDto {
	private String customerCode;
	private String registrationCode;
}
