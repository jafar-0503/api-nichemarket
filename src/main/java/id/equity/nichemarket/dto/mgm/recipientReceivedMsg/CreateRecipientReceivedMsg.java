package id.equity.nichemarket.dto.mgm.recipientReceivedMsg;

import lombok.Data;

@Data
public class CreateRecipientReceivedMsg extends RecipientReceivedMsgDto{
}
