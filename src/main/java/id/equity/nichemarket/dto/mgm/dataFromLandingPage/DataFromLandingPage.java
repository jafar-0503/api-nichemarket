package id.equity.nichemarket.dto.mgm.dataFromLandingPage;

import lombok.Data;

@Data
public class DataFromLandingPage {
    private String customerName;
    private String genderCode;
    private String dateOfBirth;
    private String occupation;
    private String phoneNo;
    private String emailAddress;
    private String appointmentTimeCode;
    private String question1;
    private String question2;
    private String question3;
    private String question4;
    private String question5;
    private String question6;
    private String question7;

}
