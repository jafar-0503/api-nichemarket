package id.equity.nichemarket.dto.mgm.payment.v2;

import java.util.List;

import id.equity.nichemarket.dto.mgm.customer.CustomerDto;
import id.equity.nichemarket.dto.mgm.payment.PaymentDto;
import id.equity.nichemarket.retrofit.mgm.EmailNotificationResponse;
import id.equity.nichemarket.retrofit.mgm.freeCovid.BaseFreeCovidResponse;
import id.equity.nichemarket.retrofit.mgm.freeCovid.FreeCovidCertificateResponse;
import lombok.Data;

@Data
public class PaymentSuccessResponseDto {
	private String registrationCode;
	private String referralCode;
	private PaymentDto payment;
	private List<BaseFreeCovidResponse> members;
	private FreeCovidCertificateResponse certificate;
	private EmailNotificationResponse emailStatus;
}
