package id.equity.nichemarket.dto.mgm.customer;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import id.equity.nichemarket.validation.constraints.Ktp;
import id.equity.nichemarket.validation.constraints.Number;
import id.equity.nichemarket.validation.groups.LinkAjaRegistration;
import id.equity.nichemarket.validation.groups.WhatsappSimedisRegistration;
import lombok.Data;


@Data
public class CustomerDto {
//    private Long id;
    private String customerCode;
    private String refCustomerCode;
    
    /**
     * Used on [simedis, akulaku , linkaja]
     * @author ferry
     */
    @Size(min = 4, max = 60, message="Customer Name Length Is Invalid")
    @NotEmpty(message="Customer Name Is Empty")
	@NotNull(message="Customer Name Is Null")
    private String customerName;
    
    /**
     * Used on [simedis, akulaku , linkaja]
     * @author ferry
     */
    @NotEmpty(message="Ktp No Is Empty")
	@NotNull(message="Ktp No Is Null")
    @Ktp
    private String ktpNO;
    
    
    /**
     * Used on [simedis, akulaku , linkaja]
     * @author ferry
     */
    @NotEmpty(message="Gender Code Is Empty")
	@NotNull(message="Gender Code Is Null")
    private String genderCode;
    
    /**
     * Used on [simedis, akulaku , linkaja]
     * @author ferry
     */
    @NotEmpty(message="Date Of Birth Is Empty")
	@NotNull(message="Date Of Birth Is Null")
    private String dateOfBirth;
    
    private String occupation;
    
    /**
     * Used on [simedis, linkaja]
     * @author ferry
     */
    @NotEmpty(groups = {WhatsappSimedisRegistration.class, LinkAjaRegistration.class}, message="Phone No Is Empty")
  	@NotNull(groups = {WhatsappSimedisRegistration.class, LinkAjaRegistration.class}, message="Phone No Is Null")
    @Number(groups = {WhatsappSimedisRegistration.class, LinkAjaRegistration.class}, message="Phone No Format Is Invalid")
    private String phoneNo;
    
    /**
     * Used on [linkaja]
     * @author ferry
     */
    @Email(groups = {LinkAjaRegistration.class}, message = "Email Format Is Invalid")
	@NotNull(groups = {LinkAjaRegistration.class}, message = "Email Address is Null")
	@NotEmpty(groups = {LinkAjaRegistration.class}, message = "Email Address is Empty")
    private String emailAddress;    

    /**
     * Used on [simedis, akulaku , linkaja]
     * @author ferry
     */
    @NotEmpty(message="Customer Status Is Empty")
	@NotNull(message="Customer Status  Is Null")
    private String customerStatus;
    
    private boolean isActive;
}
