package id.equity.nichemarket.dto.mgm.customer;

import lombok.Data;

@Data
public class CustomerWhatsappDto {
	private String ktpNO;
	private String dateOfBirth;
}
