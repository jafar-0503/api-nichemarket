package id.equity.nichemarket.dto.mgm.customerRegistration;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import id.equity.nichemarket.dto.mgm.customer.CustomerDto;
import id.equity.nichemarket.validation.groups.WhatsappSimedisRegistration;
import lombok.Data;

/**
 * Version 1
 * <p>Used for [simedis, akulaku, linkaja]</p>
 * <p>Groups used [WhatsappSimedisRegistration, linkAjaRegistration]</p>
 * <p>Use Default.class for any case</p>
 * @author ferry
 *
 */
@Data
public class CustomerRegistrationDto {
	
	@NotEmpty(message="Registration Type Is Empty")
	@NotNull(message="Registration Type Is Null")
	public String registrationTypeCode;
	
	@NotEmpty(message="App Code Is Empty")
	public String appCode;
	
	public String usedReferralCode;
	
	@Valid
	@NotEmpty(message="Customers Data Is Empty")
	public List<CustomerDto> customers;
	
	
}
