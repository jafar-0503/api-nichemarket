package id.equity.nichemarket.dto.mgm.account;

import id.equity.nichemarket.dto.mgm.customer.CustomerDto;
import lombok.Data;

@Data
public class AccountValidationResponseDto {
	private String accountCode;
	private String emailAddress;	
}
