package id.equity.nichemarket.dto.mgm.account;

import id.equity.nichemarket.dto.mgm.customer.CustomerDto;
import lombok.Data;

@Data
public class AccountPointResponseDto {
	private Integer point = 0;
	private Integer totalReferenced = 0;
	private String referralCode;
		
}
