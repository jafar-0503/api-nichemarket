package id.equity.nichemarket.dto.mgm.customer;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CreateCustomer extends CustomerDto {
}
