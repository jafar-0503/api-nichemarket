package id.equity.nichemarket.dto.mgm.answer;

import lombok.Data;

@Data
public class AnswerDto {
    private Long id;
    private String answerCode;
    private String description;
    private String questionCode;
    private Integer sequence;
    private boolean isActive;
}
