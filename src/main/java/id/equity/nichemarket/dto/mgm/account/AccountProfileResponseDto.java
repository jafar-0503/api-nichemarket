package id.equity.nichemarket.dto.mgm.account;

import lombok.Data;

@Data
public class AccountProfileResponseDto {
	private String customerName;
	private String genderCode;
	private String phoneNo;
	private String dateOfBirth;
}
