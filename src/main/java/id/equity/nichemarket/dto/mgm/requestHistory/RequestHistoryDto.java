package id.equity.nichemarket.dto.mgm.requestHistory;

import lombok.Data;

@Data
public class RequestHistoryDto {
    private Long id;
    private String requestHistoryCode;
    private String customerReferenceCode;
    private String code;
    private boolean isActive;
}
