package id.equity.nichemarket.dto.mgm.payment;


import java.math.BigDecimal;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import id.equity.nichemarket.validation.constraints.CustomDateFormat;
import lombok.Data;


/**
 * [Version 1]
 * <p>Used for validating request [simedis, linkaja]<p>
 * @author ferry
 *  
 */

@Data
public class PaymentRequestDto {
	private String paymentCode;
	
	@NotEmpty(message = "Payment Desc Code Is Empty")
	@NotNull(message = "Payment Desc Code Is Null")
	private String paymentDesc;
	
	@NotEmpty(message = "Payment Ref Code Is Empty")
	@NotNull(message = "Payment Ref Code Is Null")
	private String paymentRefCode;
	
	@NotNull(message = "Bill Code Is Null")
	@NotEmpty(message = "Bill Code Is Empty")
	private String billCode;
	
	private String usedReferralCode;
	
	@NotNull(message = "Amount Is Null")
	@Min(value = 1, message="Amount is Invalid")
	private BigDecimal amount;
	
	@NotNull(message = "Paid Amount Is Null")
	@Min(value = 1, message="Paid Amount is Invalid")
	private BigDecimal paidAmount;
	
	@NotNull(message = "Payment Status Is Null")
	private boolean paymentStatus;
	
	@CustomDateFormat(message = "Transaction Date Format is Invalid", pattern="yyyy-MM-dd")
	private String transactionDate;

	private boolean isActive = true;
}

