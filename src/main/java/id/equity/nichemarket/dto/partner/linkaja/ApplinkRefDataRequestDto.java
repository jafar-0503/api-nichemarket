package id.equity.nichemarket.dto.partner.linkaja;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ApplinkRefDataRequestDto {
	private String key;
	private String value;
	
	public ApplinkRefDataRequestDto(){}

}	
