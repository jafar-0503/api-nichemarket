package id.equity.nichemarket.dto.partner.linkaja;

import lombok.Data;

@Data
public class ApplinkDataResponseDto {
	private String url;
}
