package id.equity.nichemarket.dto.partner.linkaja;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class ApplinkCustomerRegistrationResponseDto {
	public String registrationCode;
	public String billCode;
	public String usedReferralCode;
	public BigDecimal billAmount;
	public String productName;
	public BigDecimal sumInsured;
	private ApplinkResponseDto partnerData;	
}
