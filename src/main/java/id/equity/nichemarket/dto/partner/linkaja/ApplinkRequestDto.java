package id.equity.nichemarket.dto.partner.linkaja;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class ApplinkRequestDto {
	private String trxDate;
	private String partnerTrxID;
	private String merchantID;
	private String terminalID;
	private String terminalName;
	private String partnerApplink;
	private String totalAmount;
	private List<ApplinkItemsRequestDto> items;
	private List<ApplinkRefDataRequestDto> refData;
}


