package id.equity.nichemarket.dto.partner.linkaja;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ApplinkItemsRequestDto {
	private String id;
	private String name;
	private String unitPrice;
	private String qty;
	
	public ApplinkItemsRequestDto (){}
}
