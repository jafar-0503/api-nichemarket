package id.equity.nichemarket.dto.partner.linkaja;

import java.util.List;

import lombok.Data;

@Data
public class InformPaymentDto {
	private String status;
	private String linkAjaRefnum;
	private String trxDate;
	private String partnerTrxID;
	private String merchantID;
	private String terminalID;
	private String terminalName;
	private String partnerApplink;
	private String totalAmount;
	public List<ApplinkItemsRequestDto> items;	
	public List<ApplinkRefDataRequestDto> refData;	
}
