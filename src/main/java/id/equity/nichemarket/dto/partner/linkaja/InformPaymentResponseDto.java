package id.equity.nichemarket.dto.partner.linkaja;

import lombok.Data;

@Data
public class InformPaymentResponseDto {
	private String status;
	private String message;
	private String partnerTrxID;
	private String linkAjaRefnum;
}
