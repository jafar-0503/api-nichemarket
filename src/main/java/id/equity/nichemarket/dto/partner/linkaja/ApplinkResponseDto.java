package id.equity.nichemarket.dto.partner.linkaja;

import lombok.Data;

@Data
public class ApplinkResponseDto {
	private String status;
	private String message;
	private ApplinkDataResponseDto data;
}
