package id.equity.nichemarket.config.error;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import id.equity.nichemarket.config.response.ErrorBaseResponse;
import id.equity.nichemarket.exception.CustomerRegistrationException;
import id.equity.nichemarket.exception.CustomerValidationException;

@ControllerAdvice
public class CustomGlobalExceptionHandler extends ResponseEntityExceptionHandler {

    // Let Spring BasicErrorController handle the exception, we just override the status code
//    @ExceptionHandler(NotFoundException.class)
//    public void springHandleNotFound(HttpServletResponse response) throws IOException {
//        response.sendError(HttpStatus.NOT_FOUND.value());
//    }
//    
    
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status, WebRequest request) {
		
        //Get all errors
        List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(x -> x.getDefaultMessage())
                .collect(Collectors.toList());
        
        return ResponseEntity.status(400).body(new ErrorBaseResponse(false, null, "Invalid Data" , errors));     
    }

	 @ExceptionHandler({CustomerValidationException.class})
     public ResponseEntity<Object> handleCustomerValidationException(CustomerValidationException ex) {
         List<String> errorList  = ex.getErrors();
         String errorMessage = ex.getMessage();
         return ResponseEntity.status(400).body(new ErrorBaseResponse(false, null, errorMessage , errorList));     

     }
 }


