package id.equity.nichemarket.config.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import id.equity.nichemarket.dto.rm.auth.LoginDto;
import id.equity.nichemarket.dto.rm.user.UserDto;
import id.equity.nichemarket.repository.rm.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import static id.equity.nichemarket.config.security.SecurityConstants.EXPIRATION_TIME;
import static id.equity.nichemarket.config.security.SecurityConstants.SECRET;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    @Autowired
    private AuthenticationManager authenticationManager;

    private UserRepository userRepository;

    private ModelMapper modelMapper;

    public JwtAuthenticationFilter(AuthenticationManager authenticationManager, ApplicationContext ctx) {
        this.authenticationManager = authenticationManager;
        this.setFilterProcessesUrl("/api/v1/login");
        this.userRepository = ctx.getBean("userRepo", UserRepository.class);
        this.modelMapper = ctx.getBean("modelMapper", ModelMapper.class);
    }

    //Check If User Is Valid
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request,
                                                HttpServletResponse response) throws AuthenticationException {
        try {
            LoginDto loginDto = new ObjectMapper().readValue(request.getInputStream(), LoginDto.class);
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                    loginDto.getUsername(),
                    loginDto.getPassword(),
                    new ArrayList<>()
            );

            return authenticationManager.authenticate(token);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    //Create JWT Token if Auth Success
    @Override
    public void successfulAuthentication(HttpServletRequest request,
                                         HttpServletResponse response,
                                         FilterChain chain,
                                         Authentication authResult) throws IOException, ServletException {
        String username = ((User) authResult.getPrincipal()).getUsername();
        UserDto userPartnerDto = modelMapper.map(userRepository.findByUsername(username), UserDto.class);

        String token = JWT.create()
                .withSubject(((User) authResult.getPrincipal()).getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .sign(Algorithm.HMAC512(SECRET.getBytes()));

        JsonObject json = new JsonObject();
        json.add("user", new Gson().toJsonTree(userPartnerDto));

        JsonObject data = new JsonObject();
        data.add("data", json);
        data.addProperty("token", SecurityConstants.TOKEN_PREFIX + token);

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(data.toString());
    }

    //Response if JWT is invalid
    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request,
                                              HttpServletResponse response,
                                              AuthenticationException failed) throws IOException, ServletException {

        JsonObject json = new JsonObject();
        json.add("message", new Gson().toJsonTree("username or password are invalid"));

        JsonObject data = new JsonObject();
        data.add("data", json);

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.setStatus(400);
        response.getWriter().write(data.toString());
        
    }
}
