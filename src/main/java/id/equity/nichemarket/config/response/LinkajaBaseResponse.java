package id.equity.nichemarket.config.response;

import lombok.Data;

@Data
public class LinkajaBaseResponse{
    private String status;
    private String message;
    private String partnerTrxID;
    private String linkAjaRefNum;	    

    public LinkajaBaseResponse(String status, String message, String partnerTrxID, String linkAjaRefNum) {
        this.status = status;
        this.message = message;
        this.partnerTrxID = partnerTrxID;
        this.linkAjaRefNum = linkAjaRefNum;
    }

	public LinkajaBaseResponse() {
		// TODO Auto-generated constructor stub
	}
}
