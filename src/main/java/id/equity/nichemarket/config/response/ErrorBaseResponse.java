package id.equity.nichemarket.config.response;

import java.util.List;

import lombok.Data;
import lombok.ToString;

@Data
public class ErrorBaseResponse extends BaseResponse{
	private List<String> errors;

	public ErrorBaseResponse(boolean ok, Object data, String message, List<String> errors) {
		super();
		this.setOk(ok);
		this.setData(data);
		this.setMessage(message);
		this.errors = errors;
	}

}
