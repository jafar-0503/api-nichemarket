package id.equity.nichemarket.config.response;

import java.sql.Timestamp;

import lombok.Data;

@Data
public class BaseResponse<T> {
    private boolean ok;
    private T data;
    private String message;
    private long timestamp = new Timestamp(System.currentTimeMillis()).getTime();

    public BaseResponse(boolean ok, T data, String message) {
        this.ok = ok;
        this.data = data;
        this.message = message;
        this.timestamp = timestamp;
    }

	public BaseResponse() {
		// TODO Auto-generated constructor stub
	}
}