package id.equity.nichemarket.config.response;

import java.util.List;
import java.util.Map;

import lombok.Data;

@Data
public class NewErrorBaseResponse extends BaseResponse{
	private Map<String,Object> errors;

	public NewErrorBaseResponse(boolean ok, Object data, String message, Map<String,Object> errors) {
		super();
		this.setOk(ok);
		this.setData(data);
		this.setMessage(message);
		this.errors = errors;
	}

}
