package id.equity.nichemarket.config.ftp;

import org.apache.commons.net.ftp.FTPFile;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.integration.annotation.*;
import org.springframework.integration.file.remote.session.CachingSessionFactory;
import org.springframework.integration.file.remote.session.SessionFactory;
import org.springframework.integration.ftp.outbound.FtpMessageHandler;
import org.springframework.integration.ftp.session.DefaultFtpSessionFactory;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.handler.annotation.Header;

import java.io.File;

@Configuration
public class FtpConfig {
    @Value("${ftp.host}")
    private String ftpHost;

    @Value("${ftp.port}")
    private Integer ftpPort;

    @Value("${ftp.user}")
    private String ftpUser;

    @Value("${ftp.privateKey:#{null}}")
    private Resource ftpPrivateKey;

    @Value("${ftp.privateKeyPassphrase:}")
    private String ftpPrivateKeyPassphrase;

    @Value("${ftp.password:#{null}}")
    private String ftpPassword;

    @Value("${ftp.remote.directory:/}")
    private String ftpRemoteDirectory;

    @Value("${ftp.remote.directory.download:/}")
    private String ftpRemoteDirectoryDownload;

    @Value("${ftp.local.directory.download:${java.io.tmpdir}/localDownload}")
    private String ftpLocalDirectoryDownload;

    @Value("${ftp.remote.directory.download.filter:*.*}")
    private String ftpRemoteDirectoryDownloadFilter;

    @Value("${file.upload-dir:}")
    private String fileUploadDir;

    @Bean
    public SessionFactory<FTPFile> ftpSessionFactory() {
        DefaultFtpSessionFactory sf = new DefaultFtpSessionFactory();
        sf.setHost(ftpHost);
        sf.setPort(ftpPort);
        sf.setUsername(ftpUser);
        sf.setPassword(ftpPassword);
        sf.setClientMode(2);
        return new CachingSessionFactory<FTPFile>(sf);
    }

    @Bean
    @ServiceActivator(inputChannel = "toFtpChannel")
    public MessageHandler handling() {
        FtpMessageHandler handling = new FtpMessageHandler(ftpSessionFactory());
        handling.setAutoCreateDirectory(true);
        handling.setRemoteDirectoryExpressionString("headers['ftpDir']");
        handling.setFileNameGenerator( message -> {
                if (message.getPayload() instanceof File) {
                    return (((File) message.getPayload()).getName());
                } else {
                    throw new IllegalArgumentException("File expected as payload!");
                }
            });
        return handling;
    }

    @MessagingGateway
    public interface MyGateway {
        @Gateway(requestChannel = "toFtpChannel")
        void sendToFtp(File file, @Header("ftpDir") String ftpDir);
    }

    @Bean
    @ServiceActivator(inputChannel = "fromFtpChannel")
    public MessageHandler resultFileHandling() {
        return message -> System.err.println(message.getPayload());
    }
}