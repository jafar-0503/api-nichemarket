package id.equity.nichemarket.retrofit.mgm.freeCovid;

import lombok.Data;

@Data
public class ValidationCovidResponse {
	private String card_no;
    private String member_no;
    private String sum_insured_ajb;
}
