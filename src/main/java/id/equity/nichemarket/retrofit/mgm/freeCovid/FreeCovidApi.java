package id.equity.nichemarket.retrofit.mgm.freeCovid;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface FreeCovidApi {
    @POST("grpinsurance")
    Call<BaseFreeCovidResponse> sendCertifcate(@Body JsonObject jsonEmail);
    @POST("grpinsurance")
    Call<BaseFreeCovidResponse> sendToCore(@Body JsonObject jsonEmail);
    @POST("grpinsurance")
    Call<FreeCovidCertificateResponse> downloadCertificate(@Body JsonObject jsonEmail);
    @POST("grpinsurance")
    Call<BaseValidationCovidResponse> checkUPByKtpNo(@Body JsonObject data);
}
