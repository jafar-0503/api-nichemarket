package id.equity.nichemarket.retrofit.mgm;

import com.google.gson.JsonObject;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface EmailNotificationApi {
//    @POST("api.php")
    @POST("notification")
    Call<EmailNotificationResponse> sendEmailNotif(@Body JsonObject jsonEmail);

//    @POST("api.php")
    @POST("notification")
    Call<EmailNotificationResponse> sendNotif(@Body JsonObject jsonEmail);
}
