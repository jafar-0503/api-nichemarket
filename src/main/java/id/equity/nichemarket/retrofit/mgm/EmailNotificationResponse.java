package id.equity.nichemarket.retrofit.mgm;

import lombok.Data;

@Data
public class EmailNotificationResponse {
    private String error_code;
    private String error_description;
}
