package id.equity.nichemarket.retrofit.mgm.freeCovid;

import lombok.Data;

@Data
public class FreeCovidResponse {
	private String customer_code;
    private String policy_no;
    private String member_no;
    private String fullname;
    private String insurance_type;
    private String sum_insured_ajb;
    private String insurance_period;
    private String print_date;
    private String ref_no;
    private String email;
    private String sum_insured_inpatient;
    private String product_name;
    private String file_type_code;
    private String transaction_code;
}
