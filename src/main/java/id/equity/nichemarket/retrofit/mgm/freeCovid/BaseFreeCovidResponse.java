package id.equity.nichemarket.retrofit.mgm.freeCovid;

import lombok.Data;

@Data
public class BaseFreeCovidResponse {
    private String error_code;
    private FreeCovidResponse data;
    private String error_description;
}
