package id.equity.nichemarket.retrofit.mgm;

import lombok.Data;

@Data
public class EmailNotificationRequest<T> {
    private T data;
}
