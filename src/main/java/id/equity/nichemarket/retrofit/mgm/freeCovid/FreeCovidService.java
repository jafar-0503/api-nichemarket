package id.equity.nichemarket.retrofit.mgm.freeCovid;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import id.equity.nichemarket.exception.ErrorsException;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.HashMap;

@Service
public class FreeCovidService {
//    private static final String BASE_URL = "https://eli-sit-api.myequity.id/";
//    private static final String BASE_URL = "https://sit-eli.myequity.id/";
//	private static final String BASE_URL = "https://eli-uat-api.myequity.id/";
    private static final String BASE_URL = "https://eli-pro-api.myequity.id/";

    private Retrofit retrofit;
    private FreeCovidApi freeCovidApi;

    public FreeCovidService() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
         freeCovidApi = retrofit.create(FreeCovidApi.class);
    }

    public BaseFreeCovidResponse sendToCoreGrp(JsonObject jsonObject){
        try{
            Call<BaseFreeCovidResponse> dataResponse = freeCovidApi.sendToCore(jsonObject);
            BaseFreeCovidResponse res = dataResponse.execute().body();
            return res;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    public BaseFreeCovidResponse sendCertficate(JsonObject jsonCertificate){
        try{
            Call<BaseFreeCovidResponse> dataResponse = freeCovidApi.sendCertifcate(jsonCertificate);
            BaseFreeCovidResponse res = dataResponse.execute().body();
            return res;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    public BaseValidationCovidResponse checkUPByKtpNo(JsonObject data){
        try{
            Call<BaseValidationCovidResponse> dataResponse = freeCovidApi.checkUPByKtpNo(data);
            BaseValidationCovidResponse res = dataResponse.execute().body();
            return res;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
    

    public FreeCovidCertificateResponse downloadCertificate(JsonObject jsonCertificate){
        try{
            Call<FreeCovidCertificateResponse> dataResponse = freeCovidApi.downloadCertificate(jsonCertificate);
            FreeCovidCertificateResponse res = dataResponse.execute().body();
            if (res == null){
                throw new ErrorsException("Cannot download certificate");
            }
//            JsonObject wrapper = originalResponse.getAsJsonObject();
            return res;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    public String getCoreApiUrl() {
    	return this.BASE_URL;
    }
}
