package id.equity.nichemarket.retrofit.mgm.freeCovid;

import lombok.Data;

@Data
public class FreeCovidCertificateResponse {
    private String error_code;
    private FreeCovidCertificateData data;
    private String error_description;
}

@Data
class FreeCovidCertificateData{
    private String policy_no;
    private String member_no;
    private String certificate;
}
