package id.equity.nichemarket.retrofit.mgm.freeCovid;

import lombok.Data;

@Data
public class BaseValidationCovidResponse {
  private String error_code;
  private ValidationCovidResponse data;
  private String error_description;
}
