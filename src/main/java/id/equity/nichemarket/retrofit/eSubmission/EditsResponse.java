package id.equity.nichemarket.retrofit.eSubmission;

import lombok.Data;

@Data
public class EditsResponse {
    private String status;
    private String desc;
    private String reference;

}