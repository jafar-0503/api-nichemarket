package id.equity.nichemarket.retrofit.eSubmission;

import lombok.Data;

@Data
public class EditsRequest<T> {
    private T data;

    public EditsRequest(T data) {
        this.data = data;
    }
}
