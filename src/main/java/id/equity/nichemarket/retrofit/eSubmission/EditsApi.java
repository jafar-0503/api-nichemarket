package id.equity.nichemarket.retrofit.eSubmission;

import id.equity.nichemarket.service.eSubmissionStore.ESubmissionDokumenData;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface EditsApi {
    @POST("ocr")
    Call<EditsResponse> postDataToEdits(@Body ESubmissionDokumenData data);

    @POST("ocr")
    Call<EditsResponse> resendToEdits(@Body ESubmissionDokumenData data);
}
