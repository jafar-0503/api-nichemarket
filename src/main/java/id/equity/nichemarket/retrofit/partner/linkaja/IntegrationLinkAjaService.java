package id.equity.nichemarket.retrofit.partner.linkaja;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.constant.url.GlobalConstantLinkaja;
import id.equity.nichemarket.dto.partner.linkaja.ApplinkResponseDto;
import id.equity.nichemarket.exception.ErrorsException;
import id.equity.nichemarket.service.partner.linkaja.EncryptionAESService;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

@Service
public class IntegrationLinkAjaService {	  

	  private Retrofit retrofit;
	  
	  private ILinkAjaApi iLinkAjaApi;
	  
	  private static final String BASE_URL = GlobalConstantLinkaja.LINK_AJA_URL;	  

	  private static final Logger logger = LoggerFactory.getLogger(IntegrationLinkAjaService.class);

	
	  public IntegrationLinkAjaService() {
	      HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
	      interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
	      OkHttpClient okHttpClient = new OkHttpClient.Builder()                
                  .addInterceptor(HeaderInterceptor())     
                  .addInterceptor(interceptor)
                  .build();    		  								
	      
	     	
	      retrofit = new Retrofit.Builder()
	          .baseUrl(BASE_URL)
	          .client(okHttpClient)
	          .addConverterFactory(ScalarsConverterFactory.create())	
	          .addConverterFactory(GsonConverterFactory.create())
	          .build();
	      iLinkAjaApi = retrofit.create(ILinkAjaApi.class);
	  }
	  
	  private static Interceptor HeaderInterceptor() {
          return new Interceptor() {
              @Override
              public okhttp3.Response intercept(Chain chain) throws IOException {
            	  
            	  String encodedBase64 = EncryptionAESService.encodeBase64Auth(GlobalConstantLinkaja.USERNAME_LINKAJA, GlobalConstantLinkaja.PASSWORD_LINKAJA);
            	  
                  okhttp3.Request request = chain.request();
                      request = request.newBuilder()
                    		  .header("Content-Type", "text/plain")
      	    	              .header("Authorization", "Basic " + encodedBase64)
      	    	              .header("Timestamp", GlobalConstantLinkaja.HEADER_TIMESTAMP)
                              .build();             
                                     
                  okhttp3.Response response = chain.proceed(request);
                  return response;
              }
          };
      }
	
	  public ApplinkResponseDto generateApplink(String encryptedData){
	      try{
	          Call<ApplinkResponseDto> dataResponse = iLinkAjaApi.generateApplink(encryptedData);
	          ApplinkResponseDto res = dataResponse.execute().body();
	          if (res == null){
		          logger.error("Response Generate Applink is null");
	              throw new ErrorsException("Error Generate Applink");
	          }
	          return res;
	      } catch (Exception e){
	          e.printStackTrace();
	          logger.error("Error :" ,e);
	          throw new ErrorsException("Error Generate Applink");
	      }
	  }	
	 
	  
	  public String getLinkAjaApiUrl() {
	  	return this.BASE_URL;
	  }
}

