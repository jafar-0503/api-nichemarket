package id.equity.nichemarket.retrofit.partner.linkaja;

import id.equity.nichemarket.dto.partner.linkaja.ApplinkResponseDto;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ILinkAjaApi {
	@Headers("Content-Type: text/plain")
    @POST("/applink/v1/create")
    Call<ApplinkResponseDto> generateApplink(@Body String data);
}
